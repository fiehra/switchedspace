const app = require('../app');
const request = require('supertest');
const User = require('../models/user');
const jwt = require('jsonwebtoken');

const mongoose = require('mongoose');

const checkId = new mongoose.Types.ObjectId();
const checkOne = {
  _id: checkId,
  email: 'check@auth.com',
  username: 'check',
  password: 'check',
  tokens: [{
    token: jwt.sign({
      _id: checkId
    }, process.env.JWT_SECRET)
  }]
}

beforeAll(async () => {
  await mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
})

beforeEach(async () => {
  await User.deleteMany();
  await new User(checkOne).save();
});

test('user not authenticated', async () => {
  const response = await request(app)
    .get('/api/user/' + checkId)
    .send(checkId)
    .expect(401)
});