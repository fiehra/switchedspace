const app = require('../app');
const request = require('supertest');
const Application = require('../models/application');

const mongoose = require('mongoose');

const checkId = new mongoose.Types.ObjectId();

const newApplication = {
  _id: checkId,
  applicationType: 'workshop',
  name: 'application',
  email: 'application@email.com',
  phonenumber: '111',
  company: 'company',
  title: 'title',
  description: 'crazy description',
  location: 'berlin',
  date: new Date(),
  budget: '200'
}

beforeAll(async () => {
  await mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
})

beforeEach(async () => {
  await Application.deleteMany();
  await new Application(newApplication).save();
});

test('user not authenticated', async () => {
  const response = await request(app)
    .get('/api/application/' + checkId)
    .send(checkId)
    .expect(401)
});