const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const userRepository = require('./userRepository');
const tokenRepository = require('../verification/tokenRepository');
const verificationService = require('../verification/verificationService');
const urlHelper = require('../utils/url-helper');

// post: /user/signup
exports.signupUser = (req, res, next) => {
  bcrypt.hash(req.body.password, 10).then((hash) => {
    const user = new User({
      email: req.body.email,
      username: req.body.username,
      password: hash,
    });
    userRepository
      .findOne({
        $or: [{ email: req.body.email }, { username: req.body.username }],
      })
      .then((userCheck) => {
        if (userCheck) {
          if (userCheck.username === req.body.username) {
            return res.status(400).json({
              message: 'username already exists',
            });
          } else if (userCheck.email === req.body.email) {
            return res.status(400).json({
              message: 'email already taken',
            });
          }
        } else {
          userRepository
            .save(user)
            .then((result) => {
              // there must be better way to get correct hostname url?
              const url = urlHelper.getUrl(req);
              const token = verificationService.createToken(result._id);
              tokenRepository.save(token);
              const reqBody = {
                email: req.body.email,
                token: token,
                url: url,
              };
              verificationService.sendVerificationEmail(reqBody);
              return res.status(201).json({
                message: 'user created',
              });
            })
            .catch((error) => {
              return res.status(500).json({
                message: 'signup failed, invalid credentials',
              });
            });
        }
      });
  });
};

// post: /user/login
exports.loginUser = (req, res, next) => {
  let fetchedUser;
  userRepository
    .findOne({ email: req.body.email })
    .then((user) => {
      if (!user) {
        return res.status(401).json({
          message: 'login failed!',
        });
      } else if (user.verified === false) {
        return res.status(401).json({
          message: 'not verified',
        });
      } else if (user.verified === true) {
        fetchedUser = user;
        return bcrypt.compare(req.body.password, user.password);
      }
    })
    .then((result) => {
      if (!result) {
        return res.status(401).json({
          message: 'login failed',
        });
      }
      const token = jwt.sign(
        {
          email: fetchedUser.email,
          userId: fetchedUser._id,
          username: fetchedUser.username,
          role: fetchedUser.role,
        },
        process.env.JWT_SECRET,
        { expiresIn: '3h' },
      );
      return res.status(200).json({
        token: token,
        expiresIn: 10800,
        userId: fetchedUser._id,
        username: fetchedUser.username,
        faveTheme: fetchedUser.faveTheme,
        role: fetchedUser.role,
      });
    })
    .catch((error) => {
      return res.status(500);
    });
};
