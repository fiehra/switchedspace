const User = require('../models/user');

class UserRepository {
  save(object) {
    return User(object).save();
  }

  findOne(query) {
    return User.findOne(query);
  }
}

module.exports = new UserRepository();
