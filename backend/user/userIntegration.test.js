const mongoose = require('mongoose');
const app = require('../app');
const request = require('supertest');
const User = require('../models/user');
const jwt = require('jsonwebtoken');

const userId = new mongoose.Types.ObjectId();
const userOne = {
  _id: userId,
  email: 'user@controller.com',
  verified: true,
  username: 'user',
  password: 'password',
  tokens: [
    {
      token: jwt.sign(
        {
          userId: userId,
          email: 'user@controller.com',
          username: 'user',
        },
        process.env.JWT_SECRET,
      ),
    },
  ],
};

beforeAll(async () => {
  await mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
});

beforeEach(async () => {
  await User.deleteMany();
  await new User(userOne).save();
});

test('signup', async () => {
  const userCountBefore = await User.countDocuments();
  const response = await request(app)
    .post('/api/user/signup')
    .send({
      _id: 'fiehra',
      email: 'fiehra@gsg.com',
      username: 'fiehra',
      password: 'fiehra',
    })
    .expect(201);
  expect(response.body.message).toBe('user created');

  const userCountAfter = await User.countDocuments();
  expect(userCountBefore + 1).toBe(userCountAfter);
});

test('signup fail 400 username already exists', async () => {
  const userCountBefore = await User.countDocuments();
  const response = await request(app)
    .post('/api/user/signup')
    .send({
      _id: userId,
      email: 'user@newEmail.com',
      username: 'user',
      password: 'user',
    })
    .expect(400);
  expect(response.body.message).toBe('username already exists');

  const userCountAfter = await User.countDocuments();
  expect(userCountBefore).toBe(userCountAfter);
});

test('signup fail 400 email already taken', async () => {
  const userCountBefore = await User.countDocuments();
  const response = await request(app)
    .post('/api/user/signup')
    .send({
      _id: userId,
      email: 'user@controller.com',
      username: 'newUser',
      password: 'user',
    })
    .expect(400);
  expect(response.body.message).toBe('email already taken');

  const userCountAfter = await User.countDocuments();
  expect(userCountBefore).toBe(userCountAfter);
});

// test('signup fail 500 invalid credentials', async () => {
//   const userCountBefore = await User.countDocuments();
//   const response = await request(app)
//     .post('/api/user/signup')
//     .send({
//       _id: userId,
//       email: 'fiehra@controller.com',
//       username: 'username',
//       password: 'user',
//       fail: 'fail',
//     })
//     .expect(500);
//   expect(response.body.message).toBe('signup failed, invalid credentials');

//   const userCountAfter = await User.countDocuments();
//   expect(userCountBefore).toBe(userCountAfter);
// });

test('login success', async () => {
  const fiehraId = new mongoose.Types.ObjectId();
  const responseSignup = await request(app)
    .post('/api/user/signup')
    .send({
      _id: fiehraId,
      email: 'fiehra@gsg.com',
      username: 'fiehra',
      password: 'fiehra',
    })
    .expect(201);
  expect(responseSignup.body.message).toBe('user created');

  await User.findOneAndUpdate(
    { email: 'fiehra@gsg.com' },
    {
      verified: true,
    },
  );

  const fetchedUser = await User.findOne({ email: 'fiehra@gsg.com' });
  expect(fetchedUser.verified).toBe(true);

  const responseLogin = await request(app)
    .post('/api/user/login')
    .send({
      email: 'fiehra@gsg.com',
      password: 'fiehra',
    })
    .expect(200);
  expect(responseLogin.body.username).toBe('fiehra');
});

test('login not verified', async () => {
  const responseSignup = await request(app)
    .post('/api/user/signup')
    .send({
      _id: 'fiehra',
      email: 'fiehra@gsg.com',
      username: 'fiehra',
      password: 'fiehra',
    })
    .expect(201);
  expect(responseSignup.body.message).toBe('user created');

  const responseLogin = await request(app)
    .post('/api/user/login')
    .send({
      email: 'fiehra@gsg.com',
      password: 'fiehra',
    })
    .expect(401);
  expect(responseLogin.body.message).toBe('not verified');
});

test('login failed wrong password', async () => {
  const response1 = await request(app)
    .post('/api/user/signup')
    .send({
      _id: 'fiehra',
      email: 'fiehra@gsg.com',
      username: 'fiehra',
      password: 'fiehra',
    })
    .expect(201);
  expect(response1.body.message).toBe('user created');

  await User.findOneAndUpdate(
    { email: 'fiehra@gsg.com' },
    {
      verified: true,
    },
  );

  const fetchedUser = await User.findOne({ email: 'fiehra@gsg.com' });
  expect(fetchedUser.verified).toBe(true);

  const response2 = await request(app)
    .post('/api/user/login')
    .send({
      email: 'fiehra@gsg.com',
      password: 'password',
    })
    .expect(401);
  expect(response2.body.message).toBe('login failed');
});

test('login failed user not found', async () => {
  const response1 = await request(app)
    .post('/api/user/signup')
    .send({
      _id: 'fiehra',
      email: 'fiehra@gsg.com',
      username: 'fiehra',
      password: 'fiehra',
    })
    .expect(201);
  expect(response1.body.message).toBe('user created');
  requestBodyMock = {
    email: 'fiehra@notfound.com',
    password: 'fiehra',
  };
  const response2 = await request(app).post('/api/user/login').send(requestBodyMock).expect(401);
  expect(response2.body.message).toBe('login failed!');
});
