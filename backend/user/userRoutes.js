const express = require("express");
const UserController = require("./userController");

const router = express.Router();

// create user
router.post("/user/signup", UserController.signup);

// login user
router.post("/user/login", UserController.login);

module.exports = router;