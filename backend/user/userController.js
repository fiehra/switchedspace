const userService = require('./userService');

exports.signup = (req, res, next) => {
  return userService.signupUser(req, res);
};

exports.login = (req, res, next) => {
  return userService.loginUser(req, res);
};