const mongoose = require("mongoose");

const contactRequestSchema = mongoose.Schema({
  email: { type: String, required: true },
  foundBy: { type: String, required: true },
  subject: { type: String, required: true},
  message: { type: String, required: true },
});

module.exports = mongoose.model('ContactRequest', contactRequestSchema);