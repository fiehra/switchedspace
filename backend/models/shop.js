const mongoose = require("mongoose");

const shopSchema = mongoose.Schema({
  id: String,
  name: { type: String, required: true },
  description: { type: String, required: true },
  website: { type: String, default: ''},
  lat: { type: Number, required: true },
  lng: { type: Number, required: true }
});

module.exports = mongoose.model('Shop', shopSchema);