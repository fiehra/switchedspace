const mongoose = require('mongoose');

const applicationSchema = mongoose.Schema({
  id: String,
  applicationType: { type: String, required: true },
  name: { type: String, required: true },
  email: { type: String, required: true },
  phonenumber: { type: String, default: '' },
  company: { type: String, default: '' },
  title: { type: String, required: true },
  description: { type: String, required: true },
  location: { type: String, required: true },
  date: { type: Date, required: true },
  budget: { type: String, default: '' },
});

module.exports = mongoose.model('Application', applicationSchema);
