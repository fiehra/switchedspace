const mongoose = require('mongoose');
const Comment = require('./comment');

const forumPostSchema = mongoose.Schema({
  id: String,
  title: { type: String, required: true },
  writerId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  writerUsername: { type: String, required: true },
  content: { type: String, required: true },
  votes: { type: Number, default: 0 },
  upVoters: [{ type: String, default: [] }],
  downVoters: [{ type: String, default: [] }],
  comments: [Comment],
});

module.exports = mongoose.model('ForumPost', forumPostSchema);
