const mongoose = require('mongoose');

const commentSchema = mongoose.Schema({
  id: String,
  writerId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  writerUsername: { type: String, required: true },
  content: { type: String, required: true },
  markedAsCorrect: { type: Boolean, default: false },
});

exports = mongoose.model('Comment', commentSchema);
