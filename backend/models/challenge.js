const mongoose = require('mongoose');

const challengeSchema = mongoose.Schema({
  id: String,
  challengerId: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
  date: {type: Date, required: true},
  start: {type: String, default: null},
  finish: {type: String, default: null},
  time: {type: Number, default: null},
  active: {type: Boolean, default: false},
  completed: {type: Boolean, default: false},
  record: {type: Number, default: null},
  streak: {type: Number, default: 0},
});

module.exports = mongoose.model('Challenge', challengeSchema);
