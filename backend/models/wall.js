const mongoose = require("mongoose");

const wallSchema = mongoose.Schema({
  id: String,
  name: { type: String, required: true },
  description: { type: String, required: true },
  legalState: { type: String, required: true },
  lat: { type: Number, required: true },
  lng: { type: Number, required: true },
  statusUpdates: [{type: String, default: []}]
});

module.exports = mongoose.model('Wall', wallSchema);