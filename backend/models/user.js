const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
mongoose.set('useCreateIndex', true);

const userSchema = mongoose.Schema({
  id: String,
  verified: { type: Boolean, default: false },
  email: { type: String, unique: true },
  username: { type: String, unique: true },
  password: { type: String, required: true },
  aboutme: { type: String, default: '' },
  tag: { type: String, default: '' },
  faveColor: { type: String, default: '' },
  currentLocation: { type: String, default: '' },
  age: { type: String, default: '' },
  faveTheme: { type: String, default: '' },
  reported: { type: Boolean, default: false },
  role: { type: String, default: 'user' },
  imagePath: { type: String, default: null },
  inventory: { type: Number, default: 0 },
});

userSchema.plugin(uniqueValidator);
module.exports = mongoose.model('User', userSchema);
