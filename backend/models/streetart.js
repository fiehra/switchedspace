const mongoose = require("mongoose");

const streetartSchema = mongoose.Schema({
  id: String,
  name: { type: String, required: true },
  description: { type: String, required: true },
  link: { type: String, default: '' },
  lat: { type: Number, required: true },
  lng: { type: Number, required: true }
});

module.exports = mongoose.model('Streetart', streetartSchema);