const mongoose = require("mongoose");

const verifyToken = mongoose.Schema({
  userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
  verificationToken: { type: String, required: true },
  expireAt: { type: Date, default: Date.now, expires: 3600 },
});

module.exports = mongoose.model('VerifyToken', verifyToken);