const VerifyToken = require('../models/verifyToken');

class TokenRepository {

  save(object) {
    return VerifyToken(object).save();
  }

  findOne(query) {
    return VerifyToken.findOne(query);
  }
  
  deleteOne(id) {
    return VerifyToken.deleteOne(id);
  }
}

module.exports = new TokenRepository();
