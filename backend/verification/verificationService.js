const VerifyToken = require('../models/verifyToken');
const tokenRepository = require('./tokenRepository');
const profileRepository = require('../profile/profileRepository');
const sgMail = require('@sendgrid/mail');
const userRepository = require('../user/userRepository');
const urlHelper = require('../utils/url-helper');
const EMAIL = process.env.EMAIL;

const sendGridApiKey = process.env.SENDGRID_API_KEY;

sgMail.setApiKey(sendGridApiKey);

exports.sendEmail = async (email, link) => {
  console.log('inside send email');
  try {
    return sgMail.send({
      to: email,
      from: EMAIL,
      subject: `email verification`,
      text: `verification link: ${link}`,
    });
  } catch (error) {
    return res.status(500).json({
      message: 'sending verification email failed',
    });
  }
};

exports.createToken = (userId) => {
  let token = new VerifyToken({
    userId: userId,
    verificationToken: Math.random().toString(16),
    expireAt: Date.now(),
  });
  return token;
};

exports.sendVerificationEmail = (req, res) => {
  const verification = {
    email: req.email,
    token: req.token.verificationToken,
    url: req.url,
  };
  console.log(req.email);
  const verificationUrl = `${verification.url}/api/verify/${verification.token}`;
  this.sendEmail(verification.email, verificationUrl);
};

exports.verifyToken = async (req, res) => {
  const tokenFromUrl = req.params.token;
  const fetchedToken = await tokenRepository.findOne({
    verificationToken: tokenFromUrl,
  });
  let fetchedUser;
  if (fetchedToken) {
    await userRepository.findOne({ _id: fetchedToken.userId }).then((fetchedU) => {
      fetchedUser = fetchedU;
      fetchedUser.verified = true;
    });
    await profileRepository.updateOne({ _id: fetchedUser._id }, fetchedUser);
  }

  const frontEndUrl = urlHelper.getFrontendUrl(req);
  res.redirect(frontEndUrl + '/verified');
  await tokenRepository.deleteOne({ verificationToken: tokenFromUrl });
};

exports.resendVerificationLink = async (req, res) => {
  const url = urlHelper.getUrl(req);
  userRepository.findOne({ email: req.body.email }).then((user) => {
    const token = this.createToken(user._id);
    tokenRepository.save(token);
    const verification = {
      email: req.body.email,
      token: token.verificationToken,
      url: url,
    };
    const verificationUrl = `${verification.url}/api/verify/${verification.token}`;
    this.sendEmail(verification.email, verificationUrl)
      .then((result) => {
        return res.status(201).json({
          message: 'sending successful',
        });
      })
      .catch((error) => {
        return res.status(500).json({
          message: 'sending verification email failed',
        });
      });
  });
};
