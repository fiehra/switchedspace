const express = require("express");
const VerificationController = require("./verificationController");
const router = express.Router();

// verifyEmail
router.get("/verify/:token", VerificationController.verify);

// resendEmail
router.post("/verify/resend", VerificationController.resendLink);

module.exports = router;