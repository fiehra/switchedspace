const app = require('../app');
const request = require('supertest');
const mongoose = require('mongoose');
const VerifyToken = require('../models/verifyToken');
const User = require('../models/user');
const jwt = require('jsonwebtoken');

const forumUserOneId = new mongoose.Types.ObjectId();
const vt = {
  userId: forumUserOneId,
  verificationToken: '11221122',
  expireAt: new Date(),
};

const forumUserOne = {
  _id: forumUserOneId,
  email: 'forum@controller.com',
  username: 'forum',
  password: 'forum',
  tokens: [
    {
      token: jwt.sign(
        {
          email: 'forum@controller.com',
          userId: forumUserOneId,
          username: 'forum',
        },
        process.env.JWT_SECRET,
      ),
    },
  ],
};
beforeAll(async () => {
  await mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
});

beforeEach(async () => {
  await User.deleteMany();
  await VerifyToken.deleteMany();
});

afterEach(async () => {
  await jest.clearAllMocks();
});

test('verifyToken successful 201', async () => {
  const userCountBefore = await User.countDocuments();
  await new User(forumUserOne).save();
  const userCountAfter = await User.countDocuments();
  expect(userCountBefore + 1).toBe(userCountAfter);

  const tokenCountBefore = await VerifyToken.countDocuments();
  await new VerifyToken(vt).save();
  const response = await request(app)
    .get('/api/verify/' + vt.verificationToken)
    .send()
    .expect(302);
  expect(response.body.message).toBe(undefined);
  const tokenCoutnAfter = await VerifyToken.countDocuments();
  expect(tokenCountBefore).toBe(tokenCoutnAfter);
});

test('resendLink successful 201', async () => {
  const userCountBefore = await User.countDocuments();
  await new User(forumUserOne).save();
  const userCountAfter = await User.countDocuments();
  expect(userCountBefore + 1).toBe(userCountAfter);

  const response = await request(app)
    .post('/api/verify/resend')
    .send({ email: forumUserOne.email })
    .expect(201);
  expect(response.body.message).toBe('sending successful');
});
