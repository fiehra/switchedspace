const verificationService = require('./verificationService');

exports.verify = (req, res, next) => {
  return verificationService.verifyToken(req, res);
}

exports.resendLink = (req, res, next) => {
  return verificationService.resendVerificationLink(req, res);
}
