const express = require("express");
const ContactController = require("./contactController");
const router = express.Router();

// create contactRequest
router.post("/contactRequest", ContactController.create);

module.exports = router;