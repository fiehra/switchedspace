const ContactRequest = require('../models/contact');
const sgMail = require('@sendgrid/mail');
const EMAIL = process.env.EMAIL;

const sendGridApiKey = process.env.SENDGRID_API_KEY;

sgMail.setApiKey(sendGridApiKey);
// post: /contactRequest
exports.createContactRequest = (req, res, next) => {
  const contactRequest = new ContactRequest({
    email: req.body.email,
    foundby: req.body.foundby,
    subject: req.body.subject,
    message: req.body.message,
  });

  async function sendEmail(email, subject, message) {
    return sgMail.send({
      to: EMAIL,
      from: EMAIL,
      subject: `${subject} from ${email}`,
      text: message,
    });
  }

  sendEmail(
    contactRequest.email,
    contactRequest.subject,
    contactRequest.message,
  );
  try {
    return res.status(201).json({
      message: 'sending successful',
    });
  } catch (error) {
    return res.status(500).json({
      message: 'submitting contact failed',
    });
  }
};
