const app = require('../app');
const request = require('supertest');
const mongoose = require('mongoose');
const ContactRequest = require('../models/contact');

const cr = {
  id: 'id',
  email: 'email@email.com',
  foundby: 'friends',
  subject: 'subject',
  message: 'message'
}

const crfail = {
  id: 'id',
}

beforeAll(async () => {
  await mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
})

beforeEach(async () => {
  await ContactRequest.deleteMany();
});

afterEach(async () => {
  await jest.clearAllMocks();
})

test('createContactRequest successful 201', async () => {
  const response = await request(app).post('/api/contactRequest')
    .send(cr).expect(201);
  expect(response.body.message).toBe('sending successful');
});