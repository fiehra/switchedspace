const contactService = require('./contactService');

exports.create = (req, res, next) => {
  return contactService.createContactRequest(req, res);
}