const challengeService = require('./challengeService');

exports.create = (req, res, next) => {
  return challengeService.createChallenge(req, res);
};

exports.update = (req, res, next) => {
  return challengeService.updateChallenge(req, res);
};

exports.getAll = (req, res, next) => {
  return challengeService.getAllChallenges(req, res);
};

exports.getById = (req, res, next) => {
  return challengeService.getChallengeById(req, res);
};

exports.deleteById = (req, res, next) => {
  return challengeService.deleteChallengeById(req, res);
};

exports.getAverage = (req, res, next) => {
  return challengeService.getAverageTime(req, res);
};

exports.getDifference = (req, res, next) => {
  return challengeService.getDateDifference(req, res);
};

exports.getRecord = (req, res, next) => {
  return challengeService.getChallengeRecord(req, res);
};

exports.getStreak = (req, res, next) => {
  return challengeService.getCurrentStreak(req, res);
};

exports.getLast10 = (req, res, next) => {
  return challengeService.getLast10Challenges(req, res);
};
