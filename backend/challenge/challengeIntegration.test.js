const app = require('../app');
const request = require('supertest');
const Challenge = require('../models/challenge');
const mongoose = require('mongoose');
const User = require('../models/user');
const jwt = require('jsonwebtoken');

const fiehraId = new mongoose.Types.ObjectId();
const challengeId = new mongoose.Types.ObjectId();
const challengeTwoId = new mongoose.Types.ObjectId();

const fiehra = {
  _id: fiehraId,
  email: 'fiehra@controller.com',
  username: 'fiehra',
  password: 'fiehra',
  aboutme: 'fiehra',
  tag: 'tag',
  faveColor: 'green',
  currentLocation: 'currentLocation',
  age: 'age',
  faveTheme: 'green',
  reported: false,
  role: 'fiehra',
  tokens: [{
    token: jwt.sign({
      email: 'fiehra@controller.com',
      userId: fiehraId,
      username: 'fiehra',
      role: 'fiehra',
    }, process.env.JWT_SECRET)
  }],
}

const newChallenge = {
  _id: challengeId,
  challengerId: fiehraId,
  date: new Date(),
  start: null,
  finish: null,
  time: 1000,
  active: false,
  completed: false,
  record: 10,
  streak: 10,
};

const invalidChallenge = {
  _id: challengeId,
  challengerId: fiehraId,
  date: null,
  start: new Date(),
  finish: new Date(),
  time: null,
  active: false,
  completed: false,
  record: 5,
  streak: 5,
};

const otherChallenge = {
  _id: challengeTwoId,
  challengerId: fiehraId,
  date: new Date(),
  start: new Date(),
  finish: new Date(),
  time: 2000,
  active: false,
  completed: false,
  record: 5,
  streak: 11,
};

const challengeForUpdate = {
  id: challengeId,
  challengerId: fiehraId,
  date: new Date(),
  start: new Date(),
  finish: new Date(),
  time: 1000,
  active: false,
  completed: true,
  streak: 10,
};

beforeAll(async () => {
  await mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
});

beforeEach(async () => {
  await Challenge.deleteMany();
  await User.deleteMany();
});

test('create challenge success 200', async () => {
  const challengeCountBefore = await Challenge.countDocuments();
  const response = await request(app)
    .post('/api/challenge/create')
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send(newChallenge)
    .expect(201);
  expect(response.body.message).toBe('challenge created');
  const challengecountAfter = await Challenge.countDocuments();
  expect(challengeCountBefore + 1).toBe(challengecountAfter);
});

test('create challenge error 500', async () => {
  const challengeCountBefore = await Challenge.countDocuments();
  const response = await request(app)
    .post('/api/challenge/create')
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send(invalidChallenge)
    .expect(500);
  expect(response.body.message).toBe('creating challenge failed');
  const challengecountAfter = await Challenge.countDocuments();
  expect(challengeCountBefore).toBe(challengecountAfter);
});

test('update challenge authorized success 200', async () => {
  await new Challenge(newChallenge).save();
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(1);
  const response = await request(app)
    .put('/api/challenge/update/' + challengeId)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send(challengeForUpdate)
    .expect(200);
  expect(response.body.message).toBe('challenge updated');
  let updatedChallenge = await Challenge.findById(challengeId);
  expect(updatedChallenge.time).toBe(1000);
});

test('update challenge not authorized error 401', async () => {
  await new Challenge(newChallenge).save();
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(1);
  const response = await request(app)
    .put('/api/challenge/update/' + fiehraId)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send(challengeForUpdate)
    .expect(401);
  expect(response.body.message).toBe('not authorized');
});

test('update challenge error 500', async () => {
  await new Challenge(newChallenge).save();
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(1);
  const response = await request(app)
    .put('/api/challenge/update/' + challengeId)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send(invalidChallenge)
    .expect(500);
  expect(response.body.message).toBe('updating challenge failed');
});

test('getall challenges success 200', async () => {
  await new Challenge(newChallenge).save();
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(1);
  const response = await request(app)
    .get('/api/challenges/getAll')
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send()
    .expect(200);
  expect(response.body.message).toBe('fetching challenges successful');
});

test('getChallengeById success 200', async () => {
  await new Challenge(newChallenge).save();
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(1);
  const response = await request(app)
    .get('/api/challenge/get/' + challengeId)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send()
    .expect(200);
  expect(response.body.message).toBe('fetching challenge successful');
});

test('getChallengeById error 404', async () => {
  let challenge = await Challenge.findById(challengeId);
  expect(challenge).toBeNull();
  const response = await request(app)
    .get('/api/challenge/get/' + challengeId)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send()
    .expect(404);
  expect(response.body.message).toBe('challenge not found');
});

test('getChallengeById error 500', async () => {
  await new Challenge(newChallenge).save();
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(1);
  const response = await request(app)
    .get('/api/challenge/get/' + newChallenge)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send()
    .expect(500);
  expect(response.body.message).toBe('fetching challenge failed');
});

test('deleteChallengeById success 200', async () => {
  await new Challenge(newChallenge).save();
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(1);
  const response = await request(app)
    .delete('/api/challenge/delete/' + challengeId)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send()
    .expect(200);
  expect(response.body.message).toBe('deleting challenge successful');
});

test('deleteChallengeById error 401', async () => {
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(0);
  const response = await request(app)
    .delete('/api/challenge/delete/' + challengeId)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send()
    .expect(401);
  expect(response.body.message).toBe('not authorized');
});

test('deleteChallengeById error 500', async () => {
  await new Challenge(newChallenge).save();
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(1);
  const response = await request(app)
    .delete('/api/challenge/delete/' + newChallenge)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send(newChallenge)
    .expect(500);
  expect(response.body.message).toBe('deleting challenge failed');
});

test('getAverageTime success 200', async () => {
  await new Challenge(newChallenge).save();
  await new Challenge(otherChallenge).save();
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(2);

  const response = await request(app)
    .get('/api/challenge/averageTime')
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send()
    .expect(200);
  expect(response.body.message).toBe('fetching average successful');
  expect(response.body.averageTime).toBe(1500.0);
});

test('getDifference success 200', async () => {
  await new Challenge(newChallenge).save();
  await new Challenge(otherChallenge).save();
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(2);

  const response = await request(app)
    .get('/api/challenge/getDifference')
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send()
    .expect(200);
  expect(response.body.message).toBe('calculating difference successful');
});

test('getDifference error 404', async () => {
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(0);

  const response = await request(app)
    .get('/api/challenge/getDifference')
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send()
    .expect(404);
  expect(response.body.message).toBe('challenge not found');
});

test('getChallengeRecord success 200', async () => {
  await new Challenge(newChallenge).save();
  await new Challenge(otherChallenge).save();
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(2);

  const response = await request(app)
    .get('/api/challenge/getRecord')
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send()
    .expect(200);
  expect(response.body.message).toBe('fetching record successful');
  expect(response.body.record).toBe(10);
});

test('getChallengeRecord error 404', async () => {
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(0);

  const response = await request(app)
    .get('/api/challenge/getRecord')
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send()
    .expect(404);
  expect(response.body.message).toBe('challenge not found');
});

test('getCurrentStreak success 200', async () => {
  await new Challenge(newChallenge).save();
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(1);

  const response = await request(app)
    .get('/api/challenge/getStreak')
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send()
    .expect(200);
  expect(response.body.message).toBe('fetching streak successful');
  expect(response.body.streak).toBe(10);
});

test('getCurrentStreak error 404', async () => {
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(0);

  const response = await request(app)
    .get('/api/challenge/getStreak')
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send()
    .expect(404);
  expect(response.body.message).toBe('challenge not found');
});

test('getLatest10 success 200', async () => {
  await new Challenge(newChallenge).save();
  await new Challenge(otherChallenge).save();
  const challengeCount = await Challenge.countDocuments();
  expect(challengeCount).toBe(2);
  const response = await request(app)
    .get('/api/challenges/getLatest10')
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send()
    .expect(200);
  expect(response.body.message).toBe('fetching challenges successful');
});