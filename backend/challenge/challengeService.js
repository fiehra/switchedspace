const { count } = require('../models/challenge');
const Challenge = require('../models/challenge');
const challengeRepository = require('./challengeRepository');

exports.createChallenge = (req, res, next) => {
  const challenge = new Challenge({
    id: req.body.id,
    challengerId: req.userData.userId,
    date: req.body.date,
    start: req.body.start,
    finish: req.body.finish,
    time: req.body.time,
    active: req.body.active,
    completed: req.body.completed,
    record: req.body.record,
    streak: req.body.streak,
  });
  challengeRepository
    .save(challenge)
    .then((result) => {
      return res.status(201).json({
        message: 'challenge created',
        challengeId: result._id,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'creating challenge failed',
      });
    });
};

exports.updateChallenge = (req, res, next) => {
  const challenge = new Challenge({
    _id: req.body.id,
    challengerId: req.userData.challengerId,
    date: req.body.date,
    start: req.body.start,
    finish: req.body.finish,
    time: req.body.time,
    active: req.body.active,
    completed: req.body.completed,
    record: req.body.record,
    streak: req.body.streak,
  });
  challengeRepository
    .updateOne({ _id: req.params.id }, challenge)
    .then((result) => {
      if (result.n > 0) {
        return res.status(200).json({
          message: 'challenge updated',
        });
      } else {
        return res.status(401).json({
          message: 'not authorized',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'updating challenge failed',
      });
    });
};

exports.getAllChallenges = (req, res, next) => {
  let fetchedChallenges;
  challengeRepository
    .findAll()
    .then((response) => {
      fetchedChallenges = response;
      return Challenge.countDocuments();
    })
    .then((count) => {
      return res.status(200).json({
        message: 'fetching challenges successful',
        challengesList: fetchedChallenges,
        maxChallenges: count,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching challenges failed',
      });
    });
};

exports.getChallengeById = (req, res, next) => {
  challengeRepository
    .findOne({ _id: req.params.id })
    .then((response) => {
      if (response) {
        return res.status(200).json({
          message: 'fetching challenge successful',
          challenge: response,
        });
      } else {
        return res.status(404).json({
          message: 'challenge not found',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching challenge failed',
      });
    });
};

exports.deleteChallengeById = (req, res, next) => {
  challengeRepository
    .deleteOne({ _id: req.params.id, challengerId: req.userData.userId })
    .then((result) => {
      if (result.n > 0) {
        return res.status(200).json({
          message: 'deleting challenge successful',
        });
      } else {
        return res.status(401).json({
          message: 'not authorized',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'deleting challenge failed',
      });
    });
};

exports.getAverageTime = (req, res, next) => {
  let sum = 0;
  let average;
  challengeRepository
    .findAll()
    .then((response) => {
      for (challenge of response) {
        sum += challenge.time;
      }
      let count = Challenge.countDocuments();
      return count;
    })
    .then((count) => {
      average = +(sum / count).toFixed(1);
      return res.status(200).json({
        message: 'fetching average successful',
        averageTime: average,
      });
    });
};

exports.getDateDifference = (req, res, next) => {
  challengeRepository
    .findLatest()
    .then((response) => {
      if (response) {
        const challengeDate = response.date.getDate();
        const today = new Date().getDate();
        const difference = today - challengeDate;
        return res.status(200).json({
          message: 'calculating difference successful',
          difference: difference,
        });
      } else {
        return res.status(404).json({
          message: 'challenge not found',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching difference failed',
      });
    });
};

exports.getChallengeRecord = (req, res, next) => {
  challengeRepository
    .findMaxRecord()
    .then((response) => {
      if (response) {
        return res.status(200).json({
          message: 'fetching record successful',
          record: response.record,
        });
      } else {
        return res.status(404).json({
          message: 'challenge not found',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching record failed',
      });
    });
};

exports.getCurrentStreak = (req, res, next) => {
  challengeRepository
    .findLatest()
    .then((response) => {
      if (response) {
        return res.status(200).json({
          message: 'fetching streak successful',
          streak: response.streak,
        });
      } else {
        return res.status(404).json({
          message: 'challenge not found',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching streak failed',
      });
    });
};

exports.getLast10Challenges = (req, res, next) => {
  let fetchedChallenges;
  challengeRepository
    .findLatest10()
    .then((response) => {
      fetchedChallenges = response;
    })
    .then((count) => {
      return res.status(200).json({
        message: 'fetching challenges successful',
        challengesList: fetchedChallenges,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching challenges failed',
      });
    });
};
