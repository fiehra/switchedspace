const express = require('express');
const checkRole = require('../middleware/checkRole');
const ChallengeController = require('./challengeController');

const router = express.Router();

// create challenge
router.post('/challenge/create', checkRole, ChallengeController.create);

// update challenge
router.put('/challenge/update/:id', checkRole, ChallengeController.update);

// get challenges
router.get('/challenges/getAll', checkRole, ChallengeController.getAll);

// get challenge by id
router.get('/challenge/get/:id', checkRole, ChallengeController.getById);

// remove challenge by id
router.delete('/challenge/delete/:id', checkRole, ChallengeController.deleteById);

// get challenge average
router.get('/challenge/averageTime', checkRole, ChallengeController.getAverage);

// get latest challenge
router.get('/challenge/getDifference', checkRole, ChallengeController.getDifference);

// get record challenge
router.get('/challenge/getRecord', checkRole, ChallengeController.getRecord);

// get current streak
router.get('/challenge/getStreak', checkRole, ChallengeController.getStreak);

// get current streak
router.get('/challenges/getLatest10', checkRole, ChallengeController.getLast10);

module.exports = router;
