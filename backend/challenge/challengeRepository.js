const Challenge = require('../models/challenge');

class ChallengeRepository {

  save(object) {
    return Challenge(object).save();
  }

  findOne(query) {
    return Challenge.findOne(query);
  }
  
  findAll() {
    return Challenge.find().sort({_id: -1});
  }
  
  findLatest10() {
    return Challenge.find().sort({_id: -1}).limit(10);
  }
  
  updateOne(id, challenge) {
    return Challenge.updateOne(id, challenge);
  }

  deleteOne(id) {
    return Challenge.deleteOne(id);
  }

  findLatest() {
    return Challenge.findOne().sort({_id: -1});
  }

  findMaxRecord() {
    return Challenge.findOne().sort({record: -1});
  }
}

module.exports = new ChallengeRepository();
