module.exports = {
  "testEnvironment": "node",
  "coveragePathIgnorePatterns": [
    "/node_modules/"
  ],
  "collectCoverage": true,
  "coverageDirectory": "<rootDir>/coverage/",
  "testTimeout": 50000
};