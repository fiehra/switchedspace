const app = require('../app');
const request = require('supertest');
const Application = require('../models/application');
const mongoose = require('mongoose');
const User = require('../models/user');
const jwt = require("jsonwebtoken");

const applicationId = new mongoose.Types.ObjectId();
const invalidApplicationId = new mongoose.Types.ObjectId();
const fiehraId = new mongoose.Types.ObjectId();
const userId = new mongoose.Types.ObjectId();

const fiehra = {
  _id: fiehraId,
  email: 'fiehra@controller.com',
  username: 'fiehra',
  password: 'fiehra',
  aboutme: 'fiehra',
  tag: 'tag',
  faveColor: 'green',
  currentLocation: 'currentLocation',
  age: 'age',
  faveTheme: 'green',
  reported: false,
  role: 'fiehra',
  tokens: [{
    token: jwt.sign({
      email: 'fiehra@controller.com',
      userId: fiehraId,
      username: 'fiehra',
      role: 'fiehra',
    }, process.env.JWT_SECRET)
  }],
}

const user = {
  _id: userId,
  email: 'user@controller.com',
  username: 'user',
  password: 'user',
  aboutme: 'aboutme',
  tag: 'tag',
  faveColor: 'green',
  currentLocation: 'currentLocation',
  age: 'age',
  faveTheme: 'green',
  reported: false,
  role: 'fiehra',
  tokens: [{
    token: jwt.sign({
      email: 'user@controller.com',
      userId: userId,
      username: 'user',
      role: 'user',
    }, process.env.JWT_SECRET)
  }],
}

const newApplication = {
  _id: applicationId,
  applicationType: 'workshop',
  name: 'application',
  email: 'application@email.com',
  phonenumber: '111',
  company: 'company',
  title: 'title',
  description: 'crazy description',
  location: 'berlin',
  date: new Date(),
  budget: '200'
}

const invalidApplication = {
  _id: invalidApplicationId,
  applicationType: 'workshop',
  name: 'application',
  email: 'application.com',
}

beforeAll(async () => {
  await mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
})

beforeEach(async () => {
  await Application.deleteMany();
  await User.deleteMany();
});

test('create application success', async () => {
  const applicationCountBefore = await Application.countDocuments();
  const response = await request(app).post('/api/application')
    .send(newApplication)
    .expect(201);
  expect(response.body.message).toBe('application created and sent');

  const applicationCountAfter = await Application.countDocuments();
  expect(applicationCountBefore + 1).toBe(applicationCountAfter);
})

test('create application error 500', async () => {
  const applicationCountBefore = await Application.countDocuments();
  const response = await request(app).post('/api/application')
    .send(invalidApplication)
    .expect(500);
  expect(response.body.message).toBe('creating and sending application failed');

  const applicationCountAfter = await Application.countDocuments();
  expect(applicationCountBefore).toBe(applicationCountAfter);
});

test('getall success 200', async () => {
  await new Application(newApplication).save();
  let application = await Application.findById(applicationId);
  expect(application).not.toBeNull();
  const response = await request(app).get('/api/application')
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send().expect(200);
  expect(response.body.message).toBe('applications fetched successfully');
});

// test('getall error 500', async () => {
//   await new Application(newApplication).save();
//   let application = await Application.findById(applicationId);
//   expect(application).not.toBeNull();
//   const response = await request(app).get('/api/application')
//   .send('invalidRequest')
//   .expect(500);
//   expect(response.body.message).toBe('fetching applications failed');

// });

test('getById success 200', async () => {
  await new Application(newApplication).save();
  let application = await Application.findById(applicationId);
  expect(application).not.toBeNull();
  const response = await request(app).get('/api/application/' + applicationId)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send(applicationId)
    .expect(200);
  expect(response.body.message).toBe('application fetched successfully');
});

test('getById error 404', async () => {
  let application = await Application.findById(applicationId);
  expect(application).toBeNull();
  const response = await request(app).get('/api/application/' + applicationId)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send(applicationId)
    .expect(404);
  expect(response.body.message).toBe('application not found');
});

test('getById error 500', async () => {
  await new Application(newApplication).save();
  let application = await Application.findById(applicationId);
  expect(application).not.toBeNull();
  const response = await request(app).get('/api/application/' + newApplication)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send(newApplication)
    .expect(500);
  expect(response.body.message).toBe('fetching application failed');
});

test('update success 200', async () => {
  const updateApplication = {
    id: applicationId,
    applicationType: 'workshop',
    name: 'application',
    email: 'application@email.com',
    phonenumber: 'changed',
    company: 'changed',
    title: 'changed',
    description: 'crazy changed',
    location: 'changed',
    date: new Date(),
    budget: 'changed'
  }
  await new Application(newApplication).save();
  let application = await Application.findById(applicationId);
  expect(application).not.toBeNull();
  const response = await request(app).put('/api/application/' + applicationId)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send(updateApplication)
    .expect(200);
  expect(response.body.message).toBe('application updated');
  let updatedApplication = await Application.findById(applicationId);
  expect(updatedApplication.company).toBe('changed');
});

test('update error 500', async () => {
  const invalidApplication = {
    id: invalidApplicationId,
    applicationType: 'contract',
    name: 'contract',
    email: 'contract@email.com',
    phonenumber: 'contract',
    company: 'contract',
    title: 'contract',
    description: 'crazy contract',
    location: 'contract',
    date: new Date(),
    budget: 'contract'
  }
  await new Application(newApplication).save();
  let application = await Application.findById(applicationId);
  expect(application).not.toBeNull();
  const response = await request(app).put('/api/application/' + applicationId)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send(invalidApplication)
    .expect(500);
  expect(response.body.message).toBe('updating application failed');
});

test('deleteById success 200', async () => {
  await new Application(newApplication).save();
  let application = await Application.findById(applicationId);
  expect(application).not.toBeNull();
  const applicationCountBefore = await Application.countDocuments();
  const response = await request(app).delete('/api/application/' + applicationId)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send(applicationId)
    .expect(200);
  expect(response.body.message).toBe('deleted application successfully');

  const applicationCountAfter = await Application.countDocuments();
  expect(applicationCountBefore - 1).toBe(applicationCountAfter);
});

test('deleteById error 500', async () => {
  await new Application(newApplication).save();
  let application = await Application.findById(applicationId);
  expect(application).not.toBeNull();
  const applicationCountBefore = await Application.countDocuments();
  const response = await request(app).delete('/api/application/' + application)
    .set('Authorization', `Bearer ${fiehra.tokens[0].token}`)
    .send(application)
    .expect(500);
  expect(response.body.message).toBe('deleting application failed');

  const applicationCountAfter = await Application.countDocuments();
  expect(applicationCountBefore).toBe(applicationCountAfter);
});

test('deleteById error 401 userrole not fiehra', async () => {
  await new Application(newApplication).save();
  let application = await Application.findById(applicationId);
  expect(application).not.toBeNull();
  const applicationCountBefore = await Application.countDocuments();
  const response = await request(app).delete('/api/application/' + applicationId)
    .set('Authorization', `Bearer ${user.tokens[0].token}`)
    .send(applicationId)
    .expect(401);
  expect(response.body.message).toBe('you are not fiehra');

  const applicationCountAfter = await Application.countDocuments();
  expect(applicationCountBefore).toBe(applicationCountAfter);
});