const express = require("express");
const checkRole = require("../middleware/checkRole");
const ApplicationController = require("./applicationController")

const router = express.Router();

// create application
router.post("/application", ApplicationController.create);

// get all applications
router.get("/application", checkRole, ApplicationController.getAll);

// get one application by id
router.get("/application/:id", checkRole, ApplicationController.getById);

// update application
router.put("/application/:id", checkRole, ApplicationController.update);

// close, delete application
router.delete("/application/:id", checkRole, ApplicationController.deleteById);




module.exports = router;

