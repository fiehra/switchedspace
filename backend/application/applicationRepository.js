const Application = require('../models/application');

class ApplicationRepository {

  save(object) {
    return Application(object).save();
  }

  findAll() {
    return Application.find();
  }

  findOne(query) {
    return Application.findOne(query);
  }

  updateOne(id, application) {
    return Application.updateOne(id, application);
  }

  deleteOne(id) {
    return Application.deleteOne(id);
  }
}

module.exports = new ApplicationRepository();
