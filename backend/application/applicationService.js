const Application = require('../models/application');
const applicationRepository = require('./applicationRepository');

exports.createApplication = (req, res) => {
  const application = new Application({
    id: req.body.id,
    applicationType: req.body.applicationType,
    name: req.body.name,
    email: req.body.email,
    phonenumber: req.body.phonenumber,
    company: req.body.company,
    title: req.body.title,
    description: req.body.description,
    location: req.body.location,
    date: req.body.date,
    budget: req.body.budget,
  });
  applicationRepository
    .save(application)
    .then((result) => {
      return res.status(201).json({
        message: 'application created and sent',
        applicationId: result._id,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'creating and sending application failed',
      });
    });
};

exports.getAllApplications = (res) => {
  let fetchedApplications;
  applicationRepository
    .findAll()
    .then((response) => {
      fetchedApplications = response;
      return Application.countDocuments();
    })
    .then((count) => {
      return res.status(200).json({
        message: 'applications fetched successfully',
        applications: fetchedApplications,
        maxApplications: count,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching applications failed',
      });
    });
};

exports.getApplicationById = (req, res) => {
  applicationRepository
    .findOne({ _id: req.params.id })
    .then((response) => {
      if (response) {
        return res.status(200).json({
          message: 'application fetched successfully',
          application: response,
        });
      } else {
        return res.status(404).json({
          message: 'application not found',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching application failed',
      });
    });
};

exports.updateApplication = (req, res) => {
  const application = new Application({
    _id: req.body.id,
    applicationType: req.body.applicationType,
    name: req.body.name,
    email: req.body.email,
    phonenumber: req.body.phonenumber,
    company: req.body.company,
    title: req.body.title,
    description: req.body.description,
    location: req.body.location,
    date: req.body.date,
    budget: req.body.budget,
  });
  applicationRepository
    .updateOne({ _id: req.params.id }, application)
    .then((result) => {
      return res.status(200).json({
        message: 'application updated',
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'updating application failed',
      });
    });
};

exports.deleteApplicationById = (req, res) => {
  applicationRepository
    .deleteOne({ _id: req.params.id })
    .then((result) => {
      if (result.n > 0) {
        return res.status(200).json({
          message: 'deleted application successfully',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'deleting application failed',
      });
    });
};
