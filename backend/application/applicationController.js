const applicationService = require('./applicationService');

exports.create = (req, res, next) => {
  return applicationService.createApplication(req, res);
}

exports.getAll = (req, res, next) => {
  return applicationService.getAllApplications(res);
}

exports.getById = (req, res, next) => {
  return applicationService.getApplicationById(req, res);
}

exports.update = (req, res, next) => {
  return applicationService.updateApplication(req, res);
}

exports.deleteById = (req, res, next) => {
  return applicationService.deleteApplicationById(req, res);
}