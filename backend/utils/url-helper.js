exports.getUrl = (req) => {
  let url;
  if (req.get('host').includes('localhost')) {
    url = req.protocol + '://' + req.get('host');
  } else {
    url = req.protocol + 's://' + req.get('host');
  }
  return url;
};

exports.getFrontendUrl = (req) => {
  let url;
  if (req.get('host').includes('localhost')) {
    url = 'http://localhost:4200';
  } else {
    url = 'https://skillcap.org';
  }
  return url;
};
