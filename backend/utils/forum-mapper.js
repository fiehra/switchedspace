exports.toListDTO = (forumPost) => {
  const forumListDto = {
    id: forumPost._id,
    title: forumPost.title,
    writerUsername: forumPost.writerUsername,
    votes: forumPost.votes,
  };
  return forumListDto;
};
