exports.toMyProfileDTO = (user) => {
  const myProfileDTO = {
    id: user._id,
    email: user.email,
    username: user.username,
    aboutme: user.aboutme,
    tag: user.tag,
    faveColor: user.faveColor,
    currentLocation: user.currentLocation,
    age: user.age,
    faveTheme: user.faveTheme,
    reported: user.reported,
    role: user.role,
    imagePath: user.imagePath,
    inventory: user.inventory,
    verified: user.verified,
  };
  return myProfileDTO;
};

exports.toProfileDTO = (user) => {
  const profileDTO = {
    id: user._id,
    username: user.username,
    aboutme: user.aboutme,
    tag: user.tag,
    faveColor: user.faveColor,
    currentLocation: user.currentLocation,
    age: user.age,
    faveTheme: user.faveTheme,
    reported: user.reported,
    imagePath: user.imagePath,
  };
  return profileDTO;
};
