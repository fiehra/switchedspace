const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const forumRoutes = require('./forum/forumRoutes');
const userRoutes = require('./user/userRoutes');
const contactRoutes = require('./contact/contactRoutes');
const profileRoutes = require('./profile/profileRoutes');
const applicationRoutes = require('./application/applicationRoutes');
const wallRoutes = require('./graffmap/wall/wallRoutes');
const streetartRoutes = require('./graffmap/streetart/streetartRoutes');
const shopRoutes = require('./graffmap/shop/shopRoutes');
const verificationRoutes = require('./verification/verificationRoutes');
const challengeRoutes = require('./challenge/challengeRoutes');
const dotenv = require('dotenv');
const path = require('path');

dotenv.config();

const app = express();
mongoose
  .connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log('connected to mongoDB');
    console.log(new Date());
  })
  .catch(() => {
    console.log('connection failed');
  });

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false,
  }),
);
app.use('/images', express.static(path.join(__dirname, 'images')));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization',
  );
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
  next();
});

app.use('/api', forumRoutes);
app.use('/api', userRoutes);
app.use('/api', contactRoutes);
app.use('/api', profileRoutes);
app.use('/api', applicationRoutes);
app.use('/api', wallRoutes);
app.use('/api', shopRoutes);
app.use('/api', streetartRoutes);
app.use('/api', challengeRoutes);
app.use('/api', verificationRoutes);

module.exports = app;
