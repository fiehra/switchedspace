const app = require('../app');
const request = require('supertest');
const User = require('../models/user');
const jwt = require('jsonwebtoken');

const mongoose = require('mongoose');

const profileId = new mongoose.Types.ObjectId();
const wrongId = new mongoose.Types.ObjectId();
const profileOne = {
  _id: profileId,
  verified: true,
  email: 'profile@controller.com',
  username: 'profile',
  password: 'profile',
  aboutme: 'aboutme',
  tag: 'tag',
  faveColor: 'green',
  currentLocation: 'currentLocation',
  age: 'age',
  faveTheme: 'green',
  reported: false,
  role: 'user',
  tokens: [
    {
      token: jwt.sign(
        {
          email: 'profile@controller.com',
          userId: profileId,
          username: 'profile',
        },
        process.env.JWT_SECRET
      ),
    },
  ],
};

const profileTwoId = new mongoose.Types.ObjectId();
const profileTwo = {
  _id: profileTwoId,
  verified: true,
  email: 'report@controller.com',
  username: 'report',
  password: 'report',
  aboutme: 'report',
  tag: 'tag',
  faveColor: 'red',
  currentLocation: 'currentLocation',
  age: 'age',
  faveTheme: 'red',
  reported: false,
  role: 'user',
  tokens: [
    {
      token: jwt.sign(
        {
          email: 'report@controller.com',
          userId: profileTwoId,
          username: 'report',
        },
        process.env.JWT_SECRET
      ),
    },
  ],
};

const profileForUpdate = {
  id: profileId,
  verified: true,
  email: 'profile@controller.com',
  username: 'profile',
  password: 'profile',
  aboutme: 'aboutme updated',
  tag: 'tag',
  faveColor: 'green',
  currentLocation: 'currentLocation',
  age: 'age',
  faveTheme: 'green',
  reported: false,
  tokens: [
    {
      token: jwt.sign({ _id: profileId }, process.env.JWT_SECRET),
    },
  ],
};
beforeAll(async () => {
  await mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
});

beforeEach(async () => {
  await User.deleteMany();
  await new User(profileOne).save();
});

test('getMyProfile success 200', async () => {
  const response = await request(app)
    .get('/api/user/' + profileId)
    .set('Authorization', `Bearer ${profileOne.tokens[0].token}`)
    .send(profileId)
    .expect(200);
  expect(response.body.message).toBe('user fetched successfully');
});

test('getMyProfile error 404 user not found', async () => {
  const response = await request(app)
    .get('/api/user/myProfile/' + wrongId)
    .set('Authorization', `Bearer ${profileOne.tokens[0].token}`)
    .send(profileId)
    .expect(404);
  expect(response.body.message).toBe('user not found');
});

test('getMyProfile error 500', async () => {
  const response = await request(app)
    .get('/api/user/myProfile/' + profileOne)
    .set('Authorization', `Bearer ${profileOne.tokens[0].token}`)
    .send(profileId)
    .expect(500);
  expect(response.body.message).toBe('fetching user failed');
});

test('getMyProfile success 200', async () => {
  const response = await request(app)
    .get('/api/user/myProfile/' + profileId)
    .set('Authorization', `Bearer ${profileOne.tokens[0].token}`)
    .send(profileId)
    .expect(200);
  expect(response.body.message).toBe('user fetched successfully');
});

test('getProfileById error 404 user not found', async () => {
  const response = await request(app)
    .get('/api/user/' + wrongId)
    .set('Authorization', `Bearer ${profileOne.tokens[0].token}`)
    .send(profileId)
    .expect(404);
  expect(response.body.message).toBe('user not found');
});

test('getProfileById error 500', async () => {
  const response = await request(app)
    .get('/api/user/' + profileOne)
    .set('Authorization', `Bearer ${profileOne.tokens[0].token}`)
    .send(profileId)
    .expect(500);
  expect(response.body.message).toBe('fetching user failed');
});

test('delete User success 200', async () => {
  const userCountBefore = await User.countDocuments();
  await request(app)
    .delete('/api/user/' + profileId)
    .set('Authorization', `Bearer ${profileOne.tokens[0].token}`)
    .send({
      _id: profileId,
    })
    .expect(200);
  const user = await User.findById(profileId);
  expect(user).toBeNull();

  const userCountAfter = await User.countDocuments();
  expect(userCountBefore - 1).toBe(userCountAfter);
});

test('delete User fail 500', async () => {
  const userCountBefore = await User.countDocuments();
  const response = await request(app)
    .delete('/api/user/profileId')
    .set('Authorization', `Bearer ${profileOne.tokens[0].token}`)
    .send({
      _id: profileId,
    })
    .expect(500);
  expect(response.body.message).toBe('deleting account failed');

  const userCountAfter = await User.countDocuments();
  expect(userCountBefore).toBe(userCountAfter);
});

test('update User success 200', async () => {
  const response = await request(app)
    .put('/api/user/' + profileId)
    .set({
      Authorization: `Bearer ${profileOne.tokens[0].token}`,
      userData: {
        userId: profileId,
        email: 'profile@controller.com',
        username: 'profile',
      },
    })
    .send(profileForUpdate)
    .expect(200);
  expect(response.body.message).toBe('profile updated');
  let updatedUser = await User.findById(profileId);
  expect(updatedUser.aboutme).toBe('aboutme updated');
});

test('update User error 401', async () => {
  const response = await request(app)
    .put('/api/user/' + wrongId)
    .set({
      Authorization: `Bearer ${profileOne.tokens[0].token}`,
      userData: {
        userId: profileId,
        email: 'profile@controller.com',
        username: 'profile',
      },
    })
    .send(profileForUpdate)
    .expect(401);
  expect(response.body.message).toBe('not authorized');
});

test('update User error 400 username already exists', async () => {
  const otherProfile = {
    _id: wrongId,
    verified: true,
    email: 'other@controller.com',
    username: 'other',
    password: 'wrongId',
    aboutme: 'aboutme updated',
    tag: 'tag',
    faveColor: 'green',
    currentLocation: 'currentLocation',
    age: 'age',
    faveTheme: 'green',
    reported: false,
    tokens: [
      {
        token: jwt.sign({ _id: wrongId }, process.env.JWT_SECRET),
      },
    ],
  };
  await new User(otherProfile).save();
  let otherUser = await User.findById(wrongId);
  expect(otherUser).not.toBeNull();
  const doubleUsernameProfile = profileForUpdate;
  doubleUsernameProfile.username = 'other';
  const response = await request(app)
    .put('/api/user/' + profileId)
    .set({
      Authorization: `Bearer ${profileOne.tokens[0].token}`,
      userData: {
        userId: profileId,
        email: 'profile@controller.com',
        username: 'profile',
      },
    })
    .send(doubleUsernameProfile)
    .expect(400);
  expect(response.body.message).toBe('username already exists');
});

test('update User error 500', async () => {
  const response = await request(app)
    .put('/api/user/' + profileOne)
    .set({
      Authorization: `Bearer ${profileOne.tokens[0].token}`,
      userData: {
        userId: profileId,
        email: 'profile@controller.com',
        username: 'profile',
      },
    })
    .send(profileOne)
    .expect(500);
  expect(response.body.message).toBe('updating user failed');
});

test('reportPlayer success 200', async () => {
  const reportDtoMock = {
    id: profileTwoId,
    reported: true,
  };
  await new User(profileTwo).save();
  const userToReport = await User.findById(profileTwoId);
  expect(userToReport).not.toBeNull();

  const response = await request(app)
    .put('/api/user/report/' + profileTwoId)
    .set({
      Authorization: `Bearer ${profileOne.tokens[0].token}`,
      userData: {
        userId: profileId,
        email: 'profile@controller.com',
        username: 'profile',
      },
    })
    .send(reportDtoMock)
    .expect(200);
  expect(response.body.message).toBe('profile updated');
  const userAfterReport = await User.findById(profileTwoId);
  expect(userAfterReport.reported).toBe(true);
});

test('reportPlayer error 404 fetching user failed', async () => {
  const reportDtoMock = {
    id: profileTwoId,
    reported: true,
  };
  const response = await request(app)
    .put('/api/user/report/' + profileTwoId)
    .set({
      Authorization: `Bearer ${profileOne.tokens[0].token}`,
      userData: {
        userId: profileId,
        email: 'profile@controller.com',
        username: 'profile',
      },
    })
    .send(reportDtoMock)
    .expect(404);
  expect(response.body.message).toBe('fetching user failed');
});

test('uploadImage success 200', async () => {
  const response = await request(app)
    .post('/api/user/image/' + profileId)
    .set({
      Authorization: `Bearer ${profileOne.tokens[0].token}`,
      userData: {
        userId: profileId,
        email: 'profile@controller.com',
        username: 'profile',
      },
    })
    .attach('image', `./tests/files/fiehraprofile.jpg`)
    .expect(200);
  expect(response.body.message).toBe('profileimage updated');
});

test('uploadImage error 404', async () => {
  const response = await request(app)
    .post('/api/user/image/' + profileTwoId)
    .set({
      Authorization: `Bearer ${profileOne.tokens[0].token}`,
      userData: {
        userId: profileId,
        email: 'profile@controller.com',
        username: 'profile',
      },
    })
    .attach('image', `./tests/files/fiehraprofile.jpg`)
    .expect(404);
  expect(response.body.message).toBe('fetching user failed');
});
