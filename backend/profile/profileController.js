const profileService = require('./profileService');

exports.getMyProfile = (req, res, next) => {
  return profileService.getMyProfileById(req, res);
};

exports.getById = (req, res, next) => {
  return profileService.getUserById(req, res);
};

exports.update = (req, res, next) => {
  return profileService.updateUserById(req, res);
};

exports.image = (req, res, next) => {
  return profileService.uploadImage(req, res);
};

exports.deleteById = (req, res, next) => {
  return profileService.deleteUserById(req, res);
};

exports.reportUser = (req, res, next) => {
  return profileService.reportUserById(req, res);
};
