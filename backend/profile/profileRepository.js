const User = require('../models/user');

class ProfileRepository {
  findOne(query) {
    return User.findOne(query);
  }

  updateOne(id, user) {
    return User.updateOne(id, user);
  }

  deleteOne(id) {
    return User.deleteOne(id);
  }
}

module.exports = new ProfileRepository();
