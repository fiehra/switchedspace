const User = require('../models/user');
const UserMapper = require('../utils/user-mapper');
const profileRepository = require('./profileRepository');
const forumRepository = require('../forum/forumRepository');
const urlHelper = require('../utils/url-helper');

// get: /user/myProfile      // auth required
exports.getMyProfileById = (req, res, next) => {
  profileRepository
    .findOne({ _id: req.params.id })
    .then((response) => {
      if (response) {
        const myProfileDto = UserMapper.toMyProfileDTO(response);
        res.status(200).json({
          message: 'user fetched successfully',
          myProfile: myProfileDto,
        });
      } else {
        res.status(404).json({
          message: 'user not found',
        });
      }
    })
    .catch((error) => {
      res.status(500).json({
        message: 'fetching user failed',
      });
    });
};

// get: /user/:id      // auth required
exports.getUserById = (req, res, next) => {
  profileRepository
    .findOne({ _id: req.params.id })
    .then((response) => {
      if (response) {
        const profileDto = UserMapper.toProfileDTO(response);
        res.status(200).json({
          message: 'user fetched successfully',
          profile: profileDto,
        });
      } else {
        res.status(404).json({
          message: 'user not found',
        });
      }
    })
    .catch((error) => {
      res.status(500).json({
        message: 'fetching user failed',
      });
    });
};

// put: /user/:id    // auth required
exports.updateUserById = (req, res, next) => {
  const user = new User({
    _id: req.body.id,
    verified: req.body.verified,
    email: req.body.email,
    username: req.body.username,
    aboutme: req.body.aboutme,
    tag: req.body.tag,
    faveColor: req.body.faveColor,
    currentLocation: req.body.currentLocation,
    age: req.body.age,
    faveTheme: req.body.faveTheme,
    reported: req.body.reported,
    role: req.body.role,
    imagePath: req.body.imagePath,
    inventory: req.body.inventory,
  });
  profileRepository
    .updateOne({ _id: req.params.id }, user)
    .then((result) => {
      if (result.n > 0) {
        return res.status(200).json({
          message: 'profile updated',
        });
      } else {
        return res.status(401).json({
          message: 'not authorized',
        });
      }
    })
    .catch((error) => {
      if (error.name === 'MongoError' && error.code === 11000) {
        return res.status(400).json({
          message: 'username already exists',
        });
      } else {
        return res.status(500).json({
          errorName: error.name,
          errorCode: error.code,
          message: 'updating user failed',
        });
      }
    });
};

// delete: /user/:id    // auth required
exports.deleteUserById = (req, res, next) => {
  forumRepository.deleteUserContent(req.params.id);
  profileRepository
    .deleteOne({ _id: req.params.id })
    .then((result) => {
      if (result.n > 0) {
        return res.status(200).json({
          message: 'account deleted',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'deleting account failed',
      });
    });
};

// post: /user/report/:id  // auth required
exports.reportUserById = (req, res, next) => {
  profileRepository
    .findOne({ _id: req.params.id })
    .then((response) => {
      const fetchedUser = response;
      fetchedUser.reported = req.body.reported;
      profileRepository
        .updateOne({ _id: req.params.id }, fetchedUser)
        .then((result) => {
          return res.status(200).json({
            message: 'profile updated',
          });
        });
    })
    .catch((error) => {
      return res.status(404).json({
        message: 'fetching user failed',
      });
    });
};

// post: /user/image/:id  // auth required
exports.uploadImage = (req, res, next) => {
  const url = urlHelper.getUrl(req);
  profileRepository
    .findOne({ _id: req.params.id })
    .then((response) => {
      const fetchedUser = response;
      fetchedUser.imagePath = url + '/images/' + req.file.filename;
      profileRepository
        .updateOne({ _id: req.params.id }, fetchedUser)
        .then((response) => {
          return res.status(200).json({
            message: 'profileimage updated',
          });
        })
        .catch((error) => {
          return res.status(500).json({
            message: 'uploading image failed',
          });
        });
    })
    .catch((error) => {
      return res.status(404).json({
        message: 'fetching user failed',
      });
    });
};
