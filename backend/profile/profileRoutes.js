const express = require('express');
const ProfileController = require('./profileController');
const checkAuth = require('../middleware/checkAuth');
const router = express.Router();
const fileStorage = require('../middleware/fileStorage');
const multer = require('multer');

// getMyProfile
router.get('/user/myProfile/:id', checkAuth, ProfileController.getMyProfile);

// getMyProfile
router.get('/user/:id', checkAuth, ProfileController.getById);

// updateProfile
router.put('/user/:id', checkAuth, ProfileController.update);

// imageUpload
router.post(
  '/user/image/:id',
  checkAuth,
  multer({ storage: fileStorage }).single('image'),
  ProfileController.image,
);

// deleteProfile / deleteUser
router.delete('/user/:id', checkAuth, ProfileController.deleteById);

// reportUser
router.put('/user/report/:id', checkAuth, ProfileController.reportUser);

module.exports = router;
