const express = require("express");
const checkAuth = require("../middleware/checkAuth");
const ForumController = require("./forumController");

const router = express.Router();

// create forumpost
router.post("/forumPosts", checkAuth, ForumController.create);

// update forumpost
router.put("/forumPosts/:id", checkAuth, ForumController.update);

// get all forumposts
router.get("/forumPostsList", ForumController.getAll);

// get by id
router.get("/forumPosts/:id", ForumController.getById);

// delete with id
router.delete("/forumPosts/:id", checkAuth, ForumController.deleteById);

// update forumpost for vote
router.put("/forumPosts/updatevotes/:id", ForumController.updateVotes);

// // update comments for vote
router.put("/forumPosts/updateComments/:id", ForumController.updateComments);

module.exports = router;