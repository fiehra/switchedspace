const ForumPost = require('../models/forum');
const ForumMapper = require('../utils/forum-mapper');
const forumRepository = require('./forumRepository');

// post:/forumPosts    // auth required
exports.createForumPost = (req, res, next) => {
  const forumPost = new ForumPost({
    title: req.body.title,
    content: req.body.content,
    writerId: req.userData.userId,
    writerUsername: req.body.writerUsername,
  });
  forumRepository
    .save(forumPost)
    .then((result) => {
      return res.status(201).json({
        message: 'created forumPost',
        forumPostId: result._id,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'creating forumpost failed',
      });
    });
};
// put:/forumPosts/:id    // auth required
exports.updateForumPost = (req, res, next) => {
  const forumPost = new ForumPost({
    _id: req.body.id,
    title: req.body.title,
    content: req.body.content,
    writerId: req.userData.userId,
    writerUsername: req.userData.writerUsername,
    votes: req.body.votes,
    upVoters: req.body.upVoters,
    downVoters: req.body.downVoters,
    comments: req.body.comments,
  });
  forumRepository.updateOne({ _id: req.params.id, writerId: req.userData.userId }, forumPost)
    .then((result) => {
      if (result.n > 0) {
        return res.status(200).json({
          message: 'forumpost updated',
        });
      } else {
        return res.status(401).json({
          message: 'not authorized',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'updating forumpost failed',
      });
    });
};
// get:/forumPosts
exports.getAllList = (req, res, next) => {
  const pageSize = +req.query.pagesize;
  const currentPage = +req.query.page;
  const postQuery = forumRepository.findAll();
  let fetchedForumPosts;
  let transformedPosts = [];
  if (pageSize && currentPage) {
    postQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
  }
  postQuery
    .then((response) => {
      fetchedForumPosts = response;
      for (let post of fetchedForumPosts) {
        const dto = ForumMapper.toListDTO(post);
        transformedPosts.push(dto);
      }
      return ForumPost.countDocuments();
    })
    .then((count) => {
      return res.status(200).json({
        message: 'forumposts fetched successfully',
        forumPostsList: transformedPosts,
        maxPosts: count,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching forumposts failed',
      });
    });
};
// get: /forumPosts/:id
exports.getForumPostById = (req, res, next) => {
  forumRepository.findOne({ _id: req.params.id })
    .then((response) => {
      if (response) {
        return res.status(200).json({
          message: 'forumpost fetched successfully',
          forumPost: response,
        });
      } else {
        return res.status(404).json({
          message: 'forumpost not found',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching forumpost failed',
      });
    });
};
// delete: /forumPosts/:id    // auth required
exports.deleteForumPostById = (req, res, next) => {
  forumRepository.deleteOne({ _id: req.params.id, writerId: req.userData.userId })
    .then((result) => {
      if (result.n > 0) {
        return res.status(200).json({
          message: 'post deleted',
        });
      } else {
        return res.status(401).json({
          message: 'not authorized',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'deleting forumpost failed',
      });
    });
};
// put: /forumPosts/updatevotes/:id
exports.updateForumPostVotes = (req, res, next) => {
  forumRepository.findOne({ _id: req.params.id })
    .then((response) => {
      const fetchedPost = response;
      fetchedPost.votes = req.body.votes;
      fetchedPost.upVoters = req.body.upVoters;
      fetchedPost.downVoters = req.body.downVoters;
      ForumPost.updateOne({ _id: req.params.id }, fetchedPost).then((result) => {
        return res.status(200).json({
          message: 'forumPost updated',
        });
      });
    })
    .catch((error) => {
      return res.status(404).json({
        message: 'fetching forumPost failed',
      });
    });
};
// put: /forumPosts/updateComments/:id
exports.updateForumPostComments = (req, res, next) => {
  forumRepository.findOne({ _id: req.params.id })
    .then((response) => {
      const fetchedPost = response;
      fetchedPost.comments = req.body;
      ForumPost.updateOne({ _id: req.params.id }, fetchedPost).then((result) => {
        return res.status(200).json({
          message: 'forumPost updated',
        });
      });
    })
    .catch((error) => {
      return res.status(404).json({
        message: 'fetching forumPost failed',
      });
    });
};
