const forumService = require('./forumService');

exports.create = (req, res, next) => {
  return forumService.createForumPost(req, res);
}

exports.update = (req, res, next) => {
  return forumService.updateForumPost(req, res);
}

exports.getAll = (req, res, next) => {
  return forumService.getAllList(req, res);
}

exports.getById = (req, res, next) => {
  return forumService.getForumPostById(req, res);
}

exports.deleteById = (req, res, next) => {
  return forumService.deleteForumPostById(req, res);
}

exports.updateVotes = (req, res, next) => {
  return forumService.updateForumPostVotes(req, res);
}

exports.updateComments = (req, res, next) => {
  return forumService.updateForumPostComments(req, res);
}