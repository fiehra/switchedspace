const mongoose = require('mongoose');
const app = require('../app');
const request = require('supertest');
const User = require('../models/user');
const ForumPost = require('../models/forum');
const jwt = require('jsonwebtoken');

const forumUserOneId = new mongoose.Types.ObjectId();
const postId = new mongoose.Types.ObjectId();
const post2Id = new mongoose.Types.ObjectId();
const wrongId = new mongoose.Types.ObjectId();

const forumUserOne = {
  _id: forumUserOneId,
  email: 'forum@controller.com',
  username: 'forum',
  password: 'forum',
  tokens: [
    {
      token: jwt.sign(
        {
          email: 'forum@controller.com',
          userId: forumUserOneId,
          username: 'forum',
        },
        process.env.JWT_SECRET,
      ),
    },
  ],
};

const forumPost = {
  _id: postId,
  title: 'title',
  writerId: forumUserOneId,
  writerUsername: 'forum',
  content: 'content',
  votes: 0,
  upVoters: [],
  downVoters: [],
};

const forumPost2 = {
  _id: post2Id,
  title: 'post2',
  writerId: wrongId,
  writerUsername: 'forum',
  content: 'content',
  votes: 0,
  upVoters: [],
  downVoters: [],
};

beforeAll(async () => {
  await mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
});

beforeEach(async () => {
  await User.deleteMany();
  await new User(forumUserOne).save();
  await ForumPost.deleteMany();
});

test('create authorized 201', async () => {
  const forumCountBefore = await ForumPost.countDocuments();
  const response = await request(app)
    .post('/api/forumPosts')
    .set({
      Authorization: `Bearer ${forumUserOne.tokens[0].token}`,
      userData: {
        userId: forumUserOneId,
        email: 'forum@controller.com',
        username: 'forum',
      },
    })
    .send(forumPost)
    .expect(201);
  expect(response.body.message).toBe('created forumPost');

  const forumCountAfter = await ForumPost.countDocuments();
  expect(forumCountBefore + 1).toBe(forumCountAfter);
});

test('create error 500', async () => {
  const forumCountBefore = await ForumPost.countDocuments();
  await new ForumPost(forumPost).save();
  let post = await ForumPost.findById(forumPost._id);
  expect(post).not.toBeNull();
  const response = await request(app)
    .post('/api/forumPosts')
    .set({
      Authorization: `Bearer ${forumUserOne.tokens[0].token}`,
      userData: {
        userId: forumUserOneId,
        email: 'forum@controller.com',
        username: 'forum',
      },
    })
    .send(post)
    .expect(500);
  expect(response.body.message).toBe('creating forumpost failed');

  const forumCountAfter = await ForumPost.countDocuments();
  expect(forumCountBefore + 1).toBe(forumCountAfter);
});

test('update authorized success 200', async () => {
  const postForCall = {
    id: postId,
    title: 'new title',
    writerId: forumUserOneId,
    writerUsername: 'forum',
    content: 'content',
    votes: 0,
    upVoters: [],
    downVoters: [],
  };
  await new ForumPost(forumPost).save();
  let post = await ForumPost.findById(postId);
  expect(post).not.toBeNull();
  const response = await request(app)
    .put('/api/forumPosts/' + postId)
    .set({
      Authorization: `Bearer ${forumUserOne.tokens[0].token}`,
      userData: {
        userId: forumUserOneId,
        email: 'forum@controller.com',
        username: 'forum',
      },
    })
    .send(postForCall)
    .expect(200);
  expect(response.body.message).toBe('forumpost updated');
  let updatedPost = await ForumPost.findById(postId);
  expect(updatedPost.title).toBe('new title');
});

test('update not authorized error 401', async () => {
  const postForCall = {
    id: postId,
    title: 'new title',
    writerId: forumUserOneId,
    writerUsername: 'forum',
    content: 'content',
    votes: 0,
    upVoters: [],
    downVoters: [],
  };
  await new ForumPost(forumPost).save();
  let post = await ForumPost.findById(postId);
  expect(post).not.toBeNull();
  const response = await request(app)
    .put('/api/forumPosts/' + wrongId)
    .set({
      Authorization: `Bearer ${forumUserOne.tokens[0].token}`,
      userData: {
        userId: forumUserOneId,
        email: 'forum@controller.com',
        username: 'forum',
      },
    })
    .send(postForCall)
    .expect(401);
  expect(response.body.message).toBe('not authorized');
});

test('update error 500', async () => {
  const invalidPost = {
    id: wrongId,
    title: 'new title',
    writerId: forumUserOneId,
    writerUsername: 'forum',
    content: 'content',
    votes: 0,
    upVoters: [],
    downVoters: [],
  };
  await new ForumPost(forumPost).save();
  let post = await ForumPost.findById(postId);
  expect(post).not.toBeNull();
  post.title = 'new title';
  const response = await request(app)
    .put('/api/forumPosts/' + postId)
    .set({
      Authorization: `Bearer ${forumUserOne.tokens[0].token}`,
      userData: {
        userId: forumUserOneId,
        email: 'forum@controller.com',
        username: 'forum',
      },
    })
    .send(invalidPost)
    .expect(500);
  expect(response.body.message).toBe('updating forumpost failed');
});

test('getAll success 200', async () => {
  await new ForumPost(forumPost).save();
  let post = await ForumPost.findById(postId);
  expect(post).not.toBeNull();
  const response = await request(app)
    .get('/api/forumPostsList?pagesize=1&page=1')
    .send()
    .expect(200);
  expect(response.body.message).toBe('forumposts fetched successfully');
  expect(response.body.forumPostsList.length).toBe(1);
});

test('getById success 200', async () => {
  await new ForumPost(forumPost).save();
  let post = await ForumPost.findById(forumPost._id);
  expect(post).not.toBeNull();
  const response = await request(app)
    .get('/api/forumPosts/' + postId)
    .send(postId)
    .expect(200);
  expect(response.body.message).toBe('forumpost fetched successfully');
});

test('getById error 404', async () => {
  let post = await ForumPost.findById(postId);
  expect(post).toBeNull();
  const response = await request(app)
    .get('/api/forumPosts/' + postId)
    .send(postId)
    .expect(404);
  expect(response.body.message).toBe('forumpost not found');
});

test('getById error 500', async () => {
  await new ForumPost(forumPost).save();
  let post = await ForumPost.findById(postId);
  expect(post).not.toBeNull();
  const response = await request(app)
    .get('/api/forumPosts/' + post)
    .send(post)
    .expect(500);
  expect(response.body.message).toBe('fetching forumpost failed');
});

test('deleteById success 200', async () => {
  await new ForumPost(forumPost).save();
  let post = await ForumPost.findById(postId);
  expect(post).not.toBeNull();
  const forumCountBefore = await ForumPost.countDocuments();
  const response = await request(app)
    .delete('/api/forumPosts/' + postId)
    .set({
      Authorization: `Bearer ${forumUserOne.tokens[0].token}`,
      userData: {
        userId: forumUserOneId,
        email: 'forum@controller.com',
        username: 'forum',
      },
    })
    .send({
      _id: postId,
    })
    .expect(200);
  const postCheck = await ForumPost.findById(postId);
  expect(postCheck).toBeNull();
  expect(response.body.message).toBe('post deleted');

  const forumCountAfter = await ForumPost.countDocuments();
  expect(forumCountBefore - 1).toBe(forumCountAfter);
});

test('deleteById error 401', async () => {
  let post = await ForumPost.findById(postId);
  expect(post).toBeNull();
  const forumCountBefore = await ForumPost.countDocuments();
  const response = await request(app)
    .delete('/api/forumPosts/' + postId)
    .set({
      Authorization: `Bearer ${forumUserOne.tokens[0].token}`,
      userData: {
        userId: forumUserOneId,
        email: 'forum@controller.com',
        username: 'forum',
      },
    })
    .send({
      _id: postId,
    })
    .expect(401);
  const postCheck = await ForumPost.findById(postId);
  expect(postCheck).toBeNull();
  expect(response.body.message).toBe('not authorized');

  const forumCountAfter = await ForumPost.countDocuments();
  expect(forumCountBefore).toBe(forumCountAfter);
});

test('deleteById error 500', async () => {
  await new ForumPost(forumPost).save();
  let post = await ForumPost.findById(postId);
  expect(post).not.toBeNull();
  const forumCountBefore = await ForumPost.countDocuments();
  const response = await request(app)
    .delete('/api/forumPosts/' + post)
    .set({
      Authorization: `Bearer ${forumUserOne.tokens[0].token}`,
      userData: {
        email: 'forum@controller.com',
        username: 'forum',
      },
    })
    .send(post)
    .expect(500);
  const postCheck = await ForumPost.findById(postId);
  expect(postCheck).not.toBeNull();
  expect(response.body.message).toBe('deleting forumpost failed');
  const forumCountAfter = await ForumPost.countDocuments();
  expect(forumCountBefore).toBe(forumCountAfter);
});

test('updateVote success 200', async () => {
  const voteDtoMock = {
    id: postId,
    votes: 1,
    upVoters: [wrongId],
    downVoters: [],
  };
  await new ForumPost(forumPost).save();
  let post = await ForumPost.findById(postId);
  expect(post).not.toBeNull();
  const response = await request(app)
    .put('/api/forumPosts/updatevotes/' + postId)
    .set({
      Authorization: `Bearer ${forumUserOne.tokens[0].token}`,
      userData: {
        userId: forumUserOneId,
        email: 'forum@controller.com',
        username: 'forum',
      },
    })
    .send(voteDtoMock)
    .expect(200);
  expect(response.body.message).toBe('forumPost updated');
  let postAfterUpdate = await ForumPost.findById(postId);
  expect(postAfterUpdate.upVoters.length).toBe(1);
});

test('updateVote error 404', async () => {
  const voteDtoMock = {
    id: post2Id,
    votes: 1,
    upVoters: [wrongId],
    downVoters: [],
  };
  const response = await request(app)
    .put('/api/forumPosts/updatevotes/' + post2Id)
    .set({
      Authorization: `Bearer ${forumUserOne.tokens[0].token}`,
      userData: {
        userId: forumUserOneId,
        email: 'forum@controller.com',
        username: 'forum',
      },
    })
    .send(voteDtoMock)
    .expect(404);
  expect(response.body.message).toBe('fetching forumPost failed');
  let postAfterUpdate = await ForumPost.findById(post2Id);
  expect(postAfterUpdate).toBeNull();
});

test('updateComments success 200', async () => {
  const commentsDtoMock = ['comment'];
  await new ForumPost(forumPost).save();
  let post = await ForumPost.findById(postId);
  expect(post).not.toBeNull();
  expect(post.comments.length).toBe(0);
  const response = await request(app)
    .put('/api/forumPosts/updateComments/' + postId)
    .set({
      Authorization: `Bearer ${forumUserOne.tokens[0].token}`,
      userData: {
        userId: forumUserOneId,
        email: 'forum@controller.com',
        username: 'forum',
      },
    })
    .send(commentsDtoMock)
    .expect(200);
  expect(response.body.message).toBe('forumPost updated');
  let postAfterUpdate = await ForumPost.findById(postId);
  expect(postAfterUpdate.comments.length).toBe(1);
});

test('updateComments error 404', async () => {
  const commentsDtoMock = ['comment'];
  const response = await request(app)
    .put('/api/forumPosts/updateComments/' + post2Id)
    .set({
      Authorization: `Bearer ${forumUserOne.tokens[0].token}`,
      userData: {
        userId: forumUserOneId,
        email: 'forum@controller.com',
        username: 'forum',
      },
    })
    .send(commentsDtoMock)
    .expect(404);
  expect(response.body.message).toBe('fetching forumPost failed');
  let postAfterUpdate = await ForumPost.findById(post2Id);
  expect(postAfterUpdate).toBe(null);
});
