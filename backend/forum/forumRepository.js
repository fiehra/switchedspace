const ForumPost = require('../models/forum');

class ForumRepository {
  save(object) {
    return ForumPost(object).save();
  }

  updateOne(id, forumPost) {
    return ForumPost.updateOne(id, forumPost);
  }

  findAll() {
    return ForumPost.find();
  }

  findOne(query) {
    return ForumPost.findOne(query);
  }

  deleteOne(id) {
    return ForumPost.deleteOne(id);
  }

  deleteUserContent(userId) {
    return ForumPost.deleteMany({ writerId: userId });
  }
}

module.exports = new ForumRepository();
