const wallService = require('./wallService');

exports.create = (req, res, next) => {
  return wallService.addWall(req, res);
};

exports.update = (req, res, next) => {
  return wallService.updateWallById(req, res);
};

exports.getAll = (req, res, next) => {
  return wallService.getAllWalls(req, res);
};

exports.getById = (req, res, next) => {
  return wallService.getWallById(req, res);
};

exports.deleteById = (req, res, next) => {
  return wallService.deleteWallById(req, res);
};