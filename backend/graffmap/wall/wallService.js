const Wall = require('../../models/wall');
const wallRepository = require('./wallRepository');

// post:/wall/add
exports.addWall = (req, res, next) => {
  const wall = new Wall({
    id: req.body.id,
    name: req.body.name,
    legalState: req.body.legalState,
    description: req.body.description,
    lat: req.body.lat,
    lng: req.body.lng,
    statusUpdates: req.body.statusUpdates,
  });
  wallRepository
    .save(wall)
    .then((result) => {
      return res.status(201).json({
        message: 'added wall',
        wallId: result._id,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'adding wall failed',
      });
    });
};
// put:/wall/:id
exports.updateWallById = (req, res, next) => {
  const wall = new Wall({
    _id: req.body.id,
    name: req.body.name,
    legalState: req.body.legalState,
    description: req.body.description,
    lat: req.body.lat,
    lng: req.body.lng,
    statusUpdates: req.body.statusUpdates,
  });
  wallRepository.updateOne({ _id: req.params.id }, wall)
    .then((result) => {
      return res.status(200).json({
        message: 'wall updated',
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'updating wall failed',
      });
    });
};
// get:/walls
exports.getAllWalls = (req, res, next) => {
  let fetchedWalls;
  wallRepository.findAll()
    .then((response) => {
      fetchedWalls = response;
      return Wall.countDocuments();
    })
    .then((count) => {
      return res.status(200).json({
        message: 'walls fetched successfully',
        walls: fetchedWalls,
        maxWalls: count,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching walls failed',
      });
    });
};
// get:/wall/:id
exports.getWallById = (req, res, next) => {
  wallRepository.findOne({ _id: req.params.id })
    .then((response) => {
      if (response) {
        return res.status(200).json({
          message: 'wall fetched successfully',
          wall: response,
        });
      } else {
        return res.status(404).json({
          message: 'wall not found',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching wall failed',
      });
    });
};
// delete:/wall/delete/:id
exports.deleteWallById = (req, res, next) => {
  wallRepository.deleteOne({ _id: req.params.id })
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          message: 'deleted wall successfully',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'deleting wall failed',
      });
    });
};
