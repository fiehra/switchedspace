const express = require("express");
const checkAuth = require("../../middleware/checkAuth");
const WallController = require("./wallController");

const router = express.Router();

// add wall
router.post("/wall/add", checkAuth, WallController.create);

// update wall
router.put("/wall/update/:id", checkAuth, WallController.update);

// get walls
router.get("/walls", WallController.getAll);

// get wall by id
router.get("/wall/:id", WallController.getById);

// remove wall by id
router.delete("/wall/delete/:id", checkAuth, WallController.deleteById);

module.exports = router;