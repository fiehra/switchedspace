const app = require('../../app');
const request = require('supertest');
const Wall = require('../../models/wall');
const User = require('../../models/user');
const jwt = require('jsonwebtoken');

const mongoose = require('mongoose');

const userOneId = new mongoose.Types.ObjectId();
const wallId = new mongoose.Types.ObjectId();
const wrongId = new mongoose.Types.ObjectId();

const userOne = {
  _id: userOneId,
  email: 'graffmap@controller.com',
  username: 'wall',
  password: 'wall',
  tokens: [{
    token: jwt.sign({
      email: 'graffmap@controller.com',
      userId: userOneId,
      username: 'wall'
    }, process.env.JWT_SECRET)
  }]
}

const wallMock = {
  _id: wallId,
  name: 'wall',
  legalState: 'legal',
  description: 'wall',
  lat: 35.6895000,
  lng: 139.6917100,
  statusUpdates: ['confirmed legal']
}

const invalidWall = {
  _id: wrongId,
  lat: 35.6895000,
  lng: 139.6917100
}

beforeAll(async () => {
  await mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
})

beforeEach(async () => {
  await User.deleteMany();
  await new User(userOne).save();
  await Wall.deleteMany();
});

test('addWall success 201', async () => {
  const wallCountBefore = await Wall.countDocuments();
  const response = await request(app).post('/api/wall/add')
  .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
  .send(wallMock)
  .expect(201);
  expect(response.body.message).toBe('added wall');
  
  const wallCountAfter = await Wall.countDocuments();
  expect(wallCountBefore + 1).toBe(wallCountAfter);
});

test('addWall error 500', async () => {
  const wallCountBefore = await Wall.countDocuments();
  const response = await request(app).post('/api/wall/add')
  .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
  .send(invalidWall)
  .expect(500);
  expect(response.body.message).toBe('adding wall failed');
  
  const wallCountAfter = await Wall.countDocuments();
  expect(wallCountBefore).toBe(wallCountAfter);
});

test('updateWall success 200', async () => {
  const updateWallMock = {
    id: wallId,
    name: 'noname',
    legalState: 'illegal',
    description: 'bust',
    lat: 35.6895000,
    lng: 139.6917100,
    statusUpdates: ['confirmed legal']
  }
  await new Wall(wallMock).save();
  let wall = await Wall.findById(wallId);
  expect(wall).not.toBeNull();
  const response = await request(app).put('/api/wall/update/' + wallId)
  .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
  .send(updateWallMock)
  .expect(200);
  expect(response.body.message).toBe('wall updated');
  let updatedWall = await Wall.findById(wallId);
  expect(updatedWall.legalState).toBe('illegal');
});

test('updateWall error 500', async () => {
  const invalidWallMock = {
    id: wrongId,
    lat: 35.6895000,
    lng: 139.6917100
  }
  await new Wall(wallMock).save();
  let wall = await Wall.findById(wallId);
  expect(wall).not.toBeNull();
  const response = await request(app).put('/api/wall/update/' + wallId)
  .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send(invalidWallMock)
    .expect(500);
  expect(response.body.message).toBe('updating wall failed');
});

test('getWalls success 200', async () => {
  await new Wall(wallMock).save();
  let wall = await Wall.findById(wallId);
  expect(wall).not.toBe(null);
  const response = await request(app).get('/api/walls')
    .send().expect(200);
  expect(response.body.message).toBe('walls fetched successfully');
});

test('getWall by id success 200', async () => {
  await new Wall(wallMock).save();
  let wall = await Wall.findById(wallId);
  expect(wall).not.toBe(null);
  const response = await request(app).get('/api/wall/' + wallId)
    .send(wallId)
    .expect(200);
  expect(response.body.message).toBe('wall fetched successfully');
});

test('getWall by id error 404', async () => {
  let wall = await Wall.findById(wallId);
  expect(wall).toBeNull();
  const response = await request(app).get('/api/wall/' + wallId)
    .send(wallId)
    .expect(404);
  expect(response.body.message).toBe('wall not found');
});

test('getWall by id error 500', async () => {
  await new Wall(wallMock).save();
  let wall = await Wall.findById(wallId);
  expect(wall).not.toBe(null);
  const response = await request(app).get('/api/wall/' + wallMock)
    .send(wallMock)
    .expect(500);
  expect(response.body.message).toBe('fetching wall failed');
});

test('deleteWall success 200', async () => {
  await new Wall(wallMock).save();
  let wall = await Wall.findById(wallId);
  expect(wall).not.toBe(null);
  const wallCountBefore = await Wall.countDocuments();
  const response = await request(app).delete('/api/wall/delete/' + wallId)
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send(wallId)
    .expect(200);
  expect(response.body.message).toBe('deleted wall successfully');
  const wallCountAfter = await Wall.countDocuments();
  expect(wallCountBefore - 1).toBe(wallCountAfter);
});

test('deleteWall error 500', async () => {
  await new Wall(wallMock).save();
  let wall = await Wall.findById(wallId);
  expect(wall).not.toBe(null);
  const wallCountBefore = await Wall.countDocuments();
  const response = await request(app).delete('/api/wall/delete/' + wall)
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send(wall)
    .expect(500);
  expect(response.body.message).toBe('deleting wall failed');
  const wallCountAfter = await Wall.countDocuments();
  expect(wallCountBefore).toBe(wallCountAfter);
});