const Wall = require('../../models/wall');

class WallRepository {

  save(object) {
    return Wall(object).save();
  }

  findAll() {
    return Wall.find();
  }

  findOne(query) {
    return Wall.findOne(query);
  }

  updateOne(id, wall) {
    return Wall.updateOne(id, wall);
  }

  deleteOne(id) {
    return Wall.deleteOne(id);
  }
}

module.exports = new WallRepository();
