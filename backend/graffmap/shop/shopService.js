const Shop = require('../../models/shop');
const shopRepository = require('./shopRepository');

// post:/shop/add
exports.addShop = (req, res, next) => {
  const shop = new Shop({
    id: req.body.id,
    name: req.body.name,
    website: req.body.website,
    description: req.body.description,
    lat: req.body.lat,
    lng: req.body.lng,
  });
  shopRepository
    .save(shop)
    .then((result) => {
      return res.status(201).json({
        message: 'added shop',
        shopId: result._id,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'adding shop failed',
      });
    });
};
// put:/shop/:id
exports.updateShopById = (req, res, next) => {
  const shop = new Shop({
    _id: req.body.id,
    name: req.body.name,
    website: req.body.website,
    description: req.body.description,
    lat: req.body.lat,
    lng: req.body.lng,
  });
  shopRepository.updateOne({ _id: req.params.id }, shop)
    .then((result) => {
      return res.status(200).json({
        message: 'shop updated',
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'updating shop failed',
      });
    });
};
// get:/shops
exports.getAllShops = (req, res, next) => {
  let fetchedShops;
  shopRepository.findAll()
    .then((response) => {
      fetchedShops = response;
      return Shop.countDocuments();
    })
    .then((count) => {
      return res.status(200).json({
        message: 'shops fetched successfully',
        shops: fetchedShops,
        maxShops: count,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching shops failed',
      });
    });
};
// get:/shop/:id
exports.getShopById = (req, res, next) => {
  shopRepository.findOne({ _id: req.params.id })
    .then((response) => {
      if (response) {
        return res.status(200).json({
          message: 'shop fetched successfully',
          shop: response,
        });
      } else {
        return res.status(404).json({
          message: 'shop not found',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching shop failed',
      });
    });
};
// delete:/shop/delete/:id
exports.deleteShopById = (req, res, next) => {
  shopRepository.deleteOne({ _id: req.params.id })
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          message: 'deleted shop successfully',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'deleting shop failed',
      });
    });
};
