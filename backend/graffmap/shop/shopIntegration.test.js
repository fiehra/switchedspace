const app = require('../../app');
const request = require('supertest');
const Shop = require('../../models/shop');
const User = require('../../models/user');
const jwt = require('jsonwebtoken');

const mongoose = require('mongoose');

const userOneId = new mongoose.Types.ObjectId();
const shopId = new mongoose.Types.ObjectId();
const wrongId = new mongoose.Types.ObjectId();

const userOne = {
  _id: userOneId,
  email: 'shop@controller.com',
  username: 'shop',
  password: 'shop',
  tokens: [
    {
      token: jwt.sign(
        {
          email: 'shop@controller.com',
          userId: userOneId,
          username: 'shop',
        },
        process.env.JWT_SECRET
      ),
    },
  ],
};

const shopMock = {
  _id: shopId,
  name: 'shop',
  website: 'shop.com',
  description: 'shop',
  lat: 35.6895,
  lng: 139.69171,
};

const invalidShop = {
  _id: wrongId,
  lat: 35.6895,
  lng: 139.69171,
};

beforeAll(async () => {
  await mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
});

beforeEach(async () => {
  await User.deleteMany();
  await new User(userOne).save();
  await Shop.deleteMany();
});

test('addShop success 201', async () => {
  const shopCountBefore = await Shop.countDocuments();
  const response = await request(app)
    .post('/api/shop/add')
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send(shopMock)
    .expect(201);
  expect(response.body.message).toBe('added shop');

  const shopCountAfter = await Shop.countDocuments();
  expect(shopCountBefore + 1).toBe(shopCountAfter);
});

test('addShop error 500', async () => {
  const shopCountBefore = await Shop.countDocuments();
  const response = await request(app)
    .post('/api/shop/add')
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send(invalidShop)
    .expect(500);
  expect(response.body.message).toBe('adding shop failed');

  const shopCountAfter = await Shop.countDocuments();
  expect(shopCountBefore).toBe(shopCountAfter);
});

test('updateShop success 200', async () => {
  const updateShopMock = {
    id: shopId,
    name: 'noname',
    website: 'shop.com',
    description: 'bust',
    lat: 35.6895,
    lng: 139.69171,
  };
  await new Shop(shopMock).save();
  let shop = await Shop.findById(shopId);
  expect(shop).not.toBeNull();
  const response = await request(app)
    .put('/api/shop/update/' + shopId)
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send(updateShopMock)
    .expect(200);
  expect(response.body.message).toBe('shop updated');
  let updatedShop = await Shop.findById(shopId);
  expect(updatedShop.description).toBe('bust');
});

test('updateShop error 500', async () => {
  const invalidShopMock = {
    id: wrongId,
    lat: 35.6895,
    lng: 139.69171,
  };
  await new Shop(shopMock).save();
  let shop = await Shop.findById(shopId);
  expect(shop).not.toBeNull();
  const response = await request(app)
    .put('/api/shop/update/' + shopId)
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send(invalidShopMock)
    .expect(500);
  expect(response.body.message).toBe('updating shop failed');
});

test('getShops success 200', async () => {
  await new Shop(shopMock).save();
  let shop = await Shop.findById(shopId);
  expect(shop).not.toBe(null);
  const response = await request(app).get('/api/shops').send().expect(200);
  expect(response.body.message).toBe('shops fetched successfully');
});

test('getShop by id success 200', async () => {
  await new Shop(shopMock).save();
  let shop = await Shop.findById(shopId);
  expect(shop).not.toBe(null);
  const response = await request(app)
    .get('/api/shop/' + shopId)
    .send(shopId)
    .expect(200);
  expect(response.body.message).toBe('shop fetched successfully');
});

test('getShop by id error 404', async () => {
  let shop = await Shop.findById(shopId);
  expect(shop).toBeNull();
  const response = await request(app)
    .get('/api/shop/' + shopId)
    .send(shopId)
    .expect(404);
  expect(response.body.message).toBe('shop not found');
});

test('getShop by id error 500', async () => {
  await new Shop(shopMock).save();
  let shop = await Shop.findById(shopId);
  expect(shop).not.toBe(null);
  const response = await request(app)
    .get('/api/shop/' + shopMock)
    .send(shopMock)
    .expect(500);
  expect(response.body.message).toBe('fetching shop failed');
});

test('deleteShop success 200', async () => {
  await new Shop(shopMock).save();
  let shop = await Shop.findById(shopId);
  expect(shop).not.toBe(null);
  const shopCountBefore = await Shop.countDocuments();
  const response = await request(app)
    .delete('/api/shop/delete/' + shopId)
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send(shopId)
    .expect(200);
  expect(response.body.message).toBe('deleted shop successfully');
  const shopCountAfter = await Shop.countDocuments();
  expect(shopCountBefore - 1).toBe(shopCountAfter);
});

test('deleteShop error 500', async () => {
  await new Shop(shopMock).save();
  let shop = await Shop.findById(shopId);
  expect(shop).not.toBe(null);
  const shopCountBefore = await Shop.countDocuments();
  const response = await request(app)
    .delete('/api/shop/delete/' + shop)
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send(shop)
    .expect(500);
  expect(response.body.message).toBe('deleting shop failed');
  const shopCountAfter = await Shop.countDocuments();
  expect(shopCountBefore).toBe(shopCountAfter);
});
