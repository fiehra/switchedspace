const Shop = require('../../models/shop');

class ShopRepository {

  save(object) {
    return Shop(object).save();
  }

  findAll() {
    return Shop.find();
  }

  findOne(query) {
    return Shop.findOne(query);
  }

  updateOne(id, streetart) {
    return Shop.updateOne(id, streetart);
  }

  deleteOne(id) {
    return Shop.deleteOne(id);
  }
}

module.exports = new ShopRepository();
