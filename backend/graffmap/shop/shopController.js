const shopService = require('./shopService');

exports.create = (req, res, next) => {
  return shopService.addShop(req, res);
};

exports.update = (req, res, next) => {
  return shopService.updateShopById(req, res);
};

exports.getAll = (req, res, next) => {
  return shopService.getAllShops(req, res);
};

exports.getById = (req, res, next) => {
  return shopService.getShopById(req, res);
};

exports.deleteById = (req, res, next) => {
  return shopService.deleteShopById(req, res);
};