const express = require("express");
const checkAuth = require("../../middleware/checkAuth");
const ShopController = require("./shopController");

const router = express.Router();

// add shop
router.post("/shop/add", checkAuth, ShopController.create);

// update shop
router.put("/shop/update/:id", checkAuth, ShopController.update);

// get shops
router.get("/shops", ShopController.getAll);

// get shop by id
router.get("/shop/:id", ShopController.getById);

// remove shop by id
router.delete("/shop/delete/:id", checkAuth, ShopController.deleteById);

module.exports = router;