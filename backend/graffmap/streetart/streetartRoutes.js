const express = require("express");
const checkAuth = require("../../middleware/checkAuth");
const StreetartController = require("./streetartController");

const router = express.Router();

// add streetart
router.post("/streetart/add", checkAuth, StreetartController.create);

// update streetart
router.put("/streetart/update/:id", checkAuth, StreetartController.update);

// get streetarts
router.get("/streetarts", StreetartController.getAll);

// get streetart by id
router.get("/streetart/:id", StreetartController.getById);

// remove streetart by id
router.delete("/streetart/delete/:id", checkAuth, StreetartController.deleteById);

module.exports = router;