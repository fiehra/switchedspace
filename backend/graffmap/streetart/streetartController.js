const streetartService = require('./streetartService');

exports.create = (req, res, next) => {
  return streetartService.addStreetart(req, res);
};

exports.update = (req, res, next) => {
  return streetartService.updateStreetartById(req, res);
};

exports.getAll = (req, res, next) => {
  return streetartService.getAllStreetarts(req, res);
};

exports.getById = (req, res, next) => {
  return streetartService.getStreetartById(req, res);
};

exports.deleteById = (req, res, next) => {
  return streetartService.deleteStreetartById(req, res);
};