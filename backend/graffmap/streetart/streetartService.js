const Streetart = require('../../models/streetart');
const streetartRepository = require('./streetartRepository');

// post:/streetart/add
exports.addStreetart = (req, res, next) => {
  const streetart = new Streetart({
    id: req.body.id,
    name: req.body.name,
    link: req.body.link,
    description: req.body.description,
    lat: req.body.lat,
    lng: req.body.lng,
  });
  streetartRepository
    .save(streetart)
    .then((result) => {
      return res.status(201).json({
        message: 'added streetart',
        shopId: result._id,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'adding streetart failed',
      });
    });
};
// put:/streetart/:id
exports.updateStreetartById = (req, res, next) => {
  const streetart = new Streetart({
    _id: req.body.id,
    name: req.body.name,
    link: req.body.link,
    description: req.body.description,
    lat: req.body.lat,
    lng: req.body.lng,
  });
  streetartRepository.updateOne({ _id: req.params.id }, streetart)
    .then((result) => {
      return res.status(200).json({
        message: 'streetart updated',
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'updating streetart failed',
      });
    });
};
// get:/streetarts
exports.getAllStreetarts = (req, res, next) => {
  let fetchedStreetarts;
  streetartRepository.findAll()
    .then((response) => {
      fetchedStreetarts = response;
      return Streetart.countDocuments();
    })
    .then((count) => {
      return res.status(200).json({
        message: 'streetarts fetched successfully',
        streetarts: fetchedStreetarts,
        maxStreetarts: count,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching streetarts failed',
      });
    });
};
// get:/streetart/:id
exports.getStreetartById = (req, res, next) => {
  streetartRepository.findOne({ _id: req.params.id })
    .then((response) => {
      if (response) {
        return res.status(200).json({
          message: 'streetart fetched successfully',
          streetart: response,
        });
      } else {
        return res.status(404).json({
          message: 'streetart not found',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'fetching streetart failed',
      });
    });
};
// delete:/streetart/delete/:id
exports.deleteStreetartById = (req, res, next) => {
  streetartRepository.deleteOne({ _id: req.params.id })
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          message: 'deleted streetart successfully',
        });
      }
    })
    .catch((error) => {
      return res.status(500).json({
        message: 'deleting streetart failed',
      });
    });
};
