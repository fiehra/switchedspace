const app = require('../../app');
const request = require('supertest');
const Streetart = require('../../models/streetart');
const User = require('../../models/user');
const jwt = require('jsonwebtoken');

const mongoose = require('mongoose');

const userOneId = new mongoose.Types.ObjectId();
const streetartId = new mongoose.Types.ObjectId();
const wrongId = new mongoose.Types.ObjectId();

const userOne = {
  _id: userOneId,
  email: 'streetart@controller.com',
  username: 'streetart',
  password: 'streetart',
  tokens: [{
    token: jwt.sign({
      email: 'streetart@controller.com',
      userId: userOneId,
      username: 'streetart'
    }, process.env.JWT_SECRET)
  }]
}

const streetartMock = {
  _id: streetartId,
  name: 'streetart',
  link: 'streetart.com',
  description: 'streetart',
  lat: 35.6895000,
  lng: 139.6917100
}

const invalidStreetart = {
  _id: wrongId,
  lat: 35.6895000,
  lng: 139.6917100
}

beforeAll(async () => {
  await mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
})

beforeEach(async () => {
  await User.deleteMany();
  await new User(userOne).save();
  await Streetart.deleteMany();
});

test('addStreetart success 201', async () => {
  const streetartCountBefore = await Streetart.countDocuments();
  const response = await request(app).post('/api/streetart/add')
  .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
  .send(streetartMock)
  .expect(201);
  expect(response.body.message).toBe('added streetart');
  
  const streetartCountAfter = await Streetart.countDocuments();
  expect(streetartCountBefore + 1).toBe(streetartCountAfter);
});

test('addStreetart error 500', async () => {
  const streetartCountBefore = await Streetart.countDocuments();
  const response = await request(app).post('/api/streetart/add')
  .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
  .send(invalidStreetart)
  .expect(500);
  expect(response.body.message).toBe('adding streetart failed');
  
  const streetartCountAfter = await Streetart.countDocuments();
  expect(streetartCountBefore).toBe(streetartCountAfter);
});

test('updateStreetart success 200', async () => {
  const updateStreetartMock = {
    id: streetartId,
    name: 'noname',
    link: 'streetart.com',
    description: 'bust',
    lat: 35.6895000,
    lng: 139.6917100
  }
  await new Streetart(streetartMock).save();
  let streetart = await Streetart.findById(streetartId);
  expect(streetart).not.toBeNull();
  const response = await request(app).put('/api/streetart/update/' + streetartId)
  .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
  .send(updateStreetartMock)
  .expect(200);
  expect(response.body.message).toBe('streetart updated');
  let updateStreetart = await Streetart.findById(streetartId);
  expect(updateStreetart.description).toBe('bust');
});

test('updateStreetart error 500', async () => {
  const invalidStreetartMock = {
    id: wrongId,
    lat: 35.6895000,
    lng: 139.6917100
  }
  await new Streetart(streetartMock).save();
  let streetart = await Streetart.findById(streetartId);
  expect(streetart).not.toBeNull();
  const response = await request(app).put('/api/streetart/update/' + streetartId)
  .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send(invalidStreetartMock)
    .expect(500);
  expect(response.body.message).toBe('updating streetart failed');
});

test('getStreetarts success 200', async () => {
  await new Streetart(streetartMock).save();
  let streetart = await Streetart.findById(streetartId);
  expect(streetart).not.toBe(null);
  const response = await request(app).get('/api/streetarts')
    .send().expect(200);
  expect(response.body.message).toBe('streetarts fetched successfully');
});

test('getStreetart by id success 200', async () => {
  await new Streetart(streetartMock).save();
  let streetart = await Streetart.findById(streetartId);
  expect(streetart).not.toBe(null);
  const response = await request(app).get('/api/streetart/' + streetartId)
    .send(streetartId)
    .expect(200);
  expect(response.body.message).toBe('streetart fetched successfully');
});

test('getStreetart by id error 404', async () => {
  let streetart = await Streetart.findById(streetartId);
  expect(streetart).toBeNull();
  const response = await request(app).get('/api/streetart/' + streetartId)
    .send(streetartId)
    .expect(404);
  expect(response.body.message).toBe('streetart not found');
});

test('getStreetart by id error 500', async () => {
  await new Streetart(streetartMock).save();
  let streetart = await Streetart.findById(streetartId);
  expect(streetart).not.toBe(null);
  const response = await request(app).get('/api/streetart/' + streetartMock)
    .send(streetartMock)
    .expect(500);
  expect(response.body.message).toBe('fetching streetart failed');
});

test('deleteStreetart success 200', async () => {
  await new Streetart(streetartMock).save();
  let streetart = await Streetart.findById(streetartId);
  expect(streetart).not.toBe(null);
  const streetartCountBefore = await Streetart.countDocuments();
  const response = await request(app).delete('/api/streetart/delete/' + streetartId)
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send(streetartId)
    .expect(200);
  expect(response.body.message).toBe('deleted streetart successfully');
  const streetartCountAfter = await Streetart.countDocuments();
  expect(streetartCountBefore - 1).toBe(streetartCountAfter);
});

test('deleteStreetart error 500', async () => {
  await new Streetart(streetartMock).save();
  let streetart = await Streetart.findById(streetartId);
  expect(streetart).not.toBe(null);
  const streetartCountBefore = await Streetart.countDocuments();
  const response = await request(app).delete('/api/streetart/delete/' + streetart)
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send(streetart)
    .expect(500);
  expect(response.body.message).toBe('deleting streetart failed');
  const streetartCountAfter = await Streetart.countDocuments();
  expect(streetartCountBefore).toBe(streetartCountAfter);
});