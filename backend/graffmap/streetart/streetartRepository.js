const Streetart = require('../../models/streetart');

class StreetartRepository {

  save(object) {
    return Streetart(object).save();
  }

  findAll() {
    return Streetart.find();
  }

  findOne(query) {
    return Streetart.findOne(query);
  }

  updateOne(id, streetart) {
    return Streetart.updateOne(id, streetart);
  }

  deleteOne(id) {
    return Streetart.deleteOne(id);
  }
}

module.exports = new StreetartRepository();
