# skillcap.org

1. clone repo
2. run yarn install
3. start developing

## development backend

/backend

yarn server

## development frontend

/frontend

ng serve

## Running frontend tests with jest

/frontend

yarn test 'filename/directoryname'

## Running backend tests with jest

multiple tests will be run in band (--runInBand) in order to avoid DB inconsistency

/backend

yarn test 'filename/directoryname'

## Further help

to get more help join our discord or send a message on skillcap.org/contact

## how to make custom svg icons

1. create svg icons in inkscape, make sure size is big enough

2. import svg in icomoon.io

3. generate font while selecting the icons

4. extract fonts folder content into font folder

5. add styles in styles.scss
