import { MyHammerConfig } from './helper/hammer.config';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DonationHelper } from './helper/donation.helper';
import { SnackbarHelper } from './helper/snackbar.helper';
import { ErrorComponent } from './error/error.component';
import { ErrorInterceptor } from './errorInterceptor';
import { AuthInterceptor } from './helper/authInterceptor';
import { FormHelper } from './helper/form.helper';
import { ComponentsModule } from './components/components.module';
import { UtilsModule } from './utils/utils.module';
import { PagesModule } from './pages/pages.module';
import { NgModule } from '@angular/core';
import { BrowserModule, HammerModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent, ErrorComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HammerModule,
    ComponentsModule,
    UtilsModule,
    PagesModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, 'assets/i18n/', '.json'),
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    FormHelper,
    SnackbarHelper,
    DonationHelper,

    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
