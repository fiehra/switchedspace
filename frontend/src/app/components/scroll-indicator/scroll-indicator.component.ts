import { ElementScrollPercentage } from './../../services/element-scroll-percentage';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'scroll-indicator',
  templateUrl: './scroll-indicator.component.html',
  styleUrls: ['./scroll-indicator.component.scss']
})
export class ScrollIndicatorComponent implements OnInit {

  public pageScroll: number;
  
  constructor(private elementScrollPercentage: ElementScrollPercentage) { 
 
		this.pageScroll = 0;
 }

  ngOnInit() {
    this.elementScrollPercentage
    .getScrollAsStream() // Defaults to Document if no Element supplied.
    .subscribe(
      ( percent: number ) : void => {
  
        this.pageScroll = percent;
  
      }
    );
  }
  

}
