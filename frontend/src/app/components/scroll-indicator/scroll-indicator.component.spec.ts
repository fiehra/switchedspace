import { of } from 'rxjs';
import { ElementScrollPercentage } from './../../services/element-scroll-percentage';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { MaterialModule } from './../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ScrollIndicatorComponent } from './scroll-indicator.component';
import { HttpClient } from '@angular/common/http';

describe('ScrollIndicatorComponent', () => {
  let component: ScrollIndicatorComponent;
  let fixture: ComponentFixture<ScrollIndicatorComponent>;
  let elementScrollPercentage: ElementScrollPercentage;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ScrollIndicatorComponent
      ],
      providers: [
        ElementScrollPercentage
      ],
      imports: [
        MaterialModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
            deps: [HttpClient]
          }
        })

      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrollIndicatorComponent);
    component = fixture.componentInstance;
    elementScrollPercentage = TestBed.inject(ElementScrollPercentage);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(elementScrollPercentage, 'getScrollAsStream').and.returnValue(of(0));
    component.ngOnInit();
    fixture.detectChanges();
    expect(elementScrollPercentage.getScrollAsStream).toHaveBeenCalled();
  });
});
