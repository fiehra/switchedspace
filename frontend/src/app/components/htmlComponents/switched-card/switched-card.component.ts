import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'switched-card',
  templateUrl: './switched-card.component.html',
  styleUrls: ['./switched-card.component.scss']
})
export class SwitchedCardComponent implements OnInit {

  @Input() title: string;
  @Input() text: string;
  @Input() imgSrc: string = '';
  @Input() readTime: string = '';
  @Input() button: boolean = false;
  @Input() buttonText: string = '';

  constructor() { }

  ngOnInit() {

  }

}
