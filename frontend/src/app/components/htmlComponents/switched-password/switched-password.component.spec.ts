import { FormHelper } from './../../../helper/form.helper';
import { MaterialModule } from './../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SwitchedPasswordComponent } from './switched-password.component';
import { Component } from '@angular/core';
import { FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

@Component({
  selector: 'host-component',
  template: '<switched-password [control]="control" [placeholder]="placeholder"></switched-password>'
})
class TestHostComponent {
  placeholder = 'placeholder';
  control = new FormControl('abc');
}
describe('SwitchedPasswordComponent', () => {
  let component: SwitchedPasswordComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let nativeElement: any;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        SwitchedPasswordComponent,
        TestHostComponent
      ],
      providers: [
        FormHelper  
      ],
      imports: [
        MaterialModule,
        HttpClientModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
            deps: [HttpClient]
          }
        })
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    nativeElement = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('get control', () => {
    fixture.componentInstance.control = new FormControl('abc', [Validators.required]);
    fixture.detectChanges();
    expect(component.control.hasError('required')).toBeFalsy();
  });
});
