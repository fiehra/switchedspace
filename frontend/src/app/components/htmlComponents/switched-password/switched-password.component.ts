import { FormHelper } from './../../../helper/form.helper';
import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'switched-password',
  templateUrl: './switched-password.component.html',
  styleUrls: ['./switched-password.component.scss']
})
export class SwitchedPasswordComponent implements OnInit {

  inputControl: FormControl;
  requiredErrorMessage: string;
  noValidNumberErrorMessage: string;

  @Input() placeholder: string;

  isRequired: boolean;
  hidePw = true;

  @Input() set control(controlObj: FormControl) {
    this.inputControl = controlObj;
    this.isRequired = this.formHelper.checkRequiredValidator(this.inputControl);
    if (this.placeholder && this.placeholder !== '') {
      this.placeholder = this.translateService.instant(this.placeholder);
    }
    this.requiredErrorMessage = this.formHelper.createRequiredErrorMessage(this.placeholder);
    this.noValidNumberErrorMessage = this.formHelper.createNoValidNumberErrorMessage(this.placeholder);
  }

  get control(): FormControl {
    return this.inputControl;
  }

  constructor(private translateService: TranslateService, private formHelper: FormHelper) {}

  ngOnInit() {

  }


}
