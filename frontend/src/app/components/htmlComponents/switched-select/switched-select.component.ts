import { ValueLabel } from './../../../utils/valueLabel';
import { FormHelper } from './../../../helper/form.helper';
import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'switched-select',
  templateUrl: './switched-select.component.html',
  styleUrls: ['./switched-select.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SwitchedSelectComponent implements OnInit {
  inputControl: FormControl;
  requiredErrorMessage: string;
  
  isRequired: boolean;

  @Input() placeholder: string;
  @Input() options: ValueLabel[];

  @Input() set control(controlObj: FormControl) {
    this.inputControl = controlObj;
    this.isRequired = this.formHelper.checkRequiredValidator(this.inputControl);
    this.placeholder = this.translateService.instant(this.placeholder);
    this.requiredErrorMessage = this.formHelper.createRequiredErrorMessage(this.placeholder);
  }

  get control(): FormControl {
    return this.inputControl;
  }

  constructor(private translateService: TranslateService, private formHelper: FormHelper) { }

  ngOnInit() {
  }

}
