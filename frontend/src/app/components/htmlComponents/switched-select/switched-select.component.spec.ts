import { MaterialModule } from './../../../utils/material/material.module';
import { FormHelper } from './../../../helper/form.helper';
import { ValueLabel } from './../../../utils/valueLabel';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SwitchedSelectComponent } from './switched-select.component';
import { Component } from '@angular/core';
import { FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

@Component({
  selector: 'host-component',
  template: '<switched-select [placeholder]="\'placeholder\'" [control]="control" [options]="options"></switched-select>'
})
class TestHostComponent {
  control = new FormControl('value-2');
  options: ValueLabel[] = [
    { value: 'value-1', label: 'label-1' },
    { value: 'value-2', label: 'label-2' },
    { value: 'value-3', label: 'label-3' }
  ];
}

describe('SwitchedSelectComponent', () => {
  let component: SwitchedSelectComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let nativeElement: any;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        SwitchedSelectComponent,
        TestHostComponent
      ],
      providers: [
        FormHelper
      ],
      imports: [
        MaterialModule,
        HttpClientModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
            deps: [HttpClient]
          }
        })
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    nativeElement = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('get control', () => {
    fixture.componentInstance.control = new FormControl('abc', [Validators.required]);
    fixture.detectChanges();
    expect(component.control.hasError('required')).toBeFalsy();
  });
});
