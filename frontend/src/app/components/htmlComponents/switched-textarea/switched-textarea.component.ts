import { FormHelper } from './../../../helper/form.helper';
import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'switched-textarea',
  templateUrl: './switched-textarea.component.html',
  styleUrls: ['./switched-textarea.component.scss'],
})
export class SwitchedTextareaComponent implements OnInit {
  inputControl: FormControl;
  requiredErrorMessage: string;
  @Input() placeholder: string;
  @Input() maxRows: number;
  @Input() minRows: number;

  isRequired: boolean;

  @Input() set control(controlObj: FormControl) {
    this.inputControl = controlObj;
    this.isRequired = this.formHelper.checkRequiredValidator(this.inputControl);
    if (this.placeholder && this.placeholder !== '') {
      this.placeholder = this.translateService.instant(this.placeholder);
    }
    this.requiredErrorMessage = this.formHelper.createRequiredErrorMessage(
      this.placeholder,
    );
  }

  get control(): FormControl {
    return this.inputControl;
  }

  constructor(
    private translateService: TranslateService,
    private formHelper: FormHelper,
  ) {}

  ngOnInit() {}
}
