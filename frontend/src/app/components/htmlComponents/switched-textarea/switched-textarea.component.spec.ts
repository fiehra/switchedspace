import { FormHelper } from './../../../helper/form.helper';
import { MaterialModule } from './../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SwitchedTextareaComponent } from './switched-textarea.component';
import { Component } from '@angular/core';
import { FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';

@Component({
  selector: 'host-component',
  template: '<switched-textarea [minRows]="5" [maxRows]="50" [control]="control" [placeholder]="\'placeholder\'"></switched-textarea>'
})
class TestHostComponent {

  control = new FormControl('abc');
}

describe('SwitchedTextareaComponent', () => {
  let component: SwitchedTextareaComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let nativeElement: any;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        SwitchedTextareaComponent, TestHostComponent
      ],
      providers: [
        FormHelper
      ],
      imports: [
        MaterialModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
            deps: [HttpClient]
          }
        })
        
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    nativeElement = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('get control', () => {
    fixture.componentInstance.control = new FormControl('abc', [Validators.required]);
    fixture.detectChanges();
    expect(component.control.hasError('required')).toBeFalsy();
  });
});
