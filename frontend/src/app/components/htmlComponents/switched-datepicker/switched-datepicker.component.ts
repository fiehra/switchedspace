import { FormHelper } from './../../../helper/form.helper';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'switched-datepicker',
  templateUrl: './switched-datepicker.component.html',
  styleUrls: ['./switched-datepicker.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SwitchedDatepickerComponent implements OnInit {

  
  inputControl: FormControl;
  requiredErrorMessage: string;
  noValidDateMessage: string;

  @Input() placeholder: string;
  @Input() hint = "";

  isRequired: boolean;
  showError = false;
  errorMessage = '';
  date;

  @Input() set control(controlObj: FormControl) {
    this.inputControl = controlObj;
    this.date = this.inputControl.value;
    this.isRequired = this.formHelper.checkRequiredValidator(this.inputControl);
    this.requiredErrorMessage = this.formHelper.createRequiredErrorMessage(this.placeholder);
    this.noValidDateMessage = this.translateService.instant('invalidDate');

    if (this.placeholder && this.placeholder !== '') {
      this.placeholder = this.translateService.instant(this.placeholder);
    }
  }

  get control(): FormControl {
    return this.inputControl;
  }

  constructor(
    private translateService: TranslateService,
    private formHelper: FormHelper
  ) {}

  ngOnInit() {}

  getErrorMessage(pickerInput: string): string {
    this.showError = true;
    if ((!pickerInput || pickerInput === '') && this.isRequired) {
      return this.requiredErrorMessage;
    } else if ((!pickerInput || pickerInput === '') && !this.isRequired) {
      this.showError = false;
      return '';
    }
    return this.isMyDateFormat(pickerInput);
  }

  isMyDateFormat(date: string): string {
    if (date.length !== 10) {
      return this.noValidDateMessage;
    } else {
      const da = date.split('.');
      if (da.length !== 3 || da[0].length !== 2 || da[1].length !== 2 || da[2].length !== 4) {
        return this.noValidDateMessage;
      }
    }
    return this.noValidDateMessage;
  }

  dateChanged(event: any ) {
    const value = event.value;
    if (value) {
      this.showError = false;
      this.errorMessage = '';
      this.inputControl.setValue(value);
    } else {
      this.errorMessage = this.getErrorMessage(event.targetElement.value);
    }
  }


}
