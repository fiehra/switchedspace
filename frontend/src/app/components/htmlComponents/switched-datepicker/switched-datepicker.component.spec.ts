import { ElementRef } from '@angular/core';
import { FormHelper } from './../../../helper/form.helper';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MaterialModule } from './../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';

import { SwitchedDatepickerComponent } from './switched-datepicker.component';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { MatDatepickerInput } from '@angular/material/datepicker';

@Component({
  selector: 'host-component',
  template: '<switched-datepicker [placeholder]="\'placeholder\'" [control]="control"></switched-datepicker>'
})
class TestHostComponent {
  control = new FormControl('');
}

describe('SwitchedDatepickerComponent', () => {
  let component: SwitchedDatepickerComponent;
  let fixture: ComponentFixture < TestHostComponent > ;
  let nativeElement: any;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
        declarations: [
          SwitchedDatepickerComponent,
          TestHostComponent
        ],
        providers: [
          FormHelper

        ],
        imports: [
          FormsModule,
          MaterialModule,
          ReactiveFormsModule,
          HttpClientTestingModule,
          BrowserAnimationsModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
              deps: [HttpClient]
            }
          })
        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    nativeElement = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getErrorMessage required = true, show error', () => {
    component.isRequired = true;
    component.showError = false;
    component.getErrorMessage('');
    fixture.detectChanges();
    expect(component.showError).toBe(true);
  });

  it('getErrorMessage required = false, show nothing', () => {
    component.isRequired = false;
    component.showError = false;
    component.getErrorMessage('');
    fixture.detectChanges();
    expect(component.showError).toBe(false);
  });

  it('getErrorMessage required = true, put in string', () => {
    component.isRequired = true;
    component.showError = false;
    const string = 'string';
    expect(component.showError).toBe(false);
    component.getErrorMessage(string);
    fixture.detectChanges();
    expect(component.showError).toBe(true);
  });

  it('isMyDateFormat length === 10', () => {
    const date = 'length0.10';
    const result = component.isMyDateFormat(date);
    expect(result).toBe(component.noValidDateMessage);
  });

  it('isMyDateFormat length === 10 and valid', () => {
    const date = '12.12.2020';
    const result = component.isMyDateFormat(date);
    expect(result).toBe(component.noValidDateMessage);
  });

  it('dateChanged event.value === Thu Nov 19 2020 00:00:00 GMT+0100', () => {
     expect(component.showError).toBe(false);
     const event = {
       target: MatDatepickerInput,
       targetElement: ElementRef,
       value: 'Thu Nov 19 2020 00:00:00 GMT+0100'
     };
     fixture.detectChanges();
     spyOn(component.inputControl, 'setValue').and.stub();
     component.dateChanged(event);
     fixture.detectChanges();
     expect(component.inputControl.setValue).toHaveBeenCalled();
  });

  it('dateChanged event.value === null' , () => {
     expect(component.showError).toBe(false);
     const event = {
       target: MatDatepickerInput,
       targetElement: ElementRef,
       value: null
     };
     fixture.detectChanges();
     spyOn(component, 'getErrorMessage').and.stub();
     component.dateChanged(event);
     fixture.detectChanges();
     expect(component.getErrorMessage).toHaveBeenCalled();
  });

});