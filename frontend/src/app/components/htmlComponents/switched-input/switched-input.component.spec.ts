import { FormHelper } from './../../../helper/form.helper';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './../../../utils/material/material.module';
import { FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SwitchedInputComponent } from './switched-input.component';
import { Component } from '@angular/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@Component({
  selector: 'host-component',
  template: '<switched-input [control]="control" [placeholder]="placeholder"></switched-input>',
})
class TestHostComponent {
  placeholder = 'placeholder';
  control = new FormControl('abc');
}

describe('SwitchedInputComponent', () => {
  let component: SwitchedInputComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let nativeElement: any;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SwitchedInputComponent, TestHostComponent],
      providers: [FormHelper],
      imports: [
        MaterialModule,
        HttpClientModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (http: HttpClient) =>
              new TranslateHttpLoader(
                http,
                'assets/i18n/',
                '.json?build_id=build_id_replacement_placeholder',
              ),
            deps: [HttpClient],
          },
        }),
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    nativeElement = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('get control', () => {
    fixture.componentInstance.control = new FormControl('abc', [Validators.email]);
    fixture.detectChanges();
    expect(component.control.hasError('email')).toBeTruthy();
  });
});
