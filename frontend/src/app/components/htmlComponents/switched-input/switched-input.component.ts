import { FormHelper } from './../../../helper/form.helper';
import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'switched-input',
  templateUrl: './switched-input.component.html',
  styleUrls: ['./switched-input.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SwitchedInputComponent implements OnInit {
  inputControl: FormControl;
  requiredErrorMessage: string;
  noValidNumberErrorMessage: string;

  @Input() placeholder: string;

  isRequired: boolean;

  @Input() set control(controlObj: FormControl) {
    this.inputControl = controlObj;
    this.isRequired = this.formHelper.checkRequiredValidator(this.inputControl);
    if (this.placeholder && this.placeholder !== '') {
      this.placeholder = this.translateService.instant(this.placeholder);
    }
    this.requiredErrorMessage = this.formHelper.createRequiredErrorMessage(this.placeholder);
    this.noValidNumberErrorMessage = this.formHelper.createNoValidNumberErrorMessage(this.placeholder);
  }
  
  get control(): FormControl {
    return this.inputControl;
  }

  constructor(private translateService: TranslateService, private formHelper: FormHelper) {}

  ngOnInit() {

  }


}
