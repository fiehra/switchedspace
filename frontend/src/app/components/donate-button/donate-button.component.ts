import { DonationHelper } from './../../helper/donation.helper';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'donate-button',
  templateUrl: './donate-button.component.html',
  styleUrls: ['./donate-button.component.scss']
})
export class DonateButtonComponent implements OnInit {

  constructor(private donationHelper: DonationHelper) { }

  ngOnInit(): void {
  }
  
  copyCoinAddress(crypto: string) {
    this.donationHelper.copyCryptoLink(crypto);
  }
  

}
