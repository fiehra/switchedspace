import { SnackbarHelper } from './../../helper/snackbar.helper';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { MaterialModule } from './../../utils/material/material.module';
import { DonationHelper } from './../../helper/donation.helper';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonateButtonComponent } from './donate-button.component';

describe('DonateButtonComponent', () => {
  let component: DonateButtonComponent;
  let fixture: ComponentFixture<DonateButtonComponent>;
  let donationHelper: DonationHelper;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        DonateButtonComponent
      ],
      providers: [
        DonationHelper,
        SnackbarHelper
      ],
      imports: [
        HttpClientTestingModule,
        MaterialModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
            deps: [HttpClient]
          }
        })
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DonateButtonComponent);
    component = fixture.componentInstance;
    donationHelper = TestBed.inject(DonationHelper);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('copyCoinAddress bitcoin', () => {
    spyOn(donationHelper, 'copyCryptoLink').and.stub();
    component.copyCoinAddress('bitcoin');
    fixture.detectChanges();
    expect(donationHelper.copyCryptoLink).toHaveBeenCalledWith('bitcoin');
  });
});
