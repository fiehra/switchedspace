import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  showFloatingMenu = false;

  constructor() {}

  ngOnInit() {}

  // scrollDownMenu logic
  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (
      (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) > 764
    ) {
      this.showFloatingMenu = true;
    } else if (
      this.showFloatingMenu &&
      (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) < 764
    ) {
      this.showFloatingMenu = false;
    }
  }
}
