import { LearnMenuComponent } from './parts/learn-menu/learn-menu.component';
import { ThemeMenuComponent } from './parts/theme-menu/theme-menu.component';
import { MainMenuComponent } from './parts/main-menu/main-menu.component';
import { LanguageMenuComponent } from './parts/language-menu/language-menu.component';
import { SnackbarHelper } from './../../helper/snackbar.helper';
import { FormHelper } from './../../helper/form.helper';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder } from '@angular/forms';
import { MaterialModule } from './../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ToolbarComponent } from './toolbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

describe('ToolbarComponent', () => {
  let component: ToolbarComponent;
  let fixture: ComponentFixture<ToolbarComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [
          ToolbarComponent,
          LanguageMenuComponent,
          MainMenuComponent,
          ThemeMenuComponent,
          LearnMenuComponent,
        ],
        providers: [FormBuilder, FormHelper, SnackbarHelper],
        imports: [
          MaterialModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onWindowScroll', () => {
    component.onWindowScroll();
    expect(component.showFloatingMenu).toBe(false);
    window = Object.assign(window, { pageYOffset: 2000 });
    component.onWindowScroll();
    expect(component.showFloatingMenu).toBe(true);
  });

  it('onWindowScroll', () => {
    window = Object.assign(window, { pageYOffset: 2000 });
    component.onWindowScroll();
    expect(component.showFloatingMenu).toBe(true);
    window = Object.assign(window, { pageYOffset: 0 });
    component.onWindowScroll();
    expect(component.showFloatingMenu).toBe(false);
  });
});
