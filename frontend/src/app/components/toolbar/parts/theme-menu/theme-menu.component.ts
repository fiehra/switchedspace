import { ThemeService } from './../../../../services/theme.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'theme-menu',
  templateUrl: './theme-menu.component.html',
  styleUrls: ['./theme-menu.component.scss'],
})
export class ThemeMenuComponent implements OnInit {
  constructor(private themeService: ThemeService) {}

  ngOnInit(): void {}

  updateTheme(color: string) {
    this.themeService.changeTheme(color);
  }
}
