import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { AuthService } from '../../../../services/auth.service';
import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';

@Component({
  selector: 'main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss'],
})
export class MainMenuComponent implements OnInit, OnDestroy {
  private authListenerSubs: Subscription;
  loggedIn: boolean = false;
  username: string;
  userId: string;
  userRole: string;
  currentLanguage: string;
  showFloatingMenu: boolean = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private translate: TranslateService,
  ) {}

  ngOnInit(): void {
    this.loggedIn = this.authService.getLoggedIn();
    this.userRole = this.authService.getUserRole();
    this.authListenerSubs = this.authService.getAuthStatusListener().subscribe((authStatus) => {
      this.loggedIn = authStatus;
    });
    if (this.loggedIn) {
      this.username = this.authService.getUsername();
      this.userId = this.authService.getUserId();
    }

    // setting the language for the app
    if (this.translate.currentLang === 'en') {
      this.currentLanguage = 'english';
    } else if (this.translate.currentLang === 'de') {
      this.currentLanguage = 'deutsch';
    } else if (this.translate.currentLang === 'tu') {
      this.currentLanguage = 'türkçe';
    } else if (this.translate.currentLang === 'jp') {
      this.currentLanguage = '日本語';
    }
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (
      (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) > 764
    ) {
      this.showFloatingMenu = true;
    } else if (
      this.showFloatingMenu &&
      (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) < 764
    ) {
      this.showFloatingMenu = false;
    }
  }

  logout() {
    this.authService.logout();
  }

  routeToMyProfile() {
    this.router.navigate(['/myProfile/' + this.userId]);
  }

  setCurrentLang(langEvt: string) {
    this.currentLanguage = langEvt;
  }

  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
  }
}
