import { LearnMenuComponent } from '../learn-menu/learn-menu.component';
import { ThemeMenuComponent } from '../theme-menu/theme-menu.component';
import { LanguageMenuComponent } from '../language-menu/language-menu.component';
import { SnackbarHelper } from '../../../../helper/snackbar.helper';
import { FormHelper } from '../../../../helper/form.helper';
import { FormBuilder } from '@angular/forms';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../../../../utils/material/material.module';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../../../../services/auth.service';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainMenuComponent } from './main-menu.component';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

describe('MainMenu', () => {
  let component: MainMenuComponent;
  let fixture: ComponentFixture<MainMenuComponent>;
  let authService: AuthService;
  let router: Router;
  let translateService: TranslateService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        MainMenuComponent,
        LanguageMenuComponent,
        ThemeMenuComponent,
        LearnMenuComponent,
      ],
      providers: [FormBuilder, FormHelper, SnackbarHelper],

      imports: [
        MaterialModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (http: HttpClient) =>
              new TranslateHttpLoader(
                http,
                'assets/i18n/',
                '.json?build_id=build_id_replacement_placeholder',
              ),
            deps: [HttpClient],
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainMenuComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
    translateService = TestBed.inject(TranslateService);
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(authService, 'getAuthStatusListener').and.returnValue(of(true));
    spyOn(authService, 'getUsername').and.stub();
    spyOn(authService, 'getUserId').and.stub();
    spyOn(authService, 'getUserRole').and.stub();
    expect(component.loggedIn).toBe(false);
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getAuthStatusListener).toHaveBeenCalled();
    expect(component.loggedIn).toBe(true);
    expect(authService.getUsername).toHaveBeenCalled();
    expect(authService.getUserId).toHaveBeenCalled();
    expect(authService.getUserRole).toHaveBeenCalled();
  });

  it('currentLang en', () => {
    translateService.currentLang = 'en';
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.currentLanguage).toBe('english');
  });

  it('currentLang de', () => {
    translateService.currentLang = 'de';
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.currentLanguage).toBe('deutsch');
  });

  it('currentLang tu', () => {
    translateService.currentLang = 'tu';
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.currentLanguage).toBe('türkçe');
  });

  it('currentLang jp', () => {
    translateService.currentLang = 'jp';
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.currentLanguage).toBe('日本語');
  });

  it('logout', () => {
    spyOn(authService, 'logout').and.stub();
    component.logout();
    fixture.detectChanges();
    expect(authService.logout).toHaveBeenCalled();
  });

  it('routeToMyProfile', () => {
    spyOn(router, 'navigate').and.stub();
    component.routeToMyProfile();
    fixture.detectChanges();
    expect(router.navigate).toHaveBeenCalled();
  });

  it('onWindowScroll', () => {
    component.onWindowScroll();
    expect(component.showFloatingMenu).toBe(false);
    window = Object.assign(window, { pageYOffset: 2000 });
    component.onWindowScroll();
    expect(component.showFloatingMenu).toBe(true);
  });

  it('onWindowScroll', () => {
    window = Object.assign(window, { pageYOffset: 2000 });
    component.onWindowScroll();
    expect(component.showFloatingMenu).toBe(true);
    window = Object.assign(window, { pageYOffset: 0 });
    component.onWindowScroll();
    expect(component.showFloatingMenu).toBe(false);
  });

  it('setCurrentLang', () => {
    component.setCurrentLang('english');
    fixture.detectChanges();
    expect(component.currentLanguage).toBe('english');
  });
});
