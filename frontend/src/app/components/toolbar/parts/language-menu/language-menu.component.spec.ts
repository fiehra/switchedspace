import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MaterialModule } from './../../../../utils/material/material.module';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageMenuComponent } from './language-menu.component';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

describe('LanguageMenuComponent', () => {
  let component: LanguageMenuComponent;
  let fixture: ComponentFixture<LanguageMenuComponent>;
  let translateService: TranslateService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LanguageMenuComponent],
      imports: [
        MaterialModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (http: HttpClient) =>
              new TranslateHttpLoader(
                http,
                'assets/i18n/',
                '.json?build_id=build_id_replacement_placeholder',
              ),
            deps: [HttpClient],
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageMenuComponent);
    component = fixture.componentInstance;
    translateService = TestBed.inject(TranslateService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('currentLang en', () => {
    translateService.currentLang = 'en';
    spyOn(component.currentLangChanged, 'emit').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.currentLangChanged.emit).toHaveBeenCalled();
  });

  it('currentLang de', () => {
    translateService.currentLang = 'de';
    spyOn(component.currentLangChanged, 'emit').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.currentLangChanged.emit).toHaveBeenCalled();
  });

  it('currentLang tu', () => {
    translateService.currentLang = 'tu';
    spyOn(component.currentLangChanged, 'emit').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.currentLangChanged.emit).toHaveBeenCalled();
  });

  it('currentLang jp', () => {
    translateService.currentLang = 'jp';
    spyOn(component.currentLangChanged, 'emit').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.currentLangChanged.emit).toHaveBeenCalled();
  });

  it('changeLanguage en', () => {
    spyOn(translateService, 'use').and.stub();
    spyOn(component.currentLangChanged, 'emit').and.stub();
    component.changeLanguage('en');
    fixture.detectChanges();
    expect(translateService.use).toHaveBeenCalled();
    expect(component.currentLangChanged.emit).toHaveBeenCalled();
  });

  it('changeLanguage de', () => {
    spyOn(translateService, 'use').and.stub();
    spyOn(component.currentLangChanged, 'emit').and.stub();
    component.changeLanguage('de');
    fixture.detectChanges();
    expect(translateService.use).toHaveBeenCalled();
    expect(component.currentLangChanged.emit).toHaveBeenCalled();
  });

  it('changeLanguage tu', () => {
    spyOn(translateService, 'use').and.stub();
    spyOn(component.currentLangChanged, 'emit').and.stub();
    component.changeLanguage('tu');
    fixture.detectChanges();
    expect(translateService.use).toHaveBeenCalled();
    expect(component.currentLangChanged.emit).toHaveBeenCalled();
  });

  it('changeLanguage jp', () => {
    spyOn(translateService, 'use').and.stub();
    spyOn(component.currentLangChanged, 'emit').and.stub();
    component.changeLanguage('jp');
    fixture.detectChanges();
    expect(translateService.use).toHaveBeenCalled();
    expect(component.currentLangChanged.emit).toHaveBeenCalled();
  });
});
