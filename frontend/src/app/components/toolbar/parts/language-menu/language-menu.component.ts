import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'language-menu',
  templateUrl: './language-menu.component.html',
  styleUrls: ['./language-menu.component.scss'],
})
export class LanguageMenuComponent implements OnInit {
  @Output() currentLangChanged = new EventEmitter();

  constructor(private translate: TranslateService) {}

  ngOnInit(): void {
    // setting the language for the app
    if (this.translate.currentLang === 'en') {
      this.currentLangChanged.emit('english');
    } else if (this.translate.currentLang === 'de') {
      this.currentLangChanged.emit('deutsch');
    } else if (this.translate.currentLang === 'tu') {
      this.currentLangChanged.emit('türkçe');
    } else if (this.translate.currentLang === 'jp') {
      this.currentLangChanged.emit('日本語');
    }
  }

  // language swtiching
  changeLanguage(language: string) {
    if (language === 'en') {
      this.currentLangChanged.emit('english');
    } else if (language === 'de') {
      this.currentLangChanged.emit('deutsch');
    } else if (language === 'tu') {
      this.currentLangChanged.emit('türkçe');
    } else if (language === 'jp') {
      this.currentLangChanged.emit('日本語');
    }
    this.translate.use(language);
  }
}
