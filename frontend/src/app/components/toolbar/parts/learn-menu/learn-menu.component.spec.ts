import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MaterialModule } from './../../../../utils/material/material.module';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnMenuComponent } from './learn-menu.component';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

describe('LearnMenuComponent', () => {
  let component: LearnMenuComponent;
  let fixture: ComponentFixture<LearnMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LearnMenuComponent],
      imports: [
        MaterialModule,
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (http: HttpClient) =>
              new TranslateHttpLoader(
                http,
                'assets/i18n/',
                '.json?build_id=build_id_replacement_placeholder',
              ),
            deps: [HttpClient],
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LearnMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
