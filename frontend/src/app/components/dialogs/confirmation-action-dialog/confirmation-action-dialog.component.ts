import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

export interface DialogData {
  title: string;
  text: string;
  confirmAction: any;
}

@Component({
  selector: 'confirmation-action-dialog',
  templateUrl: './confirmation-action-dialog.component.html',
  styleUrls: ['./confirmation-action-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ConfirmationActionDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData, public dialogRef: MatDialogRef<ConfirmationActionDialogComponent>) { }

  ngOnInit() {
  }

  confirmAction(): void {
    this.data.confirmAction();
    this.dialogRef.close();
  }

  cancelAction(): void {
    this.dialogRef.close();
  }

}
