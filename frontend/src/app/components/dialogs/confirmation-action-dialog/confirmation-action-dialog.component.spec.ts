import { MaterialModule } from './../../../utils/material/material.module';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {
  ConfirmationActionDialogComponent,
  DialogData,
} from './confirmation-action-dialog.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

export class MatDialogRefMock {
  close() {}
}

export class MatDialogData {
  confirmAction() {}
}

describe('ConfirmationActionDialogComponent', () => {
  let component: ConfirmationActionDialogComponent;
  let fixture: ComponentFixture<ConfirmationActionDialogComponent>;
  let dialog: MatDialogRefMock;
  let data: MatDialogData;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ConfirmationActionDialogComponent],
        providers: [
          MatDialogRefMock,
          MatDialogData,
          { provide: MAT_DIALOG_DATA, useClass: MatDialogData },
          { provide: MatDialogRef, useClass: MatDialogRefMock },
        ],
        imports: [
          MaterialModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder'
                ),
              deps: [HttpClient],
            },
          }),
        ],
      })
        .overrideModule(BrowserDynamicTestingModule, {
          set: { entryComponents: [ConfirmationActionDialogComponent] },
        })
        .compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationActionDialogComponent);
    component = fixture.componentInstance;
    dialog = TestBed.inject(MatDialogRefMock);
    data = TestBed.inject(MatDialogData);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('cancelAction', () => {
    spyOn(dialog, 'close').and.stub();
    component.cancelAction();
    fixture.detectChanges();
  });

  it('confirmAction', () => {
    spyOn(dialog, 'close').and.stub();
    spyOn(data, 'confirmAction').and.stub();
    component.confirmAction();
    fixture.detectChanges();
  });
});
