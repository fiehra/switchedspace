import { UtilsModule } from './../utils/utils.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FooterComponent } from './footer/footer.component';
import { SwitchedInputComponent } from './htmlComponents/switched-input/switched-input.component';
import { SwitchedTextareaComponent } from './htmlComponents/switched-textarea/switched-textarea.component';
import { SwitchedPasswordComponent } from './htmlComponents/switched-password/switched-password.component';
import { ConfirmationActionDialogComponent } from './dialogs/confirmation-action-dialog/confirmation-action-dialog.component';
import { SwitchedSelectComponent } from './htmlComponents/switched-select/switched-select.component';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { SwitchedCardComponent } from './htmlComponents/switched-card/switched-card.component';
import { ScrollIndicatorComponent } from './scroll-indicator/scroll-indicator.component';
import { SwitchedDatepickerComponent } from './htmlComponents/switched-datepicker/switched-datepicker.component';
import { GallerySectionComponent } from './gallery-section/gallery-section.component';
import { DonateButtonComponent } from './donate-button/donate-button.component';
import { MainMenuComponent } from './toolbar/parts/main-menu/main-menu.component';
import { LanguageMenuComponent } from './toolbar/parts/language-menu/language-menu.component';
import { ThemeMenuComponent } from './toolbar/parts/theme-menu/theme-menu.component';
import { LearnMenuComponent } from './toolbar/parts/learn-menu/learn-menu.component';

@NgModule({
  declarations: [
    ToolbarComponent,
    FooterComponent,
    SwitchedInputComponent,
    SwitchedTextareaComponent,
    SwitchedPasswordComponent,
    ConfirmationActionDialogComponent,
    SwitchedSelectComponent,
    ScrollTopComponent,
    SwitchedCardComponent,
    ScrollIndicatorComponent,
    SwitchedDatepickerComponent,
    GallerySectionComponent,
    DonateButtonComponent,
    MainMenuComponent,
    LanguageMenuComponent,
    ThemeMenuComponent,
    LearnMenuComponent,
  ],
  imports: [
    UtilsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, 'assets/i18n/', '.json'),
        deps: [HttpClient],
      },
    }),
  ],
  exports: [
    ToolbarComponent,
    FooterComponent,
    SwitchedInputComponent,
    SwitchedTextareaComponent,
    SwitchedSelectComponent,
    SwitchedPasswordComponent,
    ConfirmationActionDialogComponent,
    SwitchedSelectComponent,
    ScrollTopComponent,
    SwitchedCardComponent,
    ScrollIndicatorComponent,
    SwitchedDatepickerComponent,
    GallerySectionComponent,
    DonateButtonComponent,
    LanguageMenuComponent,
    MainMenuComponent,
    ThemeMenuComponent,
  ],
})
export class ComponentsModule {}
