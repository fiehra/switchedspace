import { ChangelogPageComponent } from './pages/changelog-page/changelog-page.component';
import { VerificationPageComponent } from './pages/verification-page/verification-page.component';
import { CreateChallengePageComponent } from './pages/challenge/create-challenge-page/create-challenge-page.component';
import { SafetyProtectionPageComponent } from './pages/tips-tricks/safety-protection-page/safety-protection-page.component';
import { MyProfilePageComponent } from './pages/profile/my-profile-page/my-profile-page.component';
import { ProfilePageComponent } from './pages/profile/profile-page/profile-page.component';
import { WorkshopsPageComponent } from './pages/workshops-page/workshops-page.component';
import { SpraypaintTechnologyPageComponent } from './pages/tips-tricks/spraypaint-technology-page/spraypaint-technology-page.component';
import { ChangingPressurePageComponent } from './pages/tips-tricks/changing-pressure-page/changing-pressure-page.component';
import { PreventCloggingPageComponent } from './pages/tips-tricks/prevent-clogging-page/prevent-clogging-page.component';
import { DisposeCansPageComponent } from './pages/tips-tricks/dispose-cans-page/dispose-cans-page.component';
import { ListTricksPageComponent } from './pages/tips-tricks/list-tricks-page/list-tricks-page.component';
import { ComingsoonPageComponent } from './pages/comingsoon-page/comingsoon-page.component';
import { ViewStreetartPageComponent } from './pages/graffmap/view-streetart-page/view-streetart-page.component';
import { ViewShopPageComponent } from './pages/graffmap/view-shop-page/view-shop-page.component';
import { ViewWallPageComponent } from './pages/graffmap/view-wall-page/view-wall-page.component';
import { GraffmapPageComponent } from './pages/graffmap/graffmap-page/graffmap-page.component';
import { AddMarkerPageComponent } from './pages/graffmap/add-marker-page/add-marker-page.component';
import { ViewApplicationPageComponent } from './pages/application/view-application-page/view-application-page.component';
import { ListApplicationPageComponent } from './pages/application/list-application-page/list-application-page.component';
import { CreateApplicationPageComponent } from './pages/application/create-application-page/create-application-page.component';
import { ComboPageComponent } from './pages/combo-page/combo-page.component';
import { DeveloperPageComponent } from './pages/developer-page/developer-page.component';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';
import { FaqPageComponent } from './pages/faq-page/faq-page.component';
import { GalleryPageComponent } from './pages/gallery-page/gallery-page.component';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { PrivacyPageComponent } from './pages/privacy-page/privacy-page.component';
import { ImprintPageComponent } from './pages/imprint-page/imprint-page.component';
import { LogoutPageComponent } from './pages/logout-page/logout-page.component';
import { AuthGuard } from './helper/auth.guard';
import { ViewForumPostPageComponent } from './pages/forum/view-forum-post-page/view-forum-post-page.component';
import { ViewForumListPageComponent } from './pages/forum/view-forum-list-page/view-forum-list-page.component';
import { SignupPageComponent } from './pages/signup-page/signup-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateForumPostPageComponent } from './pages/forum/create-forum-post-page/create-forum-post-page.component';
import { ChallengeOverviewPageComponent } from './pages/challenge/challenge-overview-page/challenge-overview-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'home', component: HomePageComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'signup', component: SignupPageComponent },
  { path: 'changelog', component: ChangelogPageComponent },
  { path: 'verification', component: VerificationPageComponent },
  { path: 'verified', component: LoginPageComponent },
  {
    path: 'profile/:id',
    component: ProfilePageComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'myProfile/:id',
    component: MyProfilePageComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'createForumPost',
    component: CreateForumPostPageComponent,
    canActivate: [AuthGuard],
  },
  { path: 'viewForumList', component: ViewForumListPageComponent },
  { path: 'viewForumPost/:id', component: ViewForumPostPageComponent },
  { path: 'logout', component: LogoutPageComponent },
  { path: 'imprint', component: ImprintPageComponent },
  { path: 'privacy', component: PrivacyPageComponent },
  { path: 'contact', component: ContactPageComponent },
  { path: 'gallery', component: GalleryPageComponent },
  { path: 'faq', component: FaqPageComponent },
  { path: 'developer', component: DeveloperPageComponent },
  { path: 'combo', component: ComboPageComponent },
  { path: 'graffmap', component: GraffmapPageComponent },
  {
    path: 'graffmap/add/:type',
    component: AddMarkerPageComponent,
    canActivate: [AuthGuard],
  },
  { path: 'graffmap/wall/:id', component: ViewWallPageComponent },
  { path: 'graffmap/shop/:id', component: ViewShopPageComponent },
  { path: 'graffmap/streetart/:id', component: ViewStreetartPageComponent },
  { path: 'application/create', component: CreateApplicationPageComponent },
  {
    path: 'application/list',
    component: ListApplicationPageComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'application/view/:id',
    component: ViewApplicationPageComponent,
    canActivate: [AuthGuard],
  },
  { path: 'challengeOverview', component: ChallengeOverviewPageComponent },
  { path: 'createChallenge', component: CreateChallengePageComponent },
  { path: 'comingSoon', component: ComingsoonPageComponent },
  { path: 'workshops', component: WorkshopsPageComponent },
  { path: 'listTricks', component: ListTricksPageComponent },
  { path: 'disposeCans', component: DisposeCansPageComponent },
  { path: 'preventClogging', component: PreventCloggingPageComponent },
  { path: 'changePressure', component: ChangingPressurePageComponent },
  {
    path: 'spraypaintTechnology',
    component: SpraypaintTechnologyPageComponent,
  },
  { path: 'safetyProtection', component: SafetyProtectionPageComponent },

  { path: '**', component: NotFoundPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class AppRoutingModule {}
