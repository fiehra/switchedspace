import { AuthService } from './services/auth.service';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'skillcap';

  constructor(private translateService: TranslateService, private authService: AuthService) {
    this.translateService.setDefaultLang('jp');
    this.translateService.use('jp');
  }

  ngOnInit() {
    this.authService.autoLoginUser();
  }
}
