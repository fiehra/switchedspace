import { ComboPageComponent } from './combo-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components/components.module';
import { UtilsModule } from '../../utils/utils.module';
import { NgModule } from '@angular/core';
import { OutlineComponent } from './components/outline/outline.component';
import { FillInComponent } from './components/fill-in/fill-in.component';
import { EffectsComponent } from './components/effects/effects.component';
import { BlockEffectsComponent } from './components/block-effects/block-effects.component';
import { BackgroundComponent } from './components/background/background.component';
import { SecondaryBackgroundComponent } from './components/secondary-background/secondary-background.component';
import { MobileSecondaryBackgroundComponent } from './components/mobile-secondary-background/mobile-secondary-background.component';
import { MobileBackgroundComponent } from './components/mobile-background/mobile-background.component';
import { MobileOutlineComponent } from './components/mobile-outline/mobile-outline.component';
import { MobileEffectsComponent } from './components/mobile-effects/mobile-effects.component';
import { MobileFillInComponent } from './components/mobile-fill-in/mobile-fill-in.component';
import { MobileBlocksComponent } from './components/mobile-blocks/mobile-blocks.component';
import { ColorMenuComponent } from './components/color-menu/color-menu.component';

@NgModule({
  declarations: [
    ComboPageComponent,
    OutlineComponent,
    FillInComponent,
    EffectsComponent,
    BlockEffectsComponent,
    BackgroundComponent,
    SecondaryBackgroundComponent,
    MobileSecondaryBackgroundComponent,
    MobileBackgroundComponent,
    MobileOutlineComponent,
    MobileEffectsComponent,
    MobileFillInComponent,
    MobileBlocksComponent,
    ColorMenuComponent,
],
  imports: [
    UtilsModule,
    ComponentsModule,
    TranslateModule
  ],
  exports: [
    ComboPageComponent
  ]
})

export class ComboModule {}