import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MobileBlocksComponent } from './mobile-blocks.component';

describe('MobileBlocksComponent', () => {
  let component: MobileBlocksComponent;
  let fixture: ComponentFixture<MobileBlocksComponent>;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [ MobileBlocksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileBlocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
