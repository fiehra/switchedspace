import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MobileFillInComponent } from './mobile-fill-in.component';

describe('MobileFillInComponent', () => {
  let component: MobileFillInComponent;
  let fixture: ComponentFixture<MobileFillInComponent>;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [ MobileFillInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileFillInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
