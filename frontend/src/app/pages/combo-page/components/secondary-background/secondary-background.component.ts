import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'secondary-background',
  templateUrl: './secondary-background.component.html',
  styleUrls: ['./secondary-background.component.scss']
})
export class SecondaryBackgroundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
