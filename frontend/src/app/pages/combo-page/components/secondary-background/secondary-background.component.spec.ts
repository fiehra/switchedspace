import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SecondaryBackgroundComponent } from './secondary-background.component';

describe('SecondaryBackgroundComponent', () => {
  let component: SecondaryBackgroundComponent;
  let fixture: ComponentFixture<SecondaryBackgroundComponent>;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [ SecondaryBackgroundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondaryBackgroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
