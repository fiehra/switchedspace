import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MobileEffectsComponent } from './mobile-effects.component';

describe('MobileEffectsComponent', () => {
  let component: MobileEffectsComponent;
  let fixture: ComponentFixture<MobileEffectsComponent>;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [ MobileEffectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileEffectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
