import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MobileSecondaryBackgroundComponent } from './mobile-secondary-background.component';

describe('MobileSecondaryBackgroundComponent', () => {
  let component: MobileSecondaryBackgroundComponent;
  let fixture: ComponentFixture<MobileSecondaryBackgroundComponent>;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [ MobileSecondaryBackgroundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileSecondaryBackgroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
