import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mobile-secondary-background',
  templateUrl: './mobile-secondary-background.component.html',
  styleUrls: ['./mobile-secondary-background.component.scss']
})
export class MobileSecondaryBackgroundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
