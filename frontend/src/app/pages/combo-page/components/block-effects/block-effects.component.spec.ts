import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BlockEffectsComponent } from './block-effects.component';

describe('BlockEffectsComponent', () => {
  let component: BlockEffectsComponent;
  let fixture: ComponentFixture<BlockEffectsComponent>;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [ BlockEffectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockEffectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
