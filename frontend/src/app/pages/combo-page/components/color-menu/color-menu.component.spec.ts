import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from './../../../../utils/material/material.module';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorMenuComponent } from './color-menu.component';

describe('ColorMenuComponent', () => {
  let component: ColorMenuComponent;
  let fixture: ComponentFixture<ColorMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ColorMenuComponent
      ],
      imports: [
        MaterialModule,
        RouterTestingModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
            deps: [HttpClient]
          }
        })
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('setColor outline', () => {
    component.currentLayer = 'outline';
    spyOn(component.colorSelected, 'emit').and.stub();
    fixture.detectChanges();
    component.emitColor('outline', '#000');
    fixture.detectChanges();
    expect(component.colorSelected.emit).toHaveBeenCalled();
  });

  it('setCurrentLayer', () => {
    component.setCurrentLayer('outline');
    fixture.detectChanges();
    expect(component.currentLayer).toBe('outline');
  });
});
