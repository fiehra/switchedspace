import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'color-menu',
  templateUrl: './color-menu.component.html',
  styleUrls: ['./color-menu.component.scss']
})
export class ColorMenuComponent implements OnInit {

  currentLayer: string;

  @Input() menuName: string = '';
  @Output() colorSelected = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  emitColor(layer: string, color: string) {
    const colorOptions = {
      layer: layer,
      color: color,
      currentLayer: this.currentLayer
    }
    this.colorSelected.emit(colorOptions);
  }

  setCurrentLayer(layer: string) {
    this.currentLayer = layer;
  }
}
