import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MobileOutlineComponent } from './mobile-outline.component';

describe('MobileOutlineComponent', () => {
  let component: MobileOutlineComponent;
  let fixture: ComponentFixture<MobileOutlineComponent>;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [ MobileOutlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileOutlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
