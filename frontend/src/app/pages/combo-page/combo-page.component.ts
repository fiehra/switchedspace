import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'combo-page',
  templateUrl: './combo-page.component.html',
  styleUrls: ['./combo-page.component.scss']
})
export class ComboPageComponent implements OnInit {

  currentLayer: string;
  menuName: string;

  constructor() { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

  setColor(event: any) {
    this.currentLayer = event.currentLayer;
    if (this.currentLayer === 'wall') {
      document.getElementById(event.layer).style.backgroundColor = event.color;
    } else {
      document.getElementById(event.layer).style.fill = event.color;
    }
    document.body.querySelector('.' + event.layer).setAttribute('style', 'color: ' + event.color);
  }





}
