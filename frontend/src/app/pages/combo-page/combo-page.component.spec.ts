import { ComponentsModule } from 'src/app/components/components.module';
import { SnackbarHelper } from './../../helper/snackbar.helper';
import { FormBuilder } from '@angular/forms';
import { FormHelper } from './../../helper/form.helper';
import { MaterialModule } from './../../utils/material/material.module';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ComboPageComponent } from './combo-page.component';
import { HttpClient } from '@angular/common/http';
import { PagesModule } from '../pages.module';

document.body.innerHTML =
  '<i class="material-icons colorPreview outline">stop_circle</i><i class="material-icons colorPreview wall">stop_circle</i>';
window.scrollTo = jest.fn();

describe('ComboPageComponent', () => {
  let component: ComboPageComponent;
  let fixture: ComponentFixture<ComboPageComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [FormHelper, FormBuilder, SnackbarHelper],
        imports: [
          ComponentsModule,
          PagesModule,
          MaterialModule,
          RouterTestingModule,
          HttpClientTestingModule,
          BrowserAnimationsModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ComboPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('setColor wall', () => {
    const mockEvent = {
      layer: 'wall',
      color: '#000',
      currentLayer: 'wall',
    };
    component.currentLayer = 'wall';
    fixture.detectChanges();
    component.setColor(mockEvent);
    fixture.detectChanges();
    expect(document.getElementById('wall').style.backgroundColor).toBe('rgb(0, 0, 0)');
  });

  it('setColor outline', () => {
    const mockEvent = {
      layer: 'outline',
      color: '#000',
      currentLayer: 'outline',
    };
    component.currentLayer = 'outline';
    fixture.detectChanges();
    component.setColor(mockEvent);
    fixture.detectChanges();
    expect(document.getElementById('outline').style.fill).toBe('#000');
  });
});
