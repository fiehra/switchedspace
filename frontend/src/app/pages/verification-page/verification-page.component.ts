import { TranslateService } from '@ngx-translate/core';
import { SnackbarHelper } from './../../helper/snackbar.helper';
import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { VerificationService } from 'src/app/services/verification.service';

@Component({
  selector: 'verification-page',
  templateUrl: './verification-page.component.html',
  styleUrls: ['./verification-page.component.scss'],
})
export class VerificationPageComponent implements OnInit {
  disableResend = true;

  constructor(
    private verificationService: VerificationService,
    private snackbarHelper: SnackbarHelper,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    setTimeout(() => {
      const stillNoMail = this.translateService.instant('stillNoMail');
      this.snackbarHelper.openSnackBar(stillNoMail);
      this.disableResend = false;
    }, 15000);
  }

  resendLink() {
    this.verificationService.resendVerificationLink().pipe(take(1)).subscribe();
    this.disableResend = true;
    setTimeout(() => {
      const stillNoMail = this.translateService.instant('stillNoMail');
      this.snackbarHelper.openSnackBar(stillNoMail);
      this.disableResend = false;
    }, 15000);
  }
}
