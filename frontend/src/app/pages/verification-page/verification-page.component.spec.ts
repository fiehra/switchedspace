import { of } from 'rxjs';
import { SnackbarHelper } from './../../helper/snackbar.helper';
import { FormHelper } from './../../helper/form.helper';
import { FormBuilder } from '@angular/forms';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MaterialModule } from './../../utils/material/material.module';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationPageComponent } from './verification-page.component';
import { VerificationService } from 'src/app/services/verification.service';
import { ComponentsModule } from 'src/app/components/components.module';
import { PagesModule } from '../pages.module';

window.scrollTo = jest.fn();

describe('VerificationPageComponent', () => {
  let component: VerificationPageComponent;
  let fixture: ComponentFixture<VerificationPageComponent>;
  let verificationService: VerificationService;
  let translateService: TranslateService;
  let snackbarHelper: SnackbarHelper;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [],
      providers: [FormBuilder, FormHelper, SnackbarHelper],
      imports: [
        ComponentsModule,
        PagesModule,
        MaterialModule,
        HttpClientTestingModule,
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (http: HttpClient) =>
              new TranslateHttpLoader(
                http,
                'assets/i18n/',
                '.json?build_id=build_id_replacement_placeholder',
              ),
            deps: [HttpClient],
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationPageComponent);
    component = fixture.componentInstance;
    verificationService = TestBed.inject(VerificationService);
    translateService = TestBed.inject(TranslateService);
    snackbarHelper = TestBed.inject(SnackbarHelper);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    jest.useFakeTimers();
    expect(component.disableResend).toBe(true);
    spyOn(translateService, 'instant').and.stub();
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    jest.advanceTimersByTime(20000);
    expect(translateService.instant).toHaveBeenCalled();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
    expect(component.disableResend).toBe(false);
  });

  it('resendLink', () => {
    jest.useFakeTimers();
    expect(component.disableResend).toBe(true);
    spyOn(verificationService, 'resendVerificationLink').and.returnValue(of());
    spyOn(translateService, 'instant').and.stub();
    spyOn(snackbarHelper, 'openSnackBar').and.stub();

    component.resendLink();
    fixture.detectChanges();
    jest.advanceTimersByTime(1000);

    expect(component.disableResend).toBe(true);
    expect(verificationService.resendVerificationLink).toHaveBeenCalled();
    jest.advanceTimersByTime(20000);

    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
    expect(translateService.instant).toHaveBeenCalled();
    expect(component.disableResend).toBe(false);
  });
});
