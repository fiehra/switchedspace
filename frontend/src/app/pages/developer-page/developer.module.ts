import { WebdevSectionComponent } from './components/webdev-section/webdev-section.component';
import { DeveloperPageComponent } from './developer-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components/components.module';
import { UtilsModule } from '../../utils/utils.module';
import { NgModule } from '@angular/core';
import { DeveloperHeaderComponent } from './components/developer-header/developer-header.component';
import { AboutSectionComponent } from './components/about-section/about-section.component';
import { ExperienceSectionComponent } from './components/experience-section/experience-section.component';

@NgModule({
  declarations: [
    DeveloperPageComponent,
    WebdevSectionComponent,
    DeveloperHeaderComponent,
    AboutSectionComponent,
    ExperienceSectionComponent
  ],
  imports: [
    UtilsModule,
    ComponentsModule,
    TranslateModule
  ],
  exports: [
    DeveloperPageComponent
  ]
})

export class DeveloperModule {}