import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from './../../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeveloperHeaderComponent } from './developer-header.component';
import { HttpClient } from '@angular/common/http';

describe('DeveloperHeaderComponent', () => {
  let component: DeveloperHeaderComponent;
  let fixture: ComponentFixture<DeveloperHeaderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeveloperHeaderComponent],
      providers: [],
      imports: [
        MaterialModule,
        RouterTestingModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (http: HttpClient) =>
              new TranslateHttpLoader(
                http,
                'assets/i18n/',
                '.json?build_id=build_id_replacement_placeholder',
              ),
            deps: [HttpClient],
          },
        }),
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeveloperHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('showMoreScroll', () => {
    spyOn(component.scrollClicked, 'emit').and.stub();
    component.showMoreScroll();
    fixture.detectChanges();
    expect(component.scrollClicked.emit).toHaveBeenCalled();
  });
});
