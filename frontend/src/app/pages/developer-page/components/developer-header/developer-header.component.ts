import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'developer-header',
  templateUrl: './developer-header.component.html',
  styleUrls: ['./developer-header.component.scss'],
})
export class DeveloperHeaderComponent implements OnInit {
  @Output() scrollClicked = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  showMoreScroll() {
    this.scrollClicked.emit();
  }
}
