import { CreateApplicationPageComponent } from './create-application-page/create-application-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components/components.module';
import { UtilsModule } from '../../utils/utils.module';
import { NgModule } from '@angular/core';
import { ApplicationFormComponent } from './components/application-form/application-form.component';
import { ListApplicationPageComponent } from './list-application-page/list-application-page.component';
import { ListApplicationFormComponent } from './components/list-application-form/list-application-form.component';
import { ViewApplicationPageComponent } from './view-application-page/view-application-page.component';
import { ApplicationToolbarComponent } from './components/application-toolbar/application-toolbar.component';

@NgModule({
  declarations: [
    CreateApplicationPageComponent,
    ApplicationFormComponent,
    ListApplicationPageComponent,
    ListApplicationFormComponent,
    ViewApplicationPageComponent,
    ApplicationToolbarComponent
  ],
  imports: [
    UtilsModule,
    ComponentsModule,
    TranslateModule
  ],
  exports: [
    CreateApplicationPageComponent,
    ListApplicationPageComponent
  ]
})

export class ApplicationModule {}