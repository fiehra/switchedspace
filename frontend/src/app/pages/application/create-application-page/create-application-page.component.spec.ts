import { ComponentsModule } from 'src/app/components/components.module';
import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { ApplicationService } from './../../../services/application.service';
import { FormHelper } from './../../../helper/form.helper';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormBuilder, FormsModule } from '@angular/forms';
import { MaterialModule } from './../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CreateApplicationPageComponent } from './create-application-page.component';
import { HttpClient } from '@angular/common/http';
import { PagesModule } from '../../pages.module';

window.scrollTo = jest.fn();

describe('CreateApplicationPageComponent', () => {
  let component: CreateApplicationPageComponent;
  let fixture: ComponentFixture<CreateApplicationPageComponent>;
  let applicationService: ApplicationService;
  let formHelper: FormHelper;
  let snackbarHelper: SnackbarHelper;
  let translateService: TranslateService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [FormHelper, FormBuilder, SnackbarHelper],
        imports: [
          PagesModule,
          ComponentsModule,
          ComponentsModule,
          MaterialModule,
          ReactiveFormsModule,
          FormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateApplicationPageComponent);
    component = fixture.componentInstance;
    applicationService = TestBed.inject(ApplicationService);
    translateService = TestBed.inject(TranslateService);
    formHelper = TestBed.inject(FormHelper);
    snackbarHelper = TestBed.inject(SnackbarHelper);
    fixture.detectChanges();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('submitApplication form valid', () => {
    component.applicationForm.get('applicationType').setValue('workshop');
    component.applicationForm.get('description').setValue('description');
    component.applicationForm.get('email').setValue('application@email.com');
    component.applicationForm.get('location').setValue('berlin');
    component.applicationForm.get('name').setValue('name');
    component.applicationForm.get('phonenumber').setValue('111');
    component.applicationForm.get('title').setValue('title');
    component.applicationForm.get('date').setValue(new Date());
    spyOn(applicationService, 'createApplication').and.stub();
    spyOn(formHelper, 'disableControls').and.stub();
    spyOn(translateService, 'instant').and.stub();
    component.submitApplication();
    fixture.detectChanges();
    expect(applicationService.createApplication).toHaveBeenCalled();
    expect(translateService.instant).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalled();
  });

  it('submitApplication form invalid', () => {
    spyOn(translateService, 'instant').and.stub();
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.submitApplication();
    fixture.detectChanges();
    expect(translateService.instant).toHaveBeenCalled();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });
});
