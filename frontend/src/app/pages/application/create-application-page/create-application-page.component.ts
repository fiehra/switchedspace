import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { FormHelper } from './../../../helper/form.helper';
import { TranslateService } from '@ngx-translate/core';
import { ApplicationService } from './../../../services/application.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'create-application-page',
  templateUrl: './create-application-page.component.html',
  styleUrls: ['./create-application-page.component.scss'],
})
export class CreateApplicationPageComponent implements OnInit {
  applicationForm = new FormGroup({
    applicationType: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    phonenumber: new FormControl(''),
    company: new FormControl(''),
    title: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    location: new FormControl('', [Validators.required]),
    date: new FormControl('', [Validators.required]),
    budget: new FormControl(''),
  });
  created = false;

  constructor(
    private applicationService: ApplicationService,
    private translateService: TranslateService,
    private formHelper: FormHelper,
    private snackbarHelper: SnackbarHelper
  ) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

  submitApplication() {
    if (this.applicationForm.valid) {
      const application = this.applicationForm.value;
      this.applicationService.createApplication(application);
      this.formHelper.disableControls(this.applicationForm);
      const messageApplied = this.translateService.instant('snackApplied');
      this.snackbarHelper.openSnackBar(messageApplied);
      this.created = true;
    } else {
      const messageInvalid = this.translateService.instant('snackInvalid');
      this.snackbarHelper.openSnackBar(messageInvalid);
    }
  }
}
