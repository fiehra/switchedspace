import { FormHelper } from './../../../../helper/form.helper';
import { FormGroup } from '@angular/forms';
import { SelectOptions } from './../../../../utils/selectOptions';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'application-form',
  templateUrl: './application-form.component.html',
  styleUrls: ['./application-form.component.scss']
})
export class ApplicationFormComponent implements OnInit {

  selectOptions = SelectOptions.applicationTypeSelects;
  
  @Input() applicationForm: FormGroup;

  constructor() {}

  ngOnInit(): void {}


}