import { FormHelper } from './../../../../helper/form.helper';
import { SwitchedDatepickerComponent } from './../../../../components/htmlComponents/switched-datepicker/switched-datepicker.component';
import { SwitchedSelectComponent } from './../../../../components/htmlComponents/switched-select/switched-select.component';
import { SwitchedTextareaComponent } from './../../../../components/htmlComponents/switched-textarea/switched-textarea.component';
import { SwitchedInputComponent } from './../../../../components/htmlComponents/switched-input/switched-input.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../../../../utils/material/material.module';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ApplicationFormComponent } from './application-form.component';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'host-component',
  template: '<application-form [applicationForm]="applicationForm"></application-form>'
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  applicationForm = this.formBuilder.group({
    id: ['id'],
    applicationType: ['workshop'],
    name: ['name'],
    email: ['email@email'],
    phonenumber: ['111'],
    company: ['none'],
    title: ['title'],
    description: ['crazy'],
    location: ['berlin'],
    date: [new Date()],
    budget: ['200']
  });
}

describe('ApplicationFormComponent', () => {
  let component: ApplicationFormComponent;
  let fixture: ComponentFixture < TestHostComponent > ;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
        declarations: [
          ApplicationFormComponent,
          TestHostComponent,
          SwitchedInputComponent,
          SwitchedTextareaComponent,
          SwitchedSelectComponent,
          SwitchedDatepickerComponent
        ],
        providers: [
          FormHelper,
          FormBuilder
        ],
        imports: [
          MaterialModule,
          ReactiveFormsModule,
          FormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
              deps: [HttpClient]
            }
          })

        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});