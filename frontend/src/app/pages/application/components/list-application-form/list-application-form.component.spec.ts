import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../../../../utils/material/material.module';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ListApplicationFormComponent } from './list-application-form.component';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'host-component',
  template: '<list-application-form [applicationForm]="applicationForm"></list-application-form>'
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  applicationForm = this.formBuilder.group({
    id: ['id'],
    applicationType: ['contract'],
    name: ['name'],
    email: ['switched@g.com'],
    phonenumber: ['1111'],
    company: ['switched'],
    title: ['title'],
    description: ['crazy'],
    location: ['berlin'],
    date: [new Date()],
    budget: ['200'],
  });
}

describe('ListApplicationFormComponent', () => {
  let component: ListApplicationFormComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListApplicationFormComponent,
        TestHostComponent,
      ],
      providers: [
        FormBuilder
      ],
      imports: [
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
            deps: [HttpClient]
          }
        })

      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.applicationType).toBe('contract');
  });
});
