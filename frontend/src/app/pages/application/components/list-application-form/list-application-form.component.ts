import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'list-application-form',
  templateUrl: './list-application-form.component.html',
  styleUrls: ['./list-application-form.component.scss']
})
export class ListApplicationFormComponent implements OnInit {

  @Input() applicationForm: FormGroup;
  applicationType: string;

  constructor() { }

  ngOnInit(): void {
    this.applicationType = this.applicationForm.get('applicationType').value;
  }

}
