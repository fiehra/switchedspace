import { SnackbarHelper } from './../../../../helper/snackbar.helper';
import { DialogService } from 'src/app/services/dialog.service';
import { of } from 'rxjs';
import { AuthService } from './../../../../services/auth.service';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../../../../utils/material/material.module';
import { FormHelper } from './../../../../helper/form.helper';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ApplicationToolbarComponent } from './application-toolbar.component';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { By } from '@angular/platform-browser';
import { ConfirmationActionDialogComponent } from 'src/app/components/dialogs/confirmation-action-dialog/confirmation-action-dialog.component';

@Component({
  selector: 'host-component',
  template: '<application-toolbar [applicationForm]="applicationForm"></application-toolbar>',
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  applicationForm = this.formBuilder.group({
    id: ['id'],
    applicationType: ['workshop'],
    name: ['name'],
    email: ['email@email'],
    phonenumber: ['111'],
    company: ['none'],
    title: ['title'],
    description: ['crazy'],
    location: ['berlin'],
    date: [new Date()],
    budget: ['200'],
  });
}

describe('ApplicationToolbarComponent', () => {
  let component: ApplicationToolbarComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let authService: AuthService;
  let dialogService: DialogService;
  let formHelper: FormHelper;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [
          ApplicationToolbarComponent,
          TestHostComponent,
          ConfirmationActionDialogComponent,
        ],
        providers: [FormHelper, SnackbarHelper],
        imports: [
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    authService = TestBed.inject(AuthService);
    dialogService = TestBed.inject(DialogService);
    formHelper = TestBed.inject(FormHelper);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    expect(component.loggedIn).toBe(false);
    spyOn(authService, 'getAuthStatusListener').and.returnValue(of(true));
    spyOn(authService, 'getUserRole').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getAuthStatusListener).toHaveBeenCalled();
    expect(authService.getUserRole).toHaveBeenCalled();
  });

  it('emitSaved', () => {
    spyOn(component.saveClicked, 'emit').and.stub();
    spyOn(component, 'disableEditing').and.stub();
    component.emitSaved();
    fixture.detectChanges();
    expect(component.saveClicked.emit).toHaveBeenCalled();
    expect(component.disableEditing).toHaveBeenCalled();
  });

  it('openDeleteConfirmation', () => {
    spyOn(component, 'openDeleteConfirmation').and.callThrough();
    spyOn(dialogService, 'openConfirmActionDialog').and.returnValue({});
    component.loggedIn = true;
    component.userRole = 'fiehra';
    fixture.detectChanges();
    component.openDeleteConfirmation();
    fixture.detectChanges();
    const deleteButton = fixture.debugElement.query(By.css('.main')).nativeElement;
    expect(deleteButton).toBeTruthy();
    deleteButton.click();
    fixture.detectChanges();
    expect(component.openDeleteConfirmation).toHaveBeenCalled();
    expect(dialogService.openConfirmActionDialog).toHaveBeenCalled();
  });

  it('disableEditing', () => {
    component.editmode = true;
    expect(component.editmode).toBe(true);
    spyOn(formHelper, 'disableControls').and.stub();
    component.disableEditing();
    fixture.detectChanges();
    expect(formHelper.disableControls).toHaveBeenCalled();
    expect(component.editmode).toBe(false);
  });

  it('enableEditing', () => {
    component.editmode = false;
    expect(component.editmode).toBe(false);
    spyOn(formHelper, 'enableControls').and.stub();
    component.enableEditing();
    fixture.detectChanges();
    expect(formHelper.enableControls).toHaveBeenCalled();
    expect(component.editmode).toBe(true);
  });
});
