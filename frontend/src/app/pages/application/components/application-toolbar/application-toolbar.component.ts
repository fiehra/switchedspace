import { TranslateService } from '@ngx-translate/core';
import { FormHelper } from 'src/app/helper/form.helper';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from './../../../../services/auth.service';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  OnDestroy,
} from '@angular/core';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'application-toolbar',
  templateUrl: './application-toolbar.component.html',
  styleUrls: ['./application-toolbar.component.scss'],
})
export class ApplicationToolbarComponent implements OnInit, OnDestroy {
  private authStatusSub: Subscription;
  loggedIn = false;
  userRole: string;
  editmode = true;

  @Input() applicationForm: FormGroup;
  @Output() saveClicked = new EventEmitter();
  @Output() deleteClicked = new EventEmitter();

  constructor(
    private authService: AuthService,
    private formHelper: FormHelper,
    private dialogService: DialogService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.loggedIn = this.authService.getLoggedIn();
    this.userRole = this.authService.getUserRole();
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe((authStatus) => {
        this.loggedIn = authStatus;
      });
  }

  emitSaved() {
    const application = this.applicationForm.value;
    this.saveClicked.emit(application);
    this.disableEditing();
  }

  openDeleteConfirmation() {
    const title = this.translateService.instant(
      'deleteApplicationConfirmationTitle'
    );
    const text = this.translateService.instant(
      'deleteApplicationConfirmationText'
    );
    this.dialogService.openConfirmActionDialog(title, text, () =>
      this.deleteClicked.emit()
    );
  }

  disableEditing() {
    this.editmode = false;
    this.formHelper.disableControls(this.applicationForm);
  }

  enableEditing() {
    this.editmode = true;
    this.formHelper.enableControls(this.applicationForm);
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }
}
