import { AuthService } from './../../../services/auth.service';
import { FormHelper } from 'src/app/helper/form.helper';
import { ApplicationService } from './../../../services/application.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'list-application-page',
  templateUrl: './list-application-page.component.html',
  styleUrls: ['./list-application-page.component.scss'],
})
export class ListApplicationPageComponent implements OnInit {
  userRole: string;
  fiehra = false;
  totalApplications = 0;
  applications: any[] = [];
  constructor(
    private applicationService: ApplicationService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.userRole = this.authService.getUserRole();
    if (this.userRole === 'fiehra') {
      this.fiehra = true;
    }
    this.loadApplications();
    window.scrollTo(0, 0);
  }

  loadApplications() {
    this.applicationService.getApplications().subscribe((response) => {
      for (const application of response.applications) {
        const appForm = this.applicationService.mapApplicationToForm(
          application
        );
        this.applications.push(appForm);
      }
      this.totalApplications = response.maxApplications;
    });
  }
}
