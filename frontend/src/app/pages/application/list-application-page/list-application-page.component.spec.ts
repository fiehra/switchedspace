import { ComponentsModule } from 'src/app/components/components.module';
import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { AuthService } from './../../../services/auth.service';
import { of } from 'rxjs';
import { ApplicationService } from './../../../services/application.service';
import { FormHelper } from './../../../helper/form.helper';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ListApplicationPageComponent } from './list-application-page.component';
import { HttpClient } from '@angular/common/http';
import { Application } from 'src/app/interfaces/application-interface';
import { PagesModule } from '../../pages.module';

window.scrollTo = jest.fn();

describe('ListApplicationPageComponent', () => {
  let component: ListApplicationPageComponent;
  let fixture: ComponentFixture<ListApplicationPageComponent>;
  let applicationService: ApplicationService;
  let authService: AuthService;

  const applicationMock: Application = {
    id: '1',
    applicationType: 'workshop',
    name: 'application',
    email: 'application@email.com',
    phonenumber: '911',
    company: 'skillcap',
    title: 'workshop',
    description: 'description',
    location: 'tokyo',
    date: new Date(),
    budget: '1000',
  };

  const applicationMock2: Application = {
    id: '2',
    applicationType: 'contract',
    name: 'contract',
    email: 'application@email.com',
    phonenumber: '911',
    company: 'skillcap',
    title: 'contract',
    description: 'description',
    location: 'tokyo',
    date: new Date(),
    budget: '1000',
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [FormHelper, SnackbarHelper],
        imports: [
          PagesModule,
          ComponentsModule,
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListApplicationPageComponent);
    component = fixture.componentInstance;
    applicationService = TestBed.inject(ApplicationService);
    authService = TestBed.inject(AuthService);
    fixture.detectChanges();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(component, 'loadApplications').and.stub();
    spyOn(authService, 'getUserRole').and.returnValue('fiehra');
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.loadApplications).toHaveBeenCalled();
    expect(authService.getUserRole).toHaveBeenCalled();
  });

  it('loadApplications', () => {
    const applications = [applicationMock, applicationMock2];
    fixture.detectChanges();
    spyOn(applicationService, 'getApplications').and.returnValue(of({ applications }));
    component.loadApplications();
    fixture.detectChanges();
    expect(applicationService.getApplications).toHaveBeenCalled();
  });
});
