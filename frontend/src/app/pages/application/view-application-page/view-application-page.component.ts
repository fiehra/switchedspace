import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { AuthService } from './../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup } from '@angular/forms';
import {
  ApplicationService,
} from './../../../services/application.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Application } from 'src/app/interfaces/application-interface';

@Component({
  selector: 'view-application-page',
  templateUrl: './view-application-page.component.html',
  styleUrls: ['./view-application-page.component.scss'],
})
export class ViewApplicationPageComponent implements OnInit {
  userRole: string;
  fiehra = false;
  application: Application;
  applicationForm: FormGroup;
  private id: string;

  constructor(
    private route: ActivatedRoute,
    private applicationService: ApplicationService,
    private translateService: TranslateService,
    private snackbarHelper: SnackbarHelper,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.userRole = this.authService.getUserRole();
    if (this.userRole === 'fiehra') {
      this.fiehra = true;
    }
    this.loadApplication();
  }

  loadApplication() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('id')) {
        this.id = paramMap.get('id');
        this.applicationService
          .getApplicationById(this.id)
          .subscribe((response) => {
            this.application = response;
            this.applicationForm = this.applicationService.mapApplicationToForm(
              this.application
            );
          });
      }
    });
  }

  deleteApplication() {
    this.applicationService.deleteApplication(this.id).subscribe(() => {
      const messageDeleted = this.translateService.instant('snackDeleted');
      this.snackbarHelper.openSnackBar(messageDeleted);
      this.router.navigate(['/application/list']);
    });
  }

  updateApplication() {
    const applicationForUpdate = this.applicationForm.value;
    this.applicationService
      .updateApplication(applicationForUpdate, this.id)
      .subscribe(() => {
        const message = this.translateService.instant('snackUpdated');
        this.snackbarHelper.openSnackBar(message);
      });
  }
}
