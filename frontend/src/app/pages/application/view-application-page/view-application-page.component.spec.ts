import { ApplicationService } from './../../../services/application.service';
import { of } from 'rxjs';
import { FormHelper } from './../../../helper/form.helper';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormGroup, FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';
import { MaterialModule } from './../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ViewApplicationPageComponent } from './view-application-page.component';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { Application } from 'src/app/interfaces/application-interface';
import { ComponentsModule } from 'src/app/components/components.module';
import { PagesModule } from '../../pages.module';

describe('ViewApplicationPageComponent', () => {
  let component: ViewApplicationPageComponent;
  let fixture: ComponentFixture<ViewApplicationPageComponent>;
  let applicationService: ApplicationService;
  let authService: AuthService;
  let snackbarHelper: SnackbarHelper;
  let router: Router;

  const applicationMock: Application = {
    id: '1',
    applicationType: 'workshop',
    name: 'application',
    email: 'application@email.com',
    phonenumber: '911',
    company: 'skillcap',
    title: 'workshop',
    description: 'description',
    location: 'tokyo',
    date: new Date(),
    budget: '1000',
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [
          SnackbarHelper,
          FormHelper,
          {
            provide: ActivatedRoute,
            useValue: { paramMap: of(convertToParamMap({ id: 'fiehra' })) },
          },
        ],
        imports: [
          ComponentsModule,
          PagesModule,
          MaterialModule,
          ReactiveFormsModule,
          FormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewApplicationPageComponent);
    component = fixture.componentInstance;
    applicationService = TestBed.inject(ApplicationService);
    authService = TestBed.inject(AuthService);
    snackbarHelper = TestBed.inject(SnackbarHelper);
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(component, 'loadApplication').and.stub();
    spyOn(authService, 'getUserRole').and.returnValue('fiehra');
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.loadApplication).toHaveBeenCalled();
    expect(authService.getUserRole).toHaveBeenCalled();
    expect(component.fiehra).toBe(true);
  });

  it('loadApplication', () => {
    fixture.detectChanges();
    spyOn(applicationService, 'getApplicationById').and.returnValue(of(applicationMock));
    component.loadApplication();
    fixture.detectChanges();
    expect(applicationService.getApplicationById).toHaveBeenCalled();
  });

  it('deleteApplication', () => {
    spyOn(applicationService, 'deleteApplication').and.returnValue(of({}));
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    spyOn(router, 'navigate').and.stub();
    component.deleteApplication();
    fixture.detectChanges();
    expect(applicationService.deleteApplication).toHaveBeenCalled();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/application/list']);
  });

  it('updateApplication', () => {
    component.loadApplication();
    fixture.detectChanges();
    component.applicationForm = new FormGroup({
      id: new FormControl('id'),
      applicationType: new FormControl('workshop'),
      description: new FormControl('description'),
      email: new FormControl('email@email.com'),
      location: new FormControl('berlin'),
      name: new FormControl('name'),
      phonenumber: new FormControl('111'),
      title: new FormControl('title'),
    });
    spyOn(applicationService, 'updateApplication').and.returnValue(of({}));
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.updateApplication();
    fixture.detectChanges();
    expect(applicationService.updateApplication).toHaveBeenCalled();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });
});
