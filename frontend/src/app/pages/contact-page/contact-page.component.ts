import { ContactRequest } from './../../interfaces/contact-interface';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { ContactService } from './../../services/contact.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.scss'],
})
export class ContactPageComponent implements OnInit {
  contactForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    foundBy: new FormControl('', [Validators.required]),
    subject: new FormControl('', [Validators.required]),
    message: new FormControl('', [Validators.required]),
  });

  contactRequest: ContactRequest;

  constructor(
    private snackbarHelper: SnackbarHelper,
    private contactService: ContactService,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    window.scrollTo(0, 0);
  }

  submitContactRequest(event: boolean) {
    if (this.contactForm.valid && !event) {
      this.contactRequest = this.contactForm.value;
      this.contactService
        .createContactRequest(this.contactRequest)
        .subscribe((response) => {
          const messageSubmitted = this.translateService.instant(
            'snackSubmittedContact'
          );
          this.snackbarHelper.openSnackBar(messageSubmitted);
        });
    } else if (!this.contactForm.valid && event) {
      const messageSubmitted = this.translateService.instant(
        'snackSubmittedContact'
      );
      this.snackbarHelper.openSnackBar(messageSubmitted);
    } else {
      const messageInvalid = this.translateService.instant('snackInvalid');
      this.snackbarHelper.openSnackBar(messageInvalid);
    }
  }
}
