import { SnackbarHelper } from './../../helper/snackbar.helper';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { FormHelper } from './../../helper/form.helper';
import { ContactService } from './../../services/contact.service';
import { MaterialModule } from './../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ContactPageComponent } from './contact-page.component';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PagesModule } from '../pages.module';
import { ComponentsModule } from 'src/app/components/components.module';

window.scrollTo = jest.fn();

describe('ContactPageComponent', () => {
  let component: ContactPageComponent;
  let fixture: ComponentFixture<ContactPageComponent>;
  let contactService: ContactService;
  let translateService: TranslateService;
  let snackbarHelper: SnackbarHelper;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [ContactService, FormHelper, SnackbarHelper],
        imports: [
          PagesModule,
          ComponentsModule,
          MaterialModule,
          ReactiveFormsModule,
          FormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactPageComponent);
    component = fixture.componentInstance;
    contactService = TestBed.inject(ContactService);
    translateService = TestBed.inject(TranslateService);
    snackbarHelper = TestBed.inject(SnackbarHelper);
    fixture.detectChanges();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('submitContactRequest validForm', () => {
    spyOn(contactService, 'createContactRequest').and.returnValue(of({}));
    spyOn(translateService, 'instant').and.stub();
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.contactForm.get('email').setValue('email@test');
    component.contactForm.get('foundBy').setValue('friends');
    component.contactForm.get('subject').setValue('subject');
    component.contactForm.get('message').setValue('message');
    fixture.detectChanges();
    component.submitContactRequest(false);
    fixture.detectChanges();
    expect(contactService.createContactRequest).toHaveBeenCalled();
    expect(translateService.instant).toHaveBeenCalledWith('snackSubmittedContact');
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });

  it('submitContactRequest invalidForm', () => {
    spyOn(translateService, 'instant').and.stub();
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.contactForm.get('email').setValue('invalidemail');
    component.contactForm.get('foundBy').setValue('friends');
    component.contactForm.get('subject').setValue('subject');
    component.contactForm.get('message').setValue('message');
    fixture.detectChanges();
    component.submitContactRequest(false);
    fixture.detectChanges();
    expect(translateService.instant).toHaveBeenCalledWith('snackInvalid');
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });

  it('submitContactRequest spam bot', () => {
    spyOn(translateService, 'instant').and.stub();
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.contactForm.get('email').setValue('email@test');
    component.contactForm.get('foundBy').setValue('friends');
    component.contactForm.get('subject').setValue('subject');
    component.contactForm.get('message').setValue('');
    fixture.detectChanges();
    component.submitContactRequest(true);
    fixture.detectChanges();
    expect(translateService.instant).toHaveBeenCalledWith('snackSubmittedContact');
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });
});
