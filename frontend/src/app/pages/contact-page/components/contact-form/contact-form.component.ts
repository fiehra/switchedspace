import { SelectOptions } from './../../../../utils/selectOptions';
import { FormHelper } from './../../../../helper/form.helper';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss'],
})
export class ContactFormComponent implements OnInit {
  created = false;
  selectOptions = SelectOptions.foundByValueSelects;
  isChecked = false;
  constructor(private formHelper: FormHelper) {}

  @Input() contactForm: FormGroup;
  @Output() submitClicked = new EventEmitter();

  ngOnInit() {}

  // emit form to parent if valid
  submitContactRequest() {
    if (this.contactForm.valid && !this.isChecked) {
      this.submitClicked.emit(false);
      this.created = true;
      this.formHelper.disableControls(this.contactForm);
    } else if (this.contactForm.valid && this.isChecked) {
      this.created = true;
      this.formHelper.disableControls(this.contactForm);
      this.submitClicked.emit(true);
    } else {
      this.submitClicked.emit();
    }
  }
}
