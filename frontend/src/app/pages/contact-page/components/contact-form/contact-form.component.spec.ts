import { SwitchedSelectComponent } from "./../../../../components/htmlComponents/switched-select/switched-select.component";
import { SwitchedInputComponent } from "./../../../../components/htmlComponents/switched-input/switched-input.component";
import { SwitchedTextareaComponent } from "./../../../../components/htmlComponents/switched-textarea/switched-textarea.component";
import { RouterTestingModule } from "@angular/router/testing";
import { MaterialModule } from "./../../../../utils/material/material.module";
import { FormHelper } from "./../../../../helper/form.helper";
import {
  ContactService,
} from "./../../../../services/contact.service";
import { ReactiveFormsModule, FormBuilder, Validators, FormsModule } from "@angular/forms";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { ContactFormComponent } from "./contact-form.component";
import { Component } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { HttpClient } from "@angular/common/http";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

@Component({
  selector: "host-component",
  template: '<contact-form [contactForm]="contactForm"></contact-form>',
})
class TestHostComponent {
  constructor(
    private contactService: ContactService,
    private formBuilder: FormBuilder
  ) {}
  contactForm = this.formBuilder.group({
    email: ["", [Validators.required, Validators.email]],
    foundBy: ["", Validators.required],
    subject: ["", Validators.required],
    message: ["", Validators.required],
  });
}
describe("ContactFormComponent", () => {
  let component: ContactFormComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let contactService: ContactService;
  let formHelper: FormHelper;
  let formBuilder: FormBuilder;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [
          ContactFormComponent,
          TestHostComponent,
          SwitchedTextareaComponent,
          SwitchedInputComponent,
          SwitchedSelectComponent,
        ],
        providers: [ContactService, FormHelper, FormBuilder],
        imports: [
          MaterialModule,
          ReactiveFormsModule,
          FormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  "assets/i18n/",
                  ".json?build_id=build_id_replacement_placeholder"
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    contactService = TestBed.inject(ContactService);
    formHelper = TestBed.inject(FormHelper);
    formBuilder = TestBed.inject(FormBuilder);
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("submitContactRequest validForm", () => {
    expect(component.created).toBe(false);
    expect(component.isChecked).toBe(false);
    component.contactForm.get("email").setValue("email@email");
    component.contactForm.get("foundBy").setValue("friends");
    component.contactForm.get("subject").setValue("subject");
    component.contactForm.get("message").setValue("message");
    fixture.detectChanges();
    spyOn(component.submitClicked, "emit").and.stub();
    spyOn(formHelper, "disableControls").and.stub();
    component.submitContactRequest();
    fixture.detectChanges();
    expect(component.created).toBe(true);
    expect(component.submitClicked.emit).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalledWith(
      component.contactForm
    );
  });

  it("submitContactRequest validForm bot", () => {
    expect(component.created).toBe(false);
    expect(component.isChecked).toBe(false);
    component.isChecked = true;
    component.contactForm.get("email").setValue("email@email");
    component.contactForm.get("foundBy").setValue("friends");
    component.contactForm.get("subject").setValue("subject");
    component.contactForm.get("message").setValue("message");
    fixture.detectChanges();
    expect(component.isChecked).toBe(true);
    spyOn(component.submitClicked, "emit").and.stub();
    spyOn(formHelper, "disableControls").and.stub();
    component.submitContactRequest();
    fixture.detectChanges();
    expect(component.created).toBe(true);
    expect(component.submitClicked.emit).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalledWith(
      component.contactForm
    );
  });

  it("submitContactRequest invalidForm", () => {
    expect(component.created).toBe(false);
    component.contactForm.get("email").setValue("invalidemail");
    component.contactForm.get("foundBy").setValue("friends");
    component.contactForm.get("subject").setValue("subject");
    component.contactForm.get("message").setValue("message");
    fixture.detectChanges();
    spyOn(component.submitClicked, "emit").and.stub();
    component.submitContactRequest();
    fixture.detectChanges();
    expect(component.created).toBe(false);
    expect(component.submitClicked.emit).toHaveBeenCalled();
  });
});
