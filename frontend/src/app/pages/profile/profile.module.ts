import { ProfilePageComponent } from './profile-page/profile-page.component';
import { ProfileFormComponent } from './components/profile-form/profile-form.component';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components/components.module';
import { UtilsModule } from '../../utils/utils.module';
import { NgModule } from '@angular/core';
import { ProfileToolbarComponent } from './components/profile-toolbar/profile-toolbar.component';
import { MyProfilePageComponent } from './my-profile-page/my-profile-page.component';
import { ProfileImageComponent } from './components/profile-image/profile-image.component';

@NgModule({
  declarations: [
    ProfileFormComponent,
    ProfilePageComponent,
    ProfileToolbarComponent,
    MyProfilePageComponent,
    ProfileImageComponent,
  ],
  imports: [
    UtilsModule,
    ComponentsModule,
    TranslateModule
  ],
  exports: [
    ProfilePageComponent,
    MyProfilePageComponent,
  ]
})

export class ProfileModule {}
