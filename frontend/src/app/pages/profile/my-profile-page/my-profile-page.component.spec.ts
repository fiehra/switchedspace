import { AuthService } from './../../../services/auth.service';
import { MyProfileDTO, User } from './../../../interfaces/user-interface';
import { SnackbarHelper } from '../../../helper/snackbar.helper';
import { ThemeService } from '../../../services/theme.service';
import { DialogService } from '../../../services/dialog.service';
import { ProfileService } from '../../../services/profile.service';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { FormHelper } from '../../../helper/form.helper';
import { MaterialModule } from '../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MyProfilePageComponent } from './my-profile-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { PagesModule } from '../../pages.module';
import { ComponentsModule } from 'src/app/components/components.module';

describe('MyProfilePageComponent', () => {
  let component: MyProfilePageComponent;
  let fixture: ComponentFixture<MyProfilePageComponent>;
  let profileService: ProfileService;
  let dialogService: DialogService;
  let formHelper: FormHelper;
  let snackbarHelper: SnackbarHelper;
  let themeService: ThemeService;
  let authService: AuthService;

  const mockUser: User = {
    id: '1',
    email: 'user@user.com',
    username: 'user',
    password: 'password',
    aboutme: 'nothing',
    tag: 'user',
    faveColor: 'green',
    currentLocation: 'berlin',
    age: '25',
    faveTheme: 'green',
    role: 'user',
    reported: false,
    imagePath: null,
    inventory: 1,
    verified: true,
  };

  const mockMyProfileDto: MyProfileDTO = {
    id: '1',
    email: 'user@user.com',
    username: 'user',
    aboutme: 'nothing',
    tag: 'user',
    faveColor: 'green',
    currentLocation: 'berlin',
    age: '25',
    faveTheme: 'green',
    role: 'user',
    reported: false,
    imagePath: null,
    inventory: 1,
    verified: true,
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [
          FormHelper,
          ThemeService,
          SnackbarHelper,
          {
            provide: ActivatedRoute,
            useValue: { paramMap: of(convertToParamMap({ id: 'fiehra' })) },
          },
        ],
        imports: [
          PagesModule,
          ComponentsModule,
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(MyProfilePageComponent);
    component = fixture.componentInstance;
    profileService = TestBed.inject(ProfileService);
    dialogService = TestBed.inject(DialogService);
    formHelper = TestBed.inject(FormHelper);
    snackbarHelper = TestBed.inject(SnackbarHelper);
    themeService = TestBed.inject(ThemeService);
    authService = TestBed.inject(AuthService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('loadMyProfile reported === false', () => {
    spyOn(profileService, 'getMyProfile').and.returnValue(of(mockMyProfileDto));
    spyOn(formHelper, 'disableControls').and.stub();
    component.loadMyProfile();
    fixture.detectChanges();
    expect(profileService.getMyProfile).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalled();
  });

  it('loadMyProfile reported === true', () => {
    const mockReportedPlayer: MyProfileDTO = {
      id: '1',
      email: 'user@user.com',
      username: 'user',
      aboutme: 'nothing',
      tag: 'user',
      faveColor: 'green',
      currentLocation: 'berlin',
      age: '25',
      faveTheme: 'green',
      role: 'user',
      reported: true,
      imagePath: null,
      inventory: 1,
      verified: true,
    };

    spyOn(profileService, 'getMyProfile').and.returnValue(of(mockReportedPlayer));
    spyOn(formHelper, 'disableControls').and.stub();
    component.loadMyProfile();
    fixture.detectChanges();
    expect(profileService.getMyProfile).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalled();
  });

  it('openDeleteConfirmation', () => {
    spyOn(dialogService, 'openConfirmActionDialog').and.stub();
    component.openDeleteAccountConfirmation(mockUser);
    fixture.detectChanges();
    expect(dialogService.openConfirmActionDialog).toHaveBeenCalled();
  });

  it('updateProfile', () => {
    spyOn(profileService, 'updateProfile').and.returnValue(of({}));
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    spyOn(themeService, 'changeTheme').and.stub();
    component.updateProfile(mockUser);
    fixture.detectChanges();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
    expect(profileService.updateProfile).toHaveBeenCalled();
    expect(themeService.changeTheme).toHaveBeenCalled();
  });

  it('deleteAccount', () => {
    spyOn(profileService, 'deleteProfile').and.returnValue(of({}));
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    spyOn(authService, 'logout').and.stub();
    component.deleteAccount(mockUser);
    fixture.detectChanges();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
    expect(profileService.deleteProfile).toHaveBeenCalled();
    expect(authService.logout).toHaveBeenCalled();
  });

  it('uploadImage', () => {
    const file: File = {
      name: 'name',
      lastModified: Date.now(),
      size: 3,
      type: 'image',
      arrayBuffer: null,
      slice: null,
      stream: null,
      text: null,
    };
    spyOn(profileService, 'uploadImage').and.returnValue(of({}));
    spyOn(authService, 'getUserId').and.returnValue(mockUser.id);
    spyOn(component, 'loadMyProfile').and.stub();
    spyOn(snackbarHelper, 'openSnackBar').and.stub();

    component.uploadImage(file);
    fixture.detectChanges();

    expect(authService.getUserId).toHaveBeenCalled();
    expect(profileService.uploadImage).toHaveBeenCalled();
    expect(component.loadMyProfile).toHaveBeenCalled();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });
});
