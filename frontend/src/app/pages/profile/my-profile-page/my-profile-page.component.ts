import { FormHelper } from 'src/app/helper/form.helper';
import { MyProfileDTO } from './../../../interfaces/user-interface';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { ThemeService } from '../../../services/theme.service';
import { DialogService } from '../../../services/dialog.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ProfileService } from '../../../services/profile.service';
import { AuthService } from '../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { User } from 'src/app/interfaces/user-interface';

@Component({
  selector: 'my-profile-page',
  templateUrl: './my-profile-page.component.html',
  styleUrls: ['./my-profile-page.component.scss'],
})
export class MyProfilePageComponent implements OnInit {
  userForm: FormGroup;
  profile: MyProfileDTO;
  profileId: string;

  constructor(
    private formHelper: FormHelper,
    private profileService: ProfileService,
    private snackbarHelper: SnackbarHelper,
    private route: ActivatedRoute,
    private translateService: TranslateService,
    private authService: AuthService,
    private dialogService: DialogService,
    private themeService: ThemeService,
  ) {}

  ngOnInit() {
    this.loadMyProfile();
  }

  loadMyProfile() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('id')) {
        this.profileId = paramMap.get('id');
        this.profileService.getMyProfile(this.profileId).subscribe((response) => {
          this.profile = response;
          this.userForm = this.profileService.mapMyProfileDtoToForm(this.profile);
          this.formHelper.disableControls(this.userForm);
        });
      }
    });
  }

  updateProfile(user: User) {
    this.profileService.updateProfile(user, user.id).subscribe((response) => {
      const message = user.username + this.translateService.instant('snackUpdated');
      this.snackbarHelper.openSnackBar(message);
      this.authService.username = user.username;
      this.themeService.changeTheme(user.faveTheme);
    });
  }

  uploadImage(event: File) {
    const userId = this.authService.getUserId();
    const imageData = new FormData();
    imageData.append('image', event);
    this.profileService.uploadImage(userId, imageData).subscribe((response) => {
      const message = this.translateService.instant('snackUploaded');
      this.snackbarHelper.openSnackBar(message);
      this.loadMyProfile();
    });
  }

  openDeleteAccountConfirmation(event: User) {
    const title = this.translateService.instant('deleteAccountTitle');
    const text = this.translateService.instant('deleteAccontText');
    this.dialogService.openConfirmActionDialog(title, text, () => this.deleteAccount(event));
  }

  deleteAccount(user: User) {
    this.profileService.deleteProfile(user.id).subscribe((res) => {
      const message = user.username + this.translateService.instant('snackDeleted');
      this.snackbarHelper.openSnackBar(message);
      this.authService.logout();
    });
  }
}
