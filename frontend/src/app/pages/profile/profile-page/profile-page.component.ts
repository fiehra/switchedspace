import { FormHelper } from 'src/app/helper/form.helper';
import {
  ProfileDTO,
  ReportPlayerDto,
} from './../../../interfaces/user-interface';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ProfileService } from '../../../services/profile.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
})
export class ProfilePageComponent implements OnInit {
  userForm: FormGroup;
  user: ProfileDTO;
  profileId: string;

  constructor(
    private formHelper: FormHelper,
    private profileService: ProfileService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.loadProfile();
  }

  loadProfile() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('id')) {
        this.profileId = paramMap.get('id');
        this.profileService
          .getProfileById(this.profileId)
          .subscribe((response) => {
            this.user = response;
            this.userForm = this.profileService.mapProfileDtoToForm(this.user);
            this.formHelper.disableControls(this.userForm);
          });
      }
    });
  }

  reportUserProfile(userId: string) {
    const report: ReportPlayerDto = {
      id: userId,
      reported: true,
    };
    this.profileService.reportUser(userId, report).subscribe((response) => {});
  }
}
