import { ProfileImageComponent } from './../components/profile-image/profile-image.component';
import { ProfileDTO } from '../../../interfaces/user-interface';
import { SnackbarHelper } from '../../../helper/snackbar.helper';
import { ProfileToolbarComponent } from './../components/profile-toolbar/profile-toolbar.component';
import { ScrollTopComponent } from 'src/app/components/scroll-top/scroll-top.component';
import { ThemeService } from '../../../services/theme.service';
import { SwitchedSelectComponent } from '../../../components/htmlComponents/switched-select/switched-select.component';
import { ProfileService } from '../../../services/profile.service';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { FormHelper } from '../../../helper/form.helper';
import { SwitchedTextareaComponent } from '../../../components/htmlComponents/switched-textarea/switched-textarea.component';
import { SwitchedInputComponent } from '../../../components/htmlComponents/switched-input/switched-input.component';
import { MaterialModule } from '../../../utils/material/material.module';
import { FooterComponent } from '../../../components/footer/footer.component';
import { ToolbarComponent } from '../../../components/toolbar/toolbar.component';
import { ProfileFormComponent } from './../components/profile-form/profile-form.component';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProfilePageComponent } from './profile-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { PagesModule } from '../../pages.module';
import { ComponentsModule } from 'src/app/components/components.module';

fdescribe('ProfilePageComponent', () => {
  let component: ProfilePageComponent;
  let fixture: ComponentFixture<ProfilePageComponent>;
  let profileService: ProfileService;
  let formHelper: FormHelper;

  const mockProfileDto: ProfileDTO = {
    id: '1',
    username: 'user',
    aboutme: 'nothing',
    tag: 'user',
    faveColor: 'green',
    currentLocation: 'berlin',
    age: '25',
    faveTheme: 'green',
    reported: false,
    imagePath: null,
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [
          FormHelper,
          ThemeService,
          SnackbarHelper,
          {
            provide: ActivatedRoute,
            useValue: { paramMap: of(convertToParamMap({ id: 'fiehra' })) },
          },
        ],
        imports: [
          PagesModule,
          ComponentsModule,
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilePageComponent);
    component = fixture.componentInstance;
    profileService = TestBed.inject(ProfileService);
    formHelper = TestBed.inject(FormHelper);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('loadProfile', () => {
    spyOn(profileService, 'getProfileById').and.returnValue(of(mockProfileDto));
    spyOn(formHelper, 'disableControls').and.stub();
    component.loadProfile();
    fixture.detectChanges();
    expect(profileService.getProfileById).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalled();
  });

  it('reportProfile', () => {
    spyOn(profileService, 'reportUser').and.returnValue(of({}));
    component.reportUserProfile(mockProfileDto.id);
    fixture.detectChanges();
    expect(profileService.reportUser).toHaveBeenCalled();
  });
});
