import { ProfileImageComponent } from './../profile-image/profile-image.component';
import { SnackbarHelper } from '../../../../helper/snackbar.helper';
import { SwitchedSelectComponent } from '../../../../components/htmlComponents/switched-select/switched-select.component';
import { AuthService } from '../../../../services/auth.service';
import { of } from 'rxjs';
import { SwitchedTextareaComponent } from '../../../../components/htmlComponents/switched-textarea/switched-textarea.component';
import { SwitchedInputComponent } from '../../../../components/htmlComponents/switched-input/switched-input.component';
import { FormHelper } from '../../../../helper/form.helper';
import { MaterialModule } from '../../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProfileFormComponent } from './profile-form.component';
import { ReactiveFormsModule, FormBuilder, Validators } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Component } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

@Component({
  selector: 'host-component',
  template: '<profile-form [userForm]="userForm"></profile-form>',
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  userForm = this.formBuilder.group({
    id: ['id'],
    email: ['', [Validators.required, Validators.email]],
    username: ['', Validators.required],
    aboutme: [''],
    tag: [''],
    faveColor: [''],
    currentLocation: [''],
    age: [''],
    faveTheme: [''],
    reported: [false],
    role: ['user'],
    imagePath: null,
    inventory: 1,
  });
}
describe('ProfileFormComponent', () => {
  let component: ProfileFormComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let authService: AuthService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [
          ProfileFormComponent,
          TestHostComponent,
          SwitchedInputComponent,
          SwitchedTextareaComponent,
          SwitchedSelectComponent,
          ProfileImageComponent,
        ],
        providers: [FormHelper, SnackbarHelper],
        imports: [
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    authService = TestBed.inject(AuthService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test ng On init reported === false', () => {
    expect(component.loggedIn).toBe(false);
    spyOn(authService, 'getLoggedIn').and.stub();
    spyOn(authService, 'getUserId').and.stub();
    spyOn(authService, 'getAuthStatusListener').and.returnValue(of(true));
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getLoggedIn).toHaveBeenCalled();
    expect(authService.getUserId).toHaveBeenCalled();
    expect(authService.getAuthStatusListener).toHaveBeenCalled();
    expect(component.loggedIn).toBe(true);
  });

  it('emitImagePicked', () => {
    const mockFile: File = {
      name: 'name',
      lastModified: Date.now(),
      size: 3,
      type: 'image',
      arrayBuffer: null,
      slice: null,
      stream: null,
      text: null,
    };
    const mockEvt = { target: { files: mockFile } };
    spyOn(component.imagePicked, 'emit').and.stub();
    component.emitImagePicked(mockEvt as any);
    fixture.detectChanges();
    expect(component.imagePicked.emit).toHaveBeenCalled();
  });
});
