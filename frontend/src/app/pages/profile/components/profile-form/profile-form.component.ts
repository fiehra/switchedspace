import { SelectOptions } from '../../../../utils/selectOptions';
import { AuthService } from '../../../../services/auth.service';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss'],
})
export class ProfileFormComponent implements OnInit, OnDestroy {
  private authStatusSub: Subscription;
  loggedIn = false;
  userId: string;
  file: File;

  selectOptions = SelectOptions.faveThemeSelects;

  @Input() userForm: FormGroup;
  @Output() saveClicked = new EventEmitter();
  @Output() deleteClicked = new EventEmitter();
  @Output() imagePicked = new EventEmitter();

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.loggedIn = this.authService.getLoggedIn();
    this.userId = this.authService.getUserId();
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe((authStatus) => {
        this.loggedIn = authStatus;
      });
  }

  emitImagePicked(evt: any) {
    this.imagePicked.emit(evt);
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }
}
