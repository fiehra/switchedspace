import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'profile-image',
  templateUrl: './profile-image.component.html',
  styleUrls: ['./profile-image.component.scss'],
})
export class ProfileImageComponent implements OnInit {
  @Input() userForm: FormGroup;
  @Input() userId: string;

  @Output() imagePicked = new EventEmitter();

  imagePreview: string;
  file: File;
  constructor() {}

  ngOnInit(): void {}

  getLoadedCallback(reader: FileReader, file: File) {
    return () => {
      this.userForm.patchValue({ image: file });
      this.userForm.updateValueAndValidity();
      this.imagePreview = reader.result as string;
    };
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = this.getLoadedCallback(reader, file);
    this.file = file;
  }

  emitUploadClicked() {
    this.imagePreview = null;
    this.imagePicked.emit(this.file);
  }
}
