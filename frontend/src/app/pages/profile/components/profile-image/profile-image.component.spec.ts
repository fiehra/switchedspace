import { AuthService } from './../../../../services/auth.service';
import { ProfileFormComponent } from './../profile-form/profile-form.component';
import { SnackbarHelper } from './../../../../helper/snackbar.helper';
import { FormHelper } from './../../../../helper/form.helper';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../../../../utils/material/material.module';
import { Component } from '@angular/core';
import { FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileImageComponent } from './profile-image.component';

@Component({
  selector: 'host-component',
  template: '<profile-image [userForm]="userForm" [userId]="userId"></profile-image>',
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  userId = 'id';

  userForm = this.formBuilder.group({
    id: ['id'],
    email: ['', [Validators.required, Validators.email]],
    username: ['', Validators.required],
    aboutme: [''],
    tag: [''],
    faveColor: [''],
    currentLocation: [''],
    age: [''],
    faveTheme: [''],
    reported: [false],
    role: ['user'],
    imagePath: null,
    inventory: 1,
  });
}
describe('ProfileImageComponent', () => {
  let component: ProfileImageComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProfileImageComponent, TestHostComponent],
      providers: [FormHelper, SnackbarHelper],
      imports: [
        MaterialModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (http: HttpClient) =>
              new TranslateHttpLoader(
                http,
                'assets/i18n/',
                '.json?build_id=build_id_replacement_placeholder',
              ),
            deps: [HttpClient],
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getLoadedCallBack', () => {
    const result = 'result';
    const mockReader = { result } as FileReader;
    const file: File = {
      name: 'name',
      lastModified: Date.now(),
      size: 3,
      type: 'image',
      arrayBuffer: null,
      slice: null,
      stream: null,
      text: null,
    };
    spyOn(component.userForm, 'patchValue').and.stub();
    const callback: () => void = component.getLoadedCallback(mockReader, file);
    callback();
    expect(component.userForm.patchValue).toHaveBeenCalled();
  });

  it('onImagePicked', () => {
    const mockFile: File = {
      name: 'name',
      lastModified: Date.now(),
      size: 3,
      type: 'image',
      arrayBuffer: null,
      slice: null,
      stream: null,
      text: null,
    };
    const mockEvt = { target: { files: mockFile as Blob } };
    spyOn(FileReader.prototype, 'readAsDataURL');
    spyOn(component, 'getLoadedCallback').and.stub();
    component.onImagePicked(mockEvt as any);
    expect(FileReader.prototype.readAsDataURL).toHaveBeenCalled();
    expect(component.getLoadedCallback).toHaveBeenCalled();
  });

  it('call imagePicked when input event changed', () => {
    spyOn(component, 'imagePicked').and.callThrough();
    const fakeChangeEvent = new Event('change');
    const target = fixture.debugElement.query(By.css('input')).nativeElement;
    target.dispatchEvent(fakeChangeEvent);

    fixture.whenStable().then(() => {
      expect(component.imagePicked).toHaveBeenCalled();
    });
  });

  it('emitUploadClicked', () => {
    spyOn(component.imagePicked, 'emit').and.stub();
    component.emitUploadClicked();
    fixture.detectChanges();
    expect(component.imagePicked.emit).toHaveBeenCalled();
    expect(component.imagePreview).toBe(null);
  });
});
