import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../../../../utils/material/material.module';
import { FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { FormHelper } from 'src/app/helper/form.helper';
import { AuthService } from '../../../../services/auth.service';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProfileToolbarComponent } from './profile-toolbar.component';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'host-component',
  template: '<profile-toolbar [userForm]="userForm"></profile-toolbar>',
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  userForm = this.formBuilder.group({
    id: ['id'],
    email: ['', [Validators.required, Validators.email]],
    username: ['', Validators.required],
    aboutme: [''],
    tag: [''],
    faveColor: [''],
    currentLocation: [''],
    age: [''],
    faveTheme: [''],
    reported: [false],
    role: ['user'],
    imagePath: null,
  });
}
describe('ProfileToolbarComponent', () => {
  let component: ProfileToolbarComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let formHelper: FormHelper;
  let snackbarHelper: SnackbarHelper;
  let authService: AuthService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ProfileToolbarComponent, TestHostComponent],
        providers: [AuthService, FormHelper, SnackbarHelper],
        imports: [
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    formHelper = TestBed.inject(FormHelper);
    snackbarHelper = TestBed.inject(SnackbarHelper);
    authService = TestBed.inject(AuthService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit userId === userform.userId', () => {
    expect(component.loggedIn).toBe(false);
    spyOn(authService, 'getUserId').and.returnValue('id');
    spyOn(authService, 'getAuthStatusListener').and.returnValue(of(true));
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getAuthStatusListener).toHaveBeenCalled();
    expect(authService.getUserId).toHaveBeenCalled();
    expect(component.loggedIn).toBe(true);
  });

  it('ngOnInit userid !== userform.userId', () => {
    expect(component.loggedIn).toBe(false);
    spyOn(authService, 'getUserId').and.returnValue('wrongId');
    spyOn(authService, 'getAuthStatusListener').and.returnValue(of(true));
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getAuthStatusListener).toHaveBeenCalled();
    expect(authService.getUserId).toHaveBeenCalled();
    expect(component.loggedIn).toBe(true);
  });

  it('ngOnInit user is reported', () => {
    expect(component.loggedIn).toBe(false);
    spyOn(authService, 'getUserId').and.returnValue('id');
    spyOn(authService, 'getAuthStatusListener').and.returnValue(of(true));
    component.userForm.get('reported').setValue(true);
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getAuthStatusListener).toHaveBeenCalled();
    expect(authService.getUserId).toHaveBeenCalled();
    expect(component.loggedIn).toBe(true);
  });

  it('emitSaved userform valid', () => {
    component.ngOnInit();
    fixture.detectChanges();
    component.userForm.get('email').setValue('email@email.com');
    component.userForm.get('username').setValue('username');
    component.ngOnInit();
    fixture.detectChanges();
    spyOn(component.saveClicked, 'emit').and.stub();
    spyOn(component, 'disableEditing').and.stub();
    component.emitSaved();
    fixture.detectChanges();
    expect(component.saveClicked.emit).toHaveBeenCalled();
    expect(component.disableEditing).toHaveBeenCalled();
  });

  it('emitSaved userform invalid', () => {
    component.ngOnInit();
    fixture.detectChanges();
    component.userForm.get('email').setValue('');
    component.userForm.get('username').setValue('');
    fixture.detectChanges();
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.emitSaved();
    fixture.detectChanges();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });

  it('enableEditing', () => {
    component.editmode = false;
    fixture.detectChanges();
    expect(component.editmode).toBe(false);
    spyOn(formHelper, 'enableControls').and.stub();
    spyOn(formHelper, 'disableControlsByName').and.stub();
    component.enableEditing();
    fixture.detectChanges();
    expect(formHelper.enableControls).toHaveBeenCalled();
    expect(formHelper.disableControlsByName).toHaveBeenCalled();
    expect(component.editmode).toBe(true);
  });

  it('disableEditing', () => {
    component.editmode = true;
    fixture.detectChanges();
    expect(component.editmode).toBe(true);
    spyOn(formHelper, 'disableControls').and.stub();
    component.disableEditing();
    fixture.detectChanges();
    expect(formHelper.disableControls).toHaveBeenCalled();
    expect(component.editmode).toBe(false);
  });

  it('emitDeleteAccount', () => {
    spyOn(component.deleteClicked, 'emit').and.stub();
    component.emitDeleteAccount();
    expect(component.deleteClicked.emit).toHaveBeenCalled();
  });

  it('logout', () => {
    spyOn(authService, 'logout').and.stub();
    component.logout();
    fixture.detectChanges();
    expect(authService.logout).toHaveBeenCalled();
  });

  // it('reportPlayer reported = true', () => {
  //   component.userForm.get('reported').setValue(true);
  //   expect(component.userForm.get('reported').value).toBe(true);
  //   spyOn(snackbarHelper, 'openSnackBar').and.stub();
  //   component.reportPlayer();
  //   fixture.detectChanges();
  //   expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  //   expect(component.userForm.get('reported').value).toBe(true);
  // });

  it('reportPlayer reported = false', () => {
    spyOn(component.reportClicked, 'emit').and.stub();
    component.userForm.get('reported').setValue(false);
    expect(component.userForm.get('reported').value).toBe(false);
    component.reportPlayer();
    fixture.detectChanges();
    expect(component.userForm.get('reported').value).toBe(true);
    expect(component.reportClicked.emit).toHaveBeenCalled();
  });
});
