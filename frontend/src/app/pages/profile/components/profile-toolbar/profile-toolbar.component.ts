import { SnackbarHelper } from '../../../../helper/snackbar.helper';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../../../services/auth.service';
import { FormHelper } from 'src/app/helper/form.helper';
import { SelectOptions } from '../../../../utils/selectOptions';
import { Subscription } from 'rxjs';
import { FormGroup } from '@angular/forms';
import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  Input,
  OnDestroy,
} from '@angular/core';

@Component({
  selector: 'profile-toolbar',
  templateUrl: './profile-toolbar.component.html',
  styleUrls: ['./profile-toolbar.component.scss'],
})
export class ProfileToolbarComponent implements OnInit, OnDestroy {
  private authStatusSub: Subscription;
  loggedIn = false;
  userId: string;
  editmode = false;

  selectOptions = SelectOptions.faveThemeSelects;

  @Input() userForm: FormGroup;
  @Output() saveClicked = new EventEmitter();
  @Output() deleteClicked = new EventEmitter();
  @Output() reportClicked = new EventEmitter();

  constructor(
    private formHelper: FormHelper,
    private snackbarHelper: SnackbarHelper,
    private authService: AuthService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.loggedIn = this.authService.getLoggedIn();
    this.userId = this.authService.getUserId();
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe((authStatus) => {
        this.loggedIn = authStatus;
      });
  }

  emitSaved() {
    if (
      this.userForm.get('username') &&
      this.userForm.get('username').value !== ''
    ) {
      const user = this.userForm.value;
      this.saveClicked.emit(user);
      this.disableEditing();
    } else {
      const messageInvalid = this.translateService.instant('snackInvalid');
      this.snackbarHelper.openSnackBar(messageInvalid);
    }
  }

  disableEditing() {
    this.editmode = false;
    this.formHelper.disableControls(this.userForm);
  }

  emitDeleteAccount() {
    const user = this.userForm.value;
    this.deleteClicked.emit(user);
  }

  enableEditing() {
    this.editmode = true;
    this.formHelper.enableControls(this.userForm);
    this.formHelper.disableControlsByName(this.userForm, ['email']);
  }

  reportPlayer() {
    const userId = this.userForm.get('id').value;
    this.userForm.get('reported').setValue(true);
    this.reportClicked.emit(userId);
    const message = this.translateService.instant('reported');
    this.snackbarHelper.openSnackBar(message);
  }

  logout() {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }
}
