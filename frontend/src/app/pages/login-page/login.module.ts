import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components/components.module';
import { UtilsModule } from '../../utils/utils.module';
import { LoginPageComponent } from './login-page.component';
import { NgModule } from '@angular/core';
import { LoginformComponent } from './components/loginform/loginform.component';

@NgModule({
  declarations: [
    LoginPageComponent,
    LoginformComponent
  ],
  imports: [
    UtilsModule,
    ComponentsModule,
    TranslateModule
  ],
  exports: [
    LoginPageComponent
  ]
})

export class LoginModule {}