import { SnackbarHelper } from './../../helper/snackbar.helper';
import { AuthService } from './../../services/auth.service';
import { FormHelper } from './../../helper/form.helper';
import { MaterialModule } from './../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LoginPageComponent } from './login-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { of } from 'rxjs';
import { ComponentsModule } from 'src/app/components/components.module';
import { PagesModule } from '../pages.module';

window.scrollTo = jest.fn();

describe('LoginPageComponent', () => {
  let component: LoginPageComponent;
  let fixture: ComponentFixture<LoginPageComponent>;
  let authService: AuthService;
  let snackbarHelper: SnackbarHelper;
  let translateService: TranslateService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [FormHelper, SnackbarHelper],
        imports: [
          ComponentsModule,
          PagesModule,
          MaterialModule,
          ReactiveFormsModule,
          RouterTestingModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPageComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
    snackbarHelper = TestBed.inject(SnackbarHelper);
    translateService = TestBed.inject(TranslateService);
    fixture.detectChanges();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(authService, 'getAuthStatusListener').and.returnValue(of(true));
    spyOn(window, 'scrollTo').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getAuthStatusListener).toHaveBeenCalled();
    expect(window.scrollTo).toHaveBeenCalledWith(0, 0);
  });

  it('login with valid form', () => {
    component.loginForm.get('email').setValue('email@email');
    component.loginForm.get('password').setValue('password');
    fixture.detectChanges();
    spyOn(authService, 'login').and.callThrough();
    component.login();
    fixture.detectChanges();
    expect(authService.login).toHaveBeenCalled();
  });

  it('login with invalid form', () => {
    component.loginForm.get('email').setValue('email');
    component.loginForm.get('password').setValue('password');
    fixture.detectChanges();
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    spyOn(translateService, 'instant').and.stub();
    component.login();
    fixture.detectChanges();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
    expect(translateService.instant).toHaveBeenCalledWith('snackInvalid');
  });
});
