import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.scss'],
})
export class LoginformComponent implements OnInit {
  @Input() loginForm: FormGroup;
  @Output() loginClicked = new EventEmitter();
  verifiedLink: boolean = false;

  constructor(private router: Router) {}

  ngOnInit() {
    if (this.router.url.includes('verified')) {
      this.verifiedLink = true;
    }
  }

  loggingIn() {
    this.loginClicked.emit(this.loginForm);
  }
}
