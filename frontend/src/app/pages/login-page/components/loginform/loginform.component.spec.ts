import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { FormHelper } from '../../../../helper/form.helper';
import { SwitchedInputComponent } from '../../../../components/htmlComponents/switched-input/switched-input.component';
import { SwitchedPasswordComponent } from '../../../../components/htmlComponents/switched-password/switched-password.component';
import { MaterialModule } from '../../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LoginformComponent } from './loginform.component';
import { ReactiveFormsModule, FormBuilder, Validators } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Component } from '@angular/core';

@Component({
  selector: 'host-component',
  template: '<loginform [loginForm]="loginform"></loginform>',
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  loginform = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required],
  });
}
describe('LoginformComponent', () => {
  let component: LoginformComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let router: Router;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [
          LoginformComponent,
          TestHostComponent,
          SwitchedPasswordComponent,
          SwitchedInputComponent,
        ],
        providers: [
          FormHelper,
          {
            provide: Router,
            useValue: { url: 'localhost:4200/verified' },
          },
        ],
        imports: [
          RouterTestingModule,
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    expect(component.verifiedLink).toBe(true);
  });

  it('loggingIn', () => {
    spyOn(component.loginClicked, 'emit').and.stub();
    component.loggingIn();
    fixture.detectChanges();
    expect(component.loginClicked.emit).toHaveBeenCalled();
  });
});
