import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { Subscription } from 'rxjs';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { LoginUserDTO } from 'src/app/interfaces/user-interface';

@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit, OnDestroy {
  private authListenerSubs: Subscription;

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  });

  constructor(
    private authService: AuthService,
    private snackbarHelper: SnackbarHelper,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    this.authListenerSubs = this.authService
      .getAuthStatusListener()
      .subscribe();
    window.scrollTo(0, 0);
  }

  login() {
    if (this.loginForm.valid) {
      const loginData: LoginUserDTO = {
        email: this.loginForm.get('email').value,
        password: this.loginForm.get('password').value,
      };
      this.authService.login(loginData);
    } else {
      const messageInvalid = this.translateService.instant('snackInvalid');
      this.snackbarHelper.openSnackBar(messageInvalid);
      this.loginForm.get('email').markAsTouched();
      this.loginForm.get('password').markAsTouched();
    }
  }

  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
  }
}
