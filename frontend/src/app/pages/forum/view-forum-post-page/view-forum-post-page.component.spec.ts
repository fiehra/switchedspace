import { ComponentsModule } from 'src/app/components/components.module';
import { ForumPost } from './../../../interfaces/forum-interface';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { AuthService } from './../../../services/auth.service';
import { AddCommentComponent } from './../components/add-comment/add-comment.component';
import { ListCommentComponent } from './../components/list-comment/list-comment.component';
import { PostToolbarComponent } from './../components/post-toolbar/post-toolbar.component';
import { ForumService } from './../../../services/forum.service';
import { FormHelper } from './../../../helper/form.helper';
import { SwitchedInputComponent } from './../../../components/htmlComponents/switched-input/switched-input.component';
import { SwitchedTextareaComponent } from './../../../components/htmlComponents/switched-textarea/switched-textarea.component';
import { FooterComponent } from './../../../components/footer/footer.component';
import { ToolbarComponent } from './../../../components/toolbar/toolbar.component';
import { MaterialModule } from './../../../utils/material/material.module';
import { ForumViewPostComponent } from './../components/forum-view-post/forum-view-post.component';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ViewForumPostPageComponent } from './view-forum-post-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { PagesModule } from '../../pages.module';

describe('ViewForumPostPageComponent', () => {
  let component: ViewForumPostPageComponent;
  let fixture: ComponentFixture<ViewForumPostPageComponent>;
  let forumService: ForumService;
  let authService: AuthService;
  let snackbarHelper: SnackbarHelper;
  let router: Router;
  let formHelper: FormHelper;

  let forumPost1: ForumPost = {
    id: '1',
    title: 'title1',
    writerId: 'fiehra',
    writerUsername: 'fiehra',
    content: 'content1',
    votes: 0,
    upVoters: [],
    downVoters: [],
    comments: [],
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [
          FormHelper,
          SnackbarHelper,
          {
            provide: ActivatedRoute,
            useValue: { paramMap: of(convertToParamMap({ id: 'fiehra' })) },
          },
        ],
        imports: [
          ComponentsModule,
          PagesModule,
          MaterialModule,
          ReactiveFormsModule,
          HttpClientTestingModule,
          RouterTestingModule,
          BrowserAnimationsModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewForumPostPageComponent);
    component = fixture.componentInstance;
    forumService = TestBed.inject(ForumService);
    authService = TestBed.inject(AuthService);
    snackbarHelper = TestBed.inject(SnackbarHelper);
    router = TestBed.inject(Router);
    formHelper = TestBed.inject(FormHelper);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(authService, 'getAuthStatusListener').and.returnValue(of(true));
    spyOn(authService, 'getLoggedIn').and.stub();
    spyOn(authService, 'getUserId').and.returnValue(of('fiehra'));
    spyOn(component, 'loadTopic').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getAuthStatusListener).toHaveBeenCalled();
    expect(authService.getLoggedIn).toHaveBeenCalled();
    expect(authService.getUserId).toHaveBeenCalled();
    expect(component.loadTopic).toHaveBeenCalled();
    expect(component.loggedIn).toBe(true);
  });

  it('loadTopic', () => {
    spyOn(forumService, 'getForumPostById').and.returnValue(of(forumPost1));
    spyOn(formHelper, 'disableControls').and.stub();
    component.loadTopic();
    fixture.detectChanges();
    expect(forumService.getForumPostById).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalled();
  });

  it('updateTopic', () => {
    spyOn(forumService, 'updatePost').and.returnValue(of({}));
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    forumPost1.content = 'updated';
    component.updateTopic(forumPost1);
    fixture.detectChanges();
    expect(forumService.updatePost).toHaveBeenCalled();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });

  it('updateVote', () => {
    spyOn(forumService, 'updateVotes').and.returnValue(of({}));
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    forumPost1.votes = 2;
    forumPost1.upVoters.push('nurias');
    component.updateVote(forumPost1);
    fixture.detectChanges();
    expect(forumService.updateVotes).toHaveBeenCalled();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });

  it('deleteTopic', () => {
    spyOn(forumService, 'deletePost').and.returnValue(of({}));
    spyOn(router, 'navigate').and.stub();
    component.deleteTopic();
    fixture.detectChanges();
    expect(forumService.deletePost).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/viewForumList']);
  });

  it('commenting set to false', () => {
    component.commentMode = true;
    expect(component.commentMode).toBe(true);
    component.commenting();
    fixture.detectChanges();
    expect(component.commentMode).toBe(false);
  });

  it('commenting set to true', () => {
    component.commentMode = false;
    expect(component.commentMode).toBe(false);
    component.commenting();
    fixture.detectChanges();
    expect(component.commentMode).toBe(true);
  });

  it('addComment', () => {
    component.commentMode = true;
    const commentMock = {
      id: 'id',
      writerId: 'id',
      writerUsername: 'id',
      content: 'id',
      markedAsCorrect: true,
    };
    forumPost1.comments.push(commentMock);
    expect(component.commentMode).toBe(true);
    spyOn(forumService, 'updateComments').and.returnValue(of({}));
    spyOn(component, 'loadTopic').and.stub();
    component.addComment(forumPost1);
    fixture.detectChanges();
    expect(component.commentMode).toBe(false);
    expect(forumService.updateComments).toHaveBeenCalledWith(forumPost1.id, forumPost1.comments);
    expect(component.loadTopic).toHaveBeenCalled();
  });

  it('deleteComment', () => {
    const commentMock = {
      id: 'id',
      writerId: 'id',
      writerUsername: 'id',
      content: 'id',
      markedAsCorrect: true,
    };
    forumPost1.comments.push(commentMock);
    component.forumPost = forumPost1;
    fixture.detectChanges();
    spyOn(forumService, 'updateComments').and.returnValue(of(forumPost1.comments));
    component.deleteComment(commentMock);
    fixture.detectChanges();
    expect(forumService.updateComments).toHaveBeenCalled();
    expect(component.commentMode).toBe(false);
  });
});
