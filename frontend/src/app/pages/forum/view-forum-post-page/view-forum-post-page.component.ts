import { FormHelper } from './../../../helper/form.helper';
import {
  ForumPost,
  ForumPostVoteDTO,
  Comment,
} from './../../../interfaces/forum-interface';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { Subscription } from 'rxjs';
import { AuthService } from './../../../services/auth.service';
import { ForumService } from './../../../services/forum.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'view-forum-post-page',
  templateUrl: './view-forum-post-page.component.html',
  styleUrls: ['./view-forum-post-page.component.scss'],
})
export class ViewForumPostPageComponent implements OnInit, OnDestroy {
  private authStatusSub: Subscription;
  commentMode = false;
  forumPost: ForumPost;
  forumPostForm: FormGroup;
  loggedIn = false;
  userId: string;

  private id: string;

  constructor(
    private forumService: ForumService,
    private snackbarHelper: SnackbarHelper,
    private route: ActivatedRoute,
    private translateService: TranslateService,
    private authService: AuthService,
    private router: Router,
    private formHelper: FormHelper,
  ) {}

  ngOnInit() {
    this.loggedIn = this.authService.getLoggedIn();
    this.userId = this.authService.getUserId();
    this.loadTopic();
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe((authStatus) => {
        this.loggedIn = authStatus;
      });
  }

  loadTopic() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('id')) {
        this.id = paramMap.get('id');
        this.forumService.getForumPostById(this.id).subscribe((response) => {
          this.forumPost = response;
          this.forumPostForm = this.forumService.mapForumPostToForm(
            this.forumPost,
          );
        });
      }
    });
  }

  deleteTopic() {
    this.forumService.deletePost(this.id).subscribe(() => {
      const messageDeleted = this.translateService.instant('snackPostDeleted');
      this.snackbarHelper.openSnackBar(messageDeleted);
      this.router.navigate(['/viewForumList']);
    });
  }

  updateTopic(event: ForumPost) {
    this.forumService.updatePost(event, this.id).subscribe((response) => {
      const message =
        event.title + this.translateService.instant('snackUpdated');
      this.snackbarHelper.openSnackBar(message);
    });
  }

  updateVote(event: ForumPostVoteDTO) {
    this.forumService.updateVotes(event).subscribe((response) => {
      const username = this.authService.getUsername();
      const message = username + this.translateService.instant('snackVoted');
      this.snackbarHelper.openSnackBar(message);
      this.loadTopic();
    });
  }

  commenting() {
    this.commentMode ? (this.commentMode = false) : (this.commentMode = true);
  }

  addComment(incPost: ForumPost) {
    this.commentMode = false;
    this.forumService
      .updateComments(incPost.id, incPost.comments)
      .subscribe((response) => {
        response;
        this.loadTopic();
      });
  }

  deleteComment(comment: Comment) {
    const array = this.forumPost.comments;
    for (let i = 0; i < array.length; i++) {
      if (array[i] === comment) {
        array.splice(i, 1);
        this.forumPost.comments = array;
      }
    }
    this.commentMode = false;
    this.forumService
      .updateComments(this.forumPost.id, this.forumPost.comments)
      .subscribe((response) => {
        this.loadTopic();
      });
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }
}
