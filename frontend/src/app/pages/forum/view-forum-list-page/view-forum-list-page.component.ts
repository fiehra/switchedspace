import { ForumPost } from './../../../interfaces/forum-interface';
import { Subscription } from 'rxjs';
import { AuthService } from './../../../services/auth.service';
import { ForumService } from './../../../services/forum.service';
import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'view-forum-list-page',
  templateUrl: './view-forum-list-page.component.html',
  styleUrls: ['./view-forum-list-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ViewForumListPageComponent implements OnInit, OnDestroy {
  private authStatusSub: Subscription;

  loggedIn = false;
  userId: string;

  totalPosts = 0;
  postsPerPage = 25;
  pageSizeOptions = [10, 25, 100];
  currentPage = 1;

  forumPosts: any[] = [];

  constructor(private forumService: ForumService, private authService: AuthService) {}

  ngOnInit() {
    this.loggedIn = this.authService.getLoggedIn();
    this.userId = this.authService.getUserId();
    this.authStatusSub = this.authService.getAuthStatusListener().subscribe((authStatus) => {
      this.loggedIn = authStatus;
    });
    this.loadTopics();
    window.scrollTo(0, 0);
  }

  loadTopics() {
    this.forumService
      .getForumListPosts(this.postsPerPage, this.currentPage)
      .subscribe((response) => {
        for (const forumPost of response.forumPostsList) {
          const post = this.forumService.mapForumPostListDtoToForm(forumPost);
          this.forumPosts.push(post);
        }
        this.totalPosts = response.maxPosts;
      });
  }

  onChangePage(pageData: PageEvent) {
    this.currentPage = pageData.pageIndex + 1;
    this.postsPerPage = pageData.pageSize;
    this.forumPosts = [];
    this.loadTopics();
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }
}
