import { ForumPost, ForumListDTO } from './../../../interfaces/forum-interface';
import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { ScrollIndicatorComponent } from './../../../components/scroll-indicator/scroll-indicator.component';
import { SortPipe } from './../../../utils/sort.pipe';
import { ForumService } from './../../../services/forum.service';
import { AuthService } from './../../../services/auth.service';
import { FormHelper } from './../../../helper/form.helper';
import { ForumToolbarComponent } from './../components/forum-toolbar/forum-toolbar.component';
import { FooterComponent } from './../../../components/footer/footer.component';
import { ToolbarComponent } from './../../../components/toolbar/toolbar.component';
import { ForumListPostComponent } from './../components/forum-list-post/forum-list-post.component';
import { ScrollTopComponent } from './../../../components/scroll-top/scroll-top.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ViewForumListPageComponent } from './view-forum-list-page.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { HAMMER_LOADER } from '@angular/platform-browser';
import { PagesModule } from '../../pages.module';
import { ComponentsModule } from 'src/app/components/components.module';

window.scrollTo = jest.fn();

describe('ViewForumListPageComponent', () => {
  let component: ViewForumListPageComponent;
  let fixture: ComponentFixture<ViewForumListPageComponent>;
  let authService: AuthService;
  let forumService: ForumService;

  const forumPost1Dto: ForumListDTO = {
    id: '1',
    title: 'title1',
    writerUsername: 'fiehra',
    votes: 0,
  };

  const forumPost2Dto: ForumListDTO = {
    id: '2',
    title: 'title2',
    writerUsername: 'nurias',
    votes: 0,
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [
          FormHelper,
          SnackbarHelper,
          {
            provide: HAMMER_LOADER,
            useValue: () => new Promise(() => {}),
          },
        ],
        imports: [
          PagesModule,
          ComponentsModule,
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewForumListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.inject(AuthService);
    forumService = TestBed.inject(ForumService);
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test ng On init', () => {
    expect(component.loggedIn).toBe(false);
    spyOn(authService, 'getLoggedIn').and.stub();
    spyOn(authService, 'getUserId').and.stub();
    spyOn(authService, 'getAuthStatusListener').and.returnValue(of(true));
    spyOn(component, 'loadTopics').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getLoggedIn).toHaveBeenCalled();
    expect(authService.getUserId).toHaveBeenCalled();
    expect(authService.getAuthStatusListener).toHaveBeenCalled();
    expect(component.loadTopics).toHaveBeenCalled();
    expect(component.loggedIn).toBe(true);
  });

  it('loadTopics', () => {
    const response = {
      message: 'success',
      forumPostsList: [forumPost1Dto, forumPost2Dto],
      maxPosts: 2,
    };
    fixture.detectChanges();
    spyOn(forumService, 'getForumListPosts').and.returnValue(of(response));
    component.loadTopics();
    fixture.detectChanges();
    expect(forumService.getForumListPosts).toHaveBeenCalled();
  });

  it('onChangePage', () => {
    spyOn(component, 'loadTopics').and.stub();
    const pageData = {
      pageSize: 1,
      pageIndex: 1,
      length: 1,
    };
    component.onChangePage(pageData);
    fixture.detectChanges();
    expect(component.currentPage).toBe(2);
    expect(component.loadTopics).toHaveBeenCalled();
  });
});
