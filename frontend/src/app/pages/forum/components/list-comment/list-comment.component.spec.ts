import { of } from 'rxjs';
import { SnackbarHelper } from './../../../../helper/snackbar.helper';
import { AuthService } from './../../../../services/auth.service';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './../../../../utils/material/material.module';
import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ListCommentComponent } from './list-comment.component';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'host-component',
  template: '<list-comment [comment]="commentMock"></list-comment>',
})
class TestHostComponent {
  constructor() {}

  commentMock = {
    id: 'id',
    writerId: 'id',
    writerUsername: 'id',
    content: 'id',
    markedAsCorrect: true,
  };
}
describe('ListCommentComponent', () => {
  let component: ListCommentComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let router: Router;
  let authService: AuthService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ListCommentComponent, TestHostComponent],
        providers: [SnackbarHelper],
        imports: [
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    router = TestBed.inject(Router);
    authService = TestBed.inject(AuthService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(authService, 'getLoggedIn').and.stub();
    spyOn(authService, 'getAuthStatusListener').and.returnValue(of(true));
    spyOn(authService, 'getUserId').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getLoggedIn).toHaveBeenCalled();
    expect(authService.getAuthStatusListener).toHaveBeenCalled();
    expect(authService.getUserId).toHaveBeenCalled();
  });

  it('emitDelete', () => {
    spyOn(component.deleteCommentClicked, 'emit').and.stub();
    component.emitDelete();
    fixture.detectChanges();
    expect(component.deleteCommentClicked.emit).toHaveBeenCalled();
  });

  it('viewProfile userId !== writerId', () => {
    spyOn(router, 'navigate').and.stub();
    component.comment.writerId = 'test';
    component.viewProfile();
    fixture.detectChanges();
    expect(router.navigate).toHaveBeenCalledWith(['/profile/test']);
  });

  it('viewProfile userId === writerId', () => {
    component.userId = 'fiehra';
    spyOn(router, 'navigate').and.stub();
    component.comment.writerId = 'fiehra';
    component.viewProfile();
    fixture.detectChanges();
    expect(router.navigate).toHaveBeenCalledWith(['/myProfile/fiehra']);
  });
});
