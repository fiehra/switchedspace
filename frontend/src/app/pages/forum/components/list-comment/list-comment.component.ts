import { Router } from '@angular/router';
import { AuthService } from './../../../../services/auth.service';
import { Subscription } from 'rxjs';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  OnDestroy,
} from '@angular/core';
import { Comment } from 'src/app/interfaces/forum-interface';

@Component({
  selector: 'list-comment',
  templateUrl: './list-comment.component.html',
  styleUrls: ['./list-comment.component.scss'],
})
export class ListCommentComponent implements OnInit, OnDestroy {
  private authStatusSub: Subscription;
  loggedIn = false;
  userId: string;

  @Input() comment: Comment;
  @Output() deleteCommentClicked = new EventEmitter();

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe((authStatus) => {
        this.loggedIn = authStatus;
      });
    this.loggedIn = this.authService.getLoggedIn();
    this.userId = this.authService.getUserId();
  }

  emitDelete() {
    this.deleteCommentClicked.emit(this.comment);
  }

  viewProfile() {
    if (this.comment.writerId === this.userId) {
      this.router.navigate(['/myProfile/' + this.comment.writerId]);
    } else {
      this.router.navigate(['/profile/' + this.comment.writerId]);
    }
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }
}
