import { DialogService } from './../../../../services/dialog.service';
import { AuthService } from './../../../../services/auth.service';
import { FormHelper } from './../../../../helper/form.helper';
import { SwitchedInputComponent } from './../../../../components/htmlComponents/switched-input/switched-input.component';
import { SwitchedTextareaComponent } from './../../../../components/htmlComponents/switched-textarea/switched-textarea.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from './../../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ForumListPostComponent } from './forum-list-post.component';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Component } from '@angular/core';

@Component({
  selector: 'host-component',
  template: '<forum-list-post [forumPostForm]="forumPostForm"></forum-list-post>'
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  forumPostForm = this.formBuilder.group({
    id: ['id'],
    title: [''],
    content: [''],
    writerUsername: [''],
    writerId: [''],
    votes: [0],
    upVoters: [['id1']],
    downVoters: [['id2']],
  });
}
describe('ForumListPostComponent', () => {
  let component: ForumListPostComponent;
  let fixture: ComponentFixture < TestHostComponent > ;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
        declarations: [
          ForumListPostComponent,
          TestHostComponent,
          SwitchedTextareaComponent,
          SwitchedInputComponent
        ],
        providers: [
          FormHelper
        ],
        imports: [
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
              deps: [HttpClient]
            }
          })
        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
