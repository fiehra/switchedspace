import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'forum-list-post',
  templateUrl: './forum-list-post.component.html',
  styleUrls: ['./forum-list-post.component.scss'],
})
export class ForumListPostComponent implements OnInit {
  votes = 0;

  @Input() forumPostForm: FormGroup;

  constructor() {}

  ngOnInit() {
    this.votes = this.forumPostForm.get('votes').value;
  }
}
