import { FormHelper } from './../../../../helper/form.helper';
import { ForumService } from './../../../../services/forum.service';
import { DialogService } from './../../../../services/dialog.service';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from './../../../../services/auth.service';
import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'post-toolbar',
  templateUrl: './post-toolbar.component.html',
  styleUrls: ['./post-toolbar.component.scss'],
})
export class PostToolbarComponent implements OnInit, OnDestroy {
  private authStatusSub: Subscription;
  loggedIn = false;
  userId: string;
  editmode = false;

  @Input() forumPostForm: FormGroup;
  @Output() deleteClicked = new EventEmitter();
  @Output() saveClicked = new EventEmitter();
  @Output() reportClicked = new EventEmitter();
  @Output() commentClicked = new EventEmitter();

  constructor(
    private authService: AuthService,
    private dialogService: DialogService,
    private translateService: TranslateService,
    private formHelper: FormHelper,
  ) {}

  ngOnInit() {
    this.loggedIn = this.authService.getLoggedIn();
    this.userId = this.authService.getUserId();
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe((authStatus) => {
        this.loggedIn = authStatus;
      });
    this.disableEditing();
  }

  emitSaved() {
    const forumPost = this.forumPostForm.value;
    this.saveClicked.emit(forumPost);
    this.disableEditing();
  }

  emitCommentClicked() {
    this.commentClicked.emit();
  }

  enableEditing() {
    this.editmode = true;
    this.formHelper.enableControls(this.forumPostForm);
  }

  disableEditing() {
    this.editmode = false;
    this.formHelper.disableControls(this.forumPostForm);
  }

  openDeleteConfirmation() {
    const title = this.translateService.instant(
      'deleteForumpostConfirmationTitle',
    );
    const text = this.translateService.instant(
      'deleteForumpostConfirmationText',
    );
    this.dialogService.openConfirmActionDialog(title, text, () =>
      this.deleteClicked.emit(),
    );
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }
}
