import { SnackbarHelper } from './../../../../helper/snackbar.helper';
import { SwitchedInputComponent } from './../../../../components/htmlComponents/switched-input/switched-input.component';
import { of } from 'rxjs';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../../../../utils/material/material.module';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { DialogService } from './../../../../services/dialog.service';
import { ForumService } from './../../../../services/forum.service';
import { AuthService } from './../../../../services/auth.service';
import { FormHelper } from './../../../../helper/form.helper';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PostToolbarComponent } from './post-toolbar.component';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'host-component',
  template: '<post-toolbar [forumPostForm]="forumPostForm"></post-toolbar>'
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  forumPostForm = this.formBuilder.group({
    id: ['id'],
    title: [''],
    content: [''],
    writerUsername: [''],
    writerId: [''],
    votes: [0],
    upVoters: [
      ['id1']
    ],
    downVoters: [
      ['id2']
    ],
  });
}
describe('PostToolbarComponent', () => {
  let component: PostToolbarComponent;
  let fixture: ComponentFixture < TestHostComponent > ;
  let formHelper: FormHelper;
  let authService: AuthService;
  let forumService: ForumService;
  let dialogService: DialogService;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
        declarations: [
          PostToolbarComponent,
          TestHostComponent,
          SwitchedInputComponent
        ],
        providers: [
          FormHelper,
          SnackbarHelper
        ],
        imports: [
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
              deps: [HttpClient]
            }
          })

        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    forumService = TestBed.inject(ForumService);
    dialogService = TestBed.inject(DialogService);
    authService = TestBed.inject(AuthService);
    formHelper = TestBed.inject(FormHelper);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    expect(component.loggedIn).toBe(false);
    spyOn(authService, 'getAuthStatusListener').and.returnValue(of (true));
    spyOn(authService, 'getUserId').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getAuthStatusListener).toHaveBeenCalled();
    expect(authService.getUserId).toHaveBeenCalled();
    expect(component.loggedIn).toBe(true);
  });

  it('enableEditing', () => {
    component.editmode = false;
    expect(component.editmode).toBe(false);
    spyOn(formHelper, 'enableControls').and.stub();
    component.enableEditing();
    fixture.detectChanges();
    expect(component.editmode).toBe(true);
    expect(formHelper.enableControls).toHaveBeenCalled();
  });

  it('disableEditing', () => {
    component.editmode = true;
    expect(component.editmode).toBe(true);
    spyOn(formHelper, 'disableControls').and.stub();
    component.disableEditing();
    fixture.detectChanges();
    expect(component.editmode).toBe(false);
    expect(formHelper.disableControls).toHaveBeenCalled();
  });

  it('emitSaved', () => {
    spyOn(component.saveClicked, 'emit').and.stub();
    spyOn(component, 'disableEditing').and.stub();
    component.emitSaved();
    fixture.detectChanges();
    expect(component.saveClicked.emit).toHaveBeenCalled();
    expect(component.disableEditing).toHaveBeenCalled();
  });

  it('emitCommentClicked', () => {
    spyOn(component.commentClicked, 'emit').and.stub();
    component.emitCommentClicked();
    fixture.detectChanges();
    expect(component.commentClicked.emit).toHaveBeenCalled();
  });

  it('openDeleteConfirmation', () => {
    spyOn(component, 'openDeleteConfirmation').and.callThrough();
    spyOn(dialogService, 'openConfirmActionDialog').and.stub();
    component.loggedIn = true;
    component.userId = 'id';
    component.forumPostForm.get('writerId').setValue('id');
    fixture.detectChanges();
    const deleteButton = fixture.debugElement.query(By.css('.main')).nativeElement;
    deleteButton.click();
    fixture.detectChanges();
    expect(component.openDeleteConfirmation).toHaveBeenCalled();
    expect(dialogService.openConfirmActionDialog).toHaveBeenCalled();
  });

});