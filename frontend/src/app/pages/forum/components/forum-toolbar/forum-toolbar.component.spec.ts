import { SnackbarHelper } from './../../../../helper/snackbar.helper';
import { AuthService } from './../../../../services/auth.service';
import { FormHelper } from './../../../../helper/form.helper';
import { MaterialModule } from './../../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ForumToolbarComponent } from './forum-toolbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

describe('ForumToolbarComponent', () => {
  let component: ForumToolbarComponent;
  let fixture: ComponentFixture<ForumToolbarComponent>;
  let authService: AuthService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ForumToolbarComponent
      ],
      providers: [
        FormBuilder,
        FormHelper,
        AuthService,
        SnackbarHelper
      ],
      imports: [
        MaterialModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
            deps: [HttpClient]
          }
        })
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumToolbarComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test ngOnInit', () => {
    expect(component.loggedIn).toBe(false);
    spyOn(authService, 'getLoggedIn').and.stub();
    spyOn(authService, 'getAuthStatusListener').and.returnValue(of (true));
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getLoggedIn).toHaveBeenCalled();
    expect(authService.getAuthStatusListener).toHaveBeenCalled();
    expect(component.loggedIn).toBe(true);
  });
});
