import { AuthService } from './../../../../services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'forum-toolbar',
  templateUrl: './forum-toolbar.component.html',
  styleUrls: ['./forum-toolbar.component.scss']
})
export class ForumToolbarComponent implements OnInit, OnDestroy {

  private authStatusSub: Subscription;
  loggedIn = false;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.loggedIn = this.authService.getLoggedIn();
    this.authStatusSub = this.authService.getAuthStatusListener()
    .subscribe(authStatus => {
      this.loggedIn = authStatus;
    });
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe(); 
  }

}
