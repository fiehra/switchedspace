import {
  ForumPost,
  ForumPostVoteDTO,
} from './../../../../interfaces/forum-interface';
import { AuthService } from './../../../../services/auth.service';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'forum-view-post',
  templateUrl: './forum-view-post.component.html',
  styleUrls: ['./forum-view-post.component.scss'],
})
export class ForumViewPostComponent implements OnInit, OnDestroy {
  private authStatusSub: Subscription;
  loggedIn = false;
  userId: string;
  votes: number;
  forumPost: ForumPost;
  alreadyUpvoted = false;
  alreadyDownvoted = false;

  @Input() forumPostForm: FormGroup;
  @Output() voteClicked = new EventEmitter();

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.assignComponentVariables();
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe((authStatus) => {
        this.loggedIn = authStatus;
      });
    this.alreadyUpvoted = this.forumPost.upVoters.includes(this.userId);
    this.alreadyDownvoted = this.forumPost.downVoters.includes(this.userId);
  }

  assignComponentVariables() {
    this.loggedIn = this.authService.getLoggedIn();
    this.userId = this.authService.getUserId();
    this.votes = this.forumPostForm.get('votes').value;
    this.forumPost = this.forumPostForm.value;
  }

  createVoteDto(forumPost: ForumPost) {
    const forumPostVoteDto: ForumPostVoteDTO = {
      id: forumPost.id,
      votes: forumPost.votes,
      upVoters: forumPost.upVoters,
      downVoters: forumPost.downVoters,
    };
    return forumPostVoteDto;
  }

  upvote() {
    if (this.forumPost.upVoters.includes(this.userId)) {
      const array = this.forumPost.upVoters;
      for (let i = 0; i < array.length; i++) {
        if (array[i] === this.userId) {
          array.splice(i, 1);
          this.forumPost.upVoters = array;
        }
      }
      this.alreadyUpvoted = false;
      this.alreadyDownvoted = false;
      this.votes = this.votes - 1;
      this.forumPost.votes = this.votes;
      const forumPostVoteDto = this.createVoteDto(this.forumPost);
      this.voteClicked.emit(forumPostVoteDto);
    } else if (this.forumPost.downVoters.includes(this.userId)) {
      const array = this.forumPost.downVoters;
      for (let i = 0; i < array.length; i++) {
        if (array[i] === this.userId) {
          array.splice(i, 1);
          this.forumPost.downVoters = array;
        }
      }
      this.alreadyDownvoted = false;
      this.alreadyUpvoted = true;
      this.votes = this.votes + 2;
      this.forumPost.upVoters.push(this.userId);
      this.forumPost.votes = this.votes;
      const forumPostVoteDto = this.createVoteDto(this.forumPost);
      this.voteClicked.emit(forumPostVoteDto);
    } else {
      this.alreadyDownvoted = false;
      this.alreadyUpvoted = true;
      this.votes = this.votes + 1;
      this.forumPost.upVoters.push(this.userId);
      this.forumPost.votes = this.votes;
      const forumPostVoteDto = this.createVoteDto(this.forumPost);
      this.voteClicked.emit(forumPostVoteDto);
    }
  }

  downvote() {
    if (this.forumPost.downVoters.includes(this.userId)) {
      const array = this.forumPost.downVoters;
      for (let i = 0; i < array.length; i++) {
        if (array[i] === this.userId) {
          array.splice(i, 1);
          this.forumPost.downVoters = array;
        }
      }
      this.alreadyDownvoted = false;
      this.alreadyUpvoted = false;
      this.votes = this.votes + 1;
      this.forumPost.votes = this.votes;
      const forumPostVoteDto = this.createVoteDto(this.forumPost);
      this.voteClicked.emit(forumPostVoteDto);
    } else if (this.forumPost.upVoters.includes(this.userId)) {
      const array = this.forumPost.upVoters;
      for (let i = 0; i < array.length; i++) {
        if (array[i] === this.userId) {
          array.splice(i, 1);
          this.forumPost.upVoters = array;
        }
      }
      this.alreadyUpvoted = false;
      this.alreadyDownvoted = true;
      this.votes = this.votes - 2;
      this.forumPost.downVoters.push(this.userId);
      this.forumPost.votes = this.votes;
      const forumPostVoteDto = this.createVoteDto(this.forumPost);
      this.voteClicked.emit(forumPostVoteDto);
    } else {
      this.alreadyUpvoted = false;
      this.alreadyDownvoted = true;
      this.votes = this.votes - 1;
      this.forumPost.downVoters.push(this.userId);
      this.forumPost.votes = this.votes;
      const forumPostVoteDto = this.createVoteDto(this.forumPost);
      this.voteClicked.emit(forumPostVoteDto);
    }
  }

  viewProfile() {
    if (this.forumPostForm.get('writerId').value === this.userId) {
      this.router.navigate([
        '/myProfile/' + this.forumPostForm.get('writerId').value,
      ]);
    } else {
      this.router.navigate([
        '/profile/' + this.forumPostForm.get('writerId').value,
      ]);
    }
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }
}
