import { SnackbarHelper } from './../../../../helper/snackbar.helper';
import { DialogService } from './../../../../services/dialog.service';
import { Router } from '@angular/router';
import { ForumService } from './../../../../services/forum.service';
import { AuthService } from './../../../../services/auth.service';
import { SwitchedInputComponent } from './../../../../components/htmlComponents/switched-input/switched-input.component';
import { SwitchedTextareaComponent } from './../../../../components/htmlComponents/switched-textarea/switched-textarea.component';
import { FormHelper } from './../../../../helper/form.helper';
import { MaterialModule } from './../../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ForumViewPostComponent } from './forum-view-post.component';
import { ReactiveFormsModule, FormGroup, FormBuilder } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { of } from 'rxjs';
import { Component } from '@angular/core';

@Component({
  selector: 'host-component',
  template:
    '<forum-view-post [forumPostForm]="forumPostForm"></forum-view-post>',
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  forumPostForm = this.formBuilder.group({
    id: ['id'],
    title: [''],
    content: [''],
    writerUsername: [''],
    writerId: ['fiehra'],
    votes: [0],
    upVoters: [['id1']],
    downVoters: [['id2']],
    comments: [[]],
  });
}
describe('ForumViewPostComponent', () => {
  let component: ForumViewPostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let authService: AuthService;
  let router: Router;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [
          ForumViewPostComponent,
          TestHostComponent,
          SwitchedTextareaComponent,
          SwitchedInputComponent,
        ],
        providers: [FormHelper, SnackbarHelper],
        imports: [
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    authService = TestBed.inject(AuthService);
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit with form initialized', () => {
    expect(component.loggedIn).toBe(false);
    spyOn(authService, 'getAuthStatusListener').and.returnValue(of(true));
    spyOn(authService, 'getUserId').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getAuthStatusListener).toHaveBeenCalled();
    expect(authService.getUserId).toHaveBeenCalled();
    expect(component.loggedIn).toBe(true);
  });

  it('assignComponentVariables', () => {
    component.forumPost.upVoters.push('id');
    component.forumPostForm.get('votes').setValue(1);
    fixture.detectChanges();
    spyOn(authService, 'getLoggedIn').and.stub();
    spyOn(authService, 'getUserId').and.returnValue('id');
    expect(component.forumPostForm.get('votes').value).toBe(1);
    component.assignComponentVariables();
    fixture.detectChanges();
    expect(authService.getLoggedIn).toHaveBeenCalled();
    expect(authService.getUserId).toHaveBeenCalled();
  });

  it('upVote id1 already voted', () => {
    component.userId = 'id1';
    fixture.detectChanges();
    expect(component.forumPost.upVoters.length).toBe(1);
    spyOn(component.voteClicked, 'emit').and.stub();
    component.upvote();
    fixture.detectChanges();
    expect(component.voteClicked.emit).toHaveBeenCalled();
    expect(component.forumPost.upVoters.length).toBe(0);
  });

  it('upVote id2 already voted', () => {
    component.userId = 'id2';
    fixture.detectChanges();
    expect(component.forumPost.downVoters.length).toBe(1);
    spyOn(component.voteClicked, 'emit').and.stub();
    component.upvote();
    fixture.detectChanges();
    expect(component.voteClicked.emit).toHaveBeenCalled();
    expect(component.forumPost.downVoters.length).toBe(0);
  });

  it('upVote none already voted', () => {
    component.forumPost.upVoters = [];
    component.forumPost.downVoters = [];
    fixture.detectChanges();
    expect(component.forumPost.upVoters.length).toBe(0);
    expect(component.forumPost.downVoters.length).toBe(0);
    spyOn(component.voteClicked, 'emit').and.stub();
    component.upvote();
    fixture.detectChanges();
    expect(component.voteClicked.emit).toHaveBeenCalled();
    expect(component.forumPost.upVoters.length).toBe(1);
    expect(component.forumPost.downVoters.length).toBe(0);
  });

  it('downVote id1 already voted', () => {
    component.userId = 'id1';
    fixture.detectChanges();
    expect(component.forumPost.upVoters.length).toBe(1);
    spyOn(component.voteClicked, 'emit').and.stub();
    component.downvote();
    fixture.detectChanges();
    expect(component.voteClicked.emit).toHaveBeenCalled();
    expect(component.forumPost.upVoters.length).toBe(0);
  });

  it('downVote id2 already voted', () => {
    component.userId = 'id2';
    fixture.detectChanges();
    expect(component.forumPost.downVoters.length).toBe(1);
    spyOn(component.voteClicked, 'emit').and.stub();
    component.downvote();
    fixture.detectChanges();
    expect(component.voteClicked.emit).toHaveBeenCalled();
    expect(component.forumPost.downVoters.length).toBe(0);
  });

  it('downVote none already voted', () => {
    component.forumPost.upVoters = [];
    component.forumPost.downVoters = [];
    fixture.detectChanges();
    expect(component.forumPost.upVoters.length).toBe(0);
    expect(component.forumPost.downVoters.length).toBe(0);
    spyOn(component.voteClicked, 'emit').and.stub();
    component.downvote();
    fixture.detectChanges();
    expect(component.voteClicked.emit).toHaveBeenCalled();
    expect(component.forumPost.upVoters.length).toBe(0);
    expect(component.forumPost.downVoters.length).toBe(1);
  });

  it('viewProfile userId !== writerId', () => {
    spyOn(router, 'navigate').and.stub();
    component.forumPostForm.get('writerId').setValue('test');
    component.viewProfile();
    fixture.detectChanges();
    expect(router.navigate).toHaveBeenCalledWith(['/profile/test']);
  });

  it('viewProfile userId === writerId', () => {
    component.userId = 'fiehra';
    spyOn(router, 'navigate').and.stub();
    component.forumPostForm.get('writerId').setValue('fiehra');
    component.viewProfile();
    fixture.detectChanges();
    expect(router.navigate).toHaveBeenCalledWith(['/myProfile/fiehra']);
  });

  it('upvote', () => {
    expect(component.votes).toBe(0);
    spyOn(component.voteClicked, 'emit').and.stub();
    component.upvote();
    fixture.detectChanges();
    expect(component.voteClicked.emit).toHaveBeenCalled();
    expect(component.votes).toBe(1);
  });

  it('downvote', () => {
    expect(component.votes).toBe(0);
    spyOn(component.voteClicked, 'emit').and.stub();
    component.downvote();
    fixture.detectChanges();
    expect(component.voteClicked.emit).toHaveBeenCalled();
    expect(component.votes).toBe(-1);
  });
});
