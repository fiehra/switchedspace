import { ForumService } from './../../../../services/forum.service';
import { AuthService } from './../../../../services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
} from '@angular/core';
import { ForumPost, Comment } from 'src/app/interfaces/forum-interface';

@Component({
  selector: 'add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.scss'],
})
export class AddCommentComponent implements OnInit {
  @Input() forumPostForm: FormGroup;
  @Output() commentAddedClicked = new EventEmitter();

  forumpost: ForumPost;
  
  commentForm = new FormGroup({
    writerId: new FormControl('', Validators.required),
    writerUsername: new FormControl('', Validators.required),
    content: new FormControl('', Validators.required),
    markedAsCorrect: new FormControl(false),
  });

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.forumpost = this.forumPostForm.value;
    this.forumPostForm.addControl('addcomment', new FormControl());
  }
  
  emitCommentClicked() {
    const comment = this.commentForm.value;
    comment.writerId = this.authService.getUserId();
    comment.writerUsername = this.authService.getUsername();
    comment.content = this.forumPostForm.get('addcomment').value;
    this.forumpost.comments.push(comment);
    this.forumPostForm.removeControl('addcomment');
    this.commentAddedClicked.emit(this.forumpost);
  }
}
