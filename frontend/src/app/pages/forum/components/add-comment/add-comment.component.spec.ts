import { SnackbarHelper } from './../../../../helper/snackbar.helper';
import { AuthService } from './../../../../services/auth.service';
import { ForumService } from './../../../../services/forum.service';
import { FormHelper } from './../../../../helper/form.helper';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../../../../utils/material/material.module';
import { SwitchedTextareaComponent } from './../../../../components/htmlComponents/switched-textarea/switched-textarea.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddCommentComponent } from './add-comment.component';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'host-component',
  template: '<add-comment [forumPostForm]="forumPostForm"></add-comment>',
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) { }

  forumPostForm = this.formBuilder.group({
    id: ['id'],
    title: [''],
    content: [''],
    writerUsername: [''],
    writerId: [''],
    votes: [0],
    upVoters: [['id1']],
    downVoters: [['id2']],
    comments: [[]],
  });
}
describe('AddCommentComponent', () => {
  let component: AddCommentComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let authService: AuthService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [
          AddCommentComponent,
          TestHostComponent,
          SwitchedTextareaComponent,
        ],
        providers: [
          FormHelper,
          SnackbarHelper
        ],
        imports: [
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder'
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    authService = TestBed.inject(AuthService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(component.forumPostForm, 'addControl').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.forumPostForm.addControl).toHaveBeenCalled();
  });

  it('emitCommentClicked', () => {
    spyOn(component.commentAddedClicked, 'emit').and.stub();
    spyOn(authService, 'getUsername').and.stub();
    spyOn(authService, 'getUserId').and.stub();
    component.forumPostForm.get('addcomment').setValue('testcomment');
    fixture.detectChanges();
    component.emitCommentClicked();
    fixture.detectChanges();
    expect(authService.getUsername).toHaveBeenCalled();
    expect(authService.getUserId).toHaveBeenCalled();
    expect(component.commentAddedClicked.emit).toHaveBeenCalled();
  });
});
