import { FormHelper } from './../../../../helper/form.helper';
import { SwitchedInputComponent } from './../../../../components/htmlComponents/switched-input/switched-input.component';
import { SwitchedTextareaComponent } from './../../../../components/htmlComponents/switched-textarea/switched-textarea.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from './../../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CreateForumPostFormComponent } from './create-forum-post-form.component';
import {
  ReactiveFormsModule,
  FormControl,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Component } from '@angular/core';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';

@Component({
  selector: 'host-component',
  template:
    '<create-forum-post-form [forumPostForm]="forumPostForm"></create-forum-post-form>',
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}
  forumPostForm = this.formBuilder.group({
    writerId: ['fiehra', Validators.required],
    writerUsername: ['fiehra', Validators.required],
    title: ['', Validators.required],
    content: ['', Validators.required],
  });
}
describe('CreateForumPostFormComponent', () => {
  let component: CreateForumPostFormComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let formBuilder: FormBuilder;
  let formHelper: FormHelper;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [
          CreateForumPostFormComponent,
          TestHostComponent,
          SwitchedTextareaComponent,
          SwitchedInputComponent,
        ],
        providers: [FormHelper, FormBuilder, SnackbarHelper],
        imports: [
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder'
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    formBuilder = TestBed.inject(FormBuilder);
    formHelper = TestBed.inject(FormHelper);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('emitCreated with valid form', () => {
    expect(component.created).toBe(false);
    component.forumPostForm.get('title').setValue('title');
    component.forumPostForm.get('content').setValue('content');
    fixture.detectChanges();
    spyOn(component.createClicked, 'emit').and.stub();
    spyOn(formHelper, 'disableControls').and.stub();
    component.emitCreated();
    fixture.detectChanges();
    expect(component.created).toBe(true);
    expect(component.createClicked.emit).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalledWith(
      component.forumPostForm
    );
  });

  it('emitCreated with invalid form', () => {
    expect(component.created).toBe(false);
    component.forumPostForm.get('title').setValue('');
    component.forumPostForm.get('content').setValue('content');
    fixture.detectChanges();
    spyOn(component.createClicked, 'emit').and.stub();
    spyOn(formHelper, 'disableControls').and.stub();
    component.emitCreated();
    fixture.detectChanges();
    expect(component.created).toBe(false);
    expect(component.createClicked.emit).toHaveBeenCalled();
    expect(formHelper.disableControls).not.toHaveBeenCalledWith(
      component.forumPostForm
    );
  });
});
