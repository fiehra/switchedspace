import { FormHelper } from './../../../../helper/form.helper';
import { FormGroup } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'create-forum-post-form',
  templateUrl: './create-forum-post-form.component.html',
  styleUrls: ['./create-forum-post-form.component.scss']
})
export class CreateForumPostFormComponent implements OnInit {

  created = false;

  @Input() forumPostForm: FormGroup;
  @Output() createClicked = new EventEmitter();

  constructor(private formHelper: FormHelper) {

  }

  ngOnInit() {}

  // emit form to parent if valid
  emitCreated() {
    if (this.forumPostForm.valid) {
      this.createClicked.emit();
      this.created = true;
      this.formHelper.disableControls(this.forumPostForm);
    } else {
      this.createClicked.emit();
    }
  }








  
}
