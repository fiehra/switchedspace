import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components/components.module';
import { UtilsModule } from '../../utils/utils.module';
import { NgModule } from '@angular/core';
import { ViewForumListPageComponent } from './view-forum-list-page/view-forum-list-page.component';
import { CreateForumPostPageComponent } from './create-forum-post-page/create-forum-post-page.component';
import { CreateForumPostFormComponent } from './components/create-forum-post-form/create-forum-post-form.component';
import { ViewForumPostPageComponent } from './view-forum-post-page/view-forum-post-page.component';
import { ForumListPostComponent } from './components/forum-list-post/forum-list-post.component';
import { ForumToolbarComponent } from './components/forum-toolbar/forum-toolbar.component';
import { ForumViewPostComponent } from './components/forum-view-post/forum-view-post.component';
import { PostToolbarComponent } from './components/post-toolbar/post-toolbar.component';
import { AddCommentComponent } from './components/add-comment/add-comment.component';
import { ListCommentComponent } from './components/list-comment/list-comment.component';

@NgModule({
  declarations: [
    ViewForumListPageComponent,
    CreateForumPostPageComponent,
    CreateForumPostFormComponent,
    ViewForumPostPageComponent,
    ForumListPostComponent,
    ForumToolbarComponent,
    ForumViewPostComponent,
    PostToolbarComponent,
    AddCommentComponent,
    ListCommentComponent
  ],
  imports: [
    UtilsModule,
    ComponentsModule,
    TranslateModule
  ],
  exports: [
    ViewForumListPageComponent,
    CreateForumPostPageComponent,
    ViewForumPostPageComponent,
  ]
})

export class ForumModule {}