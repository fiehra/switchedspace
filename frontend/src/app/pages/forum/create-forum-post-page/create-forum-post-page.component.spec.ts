import { AuthService } from './../../../services/auth.service';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { ForumService } from './../../../services/forum.service';
import { FormHelper } from './../../../helper/form.helper';
import { MaterialModule } from './../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CreateForumPostPageComponent } from './create-forum-post-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentsModule } from 'src/app/components/components.module';
import { PagesModule } from '../../pages.module';

describe('CreateForumPostPageComponent', () => {
  let component: CreateForumPostPageComponent;
  let fixture: ComponentFixture<CreateForumPostPageComponent>;
  let forumService: ForumService;
  let snackbarHelper: SnackbarHelper;
  let translateService: TranslateService;
  let authService: AuthService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [FormHelper, SnackbarHelper],
        imports: [
          ComponentsModule,
          PagesModule,
          MaterialModule,
          ReactiveFormsModule,
          RouterTestingModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateForumPostPageComponent);
    component = fixture.componentInstance;
    forumService = TestBed.inject(ForumService);
    snackbarHelper = TestBed.inject(SnackbarHelper);
    translateService = TestBed.inject(TranslateService);
    authService = TestBed.inject(AuthService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(authService, 'getUserId').and.returnValue('fiehra');
    spyOn(authService, 'getUsername').and.returnValue('fiehra');
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getUserId).toHaveBeenCalled();
    expect(authService.getUsername).toHaveBeenCalled();
    expect(component.forumPostForm.get('writerId').value).toBe('fiehra');
    expect(component.forumPostForm.get('writerUsername').value).toBe('fiehra');
  });

  it('createFormPost valid form', () => {
    component.forumPostForm.get('writerId').setValue('fiehra');
    component.forumPostForm.get('writerUsername').setValue('fiehra');
    component.forumPostForm.get('title').setValue('title');
    component.forumPostForm.get('content').setValue('content');
    fixture.detectChanges();
    spyOn(translateService, 'instant').and.stub();
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.createForumPost();
    fixture.detectChanges();
    expect(translateService.instant).toHaveBeenCalledWith('snackCreated');
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });

  it('createFormPost invalid form', () => {
    spyOn(translateService, 'instant').and.stub();
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.forumPostForm.get('title').setValue('');
    component.forumPostForm.get('content').setValue('content');
    fixture.detectChanges();
    component.createForumPost();
    fixture.detectChanges();
    expect(translateService.instant).toHaveBeenCalledWith('snackInvalid');
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });
});
