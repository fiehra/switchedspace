import { FormHelper } from './../../../helper/form.helper';
import { ForumPost } from './../../../interfaces/forum-interface';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { AuthService } from './../../../services/auth.service';
import { ForumService } from './../../../services/forum.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'create-forum-post-page',
  templateUrl: './create-forum-post-page.component.html',
  styleUrls: ['./create-forum-post-page.component.scss'],
})
export class CreateForumPostPageComponent implements OnInit {
  forumPost: ForumPost;
  username: string;
  writerId: string;
  created = false;
  forumPostForm = new FormGroup({
    title: new FormControl('', [Validators.required]),
    writerId: new FormControl('', Validators.required),
    writerUsername: new FormControl('', Validators.required),
    content: new FormControl('', Validators.required),
  });

  constructor(
    private forumService: ForumService,
    private translateService: TranslateService,
    private snackbarHelper: SnackbarHelper,
    private authService: AuthService,
    private formHelper: FormHelper
  ) {}

  ngOnInit() {
    this.username = this.authService.getUsername();
    this.writerId = this.authService.getUserId();
    this.forumPostForm.get('writerUsername').setValue(this.username);
    this.forumPostForm.get('writerId').setValue(this.writerId);
  }

  createForumPost() {
    if (this.forumPostForm.valid) {
      this.forumPost = this.forumPostForm.value;
      this.forumService.createForumPost(this.forumPost);
      const messageCreated =
        this.forumPostForm.get('title').value +
        this.translateService.instant('snackCreated');
      this.snackbarHelper.openSnackBar(messageCreated);
      this.formHelper.disableControls(this.forumPostForm);
      this.created = true;
    } else {
      const messageInvalid = this.translateService.instant('snackInvalid');
      this.snackbarHelper.openSnackBar(messageInvalid);
    }
  }
}
