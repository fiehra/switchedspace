import { AuthService } from './../../../services/auth.service';
import { DialogService } from './../../../services/dialog.service';
import { SelectOptions } from './../../../utils/selectOptions';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { LeafletHelper } from './../../../helper/leaflet.helper';
import { FormHelper } from './../../../helper/form.helper';
import { FormGroup } from '@angular/forms';
import { GraffmapService } from './../../../services/graffmap.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Wall } from 'src/app/interfaces/graffmap-interface';

@Component({
  selector: 'view-wall-page',
  templateUrl: './view-wall-page.component.html',
  styleUrls: ['./view-wall-page.component.scss'],
})
export class ViewWallPageComponent implements OnInit {
  private id: string;
  wall: Wall;
  wallForm: FormGroup;
  wallMarker = [];
  editmode = false;
  updated = false;
  commentMode = false;
  selectOptions = SelectOptions.legalStateSelects;
  loggedIn = false;

  constructor(
    private route: ActivatedRoute,
    private graffmapService: GraffmapService,
    private formHelper: FormHelper,
    private leafletHelper: LeafletHelper,
    private snackbarHelper: SnackbarHelper,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.loadWall();
    this.loggedIn = this.authService.getLoggedIn();
  }

  loadWall() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('id')) {
        this.id = paramMap.get('id');
        this.graffmapService.getWallById(this.id).subscribe((response) => {
          this.wall = response;
          this.wallForm = this.graffmapService.mapWallToForm(this.wall);
          this.formHelper.disableControls(this.wallForm);
          this.loadWallMarker();
        });
      }
    });
  }

  loadWallMarker() {
    this.wallMarker = [];
    const newMarker = this.leafletHelper.createMarker(this.wall, 'wall');
    this.wallMarker.push(newMarker);
  }

  changeLegalState() {
    this.formHelper.enableControls(this.wallForm);
    this.formHelper.disableControlsByName(this.wallForm, [
      'name',
      'description',
    ]);
    this.editmode = true;
    const message = this.translateService.instant('snackUpdateLegalState');
    this.snackbarHelper.openSnackBar(message);
  }

  openConfirmDialog() {
    const title = this.translateService.instant('confirmWallStateTitle');
    const text =
      this.translateService.instant('confirmWallStateText') +
      ' ' +
      this.translateService.instant(this.wallForm.get('legalState').value);
    this.dialogService.openConfirmActionDialog(title, text, () => {
      this.updateWall();
    });
  }

  updateWall() {
    this.wall = this.wallForm.value;
    this.wall.statusUpdates.push(this.createStatusUpdateMessage());
    this.graffmapService
      .updateWallById(this.wall.id, this.wall)
      .subscribe((response) => {
        const message = this.translateService.instant('snackUpdated');
        this.snackbarHelper.openSnackBar(message);
        this.editmode = false;
        this.updated = true;
        this.formHelper.disableControls(this.wallForm);
        this.loadWall();
      });
  }

  createStatusUpdateMessage() {
    let stringDate = new Date().toDateString();
    const newStatus = this.wall.legalState;
    const message = newStatus + ' - ' + stringDate;
    return message;
  }
}
