import { ComponentsModule } from 'src/app/components/components.module';
import { AuthService } from './../../../services/auth.service';
import { DialogService } from 'src/app/services/dialog.service';
import { GraffmapService } from './../../../services/graffmap.service';
import { of } from 'rxjs';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from './../../../utils/material/material.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletHelper } from './../../../helper/leaflet.helper';
import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { FormHelper } from './../../../helper/form.helper';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ViewWallPageComponent } from './view-wall-page.component';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { LegalState } from 'src/app/interfaces/graffmap-interface';
import { PagesModule } from '../../pages.module';

describe('ViewWallPageComponent', () => {
  let component: ViewWallPageComponent;
  let fixture: ComponentFixture<ViewWallPageComponent>;
  let graffmapService: GraffmapService;
  let formHelper: FormHelper;
  let leafletHelper: LeafletHelper;
  let snackbarHelper: SnackbarHelper;
  let dialogService: DialogService;
  let authService: AuthService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [
          FormBuilder,
          FormHelper,
          SnackbarHelper,
          LeafletHelper,
          {
            provide: ActivatedRoute,
            useValue: { paramMap: of(convertToParamMap({ id: 'fiehra' })) },
          },
        ],
        imports: [
          PagesModule,
          ComponentsModule,
          LeafletModule,
          MaterialModule,
          RouterTestingModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewWallPageComponent);
    component = fixture.componentInstance;
    graffmapService = TestBed.inject(GraffmapService);
    formHelper = TestBed.inject(FormHelper);
    leafletHelper = TestBed.inject(LeafletHelper);
    snackbarHelper = TestBed.inject(SnackbarHelper);
    dialogService = TestBed.inject(DialogService);
    authService = TestBed.inject(AuthService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(component, 'loadWall').and.stub();
    spyOn(authService, 'getLoggedIn').and.returnValue(true);
    component.ngOnInit();
    fixture.detectChanges(), expect(component.loadWall).toHaveBeenCalled();
    expect(authService.getLoggedIn).toHaveBeenCalled();
    expect(component.loggedIn).toBe(true);
  });

  it('loadWall', () => {
    const wallMock = {
      id: 'fiehra',
      name: 'wall',
      description: 'wall',
      legalState: 'legal',
      lat: 35.6895,
      lng: 139.69171,
      statusUpdates: [],
    };

    spyOn(graffmapService, 'getWallById').and.returnValue(of(wallMock));
    spyOn(formHelper, 'disableControls').and.stub();
    spyOn(component, 'loadWallMarker').and.stub();
    component.loadWall();
    fixture.detectChanges();
    expect(graffmapService.getWallById).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalled();
    expect(component.loadWallMarker).toHaveBeenCalled();
  });

  it('loadWallMarker', () => {
    spyOn(leafletHelper, 'createMarker').and.stub();
    component.loadWallMarker();
    fixture.detectChanges();
    expect(leafletHelper.createMarker).toHaveBeenCalled();
  });

  it('changeLegalState', () => {
    expect(component.editmode).toBe(false);
    spyOn(formHelper, 'enableControls').and.stub();
    spyOn(formHelper, 'disableControlsByName').and.stub();
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.changeLegalState();
    fixture.detectChanges();
    expect(formHelper.enableControls).toHaveBeenCalled();
    expect(formHelper.disableControlsByName).toHaveBeenCalled();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
    expect(component.editmode).toBe(true);
  });

  it('updateWall', () => {
    const wallMock = {
      id: 'fiehra',
      name: 'wall',
      description: 'wall',
      legalState: 'legal' as LegalState,
      lat: 35.6895,
      lng: 139.69171,
      statusUpdates: ['confirmed legal'],
    };
    component.wallForm = graffmapService.mapWallToForm(wallMock);
    component.wall = wallMock;
    fixture.detectChanges();
    spyOn(graffmapService, 'updateWallById').and.returnValue(of(wallMock));
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    spyOn(formHelper, 'disableControls').and.stub();
    spyOn(component, 'loadWall').and.stub();

    component.updateWall();
    fixture.detectChanges();
    expect(graffmapService.updateWallById).toHaveBeenCalled();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalled();
    expect(component.loadWall).toHaveBeenCalled();
    expect(component.editmode).toBe(false);
    expect(component.updated).toBe(true);
  });

  it('createStatusUpdateMessage', () => {
    const wallMock = {
      id: 'fiehra',
      name: 'wall',
      description: 'wall',
      legalState: 'legal' as LegalState,
      lat: 35.6895,
      lng: 139.69171,
      statusUpdates: ['confirmed legal'],
    };
    component.wall = wallMock;
    const message = component.createStatusUpdateMessage();
    fixture.detectChanges();
    expect(message.includes('legal')).toBeTruthy();
  });

  it('openConfirmDialog', () => {
    const wallMock = {
      id: 'fiehra',
      name: 'wall',
      description: 'wall',
      legalState: 'legal' as LegalState,
      lat: 35.6895,
      lng: 139.69171,
      statusUpdates: ['confirmed legal'],
    };
    component.wallForm = graffmapService.mapWallToForm(wallMock);
    component.wallForm.get('legalState').setValue('illegal');
    fixture.detectChanges();
    spyOn(dialogService, 'openConfirmActionDialog').and.stub();
    component.openConfirmDialog();
    fixture.detectChanges();
    expect(dialogService.openConfirmActionDialog).toHaveBeenCalled();
  });
});
