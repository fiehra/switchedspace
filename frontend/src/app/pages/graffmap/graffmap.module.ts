import { GraffmapPageComponent } from './graffmap-page/graffmap-page.component';
import { LeafletHelper } from './../../helper/leaflet.helper';
import { SwitchedLeafletComponent } from './components/switched-leaflet/switched-leaflet.component';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components/components.module';
import { UtilsModule } from '../../utils/utils.module';
import { NgModule } from '@angular/core';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { AddMarkerMenuComponent } from './components/add-marker-menu/add-marker-menu.component';
import { SwitchedLeafletAddMarkerComponent } from './components/switched-leaflet-add-marker/switched-leaflet-add-marker.component';
import { AddMarkerPageComponent } from './add-marker-page/add-marker-page.component';
import { StreetartFormComponent } from './components/streetart-form/streetart-form.component';
import { WallFormComponent } from './components/wall-form/wall-form.component';
import { ShopFormComponent } from './components/shop-form/shop-form.component';
import { ViewWallPageComponent } from './view-wall-page/view-wall-page.component';
import { SwitchedLeafletViewMarkerComponent } from './components/switched-leaflet-view-marker/switched-leaflet-view-marker.component';
import { ViewShopPageComponent } from './view-shop-page/view-shop-page.component';
import { ViewStreetartPageComponent } from './view-streetart-page/view-streetart-page.component';

@NgModule({
  declarations: [
    SwitchedLeafletComponent,
    AddMarkerMenuComponent,
    SwitchedLeafletAddMarkerComponent,
    AddMarkerPageComponent,
    GraffmapPageComponent,
    StreetartFormComponent,
    WallFormComponent,
    ShopFormComponent,
    ViewWallPageComponent,
    SwitchedLeafletViewMarkerComponent,
    ViewShopPageComponent,
    ViewStreetartPageComponent
  ],
  providers: [
    LeafletHelper
  ],
  imports: [
    UtilsModule,
    ComponentsModule,
    TranslateModule,
    LeafletModule
  ],
  exports: [
    AddMarkerPageComponent,
    GraffmapPageComponent,
    ViewWallPageComponent
  ]
})

export class GraffmapModule {}