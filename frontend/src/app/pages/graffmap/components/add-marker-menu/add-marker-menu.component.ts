import { AuthService } from './../../../../services/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'add-marker-menu',
  templateUrl: './add-marker-menu.component.html',
  styleUrls: ['./add-marker-menu.component.scss']
})
export class AddMarkerMenuComponent implements OnInit {
  loggedIn: boolean;

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.loggedIn = this.authService.getLoggedIn();
  }
  
}
