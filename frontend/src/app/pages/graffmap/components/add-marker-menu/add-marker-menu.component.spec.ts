import { AuthService } from './../../../../services/auth.service';
import { FormBuilder } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { MaterialModule } from './../../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AddMarkerMenuComponent } from './add-marker-menu.component';
import { HttpClient } from '@angular/common/http';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';

describe('AddMarkerMenuComponent', () => {
  let component: AddMarkerMenuComponent;
  let fixture: ComponentFixture<AddMarkerMenuComponent>;
  let authService: AuthService;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [AddMarkerMenuComponent],
      providers: [FormBuilder, SnackbarHelper],
      imports: [
        MaterialModule,
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (http: HttpClient) =>
              new TranslateHttpLoader(
                http,
                'assets/i18n/',
                '.json?build_id=build_id_replacement_placeholder'
              ),
            deps: [HttpClient],
          },
        }),
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMarkerMenuComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(authService, 'getLoggedIn').and.returnValue(true);
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getLoggedIn).toHaveBeenCalled();
  });
});
