import { LeafletHelper } from './../../../../helper/leaflet.helper';
import { Component, OnInit, Input } from '@angular/core';
import { Map } from "leaflet";

@Component({
  selector: 'switched-leaflet',
  templateUrl: './switched-leaflet.component.html',
  styleUrls: ['./switched-leaflet.component.scss']
})
export class SwitchedLeafletComponent implements OnInit {
  options =  this.leafletHelper.getLeafletOptions();
  map: Map;

  @Input() layers;
  constructor(private leafletHelper: LeafletHelper) { }

  ngOnInit(): void {
  }

  onMapReady(map: Map) {
    this.map = map;
  }
}
