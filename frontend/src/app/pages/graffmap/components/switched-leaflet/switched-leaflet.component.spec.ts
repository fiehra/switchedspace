import { SwitchedLeafletComponent } from './switched-leaflet.component';
import { MaterialModule } from './../../../../utils/material/material.module';
import { LeafletHelper } from './../../../../helper/leaflet.helper';
import { SnackbarHelper } from './../../../../helper/snackbar.helper';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormHelper } from 'src/app/helper/form.helper';
import { FormBuilder } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient } from '@angular/common/http';
import { AddMarkerMenuComponent } from '../add-marker-menu/add-marker-menu.component';

describe('SwitchedLeafletComponent', () => {
  let component: SwitchedLeafletComponent;
  let fixture: ComponentFixture < SwitchedLeafletComponent > ;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
        declarations: [
          SwitchedLeafletComponent,
          AddMarkerMenuComponent
        ],
        providers: [
          FormBuilder,
          FormHelper,
          SnackbarHelper,
          LeafletHelper
        ],
        imports: [
          LeafletModule,
          MaterialModule,
          RouterTestingModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
              deps: [HttpClient]
            }
          })
        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchedLeafletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});