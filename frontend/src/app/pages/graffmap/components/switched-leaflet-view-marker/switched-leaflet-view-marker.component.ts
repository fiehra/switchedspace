import { LeafletHelper } from './../../../../helper/leaflet.helper';
import { Component, OnInit, Input } from '@angular/core';
import { Map } from "leaflet";

@Component({
  selector: 'switched-leaflet-view-marker',
  templateUrl: './switched-leaflet-view-marker.component.html',
  styleUrls: ['./switched-leaflet-view-marker.component.scss']
})
export class SwitchedLeafletViewMarkerComponent implements OnInit {
  map: Map;
  options;
  
  @Input() layers;
  @Input() marker;

  constructor(private leafletHelper: LeafletHelper) { }
  
  ngOnInit(): void {
    this.options =  this.leafletHelper.getMarkerOptions(this.marker);
  }

  onMapReady(map: Map) {
    this.map = map;
  }
}
