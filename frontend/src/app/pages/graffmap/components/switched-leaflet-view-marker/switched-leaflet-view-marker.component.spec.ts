import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from './../../../../utils/material/material.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletHelper } from './../../../../helper/leaflet.helper';
import { SnackbarHelper } from './../../../../helper/snackbar.helper';
import { FormHelper } from './../../../../helper/form.helper';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SwitchedLeafletViewMarkerComponent } from './switched-leaflet-view-marker.component';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'host-component',
  template: '<switched-leaflet-view-marker [layers]="wallMarker" [marker]="wall"></switched-leaflet-view-marker>'
})
class TestHostComponent {

  wallMarker = [];

  wall = {
    id: 'id',
    name: 'wall',
    description: 'wall',
    legalState: 'legal',
    lat: 35.6895000,
    lng: 139.6917100
  }
  constructor() {}

  
}
describe('SwitchedLeafletViewMarkerComponent', () => {
  let component: SwitchedLeafletViewMarkerComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [
        SwitchedLeafletViewMarkerComponent,
        TestHostComponent
      ],
      providers: [
        FormBuilder,
        FormHelper,
        SnackbarHelper,
        LeafletHelper
      ],
      imports: [
        LeafletModule,
        MaterialModule,
        RouterTestingModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
            deps: [HttpClient]
          }
        })
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
