import { TranslateService } from '@ngx-translate/core';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { LeafletHelper } from './../../../../helper/leaflet.helper';
import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Map } from "leaflet";

@Component({
  selector: 'switched-leaflet-add-marker',
  templateUrl: './switched-leaflet-add-marker.component.html',
  styleUrls: ['./switched-leaflet-add-marker.component.scss']
})
export class SwitchedLeafletAddMarkerComponent implements OnInit {
  options = this.leafletHelper.getLeafletOptions();
  marker = [];
  map: Map;

  @Output() markerSet = new EventEmitter();
  @Input() type: string;

  constructor(private leafletHelper: LeafletHelper, private snackbarHelper: SnackbarHelper, private translateService: TranslateService) { }

  ngOnInit(): void {
  }

  mapClicked(event: any) {
    const zoom = this.getZoom();
    if (zoom > 15) {
      this.marker = [];
      this.marker.push(this.leafletHelper.createDefaultMarker(event.latlng.lat, event.latlng.lng, this.type));
      this.markerSet.emit({lat: event.latlng.lat, lng: event.latlng.lng});
    } else {
      const message = this.translateService.instant('snackZoomPls');
      this.snackbarHelper.openSnackBar(message)
    }
  }
  
  onMapReady(map: Map) {
    this.map = map;
  }
  
  getZoom() {
    return this.map.getZoom();
  }

}
