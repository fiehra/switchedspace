import { MaterialModule } from './../../../../utils/material/material.module';
import { LeafletHelper } from './../../../../helper/leaflet.helper';
import { SnackbarHelper } from './../../../../helper/snackbar.helper';
import { SwitchedLeafletAddMarkerComponent } from './switched-leaflet-add-marker.component';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormHelper } from 'src/app/helper/form.helper';
import { FormBuilder } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient } from '@angular/common/http';

describe('SwitchedLeafletAddMarkerComponent', () => {
  let component: SwitchedLeafletAddMarkerComponent;
  let fixture: ComponentFixture < SwitchedLeafletAddMarkerComponent > ;
  let snackbarHelper: SnackbarHelper;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
        declarations: [
          SwitchedLeafletAddMarkerComponent,
        ],
        providers: [
          FormBuilder,
          FormHelper,
          SnackbarHelper,
          LeafletHelper
        ],
        imports: [
          LeafletModule,
          MaterialModule,
          RouterTestingModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
              deps: [HttpClient]
            }
          })
        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchedLeafletAddMarkerComponent);
    component = fixture.componentInstance;
    snackbarHelper = TestBed.inject(SnackbarHelper);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('mapClicked zoom > 15', () => {
    spyOn(component, 'getZoom').and.returnValue(16);
    spyOn(component.markerSet, 'emit').and.stub();
    const mockEvent = {
      latlng: {
        lat: 35.561277754384555,​​
        lng: 139.66506958007815
      }
    }
    component.type = 'wall';
    fixture.detectChanges();
    component.mapClicked(mockEvent);
    expect(component.getZoom).toHaveBeenCalled();
    expect(component.markerSet.emit).toHaveBeenCalled();
    expect(component.marker.length).toBe(1);
  });

  it('mapClicked zoom > 15', () => {
    spyOn(component, 'getZoom').and.returnValue(16);
    spyOn(component.markerSet, 'emit').and.stub();
    const mockEvent = {
      latlng: {
        lat: 35.561277754384555,​​
        lng: 139.66506958007815
      }
    }
    component.type = 'shop';
    fixture.detectChanges();
    component.mapClicked(mockEvent);
    expect(component.getZoom).toHaveBeenCalled();
    expect(component.markerSet.emit).toHaveBeenCalled();
    expect(component.marker.length).toBe(1);
  });

  it('mapClicked zoom > 15', () => {
    spyOn(component, 'getZoom').and.returnValue(16);
    spyOn(component.markerSet, 'emit').and.stub();
    const mockEvent = {
      latlng: {
        lat: 35.561277754384555,​​
        lng: 139.66506958007815
      }
    }
    component.type = 'streetart';
    fixture.detectChanges();
    component.mapClicked(mockEvent);
    expect(component.getZoom).toHaveBeenCalled();
    expect(component.markerSet.emit).toHaveBeenCalled();
    expect(component.marker.length).toBe(1);
  });

  it('mapClicked zoom < 15', () => {
    spyOn(component, 'getZoom').and.returnValue(5);
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    const mockEvent = {
      latlng: {
        lat: 35.561277754384555,​​
        lng: 139.66506958007815
      }
    }

    component.mapClicked(mockEvent);
    fixture.detectChanges();
    expect(component.getZoom).toHaveBeenCalled();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });

  it('getZoom default zoom is 10', () => {
    component.ngOnInit();
    fixture.detectChanges();
    component.getZoom();
    fixture.detectChanges();
    expect(component.getZoom()).toBe(10);
  });
});