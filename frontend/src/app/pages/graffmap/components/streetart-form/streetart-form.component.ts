import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'streetart-form',
  templateUrl: './streetart-form.component.html',
  styleUrls: ['./streetart-form.component.scss']
})
export class StreetartFormComponent implements OnInit {

  @Input() streetartForm: FormGroup;
  
  constructor() { }

  ngOnInit(): void {
  }

}
