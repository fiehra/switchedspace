import { FormHelper } from './../../../../helper/form.helper';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../../../../utils/material/material.module';
import { SwitchedTextareaComponent } from './../../../../components/htmlComponents/switched-textarea/switched-textarea.component';
import { SwitchedInputComponent } from './../../../../components/htmlComponents/switched-input/switched-input.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { StreetartFormComponent } from './streetart-form.component';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'host-component',
  template: '<streetart-form [streetartForm]="streetartForm"></streetart-form>'
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  streetartForm = this.formBuilder.group({
    id: ['id'],
    name: ['name'],
    description: ['streetart@streetart'],
    link: ['streetart.com'],
    lat: [35.6895000],
    lng: [139.6917100],
  });
}

describe('StreetartFormComponent', () => {
  let component: StreetartFormComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [
        StreetartFormComponent,
        TestHostComponent,
        SwitchedInputComponent,
        SwitchedTextareaComponent
       ],
       providers: [
         FormHelper,
         FormBuilder
       ],
       imports: [
        MaterialModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
            deps: [HttpClient]
          }
        })
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
