import { SelectOptions } from './../../../../utils/selectOptions';
import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'wall-form',
  templateUrl: './wall-form.component.html',
  styleUrls: ['./wall-form.component.scss']
})
export class WallFormComponent implements OnInit {

  @Input() wallForm: FormGroup;
  selectOptions = SelectOptions.legalStateSelects;
  
  constructor() { }

  ngOnInit(): void {
  }

}
