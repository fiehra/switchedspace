import { SwitchedSelectComponent } from './../../../../components/htmlComponents/switched-select/switched-select.component';
import { WallFormComponent } from './wall-form.component';
import { FormHelper } from './../../../../helper/form.helper';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../../../../utils/material/material.module';
import { SwitchedTextareaComponent } from './../../../../components/htmlComponents/switched-textarea/switched-textarea.component';
import { SwitchedInputComponent } from './../../../../components/htmlComponents/switched-input/switched-input.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'host-component',
  template: '<wall-form [wallForm]="wallForm"></wall-form>'
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  wallForm = this.formBuilder.group({
    id: ['id'],
    name: ['name'],
    description: ['wall@wall'],
    legalState: ['legal'],
    lat: [35.6895000],
    lng: [139.6917100],
    statusUpdates: ['confirmed legal']
  });
}

describe('WallFormComponent', () => {
  let component: WallFormComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [
        WallFormComponent,
        TestHostComponent,
        SwitchedInputComponent,
        SwitchedTextareaComponent,
        SwitchedSelectComponent
       ],
       providers: [
         FormHelper,
         FormBuilder
       ],
       imports: [
        MaterialModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
            deps: [HttpClient]
          }
        })
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
