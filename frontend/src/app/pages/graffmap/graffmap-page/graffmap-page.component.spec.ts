import { ComponentsModule } from 'src/app/components/components.module';
import { of } from 'rxjs';
import { GraffmapService } from './../../../services/graffmap.service';
import { LeafletHelper } from './../../../helper/leaflet.helper';
import { RouterTestingModule } from '@angular/router/testing';
import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { FormHelper } from 'src/app/helper/form.helper';
import { FormBuilder } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { MaterialModule } from './../../../utils/material/material.module';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GraffmapPageComponent } from './graffmap-page.component';
import { HttpClient } from '@angular/common/http';
import { Shop, Streetart, Wall } from 'src/app/interfaces/graffmap-interface';
import { PagesModule } from '../../pages.module';

window.scrollTo = jest.fn();

describe('GraffmapPageComponent', () => {
  let component: GraffmapPageComponent;
  let fixture: ComponentFixture<GraffmapPageComponent>;
  let graffmapService: GraffmapService;

  const wallMock: Wall = {
    id: '1',
    name: 'wall',
    description: 'nice wall',
    legalState: 'legal',
    lat: 35.6895,
    lng: 139.69171,
    statusUpdates: [],
  };

  const wallMock2: Wall = {
    id: '2',
    name: 'wall2',
    description: 'bad wall',
    legalState: 'legal',
    lat: 35.6895,
    lng: 139.69171,
    statusUpdates: [],
  };

  const shopMock: Shop = {
    id: '1',
    name: 'shop',
    description: 'nice shop',
    website: 'skillcap.com',
    lat: 35.6895,
    lng: 139.69171,
  };

  const shopMock2: Shop = {
    id: '2',
    name: 'shop2',
    description: 'bad shop',
    website: 'skillcap.com',
    lat: 35.6895,
    lng: 139.69171,
  };

  const streetartMock: Streetart = {
    id: '1',
    name: 'streetart',
    description: 'nice art',
    link: 'skillcap.com',
    lat: 35.6895,
    lng: 139.69171,
  };

  const streetartMock2: Streetart = {
    id: '2',
    name: 'streetart2',
    description: 'bad art',
    link: 'skillcap.com',
    lat: 35.6895,
    lng: 139.69171,
  };
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [FormBuilder, FormHelper, SnackbarHelper, LeafletHelper],
        imports: [
          PagesModule,
          ComponentsModule,
          LeafletModule,
          MaterialModule,
          RouterTestingModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(GraffmapPageComponent);
    component = fixture.componentInstance;
    graffmapService = TestBed.inject(GraffmapService);
    fixture.detectChanges();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(component, 'loadWalls').and.stub();
    spyOn(component, 'loadShops').and.stub();
    spyOn(component, 'loadStreetarts').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.loadWalls).toHaveBeenCalled();
    expect(component.loadShops).toHaveBeenCalled();
    expect(component.loadStreetarts).toHaveBeenCalled();
  });

  it('loadWalls', () => {
    const mockResponse = {
      message: 'getting walls',
      walls: [wallMock, wallMock2],
      maxWalls: 2,
    };

    fixture.detectChanges();
    spyOn(graffmapService, 'getWalls').and.returnValue(of(mockResponse));
    component.loadWalls();
    fixture.detectChanges();
    expect(graffmapService.getWalls).toHaveBeenCalled();
    expect(component.markers.length).toBe(2);
  });

  it('loadShops', () => {
    const mockResponse = {
      message: 'getting shops',
      shops: [shopMock, shopMock2],
      maxShops: 2,
    };

    fixture.detectChanges();
    spyOn(graffmapService, 'getShops').and.returnValue(of(mockResponse));
    component.loadShops();
    fixture.detectChanges();
    expect(graffmapService.getShops).toHaveBeenCalled();
    expect(component.markers.length).toBe(2);
  });

  it('loadStreetarts', () => {
    const mockResponse = {
      message: 'getting streetarts',
      streetarts: [streetartMock, streetartMock2],
      maxStreetarts: 2,
    };

    fixture.detectChanges();
    spyOn(graffmapService, 'getStreetarts').and.returnValue(of(mockResponse));
    component.loadStreetarts();
    fixture.detectChanges();
    expect(graffmapService.getStreetarts).toHaveBeenCalled();
    expect(component.markers.length).toBe(2);
  });
});
