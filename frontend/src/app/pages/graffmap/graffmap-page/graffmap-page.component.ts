import { LeafletHelper } from './../../../helper/leaflet.helper';
import { GraffmapService } from './../../../services/graffmap.service';
import { Component, OnInit } from '@angular/core';
import { Shop, Streetart, Wall } from 'src/app/interfaces/graffmap-interface';

@Component({
  selector: 'graffmap-page',
  templateUrl: './graffmap-page.component.html',
  styleUrls: ['./graffmap-page.component.scss'],
})
export class GraffmapPageComponent implements OnInit {
  walls: Wall[] = [];
  shops: Shop[] = [];
  streetarts: Streetart[] = [];
  markers = [];

  constructor(
    private leafletHelper: LeafletHelper,
    private graffmapService: GraffmapService
  ) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.markers = [];
    this.loadWalls();
    this.loadShops();
    this.loadStreetarts();
  }

  loadWalls() {
    this.graffmapService.getWalls().subscribe((response) => {
      for (const wall of response.walls) {
        this.walls.push(wall);
        const newMarker = this.leafletHelper.createMarker(wall, 'wall');
        this.markers.push(newMarker);
      }
    });
  }

  loadShops() {
    this.graffmapService.getShops().subscribe((response) => {
      for (const shop of response.shops) {
        this.shops.push(shop);
        const newMarker = this.leafletHelper.createMarker(shop, 'shop');
        this.markers.push(newMarker);
      }
    });
  }

  loadStreetarts() {
    this.graffmapService.getStreetarts().subscribe((response) => {
      for (const streetart of response.streetarts) {
        this.streetarts.push(streetart);
        const newMarker = this.leafletHelper.createMarker(
          streetart,
          'streetart'
        );
        this.markers.push(newMarker);
      }
    });
  }
}
