import { FormHelper } from 'src/app/helper/form.helper';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { GraffmapService } from './../../../services/graffmap.service';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'add-marker-page',
  templateUrl: './add-marker-page.component.html',
  styleUrls: ['./add-marker-page.component.scss'],
})
export class AddMarkerPageComponent implements OnInit {
  wallForm: FormGroup;
  shopForm: FormGroup;
  streetartForm: FormGroup;
  type: string;
  created = false;

  constructor(
    private formHelper: FormHelper,
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private graffmapService: GraffmapService,
    private snackbarHelper: SnackbarHelper
  ) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.loadMarkerType();
    if (this.type === 'wall') {
      this.wallForm = this.graffmapService.initWallForm();
    } else if (this.type === 'shop') {
      this.shopForm = this.graffmapService.initShopForm();
    } else if (this.type === 'streetart') {
      this.streetartForm = this.graffmapService.initStreetartForm();
    }
  }

  loadMarkerType() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('type')) {
        this.type = paramMap.get('type');
      }
    });
  }

  setCoordinates(event: any) {
    if (this.type === 'wall') {
      this.wallForm.get('lat').setValue(event.lat);
      this.wallForm.get('lng').setValue(event.lng);
    } else if (this.type === 'shop') {
      this.shopForm.get('lat').setValue(event.lat);
      this.shopForm.get('lng').setValue(event.lng);
    } else if (this.type === 'streetart') {
      this.streetartForm.get('lat').setValue(event.lat);
      this.streetartForm.get('lng').setValue(event.lng);
    }
  }

  submitWall() {
    if (
      this.wallForm.valid &&
      this.wallForm.get('lat').value !== null &&
      this.wallForm.get('lng').value !== null
    ) {
      const wall = this.wallForm.value;
      this.graffmapService.addWall(wall);
      const message =
        this.wallForm.get('name').value +
        this.translateService.instant('snackSubmitted');
      this.snackbarHelper.openSnackBar(message);
      this.formHelper.disableControls(this.wallForm);
      this.created = true;
    } else if (
      this.wallForm.valid &&
      this.wallForm.get('lat').value === null &&
      this.wallForm.get('lng').value === null
    ) {
      const message = this.translateService.instant('setLocationPls');
      this.snackbarHelper.openSnackBar(message);
    } else {
      const message = this.translateService.instant('snackInvalid');
      this.snackbarHelper.openSnackBar(message);
      this.wallForm.get('name').markAsTouched();
      this.wallForm.get('description').markAsTouched();
      this.wallForm.get('legalState').markAsTouched();
    }
  }

  submitShop() {
    if (
      this.shopForm.valid &&
      this.shopForm.get('lat').value !== null &&
      this.shopForm.get('lng').value !== null
    ) {
      const shop = this.shopForm.value;
      this.graffmapService.addShop(shop);
      const message =
        this.shopForm.get('name').value +
        this.translateService.instant('snackSubmitted');
      this.snackbarHelper.openSnackBar(message);
      this.formHelper.disableControls(this.shopForm);
      this.created = true;
    } else if (
      this.shopForm.valid &&
      this.shopForm.get('lat').value === null &&
      this.shopForm.get('lng').value === null
    ) {
      const message = this.translateService.instant('setLocationPls');
      this.snackbarHelper.openSnackBar(message);
    } else {
      const message = this.translateService.instant('snackInvalid');
      this.snackbarHelper.openSnackBar(message);
      this.shopForm.get('name').markAsTouched();
      this.shopForm.get('description').markAsTouched();
    }
  }

  submitStreetart() {
    if (
      this.streetartForm.valid &&
      this.streetartForm.get('lat').value !== null &&
      this.streetartForm.get('lng').value !== null
    ) {
      const streetart = this.streetartForm.value;
      this.graffmapService.addStreetart(streetart);
      const message =
        this.streetartForm.get('name').value +
        this.translateService.instant('snackSubmitted');
      this.snackbarHelper.openSnackBar(message);
      this.formHelper.disableControls(this.streetartForm);
      this.created = true;
    } else if (
      this.streetartForm.valid &&
      this.streetartForm.get('lat').value === null &&
      this.streetartForm.get('lng').value === null
    ) {
      const message = this.translateService.instant('setLocationPls');
      this.snackbarHelper.openSnackBar(message);
    } else {
      const message = this.translateService.instant('snackInvalid');
      this.snackbarHelper.openSnackBar(message);
      this.streetartForm.get('name').markAsTouched();
      this.streetartForm.get('description').markAsTouched();
    }
  }
}
