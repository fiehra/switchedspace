import { ComponentsModule } from 'src/app/components/components.module';
import { FormHelper } from './../../../helper/form.helper';
import { GraffmapService } from './../../../services/graffmap.service';
import { of } from 'rxjs';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AddMarkerPageComponent } from './add-marker-page.component';
import { LeafletHelper } from './../../../helper/leaflet.helper';
import { RouterTestingModule } from '@angular/router/testing';
import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { MaterialModule } from './../../../utils/material/material.module';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { LegalState } from 'src/app/interfaces/graffmap-interface';
import { PagesModule } from '../../pages.module';

window.scrollTo = jest.fn();

describe('AddMarkerPageComponent', () => {
  let component: AddMarkerPageComponent;
  let fixture: ComponentFixture<AddMarkerPageComponent>;
  let snackbarHelper: SnackbarHelper;
  let graffmapService: GraffmapService;
  let formHelper: FormHelper;
  let route: ActivatedRoute;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [
          FormBuilder,
          FormHelper,
          SnackbarHelper,
          LeafletHelper,
          {
            provide: ActivatedRoute,
            useValue: {
              paramMap: of(convertToParamMap({ type: 'wall' })),
            },
          },
        ],
        imports: [
          PagesModule,
          ComponentsModule,
          LeafletModule,
          MaterialModule,
          RouterTestingModule,
          BrowserAnimationsModule,
          ReactiveFormsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMarkerPageComponent);
    component = fixture.componentInstance;
    snackbarHelper = TestBed.inject(SnackbarHelper);
    graffmapService = TestBed.inject(GraffmapService);
    formHelper = TestBed.inject(FormHelper);
    route = TestBed.inject(ActivatedRoute);
    fixture.detectChanges();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit type === wall', () => {
    spyOn(component, 'loadMarkerType').and.callThrough();
    spyOn(graffmapService, 'initWallForm').and.callThrough();
    component.type = 'wall';
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.loadMarkerType).toHaveBeenCalled();
    expect(graffmapService.initWallForm).toHaveBeenCalled();
  });

  it('ngOnInit type === shop', () => {
    spyOn(route.paramMap, 'subscribe').and.returnValue(of('shop'));
    spyOn(component, 'loadMarkerType').and.callThrough();
    spyOn(graffmapService, 'initShopForm').and.callThrough();
    component.type = 'shop';
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.loadMarkerType).toHaveBeenCalled();
    expect(graffmapService.initShopForm).toHaveBeenCalled();
  });

  it('ngOnInit type === streetart', () => {
    spyOn(route.paramMap, 'subscribe').and.returnValue(of('streetart'));
    spyOn(component, 'loadMarkerType').and.callThrough();
    spyOn(graffmapService, 'initStreetartForm').and.callThrough();
    component.type = 'streetart';
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.loadMarkerType).toHaveBeenCalled();
    expect(graffmapService.initStreetartForm).toHaveBeenCalled();
  });

  it('loadMarkerType', () => {
    component.loadMarkerType();
    expect(component.type).toBe('wall');
  });

  it('submitWall form valid', () => {
    const wallMock = {
      id: 'id',
      name: 'name',
      description: 'description',
      legalState: 'legal' as LegalState,
      lat: 35.6895,
      lng: 139.69171,
      statusUpdates: [],
    };
    component.wallForm = graffmapService.mapWallToForm(wallMock);
    fixture.detectChanges();

    expect(component.created).toBe(false);
    spyOn(graffmapService, 'addWall').and.returnValue({});
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    spyOn(formHelper, 'disableControls').and.stub();
    component.submitWall();
    fixture.detectChanges();
    expect(graffmapService.addWall).toHaveBeenCalled();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalled();
  });

  it('submitWall form valid position null', () => {
    const wallMock = {
      id: 'id',
      name: 'name',
      description: 'description',
      legalState: 'legal' as LegalState,
      lat: null,
      lng: null,
      statusUpdates: [],
    };
    component.wallForm = graffmapService.mapWallToForm(wallMock);
    fixture.detectChanges();

    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.submitWall();
    fixture.detectChanges();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });

  it('submitwall form invalid', () => {
    component.type = 'wall';
    component.ngOnInit();
    fixture.detectChanges();

    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.submitWall();
    fixture.detectChanges();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });

  it('setCoordinates wall', () => {
    const mockEvent = {
      lat: 35.6895,
      lng: 139.69171,
    };
    fixture.detectChanges();

    component.setCoordinates(mockEvent);
    fixture.detectChanges();
    expect(component.wallForm.get('lat').value).toBe(35.6895);
    expect(component.wallForm.get('lng').value).toBe(139.69171);
  });

  it('setCoordinates shop', () => {
    spyOn(route.paramMap, 'subscribe').and.returnValue(of('shop'));
    component.type = 'shop';
    component.ngOnInit();
    const mockEvent = {
      lat: 35.6895,
      lng: 139.69171,
    };
    fixture.detectChanges();

    component.setCoordinates(mockEvent);
    fixture.detectChanges();
    expect(component.shopForm.get('lat').value).toBe(35.6895);
    expect(component.shopForm.get('lng').value).toBe(139.69171);
  });

  it('setCoordinates streetart', () => {
    spyOn(route.paramMap, 'subscribe').and.returnValue(of('streetart'));
    component.type = 'streetart';
    component.ngOnInit();
    const mockEvent = {
      lat: 35.6895,
      lng: 139.69171,
    };
    fixture.detectChanges();

    component.setCoordinates(mockEvent);
    fixture.detectChanges();
    expect(component.streetartForm.get('lat').value).toBe(35.6895);
    expect(component.streetartForm.get('lng').value).toBe(139.69171);
  });

  it('submitShop form valid', () => {
    spyOn(route.paramMap, 'subscribe').and.returnValue(of('shop'));
    component.type = 'shop';
    component.ngOnInit();
    const shopMock = {
      id: 'id',
      name: 'name',
      description: 'description',
      website: 'shop.com',
      lat: 35.6895,
      lng: 139.69171,
    };
    component.shopForm = graffmapService.mapShopToForm(shopMock);
    fixture.detectChanges();

    expect(component.created).toBe(false);
    spyOn(graffmapService, 'addShop').and.returnValue({});
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    spyOn(formHelper, 'disableControls').and.stub();
    component.submitShop();
    fixture.detectChanges();
    expect(graffmapService.addShop).toHaveBeenCalled();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalled();
  });

  it('submitShop form valid position null', () => {
    spyOn(route.paramMap, 'subscribe').and.returnValue(of('shop'));
    component.type = 'shop';
    component.ngOnInit();
    const shopMock = {
      id: 'id',
      name: 'name',
      description: 'description',
      website: 'shop.com',
      lat: null,
      lng: null,
    };
    component.shopForm = graffmapService.mapShopToForm(shopMock);
    fixture.detectChanges();

    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.submitShop();
    fixture.detectChanges();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });

  it('submitShop form invalid', () => {
    spyOn(route.paramMap, 'subscribe').and.returnValue(of('shop'));
    component.type = 'shop';
    component.ngOnInit();
    fixture.detectChanges();
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.submitShop();
    fixture.detectChanges();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });

  it('submitStreetart form valid', () => {
    spyOn(route.paramMap, 'subscribe').and.returnValue(of('streetart'));
    component.type = 'streetart';
    component.ngOnInit();
    const streetartMock = {
      id: 'id',
      name: 'name',
      description: 'description',
      link: 'artist.com',
      lat: 35.6895,
      lng: 139.69171,
    };
    component.streetartForm = graffmapService.mapStreetartToForm(streetartMock);
    fixture.detectChanges();

    expect(component.created).toBe(false);
    spyOn(graffmapService, 'addStreetart').and.returnValue({});
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    spyOn(formHelper, 'disableControls').and.stub();
    component.submitStreetart();
    fixture.detectChanges();
    expect(graffmapService.addStreetart).toHaveBeenCalled();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalled();
  });

  it('submitStreetart form valid position null', () => {
    spyOn(route.paramMap, 'subscribe').and.returnValue(of('streetart'));
    component.type = 'streetart';
    component.ngOnInit();
    const streetartMock = {
      id: 'id',
      name: 'name',
      description: 'description',
      link: 'artist.com',
      lat: null,
      lng: null,
    };
    component.streetartForm = graffmapService.mapStreetartToForm(streetartMock);
    fixture.detectChanges();

    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.submitStreetart();
    fixture.detectChanges();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });

  it('submitStreetart form invalid', () => {
    spyOn(route.paramMap, 'subscribe').and.returnValue(of('streetart'));
    component.type = 'streetart';
    component.ngOnInit();
    fixture.detectChanges();

    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.submitStreetart();
    fixture.detectChanges();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });
});
