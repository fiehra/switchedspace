import { ComponentsModule } from 'src/app/components/components.module';
import { ViewShopPageComponent } from './view-shop-page.component';
import { GraffmapService } from './../../../services/graffmap.service';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { of } from 'rxjs';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from './../../../utils/material/material.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletHelper } from './../../../helper/leaflet.helper';
import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { FormHelper } from './../../../helper/form.helper';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { PagesModule } from '../../pages.module';

describe('ViewShopPageComponent', () => {
  let component: ViewShopPageComponent;
  let fixture: ComponentFixture<ViewShopPageComponent>;
  let graffmapService: GraffmapService;
  let formHelper: FormHelper;
  let leafletHelper: LeafletHelper;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [
          FormBuilder,
          FormHelper,
          SnackbarHelper,
          LeafletHelper,
          {
            provide: ActivatedRoute,
            useValue: { paramMap: of(convertToParamMap({ id: 'fiehra' })) },
          },
        ],
        imports: [
          PagesModule,
          ComponentsModule,
          LeafletModule,
          MaterialModule,
          RouterTestingModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewShopPageComponent);
    component = fixture.componentInstance;
    graffmapService = TestBed.inject(GraffmapService);
    formHelper = TestBed.inject(FormHelper);
    leafletHelper = TestBed.inject(LeafletHelper);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(component, 'loadShop').and.stub();
    component.ngOnInit();
    fixture.detectChanges(), expect(component.loadShop).toHaveBeenCalled();
  });

  it('loadStreetart', () => {
    const shopMock = {
      id: 'fiehra',
      name: 'shop',
      description: 'shop',
      website: 'shop.com',
      lat: 35.6895,
      lng: 139.69171,
    };

    spyOn(graffmapService, 'getShopById').and.returnValue(of(shopMock));
    spyOn(formHelper, 'disableControls').and.stub();
    spyOn(component, 'loadShopMarker').and.stub();
    component.loadShop();
    fixture.detectChanges();
    expect(graffmapService.getShopById).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalled();
    expect(component.loadShopMarker).toHaveBeenCalled();
  });

  it('loadShopMarker', () => {
    spyOn(leafletHelper, 'createMarker').and.stub();
    component.loadShopMarker();
    fixture.detectChanges();
    expect(leafletHelper.createMarker).toHaveBeenCalled();
  });
});
