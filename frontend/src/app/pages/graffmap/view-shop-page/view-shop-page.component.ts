import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { LeafletHelper } from './../../../helper/leaflet.helper';
import { FormHelper } from 'src/app/helper/form.helper';
import { GraffmapService } from './../../../services/graffmap.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Shop } from 'src/app/interfaces/graffmap-interface';

@Component({
  selector: 'view-shop-page',
  templateUrl: './view-shop-page.component.html',
  styleUrls: ['./view-shop-page.component.scss'],
})
export class ViewShopPageComponent implements OnInit {
  private id: string;
  shop: Shop;
  shopForm: FormGroup;
  shopMarker = [];

  constructor(
    private route: ActivatedRoute,
    private graffmapService: GraffmapService,
    private formHelper: FormHelper,
    private leafletHelper: LeafletHelper,
  ) {}

  ngOnInit(): void {
    this.loadShop();
  }

  loadShop() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('id')) {
        this.id = paramMap.get('id');
        this.graffmapService.getShopById(this.id).subscribe((response) => {
          this.shop = response;
          this.shopForm = this.graffmapService.mapShopToForm(this.shop);
          this.formHelper.disableControls(this.shopForm);
          this.loadShopMarker();
        });
      }
    });
  }

  loadShopMarker() {
    this.shopMarker = [];
    const newMarker = this.leafletHelper.createMarker(this.shop, 'shop');
    this.shopMarker.push(newMarker);
  }
}
