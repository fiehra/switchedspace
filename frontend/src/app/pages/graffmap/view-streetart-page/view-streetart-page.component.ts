import { TranslateService } from '@ngx-translate/core';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { LeafletHelper } from './../../../helper/leaflet.helper';
import { FormHelper } from 'src/app/helper/form.helper';
import { FormGroup } from '@angular/forms';
import { GraffmapService } from './../../../services/graffmap.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Streetart } from 'src/app/interfaces/graffmap-interface';

@Component({
  selector: 'view-streetart-page',
  templateUrl: './view-streetart-page.component.html',
  styleUrls: ['./view-streetart-page.component.scss'],
})
export class ViewStreetartPageComponent implements OnInit {
  private id: string;
  streetart: Streetart;
  streetartForm: FormGroup;
  streetartMarker = [];

  constructor(
    private route: ActivatedRoute,
    private graffmapService: GraffmapService,
    private formHelper: FormHelper,
    private leafletHelper: LeafletHelper,
  ) {}

  ngOnInit(): void {
    this.loadStreetart();
  }

  loadStreetart() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('id')) {
        this.id = paramMap.get('id');
        this.graffmapService.getStreetartById(this.id).subscribe((response) => {
          this.streetart = response;
          this.streetartForm = this.graffmapService.mapStreetartToForm(
            this.streetart
          );
          this.formHelper.disableControls(this.streetartForm);
          this.loadStreetartMarker();
        });
      }
    });
  }

  loadStreetartMarker() {
    this.streetartMarker = [];
    const newMarker = this.leafletHelper.createMarker(
      this.streetart,
      'streetart'
    );
    this.streetartMarker.push(newMarker);
  }
}
