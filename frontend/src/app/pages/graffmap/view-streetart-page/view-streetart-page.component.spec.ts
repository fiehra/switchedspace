import { GraffmapService } from './../../../services/graffmap.service';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { of } from 'rxjs';
import { ViewStreetartPageComponent } from './view-streetart-page.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from './../../../utils/material/material.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletHelper } from './../../../helper/leaflet.helper';
import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { FormHelper } from './../../../helper/form.helper';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { ComponentsModule } from 'src/app/components/components.module';
import { PagesModule } from '../../pages.module';

describe('ViewStreetartPageComponent', () => {
  let component: ViewStreetartPageComponent;
  let fixture: ComponentFixture<ViewStreetartPageComponent>;
  let graffmapService: GraffmapService;
  let formHelper: FormHelper;
  let leafletHelper: LeafletHelper;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [
          FormBuilder,
          FormHelper,
          SnackbarHelper,
          LeafletHelper,
          {
            provide: ActivatedRoute,
            useValue: { paramMap: of(convertToParamMap({ id: 'fiehra' })) },
          },
        ],
        imports: [
          ComponentsModule,
          PagesModule,
          LeafletModule,
          MaterialModule,
          RouterTestingModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewStreetartPageComponent);
    component = fixture.componentInstance;
    graffmapService = TestBed.inject(GraffmapService);
    formHelper = TestBed.inject(FormHelper);
    leafletHelper = TestBed.inject(LeafletHelper);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(component, 'loadStreetart').and.stub();
    component.ngOnInit();
    fixture.detectChanges(), expect(component.loadStreetart).toHaveBeenCalled();
  });

  it('loadStreetart', () => {
    const streetartMock = {
      id: 'fiehra',
      name: 'streetart',
      description: 'streetart',
      link: 'agas.meno',
      lat: 35.6895,
      lng: 139.69171,
    };

    spyOn(graffmapService, 'getStreetartById').and.returnValue(of(streetartMock));
    spyOn(formHelper, 'disableControls').and.stub();
    spyOn(component, 'loadStreetartMarker').and.stub();
    component.loadStreetart();
    fixture.detectChanges();
    expect(graffmapService.getStreetartById).toHaveBeenCalled();
    expect(formHelper.disableControls).toHaveBeenCalled();
    expect(component.loadStreetartMarker).toHaveBeenCalled();
  });

  it('loadStreetartMarker', () => {
    spyOn(leafletHelper, 'createMarker').and.stub();
    component.loadStreetartMarker();
    fixture.detectChanges();
    expect(leafletHelper.createMarker).toHaveBeenCalled();
  });
});
