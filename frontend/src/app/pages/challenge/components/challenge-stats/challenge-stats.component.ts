import { ChallengeService } from './../../../../services/challenge.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'challenge-stats',
  templateUrl: './challenge-stats.component.html',
  styleUrls: ['./challenge-stats.component.scss'],
})
export class ChallengeStatsComponent implements OnInit {
  averageTime: number;
  difference: number;
  record: number;
  streak: number;

  constructor(private challengeService: ChallengeService) {}

  ngOnInit(): void {
    this.loadAverage();
    this.getLatestChallenge();
    this.getMaxRecord();
    this.getStreak();
  }

  loadAverage() {
    this.challengeService.getAverageTime().subscribe((res) => {
      this.averageTime = res.averageTime;
    });
  }

  getLatestChallenge() {
    this.challengeService.getDateDifference().subscribe((res) => {
      this.difference = res.difference;
    });
  }

  getMaxRecord() {
    this.challengeService.getChallengeRecord().subscribe((res) => {
      this.record = res.record;
    });
  }

  getStreak() {
    this.challengeService.getCurrentStreak().subscribe((res) => {
      this.streak = res.streak;
    });
  }

}
