import { of } from 'rxjs';
import { ChallengeService } from './../../../../services/challenge.service';
import { FormBuilder } from '@angular/forms';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from './../../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChallengeStatsComponent } from './challenge-stats.component';

describe('ChallengeStatsComponent', () => {
  let component: ChallengeStatsComponent;
  let fixture: ComponentFixture<ChallengeStatsComponent>;
  let challengeService: ChallengeService;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [ChallengeStatsComponent],
      providers: [FormBuilder],
      imports: [
        MaterialModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (http: HttpClient) =>
              new TranslateHttpLoader(
                http,
                'assets/i18n/',
                '.json?build_id=build_id_replacement_placeholder'
              ),
            deps: [HttpClient],
          },
        }),
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengeStatsComponent);
    component = fixture.componentInstance;
    challengeService = TestBed.inject(ChallengeService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(component, 'loadAverage').and.stub();
    spyOn(component, 'getLatestChallenge').and.stub();
    spyOn(component, 'getMaxRecord').and.stub();
    spyOn(component, 'getStreak').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.loadAverage).toHaveBeenCalled();
    expect(component.getLatestChallenge).toHaveBeenCalled();
    expect(component.getMaxRecord).toHaveBeenCalled();
    expect(component.getStreak).toHaveBeenCalled();
  });

  it('loadAverage', () => {
    const mockResponse = {
      message: 'fetching average successful',
      averageTime: 10,
    };
    spyOn(challengeService, 'getAverageTime').and.returnValue(of(mockResponse));
    component.loadAverage();
    fixture.detectChanges();
    expect(challengeService.getAverageTime).toHaveBeenCalled();
  });

  it('getLatestChallenge', () => {
    const mockResponse = {
      message: 'calculating difference successful',
      difference: 1,
    };
    spyOn(challengeService, 'getDateDifference').and.returnValue(of(mockResponse));
    component.getLatestChallenge();
    fixture.detectChanges();
    expect(challengeService.getDateDifference).toHaveBeenCalled();
    expect(component.difference).toBe(1);
  });

  it('getMaxRecord', () => {
    const mockResponse = {
      message: 'fetching record successful',
      record: 10,
    };
    spyOn(challengeService, 'getChallengeRecord').and.returnValue(of(mockResponse));
    component.getMaxRecord();
    fixture.detectChanges();
    expect(challengeService.getChallengeRecord).toHaveBeenCalled();
    expect(component.record).toBe(10);
  });

  it('getStreak', () => {
    const mockResponse = {
      message: 'fetching record successful',
      streak: 2,
    };
    spyOn(challengeService, 'getCurrentStreak').and.returnValue(of(mockResponse));
    component.getStreak();
    fixture.detectChanges();
    expect(challengeService.getCurrentStreak).toHaveBeenCalled();
    expect(component.streak).toBe(2);
  });

});
