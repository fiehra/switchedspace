import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'challenge-list-element',
  templateUrl: './challenge-list-element.component.html',
  styleUrls: ['./challenge-list-element.component.scss'],
})
export class ChallengeListElementComponent implements OnInit {
  @Input() challengeForm: FormGroup;
  date: string;

  constructor() {}

  ngOnInit(): void {
    this.date = new Date(this.challengeForm.get('date').value).toDateString();
  }
}
