import { Component } from '@angular/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader } from '@ngx-translate/core';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormBuilder, Validators } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChallengeListElementComponent } from './challenge-list-element.component';

@Component({
  selector: 'host-component',
  template: '<challenge-list-element [challengeForm]="challengeForm"></challenge-list-element>',
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  challengeForm = this.formBuilder.group({
    challengerId: ['fiehra', [Validators.required, Validators.email]],
    date: [new Date(), Validators.required],
    start: ['13:23:23 pm', Validators.required],
    finish: ['14:43:23 pm', Validators.required],
    time: [100, Validators.required],
    record: [10],
    active: [false, Validators.required],
    completed: [true, Validators.required],
    streak: [2, Validators.required],
  });
}
describe('ChallengeListElementComponent', () => {
  let component: ChallengeListElementComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ChallengeListElementComponent, TestHostComponent],
        providers: [],
        imports: [
          ReactiveFormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
