import { of } from 'rxjs';
import { Challenge } from './../../../../interfaces/challenge-interface';
import { ChallengeService } from './../../../../services/challenge.service';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './../../../../utils/material/material.module';
import { ChallengeListElementComponent } from './../challenge-list-element/challenge-list-element.component';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChallengeListComponent } from './challenge-list.component';

describe('ChallengeListComponent', () => {
  let component: ChallengeListComponent;
  let fixture: ComponentFixture<ChallengeListComponent>;
  let challengeService: ChallengeService;

  const challengeMock: Challenge = {
    id: '1',
    challengerId: 'fiehra',
    date: new Date(),
    start: '9:10',
    finish: '9:20',
    time: 10,
    active: false,
    completed: true,
    record: 5,
    streak: 1,
  };

  const challengeMock2: Challenge = {
    id: '2',
    challengerId: 'nurias',
    date: new Date(),
    start: '9:10',
    finish: '9:20',
    time: 10,
    active: false,
    completed: true,
    record: 6,
    streak: 2,
  };

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [ChallengeListComponent, ChallengeListElementComponent],
      providers: [],
      imports: [
        MaterialModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (http: HttpClient) =>
              new TranslateHttpLoader(
                http,
                'assets/i18n/',
                '.json?build_id=build_id_replacement_placeholder'
              ),
            deps: [HttpClient],
          },
        }),
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengeListComponent);
    component = fixture.componentInstance;
    challengeService = TestBed.inject(ChallengeService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(component, 'loadChallenges').and.stub();
    spyOn(component, 'loadLatest10Challenges').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.loadChallenges).toHaveBeenCalled();
    expect(component.loadLatest10Challenges).toHaveBeenCalled();
  });

  it('loadChallenges', () => {
    const response = {
      message: 'success',
      challengesList: [challengeMock, challengeMock2],
      maxPosts: 2,
    };
    fixture.detectChanges();
    spyOn(challengeService, 'getChallenges').and.returnValue(of(response));
    component.loadChallenges();
    fixture.detectChanges();
    expect(challengeService.getChallenges).toHaveBeenCalled();
  });

  it('loadLatest10Challenges', () => {
    const response = {
      message: 'success',
      challengesList: [challengeMock, challengeMock2],
    };
    fixture.detectChanges();
    spyOn(challengeService, 'getLatest10Challenges').and.returnValue(of(response));
    component.loadLatest10Challenges();
    fixture.detectChanges();
    expect(challengeService.getLatest10Challenges).toHaveBeenCalled();
  });
});
