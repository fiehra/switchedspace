import { FormGroup } from '@angular/forms';
import { ChallengeService } from './../../../../services/challenge.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'challenge-list',
  templateUrl: './challenge-list.component.html',
  styleUrls: ['./challenge-list.component.scss'],
})
export class ChallengeListComponent implements OnInit {
  allChallenges: any[] = [];
  latest10: any[] = [];
  totalChallenges = 0;

  constructor(private challengeService: ChallengeService) {}

  ngOnInit(): void {
    this.loadChallenges();
    this.loadLatest10Challenges();
  }

  loadChallenges() {
    this.challengeService.getChallenges().subscribe((response) => {
      for (const challenge of response.challengesList) {
        const challengeForm = this.challengeService.mapChallengeToForm(
          challenge
        );
        this.allChallenges.push(challengeForm);
      }
      this.totalChallenges = response.maxChallenges;
    });
  }

  loadLatest10Challenges() {
    this.challengeService.getLatest10Challenges().subscribe((response) => {
      for (const challenge of response.challengesList) {
        const challengeForm = this.challengeService.mapChallengeToForm(
          challenge
        );
        this.latest10.push(challengeForm);
      }
    });
  }
}
