import { of } from 'rxjs';
import { ChallengeService } from './../../../../services/challenge.service';
import { FormHelper } from 'src/app/helper/form.helper';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../../../../utils/material/material.module';
import { SwitchedInputComponent } from './../../../../components/htmlComponents/switched-input/switched-input.component';
import { FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChallengeFormComponent } from './challenge-form.component';

@Component({
  selector: 'host-component',
  template: '<challenge-form [challengeForm]="challengeForm"></challenge-form>',
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  challengeForm = this.formBuilder.group({
    challengerId: ['fiehra', [Validators.required, Validators.email]],
    date: [new Date(), Validators.required],
    start: [null, Validators.required],
    finish: [null, Validators.required],
    time: [null, Validators.required],
    active: [false, Validators.required],
    completed: [false, Validators.required],
    record: [false],
    streak: [null, Validators.required],
  });
}
describe('ChallengeFormComponent', () => {
  let component: ChallengeFormComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let challengeService: ChallengeService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [
          ChallengeFormComponent,
          TestHostComponent,
          SwitchedInputComponent,
        ],
        providers: [FormHelper],
        imports: [
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder'
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    challengeService = TestBed.inject(ChallengeService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    const mockResponse = {
      message: 'fetching record successful',
      difference: 1,
    };
    spyOn(challengeService, 'getDateDifference').and.returnValue(of(mockResponse));
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.active).toBe(false);
    expect(component.completed).toBe(false);
    expect(challengeService.getDateDifference).toHaveBeenCalled();
    expect(component.difference).toBe(1);
  });

  it('startChallenge', () => {
    spyOn(component, 'setActivityStatus').and.stub();
    component.startChallenge();
    fixture.detectChanges();
    expect(component.setActivityStatus).toHaveBeenCalledWith(true);
  });

  it('finishChallenge difference === 1', () => {
    const mockResponse = {
      message: 'fetching record successful',
      streak: 5,
    };
    component.difference = 1;
    spyOn(component, 'setActivityStatus').and.stub();
    spyOn(challengeService, 'getCurrentStreak').and.returnValue(of(mockResponse));
    component.finishChallenge();
    fixture.detectChanges();
    expect(component.setActivityStatus).toHaveBeenCalledWith(false);
    expect(challengeService.getCurrentStreak).toHaveBeenCalled();
    expect(component.challengeForm.get('streak').value).toBe(6);
  });
  
  it('finishChallenge difference !== 1', () => {
    component.difference = 2;
    spyOn(component, 'setActivityStatus').and.stub();
    component.finishChallenge();
    fixture.detectChanges();
    expect(component.setActivityStatus).toHaveBeenCalledWith(false);
    expect(component.challengeForm.get('streak').value).toBe(1);
  });

  it('emitFinished', () => {
    spyOn(component.finishClicked, 'emit').and.stub();
    component.emitFinished();
    fixture.detectChanges();
    expect(component.finishClicked.emit).toHaveBeenCalled();
  });

  it('setActivityStatus', () => {
    expect(component.active).toBe(false);
    component.setActivityStatus(true);
    fixture.detectChanges();
    expect(component.active).toBe(true);
  });
});
