import { ChallengeService } from './../../../../services/challenge.service';
import { EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'challenge-form',
  templateUrl: './challenge-form.component.html',
  styleUrls: ['./challenge-form.component.scss'],
})
export class ChallengeFormComponent implements OnInit {
  active: boolean;
  completed: boolean;
  start: number;
  finish: number;
  difference: number = 0;

  @Input() challengeForm: FormGroup;
  @Output() startClicked = new EventEmitter();
  @Output() finishClicked = new EventEmitter();

  constructor(private challengeService: ChallengeService) {}

  ngOnInit(): void {
    this.active = this.challengeForm.get('active').value;
    this.completed = this.challengeForm.get('completed').value;
    this.challengeService.getDateDifference().subscribe((res) => {
      this.difference = res.difference;
    });
  }

  startChallenge() {
    const now = new Date().toLocaleTimeString();
    this.start = new Date().getTime();
    const date = new Date();
    this.challengeForm.get('date').setValue(date);
    this.challengeForm.get('start').setValue(now);
    this.setActivityStatus(true);
  }

  finishChallenge() {
    const now = new Date().toLocaleTimeString();
    this.finish = new Date().getTime();
    const time = +((this.finish - this.start) / 60000).toFixed(1);
    this.challengeForm.get('finish').setValue(now);
    this.challengeForm.get('time').setValue(time);
    this.challengeForm.get('completed').setValue(true);
    if(this.difference === 1) {
      let streak;
      this.challengeService.getCurrentStreak().subscribe((response) => {
        streak = response.streak + 1;
        this.challengeForm.get('streak').setValue(streak);
      });
    } else {
      this.challengeForm.get('streak').setValue(1);
    }
    this.challengeForm.updateValueAndValidity();
    this.completed = true;
    this.setActivityStatus(false);
  }
  
  emitFinished() {
    this.finishClicked.emit();
  }

  setActivityStatus(state: boolean) {
    this.active = state;
    this.challengeForm.get('active').setValue(state);
  }
}
