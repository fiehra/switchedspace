import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'challenge-overview-page',
  templateUrl: './challenge-overview-page.component.html',
  styleUrls: ['./challenge-overview-page.component.scss']
})
export class ChallengeOverviewPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

}
