import { ChallengeStatsComponent } from './../components/challenge-stats/challenge-stats.component';
import { ChallengeListComponent } from './../components/challenge-list/challenge-list.component';
import { ChallengeListElementComponent } from './../components/challenge-list-element/challenge-list-element.component';
import { FormBuilder } from '@angular/forms';
import { MaterialModule } from './../../../utils/material/material.module';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { FormHelper } from './../../../helper/form.helper';
import { FooterComponent } from './../../../components/footer/footer.component';
import { ToolbarComponent } from './../../../components/toolbar/toolbar.component';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChallengeOverviewPageComponent } from './challenge-overview-page.component';
import { PagesModule } from '../../pages.module';
import { ComponentsModule } from 'src/app/components/components.module';

window.scrollTo = jest.fn();

describe('ChallengeOverviewPageComponent', () => {
  let component: ChallengeOverviewPageComponent;
  let fixture: ComponentFixture<ChallengeOverviewPageComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [FormHelper, SnackbarHelper, FormBuilder],
        imports: [
          PagesModule,
          ComponentsModule,
          MaterialModule,
          RouterTestingModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengeOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
