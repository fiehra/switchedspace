import { AuthService } from 'src/app/services/auth.service';
import { ChallengeService } from './../../../services/challenge.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'create-challenge-page',
  templateUrl: './create-challenge-page.component.html',
  styleUrls: ['./create-challenge-page.component.scss']
})
export class CreateChallengePageComponent implements OnInit {

  challengeForm = new FormGroup({
    challengerId: new FormControl(this.authService.getUserId(), [Validators.required]),
    date: new FormControl(null, [Validators.required]),
    start: new FormControl(null),
    finish: new FormControl(null),
    time: new FormControl(null),
    active: new FormControl(false),
    completed: new FormControl(false),
    record: new FormControl(null),
    streak: new FormControl(null),
  });

  challengeId: string;

  constructor(private challengeService: ChallengeService, private authService: AuthService) { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

  submitChallenge() {
    const challenge = this.challengeForm.value;
    this.challengeService.createChallenge(challenge);
  }


}
