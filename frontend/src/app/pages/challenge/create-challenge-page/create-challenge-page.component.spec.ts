import { ChallengeService } from './../../../services/challenge.service';
import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './../../../utils/material/material.module';
import { FormHelper } from './../../../helper/form.helper';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CreateChallengePageComponent } from './create-challenge-page.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { PagesModule } from '../../pages.module';

window.scrollTo = jest.fn();

describe('CreateChallengePageComponent', () => {
  let component: CreateChallengePageComponent;
  let fixture: ComponentFixture<CreateChallengePageComponent>;
  let challengeService: ChallengeService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [FormHelper, SnackbarHelper],
        imports: [
          ComponentsModule,
          PagesModule,
          MaterialModule,
          ReactiveFormsModule,
          RouterTestingModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateChallengePageComponent);
    component = fixture.componentInstance;
    challengeService = TestBed.inject(ChallengeService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('submitChallenge', () => {
    spyOn(challengeService, 'createChallenge').and.stub();
    component.submitChallenge();
    fixture.detectChanges();
    expect(challengeService.createChallenge).toHaveBeenCalled();
  });
});
