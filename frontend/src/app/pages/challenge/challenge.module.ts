import { ChallengeOverviewPageComponent } from './challenge-overview-page/challenge-overview-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components/components.module';
import { UtilsModule } from '../../utils/utils.module';
import { NgModule } from '@angular/core';
import { ChallengeFormComponent } from './components/challenge-form/challenge-form.component';
import { CreateChallengePageComponent } from './create-challenge-page/create-challenge-page.component';
import { ChallengeListComponent } from './components/challenge-list/challenge-list.component';
import { ChallengeListElementComponent } from './components/challenge-list-element/challenge-list-element.component';
import { ChallengeStatsComponent } from './components/challenge-stats/challenge-stats.component';

@NgModule({
  declarations: [ChallengeOverviewPageComponent, ChallengeFormComponent, CreateChallengePageComponent, ChallengeListComponent, ChallengeListElementComponent, ChallengeStatsComponent],
  imports: [UtilsModule, ComponentsModule, TranslateModule],
  exports: [ChallengeOverviewPageComponent, CreateChallengePageComponent],
})
export class ChallengeModule {}
