import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'changelog-page',
  templateUrl: './changelog-page.component.html',
  styleUrls: ['./changelog-page.component.scss'],
})
export class ChangelogPageComponent implements OnInit {
  version: string;
  constructor() {}

  ngOnInit(): void {
    this.version = environment.VERSION;
    window.scrollTo(0, 0);
  }
}
