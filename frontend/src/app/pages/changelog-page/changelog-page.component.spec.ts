import { RouterTestingModule } from '@angular/router/testing';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { FormBuilder } from '@angular/forms';
import { FormHelper } from './../../helper/form.helper';
import { MaterialModule } from './../../utils/material/material.module';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChangelogPageComponent } from './changelog-page.component';
import { PagesModule } from '../pages.module';
import { ComponentsModule } from 'src/app/components/components.module';

window.scrollTo = jest.fn();
describe('ChangelogPageComponent', () => {
  let component: ChangelogPageComponent;
  let fixture: ComponentFixture<ChangelogPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [],
      providers: [FormHelper, FormBuilder, SnackbarHelper],
      imports: [
        PagesModule,
        ComponentsModule,
        MaterialModule,
        RouterTestingModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (http: HttpClient) =>
              new TranslateHttpLoader(
                http,
                'assets/i18n/',
                '.json?build_id=build_id_replacement_placeholder',
              ),
            deps: [HttpClient],
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangelogPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
