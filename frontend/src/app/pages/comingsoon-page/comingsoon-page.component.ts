import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'comingsoon-page',
  templateUrl: './comingsoon-page.component.html',
  styleUrls: ['./comingsoon-page.component.scss']
})
export class ComingsoonPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

}
