import { HomePageComponent } from './home-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components/components.module';
import { UtilsModule } from '../../utils/utils.module';
import { NgModule } from '@angular/core';
import { HeaderComponent } from './components/header/header.component';
import { ForumSectionComponent } from './components/forum-section/forum-section.component';
import { DonateSectionComponent } from './components/donate-section/donate-section.component';
import { ThanksSectionComponent } from './components/thanks-section/thanks-section.component';
import { LearnSectionComponent } from './components/learn-section/learn-section.component';
import { ToolsSectionComponent } from './components/tools-section/tools-section.component';

@NgModule({
  declarations: [
    HomePageComponent,
    HeaderComponent,
    ForumSectionComponent,
    DonateSectionComponent,
    ThanksSectionComponent,
    LearnSectionComponent,
    ToolsSectionComponent,
  ],
  imports: [
    UtilsModule,
    ComponentsModule,
    TranslateModule
  ],
  exports: [
    HomePageComponent
  ]
})

export class HomePageModule {}