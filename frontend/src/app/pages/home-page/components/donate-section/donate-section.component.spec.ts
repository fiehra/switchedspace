import { DonateButtonComponent } from './../../../../components/donate-button/donate-button.component';
import { DonationHelper } from './../../../../helper/donation.helper';
import { SnackbarHelper } from './../../../../helper/snackbar.helper';
import { FormHelper } from 'src/app/helper/form.helper';
import { MaterialModule } from './../../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { DonateSectionComponent } from './donate-section.component';

describe('DonateSectionComponent', () => {
  let component: DonateSectionComponent;
  let fixture: ComponentFixture < DonateSectionComponent > ;
  let donationHelper: DonationHelper;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
        declarations: [
          DonateSectionComponent,
          DonateButtonComponent
        ],
        providers: [
          FormHelper,
          SnackbarHelper,
          DonationHelper
        ],
        imports: [
          MaterialModule,
          RouterTestingModule,
          HttpClientTestingModule,
          BrowserAnimationsModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
              deps: [HttpClient]
            }
          })
        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonateSectionComponent);
    component = fixture.componentInstance;
    donationHelper = TestBed.inject(DonationHelper);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  

});