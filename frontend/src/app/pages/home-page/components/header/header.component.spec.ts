import { FormHelper } from 'src/app/helper/form.helper';
import { MaterialModule } from './../../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let authService: AuthService;
  let router: Router;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      providers: [FormHelper, SnackbarHelper],
      imports: [
        MaterialModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (http: HttpClient) =>
              new TranslateHttpLoader(
                http,
                'assets/i18n/',
                '.json?build_id=build_id_replacement_placeholder',
              ),
            deps: [HttpClient],
          },
        }),
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(authService, 'getLoggedIn').and.returnValue(true);
    spyOn(authService, 'getUsername').and.stub();
    spyOn(authService, 'getUserId').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getLoggedIn).toHaveBeenCalled();
    expect(authService.getUsername).toHaveBeenCalled();
    expect(authService.getUserId).toHaveBeenCalled();
  });

  it('routeToMyProfile', () => {
    jest.spyOn(router, 'navigate').mockReturnValue(Promise.resolve(true));
    component.routeToMyProfile();
    fixture.detectChanges();
    expect(router.navigate).toHaveBeenCalled();
  });

  it('showMoreScroll', () => {
    spyOn(component.scrollClicked, 'emit').and.stub();
    component.showMoreScroll();
    fixture.detectChanges();
    expect(component.scrollClicked.emit).toHaveBeenCalled();
  });
});
