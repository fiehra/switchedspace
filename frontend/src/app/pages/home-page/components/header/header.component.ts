import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  loggedIn: boolean = false;
  userId: string;
  username: string;
  @Output() scrollClicked = new EventEmitter();

  constructor(private router: Router, private authService: AuthService) {}

  ngOnInit() {
    this.loggedIn = this.authService.getLoggedIn();
    if (this.loggedIn) {
      this.username = this.authService.getUsername();
      this.userId = this.authService.getUserId();
    }
  }

  routeToMyProfile() {
    this.router.navigate(['/myProfile/' + this.userId]);
  }

  showMoreScroll() {
    this.scrollClicked.emit();
  }
}
