import { WorkshopsPageComponent } from "./workshops-page.component";
import { WorkshopInfoSectionComponent } from "./components/workshop-info-section/workshop-info-section.component";
import { TranslateModule } from "@ngx-translate/core";
import { ComponentsModule } from "src/app/components/components.module";
import { UtilsModule } from "../../utils/utils.module";
import { NgModule } from "@angular/core";
import { WorkshopTopicsSectionComponent } from "./components/workshop-topics-section/workshop-topics-section.component";
import { WorkshopImagesSectionComponent } from './components/workshop-images-section/workshop-images-section.component';

@NgModule({
  declarations: [
    WorkshopInfoSectionComponent,
    WorkshopsPageComponent,
    WorkshopTopicsSectionComponent,
    WorkshopImagesSectionComponent
  ],
  imports: [UtilsModule, ComponentsModule, TranslateModule],
  exports: [WorkshopsPageComponent],
})
export class WorkshopsModule {}
