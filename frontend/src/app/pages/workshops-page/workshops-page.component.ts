import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'workshops-page',
  templateUrl: './workshops-page.component.html',
  styleUrls: ['./workshops-page.component.scss']
})
export class WorkshopsPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

}
