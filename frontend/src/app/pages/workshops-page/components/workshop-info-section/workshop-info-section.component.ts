import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'workshop-info-section',
  templateUrl: './workshop-info-section.component.html',
  styleUrls: ['./workshop-info-section.component.scss']
})
export class WorkshopInfoSectionComponent implements OnInit {

  currentInfo: string = 'festival';

  constructor() { }

  ngOnInit(): void {
  }

  setInfo(currentInfo: string) {
    this.currentInfo = currentInfo;
  }

}
