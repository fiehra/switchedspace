import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'workshop-topics-section',
  templateUrl: './workshop-topics-section.component.html',
  styleUrls: ['./workshop-topics-section.component.scss']
})
export class WorkshopTopicsSectionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
