import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'workshop-images-section',
  templateUrl: './workshop-images-section.component.html',
  styleUrls: ['./workshop-images-section.component.scss']
})
export class WorkshopImagesSectionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
