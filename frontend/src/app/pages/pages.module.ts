import { ChallengeModule } from './challenge/challenge.module';
import { WorkshopsModule } from './workshops-page/workshops.module';
import { TricksModule } from './tips-tricks/tricks.module';
import { GraffmapModule } from './graffmap/graffmap.module';
import { ApplicationModule } from './application/application.module';
import { ComboModule } from './combo-page/combo.module';
import { DeveloperModule } from './developer-page/developer.module';
import { HomePageModule } from './home-page/home-page.module';
import { GalleryModule } from './gallery-page/gallery.module';
import { ContactModule } from './contact-page/contact.module';
import { ProfileModule } from './profile/profile.module';
import { SignupModule } from './signup-page/signup.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { UtilsModule } from './../utils/utils.module';
import { LoginModule } from './login-page/login.module';

import { NgModule } from '@angular/core';
import { ForumModule } from './forum/forum.module';
import { LogoutPageComponent } from './logout-page/logout-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { ImprintPageComponent } from './imprint-page/imprint-page.component';
import { PrivacyPageComponent } from './privacy-page/privacy-page.component';
import { FaqPageComponent } from './faq-page/faq-page.component';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { ComingsoonPageComponent } from './comingsoon-page/comingsoon-page.component';
import { VerificationPageComponent } from './verification-page/verification-page.component';
import { ChangelogPageComponent } from './changelog-page/changelog-page.component';

@NgModule({
  declarations: [
    LogoutPageComponent,
    ImprintPageComponent,
    PrivacyPageComponent,
    FaqPageComponent,
    NotFoundPageComponent,
    ComingsoonPageComponent,
    VerificationPageComponent,
    ChangelogPageComponent,
  ],
  imports: [
    SignupModule,
    LoginModule,
    ComponentsModule,
    ProfileModule,
    UtilsModule,
    ForumModule,
    TranslateModule,
    ContactModule,
    HomePageModule,
    GalleryModule,
    DeveloperModule,
    ComboModule,
    ApplicationModule,
    GraffmapModule,
    TricksModule,
    WorkshopsModule,
    ChallengeModule,
  ],
  exports: [
    LoginModule,
    ProfileModule,
    SignupModule,
    HomePageModule,
    ForumModule,
    ContactModule,
    GalleryModule,
    DeveloperModule,
    ComboModule,
    GraffmapModule,
    TricksModule,
    ChallengeModule,
  ],
})
export class PagesModule {}
