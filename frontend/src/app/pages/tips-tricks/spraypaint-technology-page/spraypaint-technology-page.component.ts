import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'spraypaint-technology-page',
  templateUrl: './spraypaint-technology-page.component.html',
  styleUrls: ['./spraypaint-technology-page.component.scss']
})
export class SpraypaintTechnologyPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

}
