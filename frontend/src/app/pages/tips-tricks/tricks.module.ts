import { ListTricksPageComponent } from './list-tricks-page/list-tricks-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components/components.module';
import { UtilsModule } from '../../utils/utils.module';
import { NgModule } from '@angular/core';
import { DisposeCansPageComponent } from './dispose-cans-page/dispose-cans-page.component';
import { PreventCloggingPageComponent } from './prevent-clogging-page/prevent-clogging-page.component';
import { ChangingPressurePageComponent } from './changing-pressure-page/changing-pressure-page.component';
import { SpraypaintTechnologyPageComponent } from './spraypaint-technology-page/spraypaint-technology-page.component';
import { SafetyProtectionPageComponent } from './safety-protection-page/safety-protection-page.component';

@NgModule({
  declarations: [
    ListTricksPageComponent,
    DisposeCansPageComponent,
    PreventCloggingPageComponent,
    ChangingPressurePageComponent,
    SpraypaintTechnologyPageComponent,
    SafetyProtectionPageComponent
  ],
  imports: [
    UtilsModule,
    ComponentsModule,
    TranslateModule
  ],
  exports: [
    ListTricksPageComponent
  ]
})

export class TricksModule {}