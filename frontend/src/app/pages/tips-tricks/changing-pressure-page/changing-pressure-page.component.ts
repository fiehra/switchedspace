import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'changing-pressure-page',
  templateUrl: './changing-pressure-page.component.html',
  styleUrls: ['./changing-pressure-page.component.scss']
})
export class ChangingPressurePageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

}
