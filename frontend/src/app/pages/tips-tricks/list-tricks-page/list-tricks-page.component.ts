import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'list-tricks-page',
  templateUrl: './list-tricks-page.component.html',
  styleUrls: ['./list-tricks-page.component.scss']
})
export class ListTricksPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

}
