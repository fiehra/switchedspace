import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'safety-protection-page',
  templateUrl: './safety-protection-page.component.html',
  styleUrls: ['./safety-protection-page.component.scss']
})
export class SafetyProtectionPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

}
