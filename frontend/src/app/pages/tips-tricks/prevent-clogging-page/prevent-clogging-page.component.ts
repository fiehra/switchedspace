import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'prevent-clogging-page',
  templateUrl: './prevent-clogging-page.component.html',
  styleUrls: ['./prevent-clogging-page.component.scss']
})
export class PreventCloggingPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

}
