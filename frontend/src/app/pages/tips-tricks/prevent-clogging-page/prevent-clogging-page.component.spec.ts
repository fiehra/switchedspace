import { ComponentsModule } from 'src/app/components/components.module';
import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { FormBuilder } from '@angular/forms';
import { ScrollIndicatorComponent } from './../../../components/scroll-indicator/scroll-indicator.component';
import { ToolbarComponent } from './../../../components/toolbar/toolbar.component';
import { FooterComponent } from './../../../components/footer/footer.component';
import { ScrollTopComponent } from './../../../components/scroll-top/scroll-top.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from './../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PreventCloggingPageComponent } from './prevent-clogging-page.component';
import { PagesModule } from '../../pages.module';

window.scrollTo = jest.fn();

describe('PreventCloggingPageComponent', () => {
  let component: PreventCloggingPageComponent;
  let fixture: ComponentFixture<PreventCloggingPageComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [FormBuilder, SnackbarHelper],
        imports: [
          PagesModule,
          ComponentsModule,
          MaterialModule,
          RouterTestingModule,
          HttpClientTestingModule,
          BrowserAnimationsModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PreventCloggingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
