import { SnackbarHelper } from './../../../helper/snackbar.helper';
import { FormBuilder } from '@angular/forms';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from './../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DisposeCansPageComponent } from './dispose-cans-page.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { PagesModule } from '../../pages.module';

window.scrollTo = jest.fn();

describe('DisposeCansPageComponent', () => {
  let component: DisposeCansPageComponent;
  let fixture: ComponentFixture<DisposeCansPageComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [FormBuilder, SnackbarHelper],
        imports: [
          ComponentsModule,
          PagesModule,
          MaterialModule,
          RouterTestingModule,
          HttpClientTestingModule,
          BrowserAnimationsModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  afterAll(() => {
    jest.clearAllMocks();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisposeCansPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
