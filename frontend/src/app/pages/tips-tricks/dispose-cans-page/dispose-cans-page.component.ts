import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'dispose-cans-page',
  templateUrl: './dispose-cans-page.component.html',
  styleUrls: ['./dispose-cans-page.component.scss']
})
export class DisposeCansPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

}
