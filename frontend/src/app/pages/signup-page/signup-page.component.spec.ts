import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { AuthService } from './../../services/auth.service';
import { FormHelper } from './../../helper/form.helper';
import { MaterialModule } from './../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SignupPageComponent } from './signup-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { of } from 'rxjs';
import { PagesModule } from '../pages.module';
import { ComponentsModule } from 'src/app/components/components.module';

describe('SignupPageComponent', () => {
  let component: SignupPageComponent;
  let fixture: ComponentFixture<SignupPageComponent>;
  let authService: AuthService;
  let snackbarHelper: SnackbarHelper;
  let translateService: TranslateService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [],
        providers: [FormHelper, SnackbarHelper],
        imports: [
          PagesModule,
          ComponentsModule,
          MaterialModule,
          ReactiveFormsModule,
          RouterTestingModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupPageComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
    snackbarHelper = TestBed.inject(SnackbarHelper);
    translateService = TestBed.inject(TranslateService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    spyOn(authService, 'getAuthStatusListener').and.returnValue(of(true));
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.getAuthStatusListener).toHaveBeenCalled();
  });

  it('signup with valid form', () => {
    component.userForm.get('email').setValue('email@email');
    component.userForm.get('username').setValue('username');
    component.userForm.get('password').setValue('password');
    component.userForm.get('passwordConfirmation').setValue('password');
    fixture.detectChanges();
    component.signupUser();
    fixture.detectChanges();
  });

  it('signup with invalid form', () => {
    component.userForm.get('email').setValue('email');
    component.userForm.get('username').setValue('username');
    component.userForm.get('password').setValue('password');
    component.userForm.get('passwordConfirmation').setValue('password');
    fixture.detectChanges();
    spyOn(translateService, 'instant').and.stub();
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    component.signupUser();
    fixture.detectChanges();
    expect(translateService.instant).toHaveBeenCalledWith('snackInvalid');
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
  });
});
