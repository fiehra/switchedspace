import { FormHelper } from './../../../../helper/form.helper';
import { SwitchedInputComponent } from './../../../../components/htmlComponents/switched-input/switched-input.component';
import { SwitchedPasswordComponent } from './../../../../components/htmlComponents/switched-password/switched-password.component';
import { MaterialModule } from './../../../../utils/material/material.module';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SignupformComponent } from './signupform.component';
import { Component } from '@angular/core';
import { FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

@Component({
  selector: 'host-component',
  template: '<signupform [userForm]="userForm"></signupform>'
})
class TestHostComponent {
  constructor(private formBuilder: FormBuilder) {}

  userForm = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    username: ['', Validators.required],
    password: ['', Validators.required],
    passwordConfirmation: ['', Validators.required]
  });
}
describe('SignupformComponent', () => {
  let component: SignupformComponent;
  let fixture: ComponentFixture < TestHostComponent > ;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
        declarations: [
          TestHostComponent,
          SignupformComponent,
          SwitchedPasswordComponent,
          SwitchedInputComponent
        ],
        providers: [
          FormHelper
        ],
        imports: [
          MaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
              deps: [HttpClient]
            }
          })
        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('signingUp', () => {
    spyOn(component.signupClicked, 'emit').and.stub();
    component.signingUp();
    fixture.detectChanges();
    expect(component.signupClicked.emit).toHaveBeenCalled();
  });
});