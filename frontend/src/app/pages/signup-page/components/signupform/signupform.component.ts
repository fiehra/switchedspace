import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'signupform',
  templateUrl: './signupform.component.html',
  styleUrls: ['./signupform.component.scss']
})
export class SignupformComponent implements OnInit {


  @Input() userForm: FormGroup;
  @Output() signupClicked = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  signingUp() {
    this.signupClicked.emit();
  }






}
