import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignupPageComponent } from './signup-page.component';
import { SignupformComponent } from './components/signupform/signupform.component';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components/components.module';
import { UtilsModule } from '../../utils/utils.module';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
      SignupPageComponent,
      SignupformComponent
  ],
  imports: [
    UtilsModule,
    ComponentsModule,
    TranslateModule
  ],
  exports: [
    SignupPageComponent
  ]
})

export class SignupModule {}