import { SignupUserDTO } from './../../interfaces/user-interface';
import { SnackbarHelper } from './../../helper/snackbar.helper';
import { Subscription } from 'rxjs';
import { FormHelper } from './../../helper/form.helper';
import { AuthService } from './../../services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss'],
})
export class SignupPageComponent implements OnInit, OnDestroy {
  private authListenerSubs: Subscription;

  userForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(
    private authService: AuthService,
    private formHelper: FormHelper,
    private translateService: TranslateService,
    private snackbarHelper: SnackbarHelper
  ) {}

  ngOnInit() {
    this.authListenerSubs = this.authService
      .getAuthStatusListener()
      .subscribe();
    this.userForm.addControl('passwordConfirmation', new FormControl(''));
    this.userForm.setValidators(this.formHelper.checkPasswords);
  }

  signupUser() {
    if (this.userForm.valid) {
      const signupData: SignupUserDTO = {
        email: this.userForm.get('email').value,
        username: this.userForm.get('username').value,
        password: this.userForm.get('password').value,
      };
      this.authService.signUp(signupData);
    } else {
      const messageInvalid = this.translateService.instant('snackInvalid');
      this.snackbarHelper.openSnackBar(messageInvalid);
    }
  }

  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
  }
}
