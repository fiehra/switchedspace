import { GalleryPageComponent } from './gallery-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components/components.module';
import { UtilsModule } from '../../utils/utils.module';
import { NgModule } from '@angular/core';
import { Angular2ImageGalleryModule } from 'angular2-image-gallery';
import { GalleryToolbarComponent } from './components/gallery-toolbar/gallery-toolbar.component'

@NgModule({
  declarations: [
    GalleryPageComponent,
    GalleryToolbarComponent
  ],
  imports: [
    UtilsModule,
    ComponentsModule,
    Angular2ImageGalleryModule,
    TranslateModule
  ],
  exports: [
    GalleryPageComponent
  ]
})

export class GalleryModule {}