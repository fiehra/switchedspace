import { RouterTestingModule } from '@angular/router/testing';
import { LeafletHelper } from './leaflet.helper';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MaterialModule } from '../utils/material/material.module';

describe('LeafletHelper', () => {
  let service: LeafletHelper;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        LeafletHelper
      ],
      imports: [
        HttpClientModule,
        MaterialModule,
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
            deps: [HttpClient]
          }
        })
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.inject(LeafletHelper);
  });

  it('initialization', () => {
    expect(service).toBeTruthy();
  });
});