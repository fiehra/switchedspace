import { SnackbarHelper } from './snackbar.helper';
import { DonationHelper } from './donation.helper';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MaterialModule } from '../utils/material/material.module';

document.execCommand = jest.fn();
describe('DonationHelper', () => {
  let service: DonationHelper;
  let snackbarHelper: SnackbarHelper;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [DonationHelper, SnackbarHelper],
        imports: [
          HttpClientModule,
          MaterialModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    service = TestBed.inject(DonationHelper);
    snackbarHelper = TestBed.inject(SnackbarHelper);
  });

  it('initialization', () => {
    expect(service).toBeTruthy();
  });

  it('copyCryptoLink bitcoin', () => {
    spyOn(snackbarHelper, 'openSnackBar').and.stub();
    spyOn(document, 'execCommand').and.callThrough();
    spyOn(service, 'selectCrypto').and.returnValue('bitcoin');
    service.copyCryptoLink('bitcoin');
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
    expect(document.execCommand).toHaveBeenCalledWith('copy');
    expect(service.selectCrypto).toHaveBeenCalledWith('bitcoin');
  });

  it('selectCrypto bitcoin', () => {
    expect(service.selectedCoin).toBe('');
    expect(service.selectedCoinAddress).toBe('');
    service.selectCrypto('bitcoin');
    expect(service.selectedCoin).toBe('bitcoin');
    expect(service.selectedCoinAddress).toBe('bc1qrtrlxg4gscqdahsxeapfpz7jp80qua64jzcj5n');
  });

  it('selectCrypto ethereum', () => {
    expect(service.selectedCoin).toBe('');
    expect(service.selectedCoinAddress).toBe('');
    service.selectCrypto('ethereum');
    expect(service.selectedCoin).toBe('ethereum');
    expect(service.selectedCoinAddress).toBe('0x1033c82b3874458b121fB44dAd75907C766cDfaD');
  });

  it('selectCrypto tron', () => {
    expect(service.selectedCoin).toBe('');
    expect(service.selectedCoinAddress).toBe('');
    service.selectCrypto('tron');
    expect(service.selectedCoin).toBe('tron');
    expect(service.selectedCoinAddress).toBe('TCbST1Yu5b4cBdg5jzABFTMd53ke2GZyuL');
  });

  it('selectCrypto cardano', () => {
    expect(service.selectedCoin).toBe('');
    expect(service.selectedCoinAddress).toBe('');
    service.selectCrypto('cardano');
    expect(service.selectedCoin).toBe('cardano');
    expect(service.selectedCoinAddress).toBe(
      'addr1qxec747uz0e8q6upxlgt3az6spa6ws2td4djdmxhxfgm3qan3atacyljwp4czd7shr694qrm5aq5km2mymkdwvj3hzps4jy8pn',
    );
  });

  it('selectCrypto solana', () => {
    expect(service.selectedCoin).toBe('');
    expect(service.selectedCoinAddress).toBe('');
    service.selectCrypto('solana');
    expect(service.selectedCoin).toBe('solana');
    expect(service.selectedCoinAddress).toBe('4JzwnS2bL2QDB457ZCTfZURY8gqPDC7nQF7H6SeZSwfV');
  });

  // it('selectCrypto ripple', () => {
  //   expect(service.selectedCoin).toBe('');
  //   expect(service.selectedCoinAddress).toBe('');
  //   service.selectCrypto('ripple');
  //   expect(service.selectedCoin).toBe('ripple');
  //   expect(service.selectedCoinAddress).toBe('rLV2PGVgeReo6diBA1Xca3gZ34Xn364QAN');
  // });

  it('selectCrypto nano', () => {
    expect(service.selectedCoin).toBe('');
    expect(service.selectedCoinAddress).toBe('');
    service.selectCrypto('nano');
    expect(service.selectedCoin).toBe('nano');
    expect(service.selectedCoinAddress).toBe(
      'nano_1m7fyhp7gdyqbpxzmtidr9cg5tfu75of6gckewaib8gf7xtxtiur74h5pwe4',
    );
  });

  it('selectCrypto stellar', () => {
    expect(service.selectedCoin).toBe('');
    expect(service.selectedCoinAddress).toBe('');
    service.selectCrypto('stellar');
    expect(service.selectedCoinAddress).toBe(
      'GACRAPT5DEFUQNSIN5ZFADXHA2HIHSU7F3Q4K6L2H3YVDHEZGSLRNT3K',
    );
  });

  it('selectCrypto theta', () => {
    expect(service.selectedCoin).toBe('');
    expect(service.selectedCoinAddress).toBe('');
    service.selectCrypto('theta');
    expect(service.selectedCoinAddress).toBe('0xB20C4086D1014685D5d684EBCe2794E9Ac74AC6d');
  });
});
