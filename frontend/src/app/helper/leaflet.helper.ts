import { Router } from '@angular/router';
import { Injectable, NgZone } from '@angular/core';
import { tileLayer, latLng, marker, icon } from 'leaflet';

@Injectable()
export class LeafletHelper {
  tokyoLat = 35.6895;
  tokyoLng = 139.69171;
  berlinLat = 52.52437;
  berlinLng = 13.41053;

  constructor(private zone: NgZone, private router: Router) {}

  getLeafletOptions() {
    return {
      layers: [
        tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          maxZoom: 18,
          minZoom: 3,
          attribution: '...',
          noWrap: true,
        }),
      ],
      maxBounds: [
        [-90, -180],
        [90, 180],
      ],
      zoom: 10,
      center: latLng(this.berlinLat, this.berlinLng),
    };
  }

  getMarkerOptions(object: any) {
    return {
      layers: [
        tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          maxZoom: 18,
          minZoom: 3,
          attribution: '...',
          noWrap: true,
        }),
      ],
      maxBounds: [
        [-90, -180],
        [90, 180],
      ],
      zoom: 13,
      center: latLng(object.lat, object.lng),
    };
  }

  createMarker(object: any, type: string) {
    switch (type) {
      case 'wall': {
        let wallMarker;
        wallMarker = marker([object.lat, object.lng], {
          icon: icon({
            iconAnchor: [12, 41],
            iconUrl: '/assets/img/marker-icon.png',
            iconRetinaUrl: '/assets/img/marker-icon.png',
            shadowUrl: '/assets/img/marker-shadow.png',
          }),
        });
        wallMarker.addEventListener('click', () => {
          this.zone.run(() => {
            this.router.navigate(['/graffmap/wall/' + object.id]);
          });
        });
        return wallMarker;
      }
      case 'shop': {
        let shopMarker;
        shopMarker = marker([object.lat, object.lng], {
          icon: icon({
            iconAnchor: [12, 41],
            iconUrl: '/assets/img/marker-icon-shop.png',
            iconRetinaUrl: '/assets/img/marker-icon-shop.png',
            shadowUrl: '/assets/img/marker-shadow.png',
          }),
        });
        shopMarker.addEventListener('click', () => {
          this.zone.run(() => {
            this.router.navigate(['/graffmap/shop/' + object.id]);
          });
        });
        return shopMarker;
      }
      case 'streetart': {
        let streetartMarker;
        streetartMarker = marker([object.lat, object.lng], {
          icon: icon({
            iconAnchor: [12, 41],
            iconUrl: '/assets/img/marker-icon-streetart.png',
            iconRetinaUrl: '/assets/img/marker-icon-streetart.png',
            shadowUrl: '/assets/img/marker-shadow.png',
          }),
        });
        streetartMarker.addEventListener('click', () => {
          this.zone.run(() => {
            this.router.navigate(['/graffmap/streetart/' + object.id]);
          });
        });
        return streetartMarker;
      }
    }
  }

  createDefaultMarker(lat: number, lng: number, type: string) {
    switch (type) {
      case 'wall': {
        return marker([lat, lng], {
          icon: icon({
            iconAnchor: [12, 41],
            iconUrl: '/assets/img/marker-icon.png',
            iconRetinaUrl: '/assets/img/marker-icon.png',
            shadowUrl: '/assets/img/marker-shadow.png',
          }),
        });
      }
      case 'shop': {
        return marker([lat, lng], {
          icon: icon({
            iconAnchor: [12, 41],
            iconUrl: '/assets/img/marker-icon-shop.png',
            iconRetinaUrl: '/assets/img/marker-icon-shop.png',
            shadowUrl: '/assets/img/marker-shadow.png',
          }),
        });
      }
      case 'streetart': {
        return marker([lat, lng], {
          icon: icon({
            iconAnchor: [12, 41],
            iconUrl: '/assets/img/marker-icon-streetart.png',
            iconRetinaUrl: '/assets/img/marker-icon-streetart.png',
            shadowUrl: '/assets/img/marker-shadow.png',
          }),
        });
      }
    }
  }
}
