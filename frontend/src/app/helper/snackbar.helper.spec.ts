import { SnackbarHelper } from './snackbar.helper';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MaterialModule } from '../utils/material/material.module';

describe('SnackbarHelper', () => {
  let service: SnackbarHelper;
  let snackbar: MatSnackBar;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        SnackbarHelper,
        MatSnackBar
      ],
      imports: [
        HttpClientModule,
        MaterialModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, "assets/i18n/", ".json?build_id=build_id_replacement_placeholder")),
            deps: [HttpClient]
          }
        })
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.inject(SnackbarHelper);
    snackbar = TestBed.inject(MatSnackBar);
  });

  it('initialization', () => {
    expect(service).toBeTruthy();
  });

  it('open snack bar', () => {
    spyOn(snackbar, 'open').and.stub();
    service.openSnackBar('message');
    expect(snackbar.open).toHaveBeenCalled();
  });
});