import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class SnackbarHelper {
  constructor(
    private translateService: TranslateService,
    private snackBar: MatSnackBar
  ) {}

  // snackbar handling
  openSnackBar(message: string) {
    const clickAction = this.translateService.instant('snackOk');
    this.snackBar.open(message, clickAction, {
      duration: 5000,
    });
  }
}
