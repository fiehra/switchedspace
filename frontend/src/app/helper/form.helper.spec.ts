import { TestBed, waitForAsync } from '@angular/core/testing';
import { FormHelper } from './form.helper';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { MaterialModule } from '../utils/material/material.module';

describe('FormHelper', () => {
  let service: FormHelper;
  let formBuilder: FormBuilder;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [FormHelper, FormBuilder],
      imports: [
        HttpClientModule,
        MaterialModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (http: HttpClient) =>
              new TranslateHttpLoader(
                http,
                'assets/i18n/',
                '.json?build_id=build_id_replacement_placeholder',
              ),
            deps: [HttpClient],
          },
        }),
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.inject(FormHelper);
    formBuilder = TestBed.inject(FormBuilder);
  });

  it('initialization', () => {
    expect(service).toBeTruthy();
  });

  it('check required validator', () => {
    let control = new FormControl('', [Validators.required]);
    expect(service.checkRequiredValidator(control)).toBeTruthy();
    control = new FormControl('', [Validators.email]);
    expect(service.checkRequiredValidator(control)).toBeFalsy();
  });

  it('create required error message', () => {
    expect(service.createRequiredErrorMessage('field')).toBe('isRequiredField');
  });

  it('create no valid number errormessage', () => {
    expect(service.createNoValidNumberErrorMessage('field')).toBe('noValidNumber');
  });

  it('disable controls', () => {
    const formGroup = formBuilder.group({
      field1: ['', []],
      field2: ['', []],
    });
    expect(formGroup.get('field1').enabled).toBeTruthy();
    expect(formGroup.get('field2').enabled).toBeTruthy();
    service.disableControls(formGroup);
    expect(formGroup.get('field1').enabled).toBeFalsy();
    expect(formGroup.get('field2').enabled).toBeFalsy();
  });

  it('disable controls by name', () => {
    const formGroup = formBuilder.group({
      field1: [{ value: '', disabled: false }],
      field2: [{ value: '', disabled: false }],
    });
    expect(formGroup.get('field1').enabled).toBeTruthy();
    expect(formGroup.get('field2').enabled).toBeTruthy();
    service.disableControlsByName(formGroup, ['field2']);
    expect(formGroup.get('field1').enabled).toBeTruthy();
    expect(formGroup.get('field2').enabled).toBeFalsy();
  });

  it('enable controls', () => {
    const formGroup = formBuilder.group({
      field1: [{ value: '', disabled: true }],
      field2: [{ value: '', disabled: true }],
    });
    expect(formGroup.get('field1').enabled).toBeFalsy();
    expect(formGroup.get('field2').enabled).toBeFalsy();
    service.enableControls(formGroup);
    expect(formGroup.get('field1').enabled).toBeTruthy();
    expect(formGroup.get('field2').enabled).toBeTruthy();
  });

  it('enable controls by name', () => {
    const formGroup = formBuilder.group({
      field1: [{ value: '', disabled: true }],
      field2: [{ value: '', disabled: true }],
    });
    expect(formGroup.get('field1').enabled).toBeFalsy();
    expect(formGroup.get('field2').enabled).toBeFalsy();
    service.enableControlsByName(formGroup, ['field2']);
    expect(formGroup.get('field1').enabled).toBeFalsy();
    expect(formGroup.get('field2').enabled).toBeTruthy();
  });

  it('create pws not same error message', () => {
    const formGroup = formBuilder.group(
      {
        password: ['123'],
        passwordConfirmation: ['456'],
      },
      { validators: [service.checkPasswords] },
    );
  });

  it('create pws not same error message', () => {
    const formGroup = formBuilder.group(
      {
        password: ['123'],
        passwordConfirmation: ['123'],
      },
      { validators: [service.checkPasswords] },
    );
  });
});
