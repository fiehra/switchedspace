import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../utils/material/material.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from 'src/app/services/auth.service';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './auth.guard';
import { ActivatedRouteSnapshot, Router, convertToParamMap } from '@angular/router';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

describe('AuthGuard', () => {
  let service: AuthGuard;
  let authService: AuthService;
  let router: Router;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [AuthGuard, SnackbarHelper],
        imports: [
          MaterialModule,
          BrowserAnimationsModule,
          HttpClientModule,
          RouterTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    service = TestBed.inject(AuthGuard);
    authService = TestBed.inject(AuthService);
    router = TestBed.inject(Router);
  });

  it('initialization', () => {
    expect(service).toBeTruthy();
  });

  it('canActivate logged in', () => {
    const mockRoute = { paramMap: convertToParamMap({ id: '123' }) } as ActivatedRouteSnapshot;
    spyOn(authService, 'getLoggedIn').and.returnValue(true);
    const canActivate = service.canActivate(mockRoute, null);
    expect(canActivate).toBe(true);
  });

  it('canActivate', () => {
    const mockRoute = { paramMap: convertToParamMap({ id: '123' }) } as ActivatedRouteSnapshot;
    spyOn(authService, 'getLoggedIn').and.returnValue(false);
    spyOn(router, 'navigate').and.stub();
    const canActivate = service.canActivate(mockRoute, null);
    expect(canActivate).toBe(false);
    expect(router.navigate).toHaveBeenCalled();
  });
});
