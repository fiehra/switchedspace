import { SnackbarHelper } from './snackbar.helper';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class DonationHelper {
  cryptos = {
    bitcoin: 'bc1qrtrlxg4gscqdahsxeapfpz7jp80qua64jzcj5n',
    ethereum: '0x1033c82b3874458b121fB44dAd75907C766cDfaD',
    tron: 'TCbST1Yu5b4cBdg5jzABFTMd53ke2GZyuL',
    cardano:
      'addr1qxec747uz0e8q6upxlgt3az6spa6ws2td4djdmxhxfgm3qan3atacyljwp4czd7shr694qrm5aq5km2mymkdwvj3hzps4jy8pn',
    solana: '4JzwnS2bL2QDB457ZCTfZURY8gqPDC7nQF7H6SeZSwfV',
    // ripple: 'rLV2PGVgeReo6diBA1Xca3gZ34Xn364QAN',
    nano: 'nano_1m7fyhp7gdyqbpxzmtidr9cg5tfu75of6gckewaib8gf7xtxtiur74h5pwe4',
    stellar: 'GACRAPT5DEFUQNSIN5ZFADXHA2HIHSU7F3Q4K6L2H3YVDHEZGSLRNT3K',
    theta: '0xB20C4086D1014685D5d684EBCe2794E9Ac74AC6d',
  };

  selectedCoin: string = '';
  selectedCoinAddress: string = '';

  constructor(private translateService: TranslateService, private snackbarHelper: SnackbarHelper) {}

  copyCryptoLink(crypto: string) {
    const invisCopyLink = document.createElement('textarea');
    invisCopyLink.style.position = 'fixed';
    invisCopyLink.style.left = '0';
    invisCopyLink.style.top = '0';
    invisCopyLink.style.opacity = '0';
    invisCopyLink.value = this.selectCrypto(crypto);
    document.body.appendChild(invisCopyLink);
    invisCopyLink.focus();
    invisCopyLink.select();
    document.execCommand('copy');
    document.body.removeChild(invisCopyLink);
    const addressCopied = this.translateService.instant('addressCopied') + ': ' + crypto;
    this.snackbarHelper.openSnackBar(addressCopied);
  }

  selectCrypto(crypto: string) {
    switch (crypto) {
      case 'bitcoin': {
        this.selectedCoin = 'bitcoin';
        this.selectedCoinAddress = 'bc1qrtrlxg4gscqdahsxeapfpz7jp80qua64jzcj5n';
        return this.cryptos.bitcoin;
      }
      case 'ethereum': {
        this.selectedCoin = 'ethereum';
        this.selectedCoinAddress = '0x1033c82b3874458b121fB44dAd75907C766cDfaD';
        return this.cryptos.ethereum;
      }
      case 'tron': {
        this.selectedCoin = 'tron';
        this.selectedCoinAddress = 'TCbST1Yu5b4cBdg5jzABFTMd53ke2GZyuL';
        return this.cryptos.tron;
      }
      case 'cardano': {
        this.selectedCoin = 'cardano';
        this.selectedCoinAddress =
          'addr1qxec747uz0e8q6upxlgt3az6spa6ws2td4djdmxhxfgm3qan3atacyljwp4czd7shr694qrm5aq5km2mymkdwvj3hzps4jy8pn';
        return this.cryptos.cardano;
      }
      case 'solana': {
        this.selectedCoin = 'solana';
        this.selectedCoinAddress = '4JzwnS2bL2QDB457ZCTfZURY8gqPDC7nQF7H6SeZSwfV';
        return this.cryptos.solana;
      }
      // case 'ripple': {
      //   this.selectedCoin = 'ripple'
      //   this.selectedCoinAddress = 'rLV2PGVgeReo6diBA1Xca3gZ34Xn364QAN'
      //   return this.cryptos.ripple
      // }
      case 'nano': {
        this.selectedCoin = 'nano';
        this.selectedCoinAddress =
          'nano_1m7fyhp7gdyqbpxzmtidr9cg5tfu75of6gckewaib8gf7xtxtiur74h5pwe4';
        return this.cryptos.nano;
      }
      case 'stellar': {
        this.selectedCoin = 'stellar';
        this.selectedCoinAddress = 'GACRAPT5DEFUQNSIN5ZFADXHA2HIHSU7F3Q4K6L2H3YVDHEZGSLRNT3K';
        return this.cryptos.stellar;
      }
      case 'theta': {
        this.selectedCoin = 'theta';
        this.selectedCoinAddress = '0xB20C4086D1014685D5d684EBCe2794E9Ac74AC6d';
        return this.cryptos.theta;
      }
    }
  }
}
