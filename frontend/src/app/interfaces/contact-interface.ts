export declare type FoundBy = 'friends' | 'searchEngine' | 'socialMedia' | 'other';

export interface ContactRequest {
  email: string;
  foundBy: FoundBy;
  subject: string;
  message: string;
}

