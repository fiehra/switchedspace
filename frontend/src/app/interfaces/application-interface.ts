export declare type ApplicationType = 'workshop' | 'contract' | 'session' | 'other';

export interface Application {
  id: string;
  applicationType: ApplicationType;
  name: string;
  email: string;
  phonenumber: string;
  company: string;
  title: string;
  description: string;
  location: string;
  date: Date;
  budget: string;
}

export interface ApplicationListDTO {
  id: string;
  applicationType: ApplicationType;
  title: string;
  location: string;
  date: Date;
  budget: string;
}
