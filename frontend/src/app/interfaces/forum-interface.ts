export interface ForumPost {
  id: string;
  title: string;
  writerId: string;
  writerUsername: string;
  content: string;
  votes: number;
  upVoters: string[];
  downVoters: string[];
  comments: Comment[];
}

export interface CreateForumPostDTO {
  id: string;
  title: string;
  writerId: string;
  writerUsername: string;
  content: string;
}

export interface Comment {
  id: string;
  writerId: string;
  writerUsername: string;
  content: string;
  markedAsCorrect: boolean;
}

export interface ForumPostVoteDTO {
  id: string;
  votes: number;
  upVoters: string[];
  downVoters: string[];
}

export interface ForumListDTO {
  id: string;
  title: string;
  writerUsername: string;
  votes: number;
}
