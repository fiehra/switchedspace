export declare type LegalState =
  | 'legal'
  | 'illegal'
  | 'tolerated'
  | 'abandoned';

export interface Wall {
  id: string;
  name: string;
  description: string;
  legalState: LegalState;
  lat: number;
  lng: number;
  statusUpdates: string[];
}

export interface WallMapDTO {
  id: string;
  lat: number;
  lng: number;
}

export interface Shop {
  id: string;
  name: string;
  description: string;
  website: string;
  lat: number;
  lng: number;
}

export interface ShopMapDTO {
  id: string;
  lat: number;
  lng: number;
}

export interface Streetart {
  id: string;
  name: string;
  description: string;
  link: string;
  lat: number;
  lng: number;
}

export interface StreetartMapDTO {
  id: string;
  lat: number;
  lng: number;
}
