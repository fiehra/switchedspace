export declare type Role = 'fiehra' | 'user';

export interface User {
  id: string;
  email: string;
  username: string;
  password: string;
  aboutme: string;
  tag: string;
  faveColor: string;
  currentLocation: string;
  age: string;
  faveTheme: string;
  reported: boolean;
  role: Role;
  imagePath: string;
  inventory: number;
  verified: boolean;
}

export interface MyProfileDTO {
  id: string;
  email: string;
  username: string;
  aboutme: string;
  tag: string;
  faveColor: string;
  currentLocation: string;
  age: string;
  faveTheme: string;
  reported: boolean;
  role: Role;
  imagePath: string;
  inventory: number;
  verified: boolean;
}

export interface ProfileDTO {
  id: string;
  username: string;
  aboutme: string;
  tag: string;
  faveColor: string;
  currentLocation: string;
  age: string;
  faveTheme: string;
  reported: boolean;
  imagePath: string;
}

export interface SignupUserDTO {
  email: string;
  username: string;
  password: string;
}

export interface LoginUserDTO {
  email: string;
  password: string;
}

export interface AuthData {
  token: string;
  expirationDate: Date;
  userId: string;
  username: string;
  faveTheme: string;
  role: Role;
}

export interface ReportPlayerDto {
  id: string;
  reported: boolean;
}
