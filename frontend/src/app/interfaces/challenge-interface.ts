export interface Challenge {
  id: string;
  challengerId: string;
  date: Date;
  start: string;
  finish: string;
  time: number;
  active: boolean;
  completed: boolean;
  record: number;
  streak: number;
}

export interface Streak {
  id: string;
  challengerId: string;
  challenges: Challenge[];
}
