import { Pipe } from '@angular/core';

@Pipe({
  name: 'sort',
  pure: false,
})
// pipe which sorts highest value first --> assign to value of formgroup
export class SortPipe {
  transform(array: Array<string>, control: string): Array<string> {
    if (array !== undefined) {
      return array.sort((a: any, b: any) => {
        const aValue = a.controls[control].value;
        const bValue = b.controls[control].value;
        if (aValue > bValue) {
          return -1;
        } else if (aValue < bValue) {
          return 1;
        } else {
          return 0;
        }
      });
    }
    return array;
  }
}
