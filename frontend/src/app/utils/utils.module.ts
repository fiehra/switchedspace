import { CommonModule } from '@angular/common';
import { SortPipe } from './sort.pipe';
import { AppRoutingModule } from './../app-routing.module';
import { MaterialModule } from './material/material.module';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [SortPipe],
  providers: [],
  imports: [
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
    MaterialModule,
    AppRoutingModule,
    CommonModule,
  ],
  exports: [
    MaterialModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SortPipe,
    CommonModule,
  ],
})
export class UtilsModule {}
