import { ValueLabel } from './valueLabel';

export class SelectOptions {
  public static foundByValueSelects: ValueLabel[] = [
    { value: 'friends', label: 'friends' },
    { value: 'searchEngine', label: 'searchEngine' },
    { value: 'socialMedia', label: 'socialMedia' },
    { value: 'other', label: 'other' },
  ];

  public static faveThemeSelects: ValueLabel[] = [
    { value: 'orange', label: 'orangeTheme' },
    { value: 'green', label: 'greenTheme' },
    { value: 'purple', label: 'purpleTheme' },
    { value: 'light', label: 'lightTheme' },
    { value: 'yellow', label: 'yellowTheme' },
    { value: 'toxic', label: 'toxicTheme' },
    { value: 'blue', label: 'blueTheme' },
    { value: 'red', label: 'redTheme' },
    { value: 'gray', label: 'grayTheme' },
    { value: 'pink', label: 'pinkTheme' },
  ];

  public static applicationTypeSelects: ValueLabel[] = [
    { value: 'workshop', label: 'workshop' },
    { value: 'contract', label: 'contract' },
    { value: 'session', label: 'session' },
    { value: 'other', label: 'other' },
  ];

  public static legalStateSelects: ValueLabel[] = [
    { value: 'legal', label: 'legal' },
    { value: 'illegal', label: 'illegal' },
    { value: 'tolerated', label: 'tolerated' },
    { value: 'abandoned', label: 'abandoned' },
  ];
}
