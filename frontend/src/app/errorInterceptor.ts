import { SnackbarHelper } from 'src/app/helper/snackbar.helper';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private snackbarHelper: SnackbarHelper) {}

  intercept(req: HttpRequest < any > , next: HttpHandler) {
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        let errorMessage = 'unknown error occured';
        if (error.error.message) {
          errorMessage = error.error.message;
        }
        this.snackbarHelper.openSnackBar(errorMessage);
        return throwError(error);
      })
    );
  }
}