import { ContactRequest } from './../interfaces/contact-interface';
import { FormBuilder } from '@angular/forms';
import { environment } from './../../environments/environment';
import { ContactService } from './contact.service';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';

describe('ContactService', () => {
  let service: ContactService;
  let http: HttpClient;
  let formBuilder: FormBuilder;

  const contactRequestMock: ContactRequest = {
    email: 'contact@email.com',
    foundBy: 'friends',
    subject: 'subject',
    message: 'hi',
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [ContactService, FormBuilder],
        imports: [HttpClientModule],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    service = TestBed.inject(ContactService);
    http = TestBed.inject(HttpClient);
    formBuilder = TestBed.inject(FormBuilder);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(http).toBeTruthy();
  });

  it('createContactRequest', () => {
    spyOn(http, 'post').and.stub();
    service.createContactRequest(contactRequestMock);
    expect(http.post).toHaveBeenCalledWith(
      environment.backendUrl + '/api/contactRequest',
      contactRequestMock
    );
  });

  it('mapContactRequestToForm', () => {
    const formGroup = service.mapContactRequestToForm(contactRequestMock);
    expect(formGroup.get('email').value).toBe('contact@email.com');
    expect(formGroup.get('foundBy').value).toBe('friends');
    expect(formGroup.get('subject').value).toBe('subject');
    expect(formGroup.get('message').value).toBe('hi');
  });
});
