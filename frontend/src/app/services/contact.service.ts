import { ContactRequest } from './../interfaces/contact-interface';
import { environment } from 'src/environments/environment';
import { HttpConfig } from './../helper/authInterceptor';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ContactService {
  config: HttpConfig = {
    headers: new HttpHeaders(),
    observe: 'response',
  };

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {}

  // send contact Request
  createContactRequest(contactRequest: ContactRequest) {
    return this.http.post(
      environment.backendUrl + '/api/contactRequest',
      contactRequest
    );
  }

  // mapping functions for forms
  mapContactRequestToForm(contactRequest: ContactRequest): FormGroup {
    const form = this.formBuilder.group(contactRequest);
    return form;
  }
}
