import {
  CreateForumPostDTO,
  ForumListDTO,
  ForumPost,
  ForumPostVoteDTO,
  Comment,
} from './../interfaces/forum-interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ForumService {
  constructor(private http: HttpClient, private formBuilder: FormBuilder) {}

  // http calls
  getForumListPosts(postsPerPage: number, currentPage: number) {
    const queryParams = `?pagesize=${postsPerPage}&page=${currentPage}`;
    return this.http
      .get<{ message: string; forumPostsList: any; maxPosts: number }>(
        environment.backendUrl + '/api/forumPostsList' + queryParams,
      )
      .pipe(
        map((responseData) => {
          return {
            forumPostsList: responseData.forumPostsList.map((forumPost) => {
              return {
                id: forumPost.id,
                title: forumPost.title,
                writerUsername: forumPost.writerUsername,
                votes: forumPost.votes,
              };
            }),
            maxPosts: responseData.maxPosts,
          };
        }),
      );
  }

  getForumPostById(id: string) {
    return this.http
      .get<{ forumPost: any }>(environment.backendUrl + '/api/forumPosts/' + id)
      .pipe(
        map((responseData) => {
          return {
            id: responseData.forumPost._id,
            title: responseData.forumPost.title,
            writerId: responseData.forumPost.writerId,
            writerUsername: responseData.forumPost.writerUsername,
            content: responseData.forumPost.content,
            votes: responseData.forumPost.votes,
            upVoters: responseData.forumPost.upVoters,
            downVoters: responseData.forumPost.downVoters,
            comments: responseData.forumPost.comments,
          };
        }),
      );
  }

  createForumPost(forumPost: CreateForumPostDTO) {
    this.http
      .post<{ message: string; forumPostId: string }>(
        environment.backendUrl + '/api/forumPosts',
        forumPost,
      )
      .subscribe((responseData) => {
        const id = responseData.forumPostId;
        forumPost.id = id;
      });
  }

  deletePost(forumPostId: string): Observable<any> {
    return this.http.delete(
      environment.backendUrl + '/api/forumPosts/' + forumPostId,
    );
  }

  updatePost(forumPost: ForumPost, forumPostId: string): Observable<any> {
    return this.http.put(
      environment.backendUrl + '/api/forumPosts/' + forumPostId,
      forumPost,
    );
  }

  updateVotes(voteDto: ForumPostVoteDTO): Observable<any> {
    return this.http.put(
      environment.backendUrl + '/api/forumPosts/updatevotes/' + voteDto.id,
      voteDto,
    );
  }

  updateComments(forumPostId: string, comments: Comment[]): Observable<any> {
    return this.http.put(
      environment.backendUrl + '/api/forumPosts/updateComments/' + forumPostId,
      comments,
    );
  }

  // mapping functions
  mapForumPostToForm(forumPost: ForumPost): FormGroup {
    const newform = new FormGroup({
      id: new FormControl(forumPost.id),
      title: new FormControl(forumPost.title),
      writerId: new FormControl(forumPost.writerId),
      writerUsername: new FormControl(forumPost.writerUsername),
      content: new FormControl(forumPost.content),
      votes: new FormControl(forumPost.votes),
      upVoters: this.formBuilder.array(forumPost.upVoters),
      downVoters: this.formBuilder.array(forumPost.downVoters),
      comments: this.formBuilder.array(forumPost.comments),
    });
    return newform;
  }

  mapForumPostListDtoToForm(forumPostDto: ForumListDTO): FormGroup {
    const newform = new FormGroup({
      id: new FormControl(forumPostDto.id),
      title: new FormControl(forumPostDto.title),
      writerUsername: new FormControl(forumPostDto.writerUsername),
      votes: new FormControl(forumPostDto.votes),
    });
    return newform;
  }
}
