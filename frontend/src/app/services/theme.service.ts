import { Injectable } from '@angular/core';

// orange theme
export const orange = {
  main: '#ff6d03',
  mainDark: '#ad4900',
  mainSuperDark: '#7a3300',
  mainLight: '#ff9447',
  mainFont: '#eee',
  alt: '#888',
  altHover: '#ddd',
  altFont: '#000',
  gradient: '#ff9e58',
  mainBackground: '#000000cc',
  altBackground: '#222222cc',
  light: '#fff',
  dark: '#000',
  image: 'url(/assets/img/horizontalMikata.jpg)',
  imageVert: 'url(/assets/img/verticalMikata.jpg)',
};

// purple theme
export const purple = {
  main: '#9651d6',
  mainDark: '#742cb7',
  mainSuperDark: '#511f80',
  mainLight: '#bf96e6',
  mainFont: '#fff',
  alt: '#888',
  altHover: '#ddd',
  altFont: '#000',
  gradient: '#9e5fd9',
  mainBackground: '#1a1a1aee',
  altBackground: '#111',
  light: '#fff',
  dark: '#000',
  image: 'url(/assets/img/shoujoHorizontal.jpg)',
  imageVert: 'url(/assets/img/shoujoVertical.jpg)',
};

// green theme
export const green = {
  main: '#05c46b',
  mainDark: '#038247',
  mainSuperDark: '#02502c',
  mainLight: '#14f98e',
  mainFont: '#eee',
  alt: '#deb33e',
  altHover: '#f4e6c1',
  altFont: '#000',
  gradient: '#35fa9e',
  mainBackground: '#000000cc',
  altBackground: '#222222cc',
  light: '#fff',
  dark: '#000',
  image: 'url(/assets/img/kokochiHorizontal.jpg)',
  imageVert: 'url(/assets/img/2x1KokoChi.jpg)',
};

// yellow theme
export const yellow = {
  main: '#ee0',
  mainDark: '#aa0',
  mainSuperDark: '#660',
  mainLight: '#ff6',
  mainFont: '#fff',
  alt: '#888',
  altHover: '#eee',
  altFont: '#000',
  gradient: '#ff8',
  mainBackground: '#222222ee',
  altBackground: '#171717',
  light: '#fff',
  dark: '#000',
  image: 'url(/assets/img/horizontalHihan.jpg)',
  imageVert: 'url(/assets/img/verticalHihan.jpg)',
};

// light theme
export const light = {
  main: '#22a5c6',
  mainDark: '#18758c',
  mainSuperDark: '#0e4452',
  mainLight: '#5ac7e3',
  mainFont: '#000',
  alt: '#888',
  altHover: '#444',
  altFont: '#eee',
  gradient: '#1b819b',
  mainBackground: '#ddd',
  altBackground: '#eee',
  light: '#fff',
  dark: '#000',
  image: 'url(/assets/img/2x1instaHoushiki.jpg)',
  imageVert: 'url(/assets/img/houshikiVertical.jpg)',
};

// toxic theme
export const toxic = {
  main: '#33db00',
  mainDark: '#239700',
  mainSuperDark: '#135300',
  mainLight: '#61ff31',
  mainFont: '#eee',
  alt: '#888',
  altHover: '#ddd',
  altFont: '#000',
  gradient: '#54ff20',
  mainBackground: '#222222ee',
  altBackground: '#171717',
  light: '#fff',
  dark: '#000',
  image: 'url(/assets/img/horizontalTsuikyuu.jpg)',
  imageVert: 'url(/assets/img/verticalTsuikyuu.jpg)',
};

// blue theme
export const blue = {
  main: '#4e94ff',
  mainDark: '#0a6aff',
  mainSuperDark: '#004ec5',
  mainLight: '#81b3ff',
  mainFont: '#000',
  alt: '#888',
  altHover: '#444',
  altFont: '#eee',
  gradient: '#5f9eff',
  mainBackground: '#ddd',
  altBackground: '#eee',
  light: '#fff',
  dark: '#000',
  image: 'url(/assets/img/horizontalHeikin.jpg)',
  imageVert: 'url(/assets/img/verticalHeikin.jpg)',
};

// red theme
export const red = {
  main: '#f90329',
  mainDark: '#b6021e',
  mainSuperDark: '#730113',
  mainLight: '#fd546e',
  mainFont: '#fff',
  alt: '#888',
  altHover: '#eee',
  altFont: '#000',
  gradient: '#fd3252',
  mainBackground: '#222222ee',
  altBackground: '#171717',
  light: '#fff',
  dark: '#000',
  image: 'url(/assets/img/2x1Shukketsu.jpg)',
  imageVert: 'url(/assets/img/verticalShukketsu.jpg)',
};

// gray theme
export const gray = {
  main: '#8281af',
  mainDark: '#5d5c90',
  mainSuperDark: '#504f7b',
  mainLight: '#c0c0d6',
  mainFont: '#eee',
  alt: '#deb33e',
  altHover: '#f4e6c1',
  altFont: '#000',
  gradient: '#c0bfd7',
  mainBackground: '#222222ee',
  altBackground: '#171717',
  light: '#fff',
  dark: '#000',
  image: 'url(/assets/img/2x1SaiRyoku.jpg)',
  imageVert: 'url(/assets/img/verticalSairyoku.jpg)',
};

// gray theme
export const pink = {
  main: '#dd22ec',
  mainDark: '#ad10ba',
  mainSuperDark: '#730b7c',
  mainLight: '#eb80f4',
  mainFont: '#eee',
  alt: '#888',
  altHover: '#ddd',
  altFont: '#000',
  gradient: '#ff83ff',
  mainBackground: '#222222ee',
  altBackground: '#171717',
  light: '#fff',
  dark: '#000',
  image: 'url(/assets/img/horizontalGairan.jpg)',
  imageVert: 'url(/assets/img/verticalGairan.jpg)',
};

@Injectable({
  providedIn: 'root',
})
export class ThemeService {
  setTheme(theme: {}) {
    Object.keys(theme).forEach((k) =>
      document.documentElement.style.setProperty(`--${k}`, theme[k]),
    );
  }

  changeTheme(color: string) {
    if (color === 'orange') {
      this.setTheme(orange);
      localStorage.setItem('faveTheme', color);
    } else if (color === 'purple') {
      this.setTheme(purple);
      localStorage.setItem('faveTheme', color);
    } else if (color === 'green') {
      this.setTheme(green);
      localStorage.setItem('faveTheme', color);
    } else if (color === 'light') {
      this.setTheme(light);
      localStorage.setItem('faveTheme', color);
    } else if (color === 'yellow') {
      this.setTheme(yellow);
      localStorage.setItem('faveTheme', color);
    } else if (color === 'toxic') {
      this.setTheme(toxic);
      localStorage.setItem('faveTheme', color);
    } else if (color === 'blue') {
      this.setTheme(blue);
      localStorage.setItem('faveTheme', color);
    } else if (color === 'red') {
      this.setTheme(red);
      localStorage.setItem('faveTheme', color);
    } else if (color === 'gray') {
      this.setTheme(gray);
      localStorage.setItem('faveTheme', color);
    } else if (color === 'pink') {
      this.setTheme(pink);
      localStorage.setItem('faveTheme', color);
    }
  }
}
