import { HttpConfig } from './../helper/authInterceptor';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class VerificationService {
  config: HttpConfig = {
    headers: new HttpHeaders(),
    observe: 'response',
  };

  constructor(private http: HttpClient) {}

  resendVerificationLink(): Observable<any> {
    let email = localStorage.getItem('email');
    const body = {
      email: email,
    };
    return this.http.post(environment.backendUrl + '/api/verify/resend', body);
  }
}
