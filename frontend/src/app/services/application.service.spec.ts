import { environment } from './../../environments/environment';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ApplicationService } from './application.service';
import { Application } from '../interfaces/application-interface';

describe('ApplicationService', () => {
  let service: ApplicationService;
  let http: HttpClient;
  let formBuilder: FormBuilder;

  const applicationMock: Application = {
    id: '1',
    applicationType: 'workshop',
    name: 'application',
    email: 'application@email.com',
    phonenumber: '911',
    company: 'skillcap',
    title: 'workshop',
    description: 'description',
    location: 'tokyo',
    date: new Date(),
    budget: '1000',
  };

  const applicationMock2: Application = {
    id: '2',
    applicationType: 'contract',
    name: 'contract',
    email: 'application@email.com',
    phonenumber: '911',
    company: 'skillcap',
    title: 'contract',
    description: 'description',
    location: 'tokyo',
    date: new Date(),
    budget: '1000',
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [ApplicationService, FormBuilder],
        imports: [HttpClientTestingModule],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    service = TestBed.inject(ApplicationService);
    http = TestBed.inject(HttpClient);
    formBuilder = TestBed.inject(FormBuilder);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(http).toBeTruthy();
  });

  it('create application', () => {
    spyOn(http, 'post').and.returnValue(of({ body: applicationMock }));
    service.createApplication(applicationMock);
    expect(http.post).toHaveBeenCalledWith(
      environment.backendUrl + '/api/application',
      applicationMock
    );
  });

  it('getApplications', () => {
    const applicationsMock = {
      message: 'message',
      applications: [applicationMock, applicationMock2],
      maxApplications: 2,
    };
    spyOn(http, 'get').and.returnValue(of(applicationsMock));
    service.getApplications().subscribe((response) => {
      expect(response.applications.length).toBe(2);
    });
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/application'
    );
  });

  it('getApplicationById', () => {
    const responseMock = {
      message: 'message',
      application: applicationMock,
    };
    spyOn(http, 'get').and.returnValue(of(responseMock));
    service.getApplicationById('2').subscribe((response) => {});
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/application/2'
    );
  });

  it('updateApplication', () => {
    spyOn(http, 'put').and.callThrough();
    service.updateApplication(applicationMock, '1');
    expect(http.put).toHaveBeenCalledWith(
      environment.backendUrl + '/api/application/1',
      applicationMock
    );
  });

  it('deleteApplication', () => {
    spyOn(http, 'delete').and.callThrough();
    service.deleteApplication('1');
    expect(http.delete).toHaveBeenCalledWith(
      environment.backendUrl + '/api/application/1'
    );
  });
});
