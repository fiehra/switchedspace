import { ThemeService, orange, purple, light, green, toxic, red, blue, yellow, gray, pink } from './theme.service';
import { TestBed, waitForAsync } from '@angular/core/testing';

describe('ThemeService', () => {
  let service: ThemeService;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      providers: [
        ThemeService
      ],
      imports: [
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.inject(ThemeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('change orange', () => {
    spyOn(service, 'setTheme').and.stub();
    service.changeTheme('orange');
    expect(service.setTheme).toHaveBeenCalledWith(orange);
    expect(localStorage.getItem('faveTheme')).toBe('orange');
  });
  
  it('change light', () => {
    spyOn(service, 'setTheme').and.stub();
    service.changeTheme('light');
    expect(service.setTheme).toHaveBeenCalledWith(light);
    expect(localStorage.getItem('faveTheme')).toBe('light');
  });
  
  it('change purple', () => {
    spyOn(service, 'setTheme').and.stub();
    service.changeTheme('purple');
    expect(service.setTheme).toHaveBeenCalledWith(purple);
    expect(localStorage.getItem('faveTheme')).toBe('purple');
  });
  
  it('change green', () => {
    spyOn(service, 'setTheme').and.stub();
    service.changeTheme('green');
    expect(service.setTheme).toHaveBeenCalledWith(green);
    expect(localStorage.getItem('faveTheme')).toBe('green');
  });
  
  it('change toxic', () => {
    spyOn(service, 'setTheme').and.stub();
    service.changeTheme('toxic');
    expect(service.setTheme).toHaveBeenCalledWith(toxic);
    expect(localStorage.getItem('faveTheme')).toBe('toxic');
  });
  
  it('change yellow', () => {
    spyOn(service, 'setTheme').and.stub();
    service.changeTheme('yellow');
    expect(service.setTheme).toHaveBeenCalledWith(yellow);
    expect(localStorage.getItem('faveTheme')).toBe('yellow');
  });
  
  it('change blue', () => {
    spyOn(service, 'setTheme').and.stub();
    service.changeTheme('blue');
    expect(service.setTheme).toHaveBeenCalledWith(blue);
    expect(localStorage.getItem('faveTheme')).toBe('blue');
  });
  
  it('change red', () => {
    spyOn(service, 'setTheme').and.stub();
    service.changeTheme('red');
    expect(service.setTheme).toHaveBeenCalledWith(red);
    expect(localStorage.getItem('faveTheme')).toBe('red');
  });
  
  it('change gray', () => {
    spyOn(service, 'setTheme').and.stub();
    service.changeTheme('gray');
    expect(service.setTheme).toHaveBeenCalledWith(gray);
    expect(localStorage.getItem('faveTheme')).toBe('gray');
  });
  
  it('change pink', () => {
    spyOn(service, 'setTheme').and.stub();
    service.changeTheme('pink');
    expect(service.setTheme).toHaveBeenCalledWith(pink);
    expect(localStorage.getItem('faveTheme')).toBe('pink');
  });
  
  it('setTheme', () => {
    service.setTheme({light: 'light'});
  });
  

});