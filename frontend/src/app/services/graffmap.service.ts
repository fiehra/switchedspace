import { environment } from './../../environments/environment';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  FormArray,
  Validators,
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Shop, Streetart, Wall } from '../interfaces/graffmap-interface';

@Injectable({
  providedIn: 'root',
})
export class GraffmapService {
  constructor(private http: HttpClient, private formBuilder: FormBuilder) {}

  // http service calls
  addWall(wall: Wall) {
    this.http
      .post<{ message: string; wallId: string }>(
        environment.backendUrl + '/api/wall/add',
        wall
      )
      .subscribe((responseData) => {
        const id = responseData.wallId;
        wall.id = id;
      });
  }

  addShop(shop: Shop) {
    this.http
      .post<{ message: string; shopId: string }>(
        environment.backendUrl + '/api/shop/add',
        shop
      )
      .subscribe((responseData) => {
        const id = responseData.shopId;
        shop.id = id;
      });
  }

  addStreetart(streetart: Streetart) {
    this.http
      .post<{ message: string; streetartId: string }>(
        environment.backendUrl + '/api/streetart/add',
        streetart
      )
      .subscribe((responseData) => {
        const id = responseData.streetartId;
        streetart.id = id;
      });
  }

  getWalls() {
    return this.http
      .get<{ message: string; walls: any; maxWalls: number }>(
        environment.backendUrl + '/api/walls'
      )
      .pipe(
        map((responseData) => {
          return {
            walls: responseData.walls.map((wall) => {
              return {
                id: wall._id,
                name: wall.name,
                description: wall.description,
                legalState: wall.legalState,
                lat: wall.lat,
                lng: wall.lng,
                statusUpdates: wall.statusUpdates,
              };
            }),
            maxWalls: responseData.maxWalls,
          };
        })
      );
  }

  getShops() {
    return this.http
      .get<{ message: string; shops: any; maxShops: number }>(
        environment.backendUrl + '/api/shops'
      )
      .pipe(
        map((responseData) => {
          return {
            shops: responseData.shops.map((shop) => {
              return {
                id: shop._id,
                name: shop.name,
                description: shop.description,
                website: shop.website,
                lat: shop.lat,
                lng: shop.lng,
              };
            }),
            maxShops: responseData.maxShops,
          };
        })
      );
  }

  getStreetarts() {
    return this.http
      .get<{ message: string; streetarts: any; maxStreetarts: number }>(
        environment.backendUrl + '/api/streetarts'
      )
      .pipe(
        map((responseData) => {
          return {
            streetarts: responseData.streetarts.map((streetart) => {
              return {
                id: streetart._id,
                name: streetart.name,
                description: streetart.description,
                link: streetart.link,
                lat: streetart.lat,
                lng: streetart.lng,
              };
            }),
            maxStreetarts: responseData.maxStreetarts,
          };
        })
      );
  }

  getWallById(id: string) {
    return this.http
      .get<{ wall: any }>(environment.backendUrl + '/api/wall/' + id)
      .pipe(
        map((responseData) => {
          return {
            id: responseData.wall._id,
            name: responseData.wall.name,
            description: responseData.wall.description,
            legalState: responseData.wall.legalState,
            lat: responseData.wall.lat,
            lng: responseData.wall.lng,
            statusUpdates: responseData.wall.statusUpdates,
          };
        })
      );
  }

  getShopById(id: string) {
    return this.http
      .get<{ shop: any }>(environment.backendUrl + '/api/shop/' + id)
      .pipe(
        map((responseData) => {
          return {
            id: responseData.shop._id,
            name: responseData.shop.name,
            description: responseData.shop.description,
            website: responseData.shop.website,
            lat: responseData.shop.lat,
            lng: responseData.shop.lng,
          };
        })
      );
  }

  getStreetartById(id: string) {
    return this.http
      .get<{ streetart: any }>(environment.backendUrl + '/api/streetart/' + id)
      .pipe(
        map((responseData) => {
          return {
            id: responseData.streetart._id,
            name: responseData.streetart.name,
            description: responseData.streetart.description,
            link: responseData.streetart.link,
            lat: responseData.streetart.lat,
            lng: responseData.streetart.lng,
            statusUpdates: responseData.streetart.statusUpdates,
          };
        })
      );
  }

  updateWallById(id: string, wall: Wall) {
    return this.http.put(
      environment.backendUrl + '/api/wall/update/' + id,
      wall
    );
  }

  updateShopById(id: string, shop: Shop) {
    return this.http.put(
      environment.backendUrl + '/api/shop/update/' + id,
      shop
    );
  }

  updateStreetartById(id: string, streetart: Streetart) {
    return this.http.put(
      environment.backendUrl + '/api/streetart/update/' + id,
      streetart
    );
  }

  // init functions for graffmap objects
  initWallForm(): FormGroup {
    const wallForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      legalState: new FormControl('', Validators.required),
      lat: new FormControl(null, Validators.required),
      lng: new FormControl(null, Validators.required),
    });
    return wallForm;
  }

  initShopForm(): FormGroup {
    const shopForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      website: new FormControl(''),
      lat: new FormControl(null, Validators.required),
      lng: new FormControl(null, Validators.required),
    });
    return shopForm;
  }

  initStreetartForm(): FormGroup {
    const streetartForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      link: new FormControl(''),
      lat: new FormControl(null, Validators.required),
      lng: new FormControl(null, Validators.required),
    });
    return streetartForm;
  }

  // mapping functions
  mapWallToForm(wall: Wall): FormGroup {
    const form = new FormGroup({
      id: new FormControl(wall.id),
      name: new FormControl(wall.name),
      description: new FormControl(wall.description),
      legalState: new FormControl(wall.legalState),
      lat: new FormControl(wall.lat),
      lng: new FormControl(wall.lng),
      statusUpdates: this.formBuilder.array(wall.statusUpdates),
    });
    return form;
  }

  mapShopToForm(shop: Shop): FormGroup {
    const form = new FormGroup({
      id: new FormControl(shop.id),
      name: new FormControl(shop.name),
      description: new FormControl(shop.description),
      website: new FormControl(shop.website),
      lat: new FormControl(shop.lat),
      lng: new FormControl(shop.lng),
    });
    return form;
  }

  mapStreetartToForm(streetart: Streetart): FormGroup {
    const form = new FormGroup({
      id: new FormControl(streetart.id),
      name: new FormControl(streetart.name),
      description: new FormControl(streetart.description),
      link: new FormControl(streetart.link),
      lat: new FormControl(streetart.lat),
      lng: new FormControl(streetart.lng),
    });
    return form;
  }
}
