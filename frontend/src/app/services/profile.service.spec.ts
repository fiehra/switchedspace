import { ReportPlayerDto } from './../interfaces/user-interface';
import { of } from 'rxjs';
import { environment } from './../../environments/environment';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ProfileService } from './profile.service';
import { FormBuilder } from '@angular/forms';
import { User } from '../interfaces/user-interface';

describe('ProfileService', () => {
  let service: ProfileService;
  let http: HttpClient;
  let formBuilder: FormBuilder;

  const mockUser: User = {
    id: '1',
    email: 'user@user.com',
    username: 'user',
    password: 'password',
    aboutme: 'nothing',
    tag: 'user',
    faveColor: 'green',
    currentLocation: 'berlin',
    age: '25',
    faveTheme: 'green',
    role: 'user',
    reported: false,
    imagePath: null,
    inventory: 1,
    verified: true,
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [ProfileService, FormBuilder],
        imports: [HttpClientModule],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    service = TestBed.inject(ProfileService);
    http = TestBed.inject(HttpClient);
    formBuilder = TestBed.inject(FormBuilder);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(http).toBeTruthy();
  });

  it('getMyProfile', () => {
    const mockResponse = {
      myProfile: {
        id: '1',
        email: 'user@user.com',
        username: 'user',
        aboutme: 'nothing',
        tag: 'user',
        faveColor: 'green',
        currentLocation: 'berlin',
        age: '25',
        faveTheme: 'green',
        role: 'user',
        reported: false,
      },
    };
    spyOn(http, 'get').and.returnValue(of(mockResponse));
    service.getMyProfile('1').subscribe((response) => {});
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/user/myProfile/1',
    );
  });

  it('getProfileById', () => {
    const mockResponse = {
      profile: {
        id: '1',
        username: 'user',
        aboutme: 'nothing',
        tag: 'user',
        faveColor: 'green',
        currentLocation: 'berlin',
        age: '25',
        faveTheme: 'green',
        reported: false,
      },
    };
    spyOn(http, 'get').and.returnValue(of(mockResponse));
    service.getProfileById('1').subscribe((response) => {});
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/user/1',
    );
  });

  it('updateProfile', () => {
    spyOn(http, 'put').and.stub();
    service.updateProfile(mockUser, mockUser.id);
    expect(http.put).toHaveBeenCalledWith(
      environment.backendUrl + '/api/user/' + mockUser.id,
      mockUser,
    );
  });

  it('deleteProfile', () => {
    spyOn(http, 'delete').and.stub();
    service.deleteProfile(mockUser.id);
    expect(http.delete).toHaveBeenCalledWith(
      environment.backendUrl + '/api/user/' + mockUser.id,
    );
  });

  it('reportUser', () => {
    const reportDtoMock: ReportPlayerDto = {
      id: mockUser.id,
      reported: true,
    };
    spyOn(http, 'put').and.stub();
    service.reportUser(mockUser.id, reportDtoMock);
    expect(http.put).toHaveBeenCalledWith(
      environment.backendUrl + '/api/user/report/' + mockUser.id,
      reportDtoMock,
    );
  });

  it('uploadImage', () => {
    const file: File = {
      name: 'name',
      lastModified: Date.now(),
      size: 3,
      type: 'image',
      arrayBuffer: null,
      slice: null,
      stream: null,
      text: null,
    };
    const imageDataMock = new FormData();
    imageDataMock.append('image', file);
    spyOn(http, 'post').and.stub();
    service.uploadImage('fiehra', imageDataMock);
    expect(http.post).toHaveBeenCalledWith(
      environment.backendUrl + '/api/user/image/fiehra',
      imageDataMock,
    );
  });
});
