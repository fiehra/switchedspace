import { DialogService } from './dialog.service';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { of } from 'rxjs';

class MdDialogMock {
  open() {
    return {
      afterClosed: () => of ({})
    };
  }
}

describe('DialogService', () => {
  let service: DialogService;
  let http: HttpClient;
  let dialog: MatDialog;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      providers: [
        DialogService,
        {
          provide: MatDialog,
          useClass: MdDialogMock
        }
      ],
      imports: [
        HttpClientModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.inject(DialogService);
    http = TestBed.inject(HttpClient);
    dialog = TestBed.inject(MatDialog);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(http).toBeTruthy();
  });

  it('openConfirmationDialog', () => {
    spyOn(dialog, 'open').and.stub();
    service.openConfirmActionDialog('test', 'text', 'yes');
    expect(dialog.open).toHaveBeenCalled();
  });

});