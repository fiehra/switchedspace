import {
  MyProfileDTO,
  ProfileDTO,
  ReportPlayerDto,
} from './../interfaces/user-interface';
import { HttpConfig } from './../helper/authInterceptor';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { User } from '../interfaces/user-interface';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  config: HttpConfig = {
    headers: new HttpHeaders(),
    observe: 'response',
  };

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {}

  getMyProfile(id: string) {
    return this.http
      .get<{ myProfile: any }>(
        environment.backendUrl + '/api/user/myProfile/' + id,
      )
      .pipe(
        map((responseData) => {
          return {
            id: responseData.myProfile.id,
            email: responseData.myProfile.email,
            username: responseData.myProfile.username,
            aboutme: responseData.myProfile.aboutme,
            tag: responseData.myProfile.tag,
            faveColor: responseData.myProfile.faveColor,
            age: responseData.myProfile.age,
            currentLocation: responseData.myProfile.currentLocation,
            faveTheme: responseData.myProfile.faveTheme,
            reported: responseData.myProfile.reported,
            role: responseData.myProfile.role,
            imagePath: responseData.myProfile.imagePath,
            inventory: responseData.myProfile.inventory,
            verified: responseData.myProfile.verified,
          };
        }),
      );
  }

  getProfileById(id: string) {
    return this.http
      .get<{ profile: any }>(environment.backendUrl + '/api/user/' + id)
      .pipe(
        map((responseData) => {
          return {
            id: responseData.profile.id,
            username: responseData.profile.username,
            aboutme: responseData.profile.aboutme,
            tag: responseData.profile.tag,
            faveColor: responseData.profile.faveColor,
            age: responseData.profile.age,
            currentLocation: responseData.profile.currentLocation,
            faveTheme: responseData.profile.faveTheme,
            reported: responseData.profile.reported,
            imagePath: responseData.profile.imagePath,
          };
        }),
      );
  }

  updateProfile(user: User, userId: string) {
    return this.http.put(environment.backendUrl + '/api/user/' + userId, user);
  }

  uploadImage(userId: string, image: FormData) {
    return this.http.post(
      environment.backendUrl + '/api/user/image/' + userId,
      image,
    );
  }

  deleteProfile(userId: string) {
    return this.http.delete(environment.backendUrl + '/api/user/' + userId);
  }

  reportUser(userId: string, playerReport: ReportPlayerDto) {
    return this.http.put(
      environment.backendUrl + '/api/user/report/' + userId,
      playerReport,
    );
  }

  mapMyProfileDtoToForm(profile: MyProfileDTO): FormGroup {
    const form = this.formBuilder.group(profile);
    return form;
  }

  mapProfileDtoToForm(profile: ProfileDTO): FormGroup {
    const form = this.formBuilder.group(profile);
    return form;
  }
}
