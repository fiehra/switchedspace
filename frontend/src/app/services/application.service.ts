import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Application } from '../interfaces/application-interface';

@Injectable({
  providedIn: 'root',
})
export class ApplicationService {
  constructor(private http: HttpClient, private formBuilder: FormBuilder) {}

  createApplication(application: Application) {
    this.http
      .post<{ message: string; applicationId: string }>(
        environment.backendUrl + '/api/application',
        application
      )
      .subscribe((responseData) => {
        const id = responseData.applicationId;
        application.id = id;
      });
  }

  getApplications() {
    return this.http
      .get<{ message: string; applications: any; maxApplications: number }>(
        environment.backendUrl + '/api/application'
      )
      .pipe(
        map((responseData) => {
          return {
            applications: responseData.applications.map((application) => {
              return {
                id: application._id,
                applicationType: application.applicationType,
                name: application.name,
                email: application.email,
                phonenumber: application.phonenumber,
                company: application.company,
                title: application.title,
                description: application.description,
                location: application.location,
                date: application.date,
                budget: application.budget,
              };
            }),
            maxApplications: responseData.maxApplications,
          };
        })
      );
  }

  getApplicationById(id: string) {
    return this.http
      .get<{ application: any }>(
        environment.backendUrl + '/api/application/' + id
      )
      .pipe(
        map((responseData) => {
          return {
            id: responseData.application._id,
            applicationType: responseData.application.applicationType,
            name: responseData.application.name,
            email: responseData.application.email,
            phonenumber: responseData.application.phonenumber,
            company: responseData.application.company,
            title: responseData.application.title,
            description: responseData.application.description,
            location: responseData.application.location,
            date: responseData.application.date,
            budget: responseData.application.budget,
          };
        })
      );
  }

  updateApplication(application: Application, id: string): Observable<any> {
    return this.http.put(
      environment.backendUrl + '/api/application/' + id,
      application
    );
  }

  deleteApplication(id: string): Observable<any> {
    return this.http.delete(environment.backendUrl + '/api/application/' + id);
  }

  mapApplicationToForm(application: Application): FormGroup {
    const form = this.formBuilder.group(application);
    return form;
  }
}
