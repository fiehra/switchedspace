import { SignupUserDTO, LoginUserDTO } from './../interfaces/user-interface';
import { SnackbarHelper } from './../helper/snackbar.helper';
import { ThemeService } from './theme.service';
import { MaterialModule } from './../utils/material/material.module';
import { FormHelper } from './../helper/form.helper';
import { AuthService } from './auth.service';
import {
  TestBed,
  fakeAsync,
  tick,
  waitForAsync,
  flush,
} from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import {
  TranslateModule,
  TranslateLoader,
  TranslateService,
} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { User } from '../interfaces/user-interface';

describe('AuthService', () => {
  let service: AuthService;
  let http: HttpTestingController;
  let translateService: TranslateService;
  let themeService: ThemeService;
  let router: Router;

  const mockUser: User = {
    id: '1',
    email: 'user@user.com',
    username: 'user',
    password: 'password',
    aboutme: 'nothing',
    tag: 'user',
    faveColor: 'green',
    currentLocation: 'berlin',
    age: '25',
    faveTheme: 'green',
    role: 'user',
    reported: false,
    imagePath: null,
    inventory: 1,
    verified: true,
  };

  const mockSignupDto: SignupUserDTO = {
    email: 'user@user.com',
    username: 'user',
    password: 'password',
  };

  const mockLoginDto: LoginUserDTO = {
    email: 'user@user.com',
    password: 'password',
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [AuthService, FormBuilder, FormHelper, SnackbarHelper],
        imports: [
          MaterialModule,
          HttpClientTestingModule,
          BrowserAnimationsModule,
          RouterTestingModule,
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (http: HttpClient) =>
                new TranslateHttpLoader(
                  http,
                  'assets/i18n/',
                  '.json?build_id=build_id_replacement_placeholder',
                ),
              deps: [HttpClient],
            },
          }),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    service = TestBed.inject(AuthService);
    http = TestBed.inject(HttpTestingController);
    translateService = TestBed.inject(TranslateService);
    themeService = TestBed.inject(ThemeService);
    router = TestBed.inject(Router);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(http).toBeTruthy();
  });

  it('getToken', () => {
    service.getToken();
  });

  it('getAuthStatusListener', () => {
    service.getAuthStatusListener();
  });

  it('getLoggedIn', () => {
    service.getLoggedIn();
  });

  it('getUserId', () => {
    service.getUserId();
  });

  it('getUsername', () => {
    service.getUsername();
  });

  it('getUserRole', () => {
    service.getUserRole();
  });

  it('signupUser success', fakeAsync(() => {
    spyOn(router, 'navigate').and.stub();
    const expectedHeaders = { location: 'test/url/id' };
    service.signUp(mockSignupDto).then((res) => {
      expect(res).toEqual('id');
    });
    const req = http.expectOne('http://localhost:3000/api/user/signup');
    expect(req.request.method).toBe('POST');
    req.flush({}, { headers: expectedHeaders });
    tick(1000);
  }));

  it('signupUser error', fakeAsync(() => {
    spyOn(router, 'navigate').and.stub();
    service.signUp(mockSignupDto).catch((error) => {
      expect(error.status).toEqual(400);
      expect(error.statusText).toEqual('error occured');
    });
    const req = http.expectOne('http://localhost:3000/api/user/signup');
    expect(req.request.method).toBe('POST');
    req.flush({}, { status: 400, statusText: 'error occured' });
    tick();
  }));

  it('login success', fakeAsync(() => {
    spyOn(router, 'navigate').and.stub();
    spyOn(translateService, 'instant').and.stub();
    spyOn(themeService, 'changeTheme').and.stub();
    const expectedHeaders = { location: 'test/url/id' };
    service.login(mockLoginDto).then((response) => {
      expect(response).toEqual('id');
    });
    const req = http.expectOne('http://localhost:3000/api/user/login');
    expect(req.request.method).toBe('POST');
    req.flush(
      {
        token: 'token',
        userId: 'id',
        username: 'username',
        role: 'fiehra',
        faveTheme: 'green',
        expiresIn: '200',
      },
      { headers: expectedHeaders },
    );
    flush();
    expect(router.navigate).toHaveBeenCalled();
    expect(themeService.changeTheme).toHaveBeenCalledWith('green');
    expect(translateService.instant).toHaveBeenCalled();
  }));

  it('login error', fakeAsync(() => {
    service.login(mockLoginDto).catch((error) => {
      expect(error.status).toEqual(400);
      expect(error.statusText).toEqual('error occured');
    });
    const req = http.expectOne('http://localhost:3000/api/user/login');
    expect(req.request.method).toBe('POST');
    req.flush({}, { status: 400, statusText: 'error occured' });
    tick();
  }));

  it('autoLoginUser expiresIn > 0', () => {
    const now = new Date();
    const expireDate = new Date(now.getTime() + 1000);
    localStorage.setItem('token', 'token');
    localStorage.setItem('userId', 'userId');
    localStorage.setItem('username', 'username');
    localStorage.setItem('faveTheme', 'faveTheme');
    localStorage.setItem('role', 'user');
    localStorage.setItem('expirationDate', expireDate.toISOString());
    spyOn(themeService, 'changeTheme').and.stub();
    spyOn(service, 'setAuthTimer').and.stub();
    service.autoLoginUser();
    expect(themeService.changeTheme).toHaveBeenCalled();
    expect(service.setAuthTimer).toHaveBeenCalled();
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('username');
    localStorage.removeItem('faveTheme');
    localStorage.removeItem('role');
    localStorage.removeItem('expirationDate');
  });

  it('autoLoginUser expiresIn < 0', () => {
    const now = new Date();
    const expireDate = new Date(now.getTime() - 1000);
    localStorage.setItem('token', 'token');
    localStorage.setItem('userId', 'userId');
    localStorage.setItem('username', 'username');
    localStorage.setItem('faveTheme', 'faveTheme');
    localStorage.setItem('role', 'user');
    localStorage.setItem('expirationDate', expireDate.toISOString());
    spyOn(themeService, 'changeTheme').and.stub();
    service.autoLoginUser();
    expect(themeService.changeTheme).not.toHaveBeenCalled();
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('username');
    localStorage.removeItem('faveTheme');
    localStorage.removeItem('role');
    localStorage.removeItem('expirationDate');
  });

  it('autoLoginUser no token', () => {
    spyOn(themeService, 'changeTheme').and.stub();
    service.autoLoginUser();
    expect(themeService.changeTheme).not.toHaveBeenCalled();
  });

  it('logout', () => {
    spyOn(router, 'navigate').and.stub();
    service.logout();
    expect(router.navigate).toHaveBeenCalled();
  });
});
