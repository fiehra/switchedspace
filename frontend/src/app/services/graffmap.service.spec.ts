import { GraffmapService } from './graffmap.service';
import { of } from 'rxjs';
import { environment } from './../../environments/environment';
import { FormBuilder } from '@angular/forms';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Shop, Streetart, Wall } from '../interfaces/graffmap-interface';

describe('GraffmapService', () => {
  let service: GraffmapService;
  let http: HttpClient;
  let formBuilder: FormBuilder;

  const wallMock: Wall = {
    id: '1',
    name: 'wall',
    description: 'nice wall',
    legalState: 'legal',
    lat: 35.6895,
    lng: 139.69171,
    statusUpdates: [],
  };

  const wallMock2: Wall = {
    id: '2',
    name: 'wall2',
    description: 'bad wall',
    legalState: 'legal',
    lat: 35.6895,
    lng: 139.69171,
    statusUpdates: [],
  };

  const shopMock: Shop = {
    id: '1',
    name: 'shop',
    description: 'nice shop',
    website: 'skillcap.com',
    lat: 35.6895,
    lng: 139.69171,
  };

  const shopMock2: Shop = {
    id: '2',
    name: 'shop2',
    description: 'bad shop',
    website: 'skillcap.com',
    lat: 35.6895,
    lng: 139.69171,
  };

  const streetartMock: Streetart = {
    id: '1',
    name: 'streetart',
    description: 'nice art',
    link: 'skillcap.com',
    lat: 35.6895,
    lng: 139.69171,
  };

  const streetartMock2: Streetart = {
    id: '2',
    name: 'streetart2',
    description: 'bad art',
    link: 'skillcap.com',
    lat: 35.6895,
    lng: 139.69171,
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [GraffmapService, FormBuilder],
        imports: [HttpClientTestingModule],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    service = TestBed.inject(GraffmapService);
    http = TestBed.inject(HttpClient);
    formBuilder = TestBed.inject(FormBuilder);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(http).toBeTruthy();
  });

  it('addWall', () => {
    spyOn(http, 'post').and.returnValue(of({ body: wallMock }));
    service.addWall(wallMock);
    expect(http.post).toHaveBeenCalledWith(
      environment.backendUrl + '/api/wall/add',
      wallMock
    );
  });

  it('addShop', () => {
    spyOn(http, 'post').and.returnValue(of({ body: shopMock }));
    service.addShop(shopMock);
    expect(http.post).toHaveBeenCalledWith(
      environment.backendUrl + '/api/shop/add',
      shopMock
    );
  });

  it('addStreetart', () => {
    spyOn(http, 'post').and.returnValue(of({ body: streetartMock }));
    service.addStreetart(streetartMock);
    expect(http.post).toHaveBeenCalledWith(
      environment.backendUrl + '/api/streetart/add',
      streetartMock
    );
  });

  it('updateWallById', () => {
    spyOn(http, 'put').and.callThrough();
    service.updateWallById('1', wallMock);
    expect(http.put).toHaveBeenCalledWith(
      environment.backendUrl + '/api/wall/update/1',
      wallMock
    );
  });

  it('updateShopById', () => {
    spyOn(http, 'put').and.callThrough();
    service.updateShopById('1', shopMock);
    expect(http.put).toHaveBeenCalledWith(
      environment.backendUrl + '/api/shop/update/1',
      shopMock
    );
  });

  it('updateStreetartById', () => {
    spyOn(http, 'put').and.callThrough();
    service.updateStreetartById('1', streetartMock);
    expect(http.put).toHaveBeenCalledWith(
      environment.backendUrl + '/api/streetart/update/1',
      streetartMock
    );
  });

  it('getWallById', () => {
    const responseMock = {
      wall: wallMock,
    };
    spyOn(http, 'get').and.returnValue(of(responseMock));
    service.getWallById('wall').subscribe((response) => {});
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/wall/wall'
    );
  });

  it('getShopById', () => {
    const responseMock = {
      shop: shopMock,
    };
    spyOn(http, 'get').and.returnValue(of(responseMock));
    service.getShopById('shop').subscribe((response) => {});
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/shop/shop'
    );
  });

  it('getStreetartById', () => {
    const responseMock = {
      streetart: streetartMock,
    };
    spyOn(http, 'get').and.returnValue(of(responseMock));
    service.getStreetartById('streetart').subscribe((response) => {});
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/streetart/streetart'
    );
  });

  it('getWalls', () => {
    const wallsMock = {
      message: 'message',
      walls: [wallMock, wallMock2],
      maxWalls: 2,
    };
    spyOn(http, 'get').and.returnValue(of(wallsMock));
    service.getWalls().subscribe((response) => {
      expect(response.walls.length).toBe(2);
    });
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/walls'
    );
  });

  it('getShops', () => {
    const shopsMock = {
      message: 'message',
      shops: [shopMock, shopMock2],
      maxShops: 2,
    };
    spyOn(http, 'get').and.returnValue(of(shopsMock));
    service.getShops().subscribe((response) => {
      expect(response.shops.length).toBe(2);
    });
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/shops'
    );
  });

  it('getStreetarts', () => {
    const streetartsMock = {
      message: 'message',
      streetarts: [streetartMock, streetartMock2],
      maxStreetarts: 2,
    };
    spyOn(http, 'get').and.returnValue(of(streetartsMock));
    service.getStreetarts().subscribe((response) => {
      expect(response.streetarts.length).toBe(2);
    });
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/streetarts'
    );
  });

  it('mapWallToForm', () => {
    const formGroup = service.mapWallToForm(wallMock2);
    expect(formGroup.get('id').value).toBe('2');
    expect(formGroup.get('name').value).toBe('wall2');
    expect(formGroup.get('description').value).toBe('bad wall');
    expect(formGroup.get('legalState').value).toBe('legal');
    expect(formGroup.get('lat').value).toBe(35.6895);
    expect(formGroup.get('lng').value).toBe(139.69171);
  });

  it('mapShopToForm', () => {
    const formGroup = service.mapShopToForm(shopMock2);
    expect(formGroup.get('id').value).toBe('2');
    expect(formGroup.get('name').value).toBe('shop2');
    expect(formGroup.get('description').value).toBe('bad shop');
    expect(formGroup.get('website').value).toBe('skillcap.com');
    expect(formGroup.get('lat').value).toBe(35.6895);
    expect(formGroup.get('lng').value).toBe(139.69171);
  });

  it('mapStreetartToForm', () => {
    const formGroup = service.mapStreetartToForm(streetartMock2);
    expect(formGroup.get('id').value).toBe('2');
    expect(formGroup.get('name').value).toBe('streetart2');
    expect(formGroup.get('description').value).toBe('bad art');
    expect(formGroup.get('link').value).toBe('skillcap.com');
    expect(formGroup.get('lat').value).toBe(35.6895);
    expect(formGroup.get('lng').value).toBe(139.69171);
  });
});
