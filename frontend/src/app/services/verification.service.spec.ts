import { FormBuilder } from '@angular/forms';
import { environment } from './../../environments/environment';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { VerificationService } from './verification.service';

describe('VerificationService', () => {
  let service: VerificationService;
  let http: HttpClient;
  let formBuilder: FormBuilder;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [VerificationService, FormBuilder],
        imports: [HttpClientModule],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    service = TestBed.inject(VerificationService);
    http = TestBed.inject(HttpClient);
    formBuilder = TestBed.inject(FormBuilder);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(http).toBeTruthy();
  });

  it('resendVerificationLink', () => {
    localStorage.setItem('email', 'email@email.com');
    spyOn(http, 'post').and.callThrough();
    service.resendVerificationLink();
    expect(http.post).toHaveBeenCalledWith(
      environment.backendUrl + '/api/verify/resend',
      { email: 'email@email.com' },
    );
  });
});
