// Import the core angular services.
import { fromEvent } from 'rxjs';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

type Target = Document | Element;

@Injectable({
  providedIn: 'root',
})
export class ElementScrollPercentage {
  constructor() {}

  // return the current scroll percentage (0,100) of the given DOM node.
  public getScroll(node: Target = document): number {
    var currentScroll = this.getCurrentScroll(node);
    var maxScroll = this.getMaxScroll(node);
    // Ensure that the percentage falls strictly within (0,1).
    var percent = currentScroll / Math.max(maxScroll, 1);
    percent = Math.max(percent, 0);
    percent = Math.min(percent, 1);
    // Return the percentage in a more human-consumable format.
    return percent * 100;
  }

  // return the current scroll percentage (0,100) of the given DOM node as a STREAM.
  public getScrollAsStream(node: Target = document): Observable<number> {
    if (node instanceof Document) {
      // When we watch the DOCUMENT, we need to pull the scroll event from the
      // WINDOW, but then check the scroll offsets of the DOCUMENT.
      var stream = fromEvent(window, 'scroll').pipe(
        map((event: UIEvent): number => {
          return this.getScroll(node);
        }),
      );
    } else {
      // When we watch an ELEMENT node, we can pull the scroll event and the scroll
      // offsets from the same ELEMENT node (unlike the Document version).
      var stream = fromEvent(node, 'scroll').pipe(
        map((event: UIEvent): number => {
          return this.getScroll(node);
        }),
      );
    }
    return stream;
  }

  // return the current scroll offset (in pixels) of the given DOM node.
  getCurrentScroll(node: Target): number {
    if (node instanceof Document) {
      return window.pageYOffset;
    } else {
      return node.scrollTop;
    }
  }

  // return the maximum scroll offset (in pixels) of the given DOM node.
  getMaxScroll(node: Target): number {
    if (node instanceof Document) {
      var scrollHeight = Math.max(
        node.body.scrollHeight,
        node.body.offsetHeight,
        node.body.clientHeight,
        node.documentElement.scrollHeight,
        node.documentElement.offsetHeight,
        node.documentElement.clientHeight,
      );
      var clientHeight = node.documentElement.clientHeight;
      return scrollHeight - clientHeight;
    } else {
      return node.scrollHeight - node.clientHeight;
    }
  }
}
