import {
  CreateForumPostDTO,
  ForumPost,
  ForumPostVoteDTO,
} from './../interfaces/forum-interface';
import { of } from 'rxjs';
import { environment } from './../../environments/environment';
import { FormBuilder } from '@angular/forms';
import { ForumService } from './forum.service';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ForumService', () => {
  let service: ForumService;
  let http: HttpClient;

  const forumPost1: ForumPost = {
    id: '1',
    title: 'title1',
    writerId: 'fiehra',
    writerUsername: 'fiehra',
    content: 'content1',
    votes: 0,
    upVoters: [],
    downVoters: [],
    comments: [],
  };

  const forumPost2: ForumPost = {
    id: '2',
    title: 'title2',
    writerId: 'nurias',
    writerUsername: 'nurias',
    content: 'content2',
    votes: 0,
    upVoters: [],
    downVoters: [],
    comments: [],
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [ForumService, FormBuilder],
        imports: [HttpClientTestingModule],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    service = TestBed.inject(ForumService);
    http = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(http).toBeTruthy();
  });

  it('getForumListPosts', () => {
    const forumListMock = {
      message: 'message',
      forumPostsList: [
        {
          id: '1',
          title: 'title1',
          writerUsername: 'fiehra',
          votes: 2,
        },
        {
          id: '2',
          title: 'title2',
          writerUsername: 'nurias',
          votes: 0,
        },
      ],
      maxPosts: 2,
    };
    spyOn(http, 'get').and.returnValue(of(forumListMock));
    const postsPerPage = 10;
    const currentPage = 1;
    service
      .getForumListPosts(postsPerPage, currentPage)
      .subscribe((response) => {
        expect(response.forumPostsList.length).toBe(2);
      });
    const queryParams = `?pagesize=${postsPerPage}&page=${currentPage}`;
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/forumPostsList' + queryParams
    );
  });

  it('getForumPostById', () => {
    const responseMock = {
      forumPost: forumPost1,
    };
    spyOn(http, 'get').and.returnValue(of(responseMock));
    const id = 'id';
    service.getForumPostById(id).subscribe((response) => {});
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/forumPosts/id'
    );
  });

  it('createForumPost', () => {
    const forumPost: CreateForumPostDTO = {
      id: '1',
      title: 'title1',
      writerId: 'fiehra',
      writerUsername: 'fiehra',
      content: 'content1',
    };
    const response = forumPost1;
    spyOn(http, 'post').and.returnValue(of({ body: response }));
    service.createForumPost(forumPost);
    expect(http.post).toHaveBeenCalledWith(
      environment.backendUrl + '/api/forumPosts',
      forumPost
    );
  });

  it('deletePost', () => {
    spyOn(http, 'delete').and.stub();
    service.deletePost('id');
    expect(http.delete).toHaveBeenCalledWith(
      environment.backendUrl + '/api/forumPosts/id'
    );
  });

  it('updatePost', () => {
    spyOn(http, 'put').and.stub();
    const forumPost: ForumPost = {
      id: '1',
      title: 'title1',
      writerId: 'fiehra',
      writerUsername: 'fiehra',
      content: 'updated',
      votes: 0,
      upVoters: [],
      downVoters: [],
      comments: [],
    };
    service.updatePost(forumPost, forumPost.id);
    expect(http.put).toHaveBeenCalledWith(
      environment.backendUrl + '/api/forumPosts/1',
      forumPost
    );
  });

  it('updateVotes', () => {
    const voteDto: ForumPostVoteDTO = {
      id: '1',
      votes: 1,
      upVoters: ['1'],
      downVoters: [],
    };
    spyOn(http, 'put').and.stub();
    service.updateVotes(voteDto);
    expect(http.put).toHaveBeenCalledWith(
      environment.backendUrl + '/api/forumPosts/updatevotes/1',
      voteDto
    );
  });

  it('updateComments', () => {
    const commentMock = {
      id: 'id',
      writerId: 'id',
      writerUsername: 'id',
      content: 'id',
      markedAsCorrect: true,
    };
    spyOn(http, 'put').and.stub();
    service.updateComments('1', ([commentMock]));
    expect(http.put).toHaveBeenCalledWith(
      environment.backendUrl + '/api/forumPosts/updateComments/1',
      [commentMock]
    );
  });

  it('mapForumPostToForm', () => {
    const formGroup = service.mapForumPostToForm(forumPost1);
    expect(formGroup.get('id')).not.toBeNull();
    expect(formGroup.get('title')).not.toBeNull();
    expect(formGroup.get('writerId')).not.toBeNull();
    expect(formGroup.get('writerUsername')).not.toBeNull();
    expect(formGroup.get('content')).not.toBeNull();
  });
});
