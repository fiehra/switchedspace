import { Challenge } from './../interfaces/challenge-interface';
import { ChallengeService } from './challenge.service';
import { environment } from './../../environments/environment';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

describe('ChallengeService', () => {
  let service: ChallengeService;
  let http: HttpClient;
  let formBuilder: FormBuilder;

  const challengeMock: Challenge = {
    id: '1',
    challengerId: 'fiehra',
    date: new Date(),
    start: '9:10',
    finish: '9:20',
    time: 10,
    active: false,
    completed: true,
    record: 10,
    streak: 9,
  };

  const challengeMock2: Challenge = {
    id: '2',
    challengerId: 'nurias',
    date: new Date(),
    start: '9:10',
    finish: '9:20',
    time: 10,
    active: false,
    completed: true,
    record: 8,
    streak: 10,
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [ChallengeService, FormBuilder],
        imports: [HttpClientTestingModule],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    service = TestBed.inject(ChallengeService);
    http = TestBed.inject(HttpClient);
    formBuilder = TestBed.inject(FormBuilder);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(http).toBeTruthy();
  });

  it('create application', () => {
    spyOn(http, 'post').and.returnValue(of({ body: challengeMock }));
    service.createChallenge(challengeMock);
    expect(http.post).toHaveBeenCalledWith(
      environment.backendUrl + '/api/challenge/create',
      challengeMock
    );
  });

  it('getChallenges', () => {
    const challengesMock = {
      message: 'message',
      challengesList: [challengeMock, challengeMock2],
      maxChallenges: 2,
    };
    spyOn(http, 'get').and.returnValue(of(challengesMock));
    service.getChallenges().subscribe((response) => {
      expect(response.challengesList.length).toBe(2);
    });
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/challenges/getAll'
    );
  });

  it('getChallenges', () => {
    const challengesMock = {
      message: 'message',
      challengesList: [challengeMock, challengeMock2],
      maxChallenges: 2,
    };
    spyOn(http, 'get').and.returnValue(of(challengesMock));
    service.getLatest10Challenges().subscribe((response) => {
      expect(response.challengesList.length).toBe(2);
    });
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/challenges/getLatest10'
    );
  });

  it('getChallengeById', () => {
    const responseMock = {
      message: 'message',
      challenge: challengeMock,
    };
    spyOn(http, 'get').and.returnValue(of(responseMock));
    service.getChallengeById('2').subscribe((response) => {});
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/challenge/get/2'
    );
  });

  it('updateChallenge', () => {
    spyOn(http, 'put').and.callThrough();
    service.updateChallenge(challengeMock, '1');
    expect(http.put).toHaveBeenCalledWith(
      environment.backendUrl + '/api/challenge/update/1',
      challengeMock
    );
  });

  it('deleteChallenge', () => {
    spyOn(http, 'delete').and.callThrough();
    service.deleteChallenge('1');
    expect(http.delete).toHaveBeenCalledWith(
      environment.backendUrl + '/api/challenge/delete/1'
    );
  });

  it('getAverageTime', () => {
    spyOn(http, 'get').and.callThrough();
    service.getAverageTime();
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/challenge/averageTime'
    );
  });
  
  it('getDateDifference', () => {
    spyOn(http, 'get').and.callThrough();
    service.getDateDifference();
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/challenge/getDifference'
    );
  });
  
  it('getChallengeRecord', () => {
    spyOn(http, 'get').and.callThrough();
    service.getChallengeRecord();
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/challenge/getRecord'
    );
  });
  
  it('getCurrentStreak', () => {
    spyOn(http, 'get').and.callThrough();
    service.getCurrentStreak();
    expect(http.get).toHaveBeenCalledWith(
      environment.backendUrl + '/api/challenge/getStreak'
    );
  });
  
  it('mapChallengeToForm', () => {
    const formGroup = service.mapChallengeToForm(challengeMock);
    expect(formGroup.get('id')).not.toBeNull();
    expect(formGroup.get('challengerId')).not.toBeNull();
    expect(formGroup.get('date')).not.toBeNull();
    expect(formGroup.get('start')).not.toBeNull();
    expect(formGroup.get('finish')).not.toBeNull();
    expect(formGroup.get('time')).not.toBeNull();
    expect(formGroup.get('active')).not.toBeNull();
    expect(formGroup.get('completed')).not.toBeNull();
  });
});
