import { ConfirmationActionDialogComponent } from './../components/dialogs/confirmation-action-dialog/confirmation-action-dialog.component';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  constructor(private dialog: MatDialog) {}
  // confirmation dialog with a title, text below title and 1 action which can be a function to call upon OK click
  openConfirmActionDialog(
    title: string,
    text: string,
    confirmAction: any,
  ): void {
    this.dialog.open(ConfirmationActionDialogComponent, {
      data: {
        title,
        text,
        confirmAction,
      },
    });
  }
}
