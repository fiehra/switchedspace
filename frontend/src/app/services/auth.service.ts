import { AuthData, LoginUserDTO, Role, SignupUserDTO, User } from './../interfaces/user-interface';
import { SnackbarHelper } from './../helper/snackbar.helper';
import { ThemeService } from './theme.service';
import { HttpConfig } from './../helper/authInterceptor';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private token: string;
  private loggedIn = false;
  private tokenTimer: any;
  private userId: string;
  role: string;
  username: string;

  private authStatusListener = new Subject<boolean>();

  config: HttpConfig = {
    headers: new HttpHeaders(),
    observe: 'response',
  };

  constructor(
    private http: HttpClient,
    private router: Router,
    private snackbarHelper: SnackbarHelper,
    private translateService: TranslateService,
    private themeService: ThemeService,
  ) {}

  getToken() {
    return this.token;
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  getLoggedIn() {
    return this.loggedIn;
  }

  getUserId() {
    return this.userId;
  }

  getUsername() {
    return this.username;
  }

  getUserRole() {
    return this.role;
  }

  // signup user for application via http post call
  signUp(signupDto: SignupUserDTO): Promise<string> {
    const promise = new Promise<string>((resolve, reject) => {
      this.http
        .post(environment.backendUrl + '/api/user/signup', signupDto, this.config)
        .toPromise<HttpConfig>()
        .then((createdUser) => {
          localStorage.setItem('email', signupDto.email);
          this.router.navigate(['/verification']);
          const messageSignup = this.translateService.instant('snackSignup');
          this.snackbarHelper.openSnackBar(messageSignup);
        })
        .catch((error) => {
          this.authStatusListener.next(false);
        });
    });
    return promise;
  }

  // log in user to application via http post call
  login(loginDto: LoginUserDTO): Promise<string> {
    const promise = new Promise<string>((resolve, reject) => {
      const authData = loginDto;
      this.http
        .post<{
          token: string;
          expiresIn: number;
          userId: string;
          username: string;
          faveTheme: string;
          role: Role;
        }>(environment.backendUrl + '/api/user/login', authData)
        .toPromise()
        .then((response) => {
          const token = response.token;
          this.token = token;
          if (token) {
            const expiresInDuration = response.expiresIn;
            this.setAuthTimer(expiresInDuration * 1000);
            this.loggedIn = true;
            this.userId = response.userId;
            this.username = response.username;
            this.role = response.role;
            this.themeService.changeTheme(response.faveTheme);
            this.authStatusListener.next(true);
            this.router.navigate(['/']);
            const messageLogin = this.translateService.instant('snackLogin');
            this.snackbarHelper.openSnackBar(messageLogin);
            const now = new Date();
            const expireDate = new Date(now.getTime() + expiresInDuration * 1000);
            const authObject = {
              token: response.token,
              expirationDate: expireDate,
              userId: response.userId,
              username: response.username,
              faveTheme: response.faveTheme,
              role: response.role,
            };
            this.saveAuthData(authObject);
          }
        })
        .catch((error) => {
          this.authStatusListener.next(false);
        });
    });
    return promise;
  }

  // auto login user for page reloads
  autoLoginUser() {
    const authData = this.getAuthData();
    if (!authData) {
      return;
    }
    const now = new Date();
    const expiresIn = authData.expirationDate.getTime() - now.getTime();
    if (expiresIn > 0) {
      this.token = authData.token;
      this.loggedIn = true;
      this.userId = authData.userId;
      this.username = authData.username;
      this.role = authData.role;
      this.themeService.changeTheme(localStorage.getItem('faveTheme'));
      this.setAuthTimer(expiresIn);
      this.authStatusListener.next(true);
    }
  }

  // log out user
  logout() {
    this.token = null;
    this.loggedIn = false;
    this.authStatusListener.next(false);
    this.userId = null;
    clearTimeout(this.tokenTimer);
    this.clearAuthData();
    this.router.navigate(['/logout']);
  }

  // token handling in local storage
  private saveAuthData(authData: AuthData) {
    localStorage.setItem('token', authData.token);
    localStorage.setItem('userId', authData.userId);
    localStorage.setItem('username', authData.username);
    localStorage.setItem('faveTheme', authData.faveTheme);
    localStorage.setItem('role', authData.role);
    localStorage.setItem('expirationDate', authData.expirationDate.toISOString());
  }

  private clearAuthData() {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('username');
    localStorage.removeItem('faveTheme');
    localStorage.removeItem('role');
    localStorage.removeItem('expirationDate');
  }

  private getAuthData() {
    const token = localStorage.getItem('token');
    const userId = localStorage.getItem('userId');
    const username = localStorage.getItem('username');
    const faveTheme = localStorage.getItem('faveTheme');
    const role = localStorage.getItem('role');
    const expirationDate = localStorage.getItem('expirationDate');
    if (!token || !expirationDate) {
      return;
    }
    const authData = {
      token,
      expirationDate: new Date(expirationDate),
      userId,
      username,
      faveTheme,
      role,
    };
    return authData;
  }

  setAuthTimer(duration: number) {
    console.log('setting timer: ' + duration);
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration);
  }
}
