import { Challenge } from './../interfaces/challenge-interface';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ChallengeService {
  constructor(private http: HttpClient, private formBuilder: FormBuilder) {}

  createChallenge(challenge: Challenge) {
    this.http
      .post<{ message: string; challengeId: string }>(
        environment.backendUrl + '/api/challenge/create',
        challenge
      )
      .subscribe((responseData) => {
        const id = responseData.challengeId;
        challenge.id = id;
      });
  }

  getLatest10Challenges() {
    return this.http
      .get<{ message: string; challengesList: any }>(
        environment.backendUrl + '/api/challenges/getLatest10'
      )
      .pipe(
        map((responseData) => {
          return {
            challengesList: responseData.challengesList.map((challenge) => {
              return {
                id: challenge._id,
                challengerId: challenge.challengerId,
                date: challenge.date,
                start: challenge.start,
                finish: challenge.finish,
                time: challenge.time,
                active: challenge.active,
                completed: challenge.completed,
                record: challenge.record,
              };
            }),
          };
        })
      );
  }

  getChallenges() {
    return this.http
      .get<{ message: string; challengesList: any; maxChallenges: number }>(
        environment.backendUrl + '/api/challenges/getAll'
      )
      .pipe(
        map((responseData) => {
          return {
            challengesList: responseData.challengesList.map((challenge) => {
              return {
                id: challenge._id,
                challengerId: challenge.challengerId,
                date: challenge.date,
                start: challenge.start,
                finish: challenge.finish,
                time: challenge.time,
                active: challenge.active,
                completed: challenge.completed,
                record: challenge.record,
              };
            }),
            maxChallenges: responseData.maxChallenges,
          };
        })
      );
  }

  getChallengeById(id: string) {
    return this.http
      .get<{ challenge: any }>(
        environment.backendUrl + '/api/challenge/get/' + id
      )
      .pipe(
        map((responseData) => {
          return {
            id: responseData.challenge._id,
            challengerId: responseData.challenge.challengerId,
            date: responseData.challenge.date,
            start: responseData.challenge.start,
            finish: responseData.challenge.finish,
            time: responseData.challenge.time,
            active: responseData.challenge.active,
            completed: responseData.challenge.completed,
            record: responseData.challenge.record,
          };
        })
      );
  }

  updateChallenge(challenge: Challenge, id: string): Observable<any> {
    return this.http.put(
      environment.backendUrl + '/api/challenge/update/' + id,
      challenge
    );
  }

  deleteChallenge(id: string): Observable<any> {
    return this.http.delete(
      environment.backendUrl + '/api/challenge/delete/' + id
    );
  }

  getAverageTime() {
    return this.http.get<{ message: string; averageTime: number }>(
      environment.backendUrl + '/api/challenge/averageTime'
    );
  }

  getDateDifference() {
    return this.http.get<{ message: string; difference: number }>(
      environment.backendUrl + '/api/challenge/getDifference'
    );
  }

  getChallengeRecord() {
    return this.http.get<{ message: string; record: number }>(
      environment.backendUrl + '/api/challenge/getRecord'
    );
  }

  getCurrentStreak() {
    return this.http.get<{ message: string; streak: number }>(
      environment.backendUrl + '/api/challenge/getStreak'
    );
  }

  mapChallengeToForm(challenge: Challenge): FormGroup {
    const form = this.formBuilder.group(challenge);
    return form;
  }
}
