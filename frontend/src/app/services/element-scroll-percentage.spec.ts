import { ElementScrollPercentage } from './element-scroll-percentage';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';

describe('ElementScrollPercentage', () => {
  let service: ElementScrollPercentage;
  let http: HttpClient;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      providers: [
        ElementScrollPercentage
      ],
      imports: [
        HttpClientModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.inject(ElementScrollPercentage);
    http = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(http).toBeTruthy();
  });

  it('getScroll', () => {
    spyOn(service, 'getCurrentScroll').and.stub();
    spyOn(service, 'getMaxScroll').and.stub();
    service.getScroll(document);
    expect(service.getCurrentScroll).toHaveBeenCalled();
    expect(service.getMaxScroll).toHaveBeenCalled();
  });

  //   it('getScrollAsStream node document', () => {
  //     window = Object.assign(window, { pageYOffset: 1000 });
  //     // spyOn(service, 'getScroll').and.stub();
  //     service.getScrollAsStream(document);
  //     // expect(service.getScroll).toHaveBeenCalled();
  // });

  // it('getScrollAsStream node element', () => {
  //     window = Object.assign(window, { pageYOffset: 1000 });
  //     service.getScrollAsStream(document.body);
  //     expect(service.getScroll()).toBe(1100)
  // });

  it('getCurrentScroll document', () => {
    window = Object.assign(window, { pageYOffset: 0 });
    service.getCurrentScroll(document);
    expect(window.pageYOffset).toBe(0);
  });

  it('getCurrentScroll document.body', () => {
    window = Object.assign(window, { pageYOffset: 0 });
    service.getCurrentScroll(document.body);
    expect(document.body.scrollTop).toBe(0);
  });

  it('getMaxScroll document', () => {
    service.getMaxScroll(document);
  });

  it('getMaxScroll document.body', () => {
    service.getMaxScroll(document.body);
  });

});