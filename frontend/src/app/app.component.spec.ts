import { SnackbarHelper } from './helper/snackbar.helper';
import { FormHelper } from './helper/form.helper';
import { FormBuilder } from '@angular/forms';
import { MaterialModule } from './utils/material/material.module';
import { AuthService } from './services/auth.service';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TestBed, ComponentFixture, waitForAsync } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture < AppComponent > ;
  let debugElement: DebugElement;
  let authService: AuthService;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        AuthService,
        HttpClient,
        FormBuilder,
        FormHelper,
        SnackbarHelper
      ],
      imports: [
        RouterTestingModule,
        MaterialModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: ((http: HttpClient) => new TranslateHttpLoader(http, 'assets/i18n/', '.json?build_id=build_id_replacement_placeholder')),
            deps: [HttpClient]
          }
        })
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    authService = TestBed.inject(AuthService);
  });

  it('should create the app', () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'skillcap'`, () => {
    const app = fixture.debugElement.componentInstance;
    fixture.detectChanges();
    expect(app.title).toEqual('skillcap');
  });

  it('ngOnInit', () => {
    spyOn(authService, 'autoLoginUser').and.stub();
    component.ngOnInit();
    fixture.detectChanges();
    expect(authService.autoLoginUser).toHaveBeenCalled();
  });

});
