export const environment = {
  VERSION: require('../../package.json').version,
  production: true,
  backendUrl: 'https://api.skillcap.org',
};
